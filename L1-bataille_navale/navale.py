from upemtk import *
from time import sleep
from random import randint


# dimensions du jeu, largeur hauteur
taille_case = 20
dimentio = (43,26)


def case_vers_pixel(case):
    """
	Fonction recevant les coordonnées d'une case du plateau sous la 
	forme d'un couple d'entiers (ligne, colonne) et renvoyant les 
	coordonnées du pixel se trouvant au centre de cette case. Ce calcul 
	prend en compte la taille de chaque case, donnée par la variable 
	globale taille_case.
    select-selectio-jeu
    """
    i, j = case
    return (i + .5) * taille_case, (j + .5) * taille_case


def affiche_curseur(a,b,j2,menu):
    # affichage du curseur
    """
    j2 = True
    menu = jeu
    >>> on aura un curseur complement bleu
    """
    if j2 == False:
        i = 0
    else:
        i = 1
    c = ["red", "blue"]
    cercl = {"select": 'black', "selectio": c[i], "jeu": c[i]}
    lign = {"select": c[i], "selectio": 'black', "jeu": c[i]}
    x, y = case_vers_pixel((a, b))
    cercle(x, y, 12, couleur=cercl[menu], epaisseur=4, tag="c")
    cercle(x, y, 1, couleur=cercl[menu], epaisseur=2, tag="c")
    ligne(x-20, y, x-5, y, couleur=lign[menu], epaisseur=2, tag="c")
    ligne(x+20, y, x+5, y, couleur=lign[menu], epaisseur=2, tag="c")
    ligne(x, y-20, x, y-5, couleur=lign[menu], epaisseur=2, tag="c")
    ligne(x, y+20, x, y+5, couleur=lign[menu], epaisseur=2, tag="c")
    
    
def mouvement(x,y,direction):
    # gere les directions du curseur
    direc = {"Right": 1, "Left": -1, "Up": -1, "Down":1}
    if direction == "Right" or direction == "Left":
        x = x + direc[direction]
        return x, y
    else:
        y = y + direc[direction]
        return x, y
        
        
def torique(x, y, j2, jeu):
    # modulo permet à l'arène d'être torique
    # lors du placement des bateaux, jeu = False
    # chacun place les bateaux sur son arene dans un premier temps
    # puis pendant le jeu, jeu = True, et notre curseur se trouve sur l'arène adverse
    """
    j2 = False
    (x,y) = (12,17)
    >>> (12,17)
    j2 = False
    (x,y) = (-1,17)
    >>> (19,17)
    j2 = True
    (x,y) = (26,43)
    >>> (26,23)
    """
    if jeu == False:
        if j2 == False:
            return x % 20, y % 20
        else:
            return ((x-23) % 20) + 23, y % 20
    else:
        if j2 == True:
            return x % 20, y % 20
        else:
            return ((x-23) % 20) + 23, y % 20
        
        
        
        
def plouf(x, y, list_dejatouch, bateaux, envue, d, j2, adversaire, x_j1, y_j1, x_j2, y_j2, f, kill, mode_bombe, munition):
    global kill_qui 
    # elle permet de gérer les tirs effectués, s' il y a rien, touché, coulé, ou en vue
    # dans un premier temps on regarde si c'est le tour de l'adversaire controlé par ordi qui tire
    # si oui, on gère ici le tir aléatoire de l'ordi
    if adversaire and j2 and not kill:
        x = randint(0,19)
        y = randint(0,19)
        while (x, y) in list_dejatouch:
            x = randint(0,19)
            y = randint(0,19)
    elif adversaire and j2 and kill:
        k = randint(0,len(list_kill[kill_qui[0]])-1)
        (x, y) = list_kill[kill_qui[0]][k]
        while (x, y) in list_dejatouch or not (0 <= x <= 19) or not (0 <= y <= 19):
            k = randint(0,len(list_kill[kill_qui[0]])-1)
            (x, y) = list_kill[kill_qui[0]][k]
    # si le tir effectué n'as pas déjà été fait ( donc le couple (x, y) pas dans la liste interdit )
    if (x, y) not in list_dejatouch:
        # si a_print est false après tout ça, le tir n'aura rien fait
        a_print = False
        # on verifie sur tout les bateaux si le tir se trouve dans la liste bateaux
        for i, elem in enumerate(bateaux):
            if (x, y) in bateaux[i]:
                # si les coordonnées correspondent bien à un bateau
                # si le bateau est de longueur 1
                if len(bateaux[i]) == 1:
                    if [(x, y)] == bateaux[i]:
                        bateaux[i] = [(-10,-10)]
                        # la liste contenant les 8 coordonnées autour du bateau coulé se modifient en une liste de coordonnées (-10,-10)
                        # on ne peut pas l'enlever simplement car sinon on modifie l'ordre, et cela permet de ne pas avoir du 'en vue' d'un bateau coulé
                        envue[i] = [(-10,-10)]
                        rectangle(d[i][0], d[i][1], d[i][2], d[i][3], couleur='black', remplissage='black', epaisseur=1, tag="coule")
                        # on enlève le bateau concerné de la liste des bateaux
                        if j2 == False:
                            compt[0] -= 1
                        else:
                            compt_j1[0] -=1
                        if adversaire and j2:
                            kill_qui = kill_qui[1:]
                            if kill_qui == []:
                                kill = False
                # pour les bateaux de longueurs superieurs à 1, à la différence des bateaux de 1 case
                # le bateau possède plusieurs coordonnées pour chaques cases
                else:
                    if i not in kill_qui_deja and adversaire and j2:
                        kill = True
                        kill_qui.append(i)
                        kill_qui_deja.append(i)
                    # on regarde quelle case a été touché
                    for ii in range(len(bateaux[i])):
                        if (x, y) == bateaux[i][ii]:
                            envue[i][ii] = (-20, -20)
                            bateaux[i][ii] = (-20, -20)
                            image(x*20+.5, y*20+.5, str(f)+".pgm", ancrage='nw', tag="apercu")
                # on verifie si le bateau est coulé complement
                if bateaux[i].count((-20, -20)) == len(bateaux[i]):
                    if adversaire and j2:
                        kill_qui = kill_qui[1:]
                        if kill_qui == []:
                            kill = False
                    bateaux[i] = [(-10,-10)]
                    rectangle(d[i][0], d[i][1], d[i][2], d[i][3], couleur='black', remplissage='black', epaisseur=1, tag="coule")
                    if j2 == False:
                        compt[ii] -= 1
                    else:
                        compt_j1[ii] -=1
                texte(x*20+1, y*20-6, chaine="X", couleur='red', taille=22, tag="croix")
                a_print = True
                break
            else:
                # si les coordonnées (x,y) ne sont pas dans la liste bateaux, donc ni touché ni coulé
                # on verifie si au moins elles sont dans la liste envue
                for j, elem in enumerate(envue[i]):
                    if (x, y) in envue[i][j]:
                        texte(x*20, y*20-6, chaine="X", couleur='black', taille=21, tag="croix")
                        texte(x*20+2, y*20-6, chaine="X", couleur='black', taille=21, tag="croix")
                        texte(x*20+5, y*20+1, chaine="X", couleur='orange', taille=12, tag="croix")
                        a_print = True
                        if i not in kill_qui_deja and adversaire and j2:
                            kill_qui.append(i)
                            kill_qui_deja.append(i)
                            kill = True
                        break
        # au moins si on trouvé une bombe
        if (x,y) in bombes:
            munition += 1
            cercle(x*20+10, y*20+10, 10, couleur="black", remplissage="black")
        # rien n'a été touché
        elif not a_print:
            texte(x*20, y*20-6, chaine="X", couleur='black', taille=21, tag="croix")
            texte(x*20+2, y*20-6, chaine="X", couleur='black', taille=21, tag="croix")
            texte(x*20+5, y*20+1, chaine="X", couleur='white', taille=12, tag="croix")
        # peu importe ce qui s'est passé, le couple de coordonnées être enregistré dans la liste list_dejatouch
        list_dejatouch.append((x,y))
        # cela permet que le curseur reste à la position où il avait tiré
        if j2 == False:
            x, y = x_j2, y_j2
        else:
            x, y = x_j1, y_j1
        # j2 s'inverse, c'est à dire que c'est le tour à l'autre joueur
        return bool(not j2), x, y, kill, munition
    # déjà tiré sur cette case
    else:
        return j2, x, y, kill, munition
        
        
        
def bombe(x, y, bateaux, munition, x_j1, y_j1, x_j2, y_j2, j2, kill):
    # il permet de faire 9 fois plouf, sur un carré 3x3 pour former un tir d'une portée + grande
    # dans un premier temps on va créer une liste list_bombe_tri, former de ces 9 coordonnées
    # cette liste gardera que les coordonnées correspondant à une case du terrain et à des cases pas encore touché
    global munition_j1
    global munition_j2
    j2_sauv = j2
    list_bombe = []
    list_bombe_tri = []
    ens = []
    autour_sans(x,y,list_bombe)
    for i, elem in enumerate(list_bombe):
        for ii, elem in enumerate(bateaux):
            (a, b) = list_bombe[i]
            if list_bombe[i] not in list_dejatouch and ( ((0 <= a <= 19)and(0 <= b <= 19)) or ((23 <= a <= 42)and(0 <= b <= 19)) ):
                if list_bombe[i] in bateaux[ii]:
                    ens.insert(0,list_bombe[i])
                else:
                    ens.append(list_bombe[i])
    # on supprime les doublons 
    for i in ens : 
        if i not in list_bombe_tri: 
            list_bombe_tri.append(i)
    # si après le tri, on remarque que le tir de la bombe ne touche que des cases invalides, on return comme si de rien n'était
    if list_bombe_tri == []:
        return j2, x, y, kill, munition
    else:
        # c'est ici où plouf va être répété autant de fois que de cases valides, selon le joueur qui tire
        for j, elem in enumerate(list_bombe_tri):
            (a, b) = list_bombe_tri[j]
            if not j2:
                j2, x, y, kill, munition_j1 = plouf(a, b, list_dejatouch, bateaux, envue_j2, dessin, j2, adversaire, x_j1, y_j1, x_j2, y_j2, f2, kill, mode_bombe, munition_j1)
                munition = munition_j1
                j2 = j2_sauv
            else:
                j2, x, y, kill, munition_j2 = plouf(a, b, list_dejatouch, bateaux_j1, envue_j1, dessin_j1, j2, adversaire, x_j1, y_j1, x_j2, y_j2, f1, kill, mode_bombe, munition_j2)
                munition = munition_j2
                j2 = j2_sauv
        # cela permet que le curseur reste à la position où il avait tiré
        if j2_sauv == False:
            x, y = x_j2, y_j2
        else:
            x, y = x_j1, y_j1
        # dans le mode explosif en ON, on perd une munition
        if mode_bombe == 1:
            return bool(not j2_sauv), x, y, kill, munition-1
        # dans le mode mortel, pas besoin de baisse de munition car c'est illimité
        else:
            return bool(not j2_sauv), x, y, kill, munition
        
        
def restreint(x, y, x2, y2):
    # après qu'on ait choisi l'endoit où placé le bateau, il faut redéplacé le curseur pour choisir sa longueur
    # cette fonction va permettre d'éviter au curseur d'aller aux bords, de faire des diagonales ou de faire des bateaux trop longs
    lim_x = [-1, 20, 22, 43]
    if x in lim_x or y == -1 or y == 20:
        return x2, y2
    elif x != x2 and y != y2:
        return x2, y2
    elif abs(x-x2)>5 or abs(y-y2)>5:
        return x2, y2
    else:
        return x, y
    
    
def autour(q, w, l):
    # elle permet de créer les 8 coordonnées autour d'une case plus celle de la case
    # tout cela sera mis dans une liste, elle servira pour le 'en vue' de chaques cases d'un bateau
    """
    autour(1,1,lst)
    >>> [[(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]]
    """
    ens = []
    for j in range(q-1,q+2):
        for jj in range(w-1,w+2):
            ens.append((j,jj))
    l.append(ens)
    
    
def autour_sans(q, w, l):
    # elle permet de créer les 8 coordonnées autour d'une case plus celle de la case
    # tout cela ne sera pas mis dans une liste, elle servira pour la liste interdit
    # elle contiendra toutes les coordonnées interdites lors des placements de nouveaux bateaux
    # la liste interdit ne contiendra pas de sous liste, ce qui sera plus simple
    """
    autour(1,1,lst)
    >>> [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]
    """
    ens = []
    for j in range(q-1,q+2):
        for jj in range(w-1,w+2):
            l.append((j,jj))
    
    
    
    
def verifie(x, selec_x, y, selec_y, interdit, ens, bateaux, dessin, selectio, ok, compt, f, nb_bateaux, nb_bateaux_cmpt):
    # cette fonction est appellée au moment de valider l'emplacement du bateau
    # pour commencer on regarde si au moment de choisir sa longeur le bateau est au norme
    # ok = False si le bateau de longueur > à 1 se trouve proche d'un autre bateau ou dans un bateau

    # si le bateau est horizontale
    if x != selec_x:
        if x > selec_x:
            for i in range(selec_x, x+1):
                if (i, y) in interdit:
                    ok = False
        else:
            for i in range(x, selec_x+1):
                if (i, y) in interdit:
                    ok = False
    # si le bateau est verticale
    elif y != selec_y:
        if y > selec_y:
            for i in range(selec_y, y+1):
                if (x, i) in interdit:
                    ok = False
        else:
            for i in range(y, selec_y+1):
                if (x, i) in interdit:
                    ok = False
                        
    # le bateau qui va être placé est ok, alors pour chaques façons comment le bateau a été placé (verticale, horizontale), on va:
    # - créer une liste, contenant un couple de coordonnées pour chaques cases du bateau, dans la liste bateaux
    # - créer 9 couples de coordonnées pour chaques cases du bateau (8 cases autour + la case), dans la liste interdit
    # - créer une liste, contenant 4 coordonnées pour dessiner un rectangle de la taille d'un bateau, dans la liste dessin
    # tout cela grâce à la fonction création
    if ok == True:

        creation(x, selec_x, y, selec_y)
            
        cmp = max(abs(x-selec_x),abs(y-selec_y))
        compt[cmp] += 1
        nb_bateaux_cmpt -= 1
        
        selectio = False
 
    ok = True
    
    return selectio, ok, nb_bateaux_cmpt
    
    
    
    
def verifiee(x, selec_x, y, selec_y, interdit, ens, bateaux, dessin, compt, f):
    # pareil que verifie, mais pour l'ordinateur on va placer aléatoirement ces bateaux
    # x et y seront choisies au hasard et on va répéter l'opération autant de bateaux qu'on a placé

    for com, elem in enumerate(compt):
        for nb in range(compt[com]):
        
            x = randint(23,42)
            y = randint(0,19)
            choix = randint(0,1)
            
            ok = False
            
            while ok == False:
            
            
                if choix == 0:
                    selec_x = randint(23,42)
                    while ((x, y)in interdit) or ((selec_x, y) in interdit) or (abs(x - selec_x) != com): 
                        x = randint(23,42)
                        y = randint(0,19)
                        selec_x = randint(23,42)
                    selec_y = y
                else:
                    selec_y = randint(0,19)
                    while ((x, y)in interdit) or ((x, selec_y) in interdit) or (abs(y - selec_y) != com): 
                        x = randint(23,42)
                        y = randint(0,19)
                        selec_y = randint(0,19)
                    selec_x = x

                    
                if x != selec_x:
                    if x > selec_x:
                        for i in range(selec_x, x+1):
                            if (i, y) in interdit:
                                ok = False
                                break
                            else:
                                ok = True
                    else:
                        for i in range(x, selec_x+1):
                            if (i, y) in interdit:
                                ok = False
                                break
                            else:
                                ok = True
                elif y != selec_y:
                    if y > selec_y:
                        for i in range(selec_y, y+1):
                            if (x, i) in interdit:
                                ok = False
                                break
                            else:
                                ok = True
                    else:
                        for i in range(y, selec_y+1):
                            if (x, i) in interdit:
                                ok = False
                                break
                            else:
                                ok = True
                elif x == selec_x and y == selec_y:
                    if (x, y) in interdit:
                        ok = False
                        break
                    else:
                        ok = True
                
            ens = []
                    
            creation(x, selec_x, y, selec_y)
                
    select = False
    
    return select
    
    
    
    
def creation(x, selec_x, y, selec_y):
    ens = []
    if x != selec_x:
        if x > selec_x:
            for i in range(selec_x, x+1):
                ens.append((i,y))
                autour_sans(i, y, interdit)
                image(i*20+1, y*20+1, str(f)+".pgm", ancrage='nw', tag="apercu")
            bateaux.append(ens)
            dessin.append([(x+.5)*20+10,(y+.5)*20+10,(selec_x+.5)*20-10,(selec_y+.5)*20-10])
        else:
            for i in range(x, selec_x+1):
                ens.append((i,y))
                autour_sans(i, y, interdit)
                image(i*20+1, y*20+1, str(f)+".pgm", ancrage='nw', tag="apercu")
            bateaux.append(ens)
            dessin.append([(selec_x+.5)*20+10,(selec_y+.5)*20+10,(x+.5)*20-10,(y+.5)*20-10])
    elif y != selec_y:
        if y > selec_y:
            for i in range(selec_y, y+1):
                ens.append((x,i))
                autour_sans(x, i, interdit)
                image(x*20+1, i*20+1, str(f)+".pgm", ancrage='nw', tag="apercu")
            bateaux.append(ens)
            dessin.append([(x+.5)*20+10,(y+.5)*20+10,(selec_x+.5)*20-10,(selec_y+.5)*20-10])
        else:
            for i in range(y, selec_y+1):
                ens.append((x,i))
                autour_sans(x, i, interdit)
                image(x*20+1, i*20+1, str(f)+".pgm", ancrage='nw', tag="apercu")
            bateaux.append(ens)
            dessin.append([(selec_x+.5)*20+10,(selec_y+.5)*20+10,(x+.5)*20-10,(y+.5)*20-10])
    else:
        autour_sans(x, y, interdit)
        bateaux.append([(x,y)])
        image(x*20+1, y*20+1, str(f)+".pgm", ancrage='nw', tag="apercu")
        dessin.append([(x+.5)*20+10,(y+.5)*20+10,(selec_x+.5)*20-10,(selec_y+.5)*20-10])
            
            
def creer_envue(bateaux, envue):
    # elle permet de créer une liste, contenant 9 couples de coordonnées pour chaques cases du bateau, dans la liste envue
    for i, elem in enumerate(bateaux):
        ens = []
        if len(bateaux[i]) == 1:
            [(s, d)] = (bateaux[i])
            autour(s, d, ens)
        else:
            for u, elem in enumerate(bateaux[i]):
                (q, w) = (bateaux[i][u])
                autour(q, w, ens)
        envue.append(ens)
    return envue
    
    
    
# le programme

cree_fenetre(taille_case * dimentio[0] + 1, taille_case * dimentio[1] + 1)
programme = True
    
if __name__ == "__main__":
    
    while programme:
    
        efface_tout()
                
        # bateaux est la liste comportant les coordonnées des bateaux
        # envue est la liste des coordonnées aux alentours des bateaux, permettant de ne pas placer lea bateaux cote à cote et de permettre les "en vue"
        # dessin est la liste pour pouvoir retracer les rectangles
        bateaux = []
        envue = []
        dessin = []
        
        # position du curseur
        x = 0
        y = 0
        
        # variables utilisées pour des boucle while, tant qu'ils sont True on est dans la boucle
        # "jeu" pendant le jeu ou on lance les missiles, "select" pendant qu'on place les bateaux
        # "selectio" pendant qu'on modifie la taille des bateaux places et "fin" pour la fin du jeu
        debut = True
        fond = True
        select = True
        selectio = True
        jeu = False
        fin = True
        
        # si au moment de changer la taille du bateau on le place près d'un autre, ok devient False
        # car on veut éviter que les bateaux se touchent
        ok = True
        
        # pendant le jeu, cette liste permet de ne pas tirer plusieurs fois sur la même case
        list_dejatouch = []
        
        # liste de directions
        list_direc = ["Right", "Left", "Up", "Down"]
        
        # coordonnées qui permettent au curseur de garder leur position après 'avoir tire
        x_j1, y_j1, x_j2, y_j2 = 0, 23, 0, 0
        
        # liste contenant des coordonnées, elle se remplit à chaque bateaux crees
        # les 8 cases entourant le bateau et la case du bateau en lui même sont dedans
        # cela permet pour la creation de bateaux de comparer ces coordonnées puis de verifier si elles se trouvent dans interdit
        interdit = []
        
        # si mode_j2 est True, nous sommes en mode deux joueurs
        # si j2 est True, c'est soit le tour de l'adversaire (controlé par l'ordinateur), soit du joueur 2
        j2 = False
        mode_j2 = False
        
        # nombre de bateaux a placer
        nb_bateaux = 3
        
        # si c'est true, on est contre un adversaire controle par l'ordinateur
        adversaire = False
        
        # le mode kill s'active quand le tir de l'adversaire  a effectué un envue ou un touché, comme une personne normale il voudra essayer de le couler
        # list_kill est comme la liste envue sauf il n'y a pas de sous-sous listes mais seulement des sous listes
        # kill_qui est la liste contenant le bateau qui va être coulé, il peut essayer de couler plusieurs à la fois
        # kill_qui_deja permet à lèadversaire de ne pas revenir sur d'anciens bateaux déjà coulé
        kill = False
        list_kill = []
        kill_qui = []
        kill_qui_deja = []
        
        # une liste permettant de connaitre le nombre de bateaux places en fonction de leur taille
        compt = [0,0,0,0,0,0]
        
        # une liste qui represente les initiales des differents themes pour la bataille navale
        fonds = ["k","z","n"]
        
        # cela concerne le mode explosif, pour que les tirs ont une portée de 9 cases
        # mode bombe = 0 si le mode explosif est off, 1 pour on est 2 pour le mode toujours bombes
        # "bombes" contient les coordonnées où il y a les bombes
        mode_bombe = 0
        munition_j1 = 1
        munition_j2 = 1
        bombes = []
        
        
        image(0,0, "acceuil.pgm", ancrage='nw', tag="acc")
        
        
        
        
        
        
        
        
        
        while debut == True:
        
            ev = donne_ev()
            ty = type_ev(ev)
            
            efface("nb")
            texte(430, 300, nb_bateaux, couleur="black", ancrage='center', police='Helvetica', taille=40, tag="nb")
        
            if  ty == 'Touche' and touche(ev) == "d":
                debut = False
                mode_j2 = True
                
            elif  ty == 'Touche' and touche(ev) == "s":
                adversaire = True
                debut = False
                
            elif  ty == 'Touche' and touche(ev) == "Up" and nb_bateaux != 9:
                nb_bateaux += 1
                
            elif  ty == 'Touche' and touche(ev) == "Down" and nb_bateaux != 1:
                nb_bateaux -= 1
                
            else:
                pass
        
            mise_a_jour()
        
        
        
        
        
        
        
        
        
        
        
        
        efface_tout()
        image(0,0, "theme.pgm", ancrage='nw', tag="acc")
        texte(430, 420, "Thème pour le J1", couleur="black", ancrage='center', police='Helvetica', taille=26, tag="fond1")
        
        while fond == True:
        
            ev = donne_ev()
            ty = type_ev(ev)
            
            efface("bombe")
            if mode_j2:
                image(430,200, str(mode_bombe)+"explo.pgm", ancrage='c', tag="bombe")
                
                
            if  ty == 'Touche' and touche(ev) == "Right":
                mode_bombe = (mode_bombe + 1) % 3
                
            elif  ty == 'Touche' and touche(ev) == "Left":
                mode_bombe = (mode_bombe - 1) % 3
                
            
            elif  ty == 'Touche' and touche(ev) in fonds:
                efface("bombe")
                efface("fond1")
                if mode_j2:
                    texte(430, 420, "Thème pour le J2", couleur="black", ancrage='center', police='Helvetica', taille=26, tag="fond1")
                else:
                    texte(430, 420, "Thème pour l'adversaire", couleur="black", ancrage='center', police='Helvetica', taille=26, tag="fond1")
                f1 = touche(ev)
                fond2 = True
                
                while fond2 == True:

                    eve = donne_ev()
                    tye = type_ev(eve)

                    if  tye == 'Touche' and touche(eve) in fonds:
                        f2 = touche(eve)
                        fond2 = False
                
                    else:
                        pass
                
                    mise_a_jour()
                    
                fond = False
        
            else:
                pass
        
            mise_a_jour()
        
        
        
        
        
        
        
        
        
        
        
        
        efface_tout()
        nb_bateaux_cmpt = nb_bateaux
        if mode_j2 == True:
            image(0, 0, "terrain1.pgm", ancrage='nw', tag="fond")
        else:
            image(0, 0, "terrain2.pgm", ancrage='nw', tag="fond")
        image(0, 0, str(f1)+"fond.pgm", ancrage='nw', tag="fond")
        image(460, 0, str(f2)+"fond.pgm", ancrage='nw', tag="fond")
        for j in range(0,461,460):
            for i in range(0,401,20):
                ligne(j, i, 400+j, i, couleur='black', epaisseur=1, tag="jeu") # lignes horizontales
                ligne(i+j, 0, i+j, 400, couleur='black', epaisseur=1, tag="jeu") # lignes verticales
        texte(549, 474, [0,0,0,0,0,0], couleur="blue", ancrage='nw', police='Helvetica', taille=24, tag="t2")
        
        while select == True:
        
            menu = "select"
            selectio = True
            ev = donne_ev()
            ty = type_ev(ev)
            
            
            efface("c")
            efface("selec_c")
            affiche_curseur(x,y,j2,menu)
            
            efface("regle1")
            efface("regle2")
            efface("restant")
            if nb_bateaux_cmpt != 0:
                image(0, 0, "regle1.pgm", ancrage='nw', tag="regle1")
                texte(86, 490, nb_bateaux_cmpt, couleur="black", ancrage='nw', police='Helvetica', taille=14, tag="restant")
            else:
                image(0, 0, "regle1_fin.pgm", ancrage='nw', tag="regle1")
            
            efface("t")
            if j2 == True:
                texte(549, 474, compt, couleur="blue", ancrage='nw', police='Helvetica', taille=24, tag="t")
            else:
                texte(549, 445, compt, couleur="red", ancrage='nw', police='Helvetica', taille=24, tag="t")
            
            if ty == 'Quitte':
                ferme_fenetre()
                
            elif ty == 'Touche' and touche(ev) in list_direc:
                x, y = mouvement(x, y, touche(ev))
                x, y = torique(x, y, j2, jeu)
                
            elif ty == 'Touche' and touche(ev) == "Escape":
                nb_bateaux_cmpt = nb_bateaux
                bateaux = []
                dessin = []
                interdit = []
                efface("apercu")
                efface("t")
                compt = [0,0,0,0,0,0]
                
            elif ty == 'Touche' and touche(ev) == "space" and sum(compt) == nb_bateaux:
                if j2 == False:
                    nb_bateaux_cmpt = nb_bateaux
                    bateaux_j1 = bateaux
                    dessin_j1 = dessin
                    envue_j1 = creer_envue(bateaux, envue)
                    j2 = True
                    x = 23
                    y = 0
                    bateaux = []
                    dessin = []
                    interdit = []
                    envue = []
                    texte(549, 445, compt, couleur="red", ancrage='nw', police='Helvetica', taille=24, tag="t1")
                    efface("t2")
                    f = f2
                    compt_j1 = compt*1
                    if adversaire == True:
                        texte(549, 474, compt, couleur="blue", ancrage='nw', police='Helvetica', taille=24, tag="tadv")
                        select = verifiee(x, selec_x, y, selec_y, interdit, ens, bateaux, dessin, compt, f)
                        envue_j2 = creer_envue(bateaux, envue)

                    else:
                        compt = [0,0,0,0,0,0]
                else:
                    envue_j2 = creer_envue(bateaux, envue)
                    select = False
                efface("apercu")
                
                
                
            elif ty == 'Touche' and touche(ev) == "Return" and ((x, y) not in interdit) and sum(compt) != nb_bateaux:
                            
                selec_x = x
                selec_y = y
                ens = []
                cercle_origine_1, cercle_origine_2 = case_vers_pixel((selec_x, selec_y))
                cercle(cercle_origine_1, cercle_origine_2, 1, couleur="black", epaisseur=2, tag="selec_c")
                cercle(cercle_origine_1, cercle_origine_2, 6, couleur="black", epaisseur=4, tag="selec_c")
                
                while selectio == True:
                    
                    menu = "selectio"
                    ev2 = donne_ev()
                    ty2 = type_ev(ev2)
                    
                    efface("c")
                    affiche_curseur(x,y,j2,menu)
                    
                    efface("restant")
                    efface("regle1")
                    efface("regle2")
                    image(0, 0, "regle2.pgm", ancrage='nw', tag="regle2")
                    
                    x, y = restreint(x, y, selec_x, selec_y)
                    
                    if ty2 == 'Quitte':
                        ferme_fenetre()
                    
                    elif ty2 == 'Touche' and touche(ev2) in list_direc:
                        x, y = mouvement(x, y, touche(ev2))
                        
                    elif ty2 == 'Touche' and touche(ev2) == "Escape":
                        selectio = False
                        
                    elif ty2 == 'Touche' and touche(ev2) == "Return":
                        if j2 == False:
                            f = f1
                        else:
                            f = f2
                        selectio, ok, nb_bateaux_cmpt = verifie(x, selec_x, y, selec_y, interdit, ens, bateaux, dessin, selectio, ok, compt, f, nb_bateaux, nb_bateaux_cmpt)
                        
                    else:
                        pass
                        
                    mise_a_jour()
     
                    
            else:
                pass
                
            mise_a_jour()
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        # list_kill contiendra autant de liste que de bateaux qu'on a placé, dans chaques listes elles contiendront le envue du bateau concerné
        # ce passage permet de rassembler par exemple pour les bateaux longs tous le envue concernant le bateau en une seule liste
        list_kill = envue_j1 * 1
        for i, elem in enumerate(list_kill):
            result_list = []
            for elem_list in list_kill[i]:
                result_list.extend(elem_list)
            list_kill[i] = result_list
                
                
        if mode_bombe == 1:
            for i in range(0,30,23):
                for j in range(3):
                    x = randint(0+i,19+i)
                    y = randint(0,19)
                    while (x, y) in interdit:
                        x = randint(0+i,19+i)
                        y = randint(0,19)
                    bombes.append((x,y))
                    interdit.append((x,y))
            print(bombes)
        
        
        x = 23
        y = 0
        j2 = False
        jeu = True
        efface("t1")
        efface("t")
        efface("t2")
        efface("tadv")
        efface("regle1")
        efface("regle2")
        efface("restant")
        
        image(0, 0, str(mode_bombe)+"regle_jeu.pgm", ancrage='nw')

        while jeu == True:
            
            menu = "jeu"
            ev = donne_ev()
            ty = type_ev(ev)
            
            efface("c")
            affiche_curseur(x,y,j2,menu)
            
            if ty == 'Quitte':
                ferme_fenetre()
                
            elif ty == 'Touche' and touche(ev) in list_direc:
                x, y = mouvement(x, y, touche(ev))
                x, y = torique(x, y, j2, jeu)
                
                
            elif ty == 'Touche' and touche(ev) == "Return" and mode_bombe != 2:
                if j2 == False :
                    x_j1, y_j1 = x, y
                    j2, x, y, kill, munition_j1 = plouf(x, y, list_dejatouch, bateaux, envue_j2, dessin, j2, adversaire, x_j1, y_j1, x_j2, y_j2, f2, kill, mode_bombe, munition_j1)
                    if adversaire == True:
                        j2, x ,y, kill, munition_j2 = plouf(x, y, list_dejatouch, bateaux_j1, envue_j1, dessin_j1, j2, adversaire, x_j1, y_j1, x_j2, y_j2, f1, kill, mode_bombe, munition_j2)
                else:
                    x_j2, y_j2 = x, y
                    j2, x ,y, kill, munition_j2 = plouf(x, y, list_dejatouch, bateaux_j1, envue_j1, dessin_j1, j2, adversaire, x_j1, y_j1, x_j2, y_j2, f1, kill, mode_bombe, munition_j2)
                    
                if compt == [0, 0, 0, 0, 0, 0]:
                    jeu = False
                    gagnant = "j1"
                elif compt_j1 == [0, 0, 0, 0, 0, 0]:
                    jeu = False
                    gagnant = "j2"
                    
                    
            elif ty == 'Touche' and touche(ev) == "space" and mode_bombe != 0:
                if j2 == False and munition_j1 != 0:
                    x_j1, y_j1 = x, y
                    j2, x, y, kill, munition_j1 = bombe(x, y, bateaux, munition_j1, x_j1, y_j1, x_j2, y_j2, j2, kill)
                elif j2 and munition_j2 != 0:
                    x_j2, y_j2 = x, y
                    j2, x ,y, kill, munition_j2 = bombe(x, y, bateaux_j1, munition_j2, x_j1, y_j1, x_j2, y_j2, j2, kill)
                    
                if compt == [0, 0, 0, 0, 0, 0]:
                    jeu = False
                    gagnant = "j1"
                elif compt_j1 == [0, 0, 0, 0, 0, 0]:
                    jeu = False
                    gagnant = "j2"
                
            else:
                pass
                
            efface("c_jeu")
            texte(549, 445, compt_j1, couleur="red", ancrage='nw', police='Helvetica', taille=24, tag="c_jeu")
            texte(549, 474, compt, couleur="blue", ancrage='nw', police='Helvetica', taille=24, tag="c_jeu")
            
            efface("bombe")
            if mode_bombe == 1:
                image(430, 16, "bombe.pgm", ancrage='c', tag="bombe")
                texte(430, 50, munition_j1, couleur="red", ancrage='c', police='Helvetica', taille=20, tag="bombe")
                texte(430, 80, munition_j2, couleur="blue", ancrage='c', police='Helvetica', taille=20, tag="bombe")
                
            mise_a_jour()
            
            
            
            
            
            
            
            
        
        
        
        
        
            
        if gagnant == "j1":
            for i, elem in enumerate(bateaux_j1):
                if len(bateaux_j1[i]) != 1:
                    for j, elem in enumerate(bateaux_j1[i]):
                        (a,b) = bateaux_j1[i][j]
                        image(a*20+1, b*20+1, str(f1)+".pgm", ancrage='nw')
                else:
                    [(a,b)] = bateaux_j1[i]
                    image(a*20+1, b*20+1, str(f1)+".pgm", ancrage='nw')
            image(460,0, "filtre_au_perdant.pgm", ancrage='nw')
            if mode_j2:
                texte(660, 150, "Joueur 1 a gagné !", couleur="red", ancrage='c', police='Helvetica', taille=34)
            else:
                texte(660, 150, "Vous avez gagné !", couleur="red", ancrage='c', police='Helvetica', taille=34)
            texte(660, 250, "Rejouez?    O / N", couleur="white", ancrage='c', police='Helvetica', taille=24)
        else:
            for i, elem in enumerate(bateaux):
                if len(bateaux[i]) != 1:
                    for j, elem in enumerate(bateaux[i]):
                        (a,b) = bateaux[i][j]
                        image(a*20+1, b*20+1, str(f2)+".pgm", ancrage='nw')
                else:
                    [(a,b)] = bateaux[i]
                    image(a*20+1, b*20+1, str(f2)+".pgm", ancrage='nw')
            image(0,0, "filtre_au_perdant.pgm", ancrage='nw')
            if mode_j2:
                texte(200, 150, "Joueur 2 a gagné !", couleur="blue", ancrage='c', police='Helvetica', taille=34)
            else:
                texte(200, 150, "Perdu...", couleur="blue", ancrage='c', police='Helvetica', taille=34)
            texte(200, 250, "Rejouez?    O / N", couleur="white", ancrage='c', police='Helvetica', taille=24)
            
                
                
        while fin:
        
            ev = donne_ev()
            ty = type_ev(ev)
            
            if ty == 'Quitte':
                ferme_fenetre()
                
            elif ty == 'Touche' and touche(ev) == ("o"):
                fin = False
                
            elif ty == 'Touche' and touche(ev) == ("n"):
                fin = False
                programme = False
                
            else:
                pass
            
            mise_a_jour()
            
            
            
            
    ferme_fenetre()