from upemtk import *
from time import sleep
import time
from random import randint
from copy import deepcopy
import os
import json
import pygame
import sys

log = open("voir.txt","w")
sys.stderr = log




#=========================================================================================================================================================================================

# Voici les fonctions utilisees pour le programme,
# certaines sont dans l'autre partie ci-dessous (qui permmettent une simplification du programme principal)
# les autres sont dans la classe "Logimage" plus en-dessous.

def pixel_en_coordonne(a,b):
    """
    La fonction prend en paramètres les coordonnées du clic gauche de la souris en pixels,
    puis les renvoie sous forme de coordonnées de case du plateau.
    >>>pixel_en_coordonne(12,9)
    [0,0]
    >>>pixel_en_coordonne(47,23)
    [2,1]
    """
    return [a//20,b//20]
    
def pour_carre(a,b):
    """
    La fonction prend en paramètres les coordonnées du clic gauche de la souris en pixels, puis renvoie les
    résultats entier d'une division par 20 de l'abscisse et de l'ordonnée, puis les multiplie chacun par 20.
    Cela permet de créer les coordonnées pour que la fonction rectangle de upemtk puisse créer les carrés 
    pour colorier les cases.
    >>>pixel_en_coordonne(12,9)
    (0,0)
    >>>pixel_en_coordonne(47,23)
    (80,20)
    """
    return a//20*20,b//20*20
    
def plateau(x,y):
    """
    La fonction prend deux listes comme paramètres, et permet de créer une matrice.
    Elle de taille largeur x hauteur, avec que des 0. C'est une representation du plateau avec 
    toutes les chaques cases.
    """
    plateau=[]
    for contrainte in range(len(x)):
        ligne_plateau = []
        for besoin in range(len(y)):
            ligne_plateau.append(0)
        plateau.append(ligne_plateau)
    return plateau
    
def plateau_inverse(x,y):
    """
    Pareil que plateau, mais de taille hauteur x largeur.
    Cela permet de mieux gerer les contraintes pour les colonnes.
    """
    plateau=[]
    for contrainte in range(len(y)):
        ligne_plateau = []
        for besoin in range(len(x)):
            ligne_plateau.append(0)
        plateau.append(ligne_plateau)
    return plateau
    
def affiche_grille(lst):
    """
    Permet d'afficher les matrices créees par les fonctions plateau et plateau_inverse
    sous forme de grille.
    """
    for i in range(0,len(lst)):
        print(lst[i])
        
    
def affiche_cont(lst):
    """
    La fonction permet de savoir les contraintes actuels. Elle 
    compte les 1 qui sont ensembles, et retourne une matrice avec le nombre de contrainte et leur taille
    pour chaques lignes, ces matrices sont appellées des contraintes actuels.
    """
    contrainte = []
    combo = 0
    place = 0
    fin_de_cont_index = []
    if sum(lst) == 0:
        contrainte = [0]
    else:
        
        for j in range(len(lst)):
            
            # si on termine par un 1
            if j == (len(lst)-1) and lst[j] == 1:
                contrainte.append(combo+1)
                place = 0
                
            # si c'est un 1
            elif lst[j] == 1:
                combo += 1
                
            # casseur de combo
            elif lst[j] == 0 and combo != 0:
                contrainte.append(combo)
                combo = 0
                place = 0
                fin_de_cont_index.append(j)
                
            if combo == 0 and lst[j] == 0:
                place += 1
                
    if fin_de_cont_index == []:
        fin_de_cont_index.append(contrainte[-1]+1)
                
    return (contrainte, place, fin_de_cont_index)


def contraintes(l):
    """
    Elle prend en parametre une liste contenant les contraintes verticale et horizontale
    d'un fichier texte qui sont en str, et renvoie une matrice composée de deux listes:
    une pour les contraintes horizontale l'autre verticale
    
    Par exemple, le fichier texte canard resssemble à ceci:
    3/2-1/3-2/2-2/6/1-5/6/1/2
    1-2/3-1/1-5/7-1/5/3/4/3
    
    l = ["3/2-1/3-2/2-2/6/1-5/6/1/2", "1-2/3-1/1-5/7-1/5/3/4/3"]
    
    >>> contrainte(l)
    [[3], [2, 1], [3, 2], [2, 2], [6], [1, 5], [6], [1], [2]]
    [[1, 2], [3, 1], [1, 5], [7, 1], [5], [3], [4], [3]]
    """
    ligne_rep = l[0]
    colonne_rep = l[1]
    
    lr = (ligne_rep.replace("\n", "").replace(" ", "").replace("-", "*")).split("/")
    cr = (colonne_rep.replace("\n", "").replace(" ", "").replace("-", "*")).split("/")
    #print(lr, cr)
    
    ligne_reponse = []
    colonne_reponse = []
    for i in lr:
        if "*" in i:
            a = i.split("*")
            b = []
            for j in a:
                b = b + [int(j)]
            ligne_reponse.append(b)
        else:
            ligne_reponse.append([int(i)])
    for i in cr:
        if "*" in i:
            a = i.split("*")
            b = []
            for j in a:
                b = b + [int(j)]
            colonne_reponse.append(b)
        else:
            colonne_reponse.append([int(i)])
            
    return [ligne_reponse,colonne_reponse]
    
    
def rempli(lst, element='case_rouge'):
    """
    La fonction aide va prendre en parametre une liste de coordonnes, elle va remplir le plateau
    avec les coordonnes de la liste, utile pour l'ecran titre pour l'affichage de "LOGIMAGE" ou
    pour reprendre la partie
    """
    if element == 'case_rouge':
        for i in lst:
            (c1, c2) = i
            (c1, c2) = (c1*20, c2*20)
            rectangle(c1+1, c2+1, c1+19, c2+19, couleur='white', remplissage='red', epaisseur=1, tag='')
            
    elif element == 'blanc':
        for i in lst:
            (c1, c2) = i
            rempli([c1, c2], element="damier_1")
            
    elif element == 'damier':
        for i in range(lst[0]):
            for j in range(lst[1]):
                (c1, c2) = (i*20, j*20)
                if (i%2 == 0 and j%2 == 0) or (i%2 != 0 and j%2 != 0):
                    rectangle(c1+1, c2+1, c1+19, c2+19, couleur='gray84', remplissage='gray84', epaisseur=1, tag='')
                else:
                    rectangle(c1+1, c2+1, c1+19, c2+19, couleur='gray94', remplissage='gray94', epaisseur=1, tag='')
                    
    elif element == 'damier_1':
        (c1, c2) = (lst[0]*20, lst[1]*20)
        if (lst[0]%2 == 0 and lst[1]%2 == 0) or (lst[0]%2 != 0 and lst[1]%2 != 0):
            rectangle(c1+1, c2+1, c1+19, c2+19, couleur='gray84', remplissage='gray84', epaisseur=1, tag='')
        else:
            rectangle(c1+1, c2+1, c1+19, c2+19, couleur='gray94', remplissage='gray94', epaisseur=1, tag='')
            
    else:
        for i in lst:
            (c1, c2) = i
            (c1, c2) = (c1*20, c2*20)
            rempli([c1/20, c2/20], element="damier_1")
            ligne(c1+1, c2+1, c1+20, c2+20, couleur='black', epaisseur=2, tag='')
            ligne(c1+1, c2+20, c1+20, c2+1, couleur='black', epaisseur=2, tag='')
    
    
def aide(lst):
    """
    La fonction aide va prendre en parametre une liste de coordonnes, elle permet
    de creer les contraintes pour le dessin crée dans l'editeur
    """
    cont = []
    combo = 0
    for i in lst:
        if i != [0]:
            if cont != [] and combo != 0:
                for j in range(combo):
                    cont.append("0")
            a = str(i).replace("[", "").replace("]", "").replace(" ", "").replace(",", "-")
            cont.append(a)
            combo = 0
        else:
            combo += 1
    print("/".join(cont))
    return("/".join(cont))
    
    
def affiche_contrainte(plat):
    """
    En utilisant les matrices créees par les fonctions plateau et plateau_inverse, la fonction est 
    utile pour avoir un apercu du plateau, car elle permet de savoir les contraintes actuels. Elle 
    compte les 1 qui sont ensembles, et retourne une matrice avec le nombre de contrainte et leur taille
    pour chaques lignes, ces matrices sont appellées des contraintes actuels.
    """
    contrainte = []
    for i in plat:
        pseudo_liste = []
        combo = 0
        if sum(i) == 0:
            pseudo_liste = [0]
        else:
            for j in range(len(i)):
                if j == (len(i)-1) and i[j] == 1:
                    pseudo_liste.append(combo+1)
                elif i[j] == 1:
                    combo += 1
                elif i[j] == 0 and combo != 0:
                    pseudo_liste.append(combo)
                    combo = 0
        contrainte.append(pseudo_liste)
    return contrainte
    
    
def completable(lst_c, lst_p):
    """
    On prend en parametre lst_c, une liste de contrainte concernant une ligne d'un plateau
    lst_p est la ligne du plateau concerné, avec des 0 et 1 qui representent les cases coches ou non
    Retourne True s'il est encore possible que lst_p puisse satisfaire les contraintes en ne
    rajoutant que des 1 a partir du dernier 1 placé.
    """
    # si la ligne du plateau est vide, evidemment on peut la remplir
    if sum(lst_p) == 0:
        return True
    
    else:
        
        # on regarde les contraintes actuels de la ligne du plateau
        lst_p_to_c = affiche_cont(lst_p)
        
        # si les contraintes actuels sont egales aux contraintes correspondantes à cette ligne
        if lst_p_to_c[0] == lst_c:
            return True
        
        # si on ne peut pas colorier de case à la fin
        elif lst_p_to_c[2] == ["nope"] and lst_p_to_c[0] != lst_c:
            return False
        
        # s'il y a trop de contraintes sur le plateau comparé aux contraintes correspondantes à cette ligne
        elif len(lst_p_to_c[0]) > len(lst_c):
            return False
        
        # s'il y a plusieurs contraintes sur le plateau, et que une de ces dernieres soient fausses
        # la contrainte tout au bout peut etre egal ou inferieure
        deja = 0
        deja_mis = 0
        for j in range(len(lst_p_to_c[0])):
            if j != len(lst_p_to_c[0])-1 and lst_p_to_c[0][j] != lst_c[j]:
                return "stop"
            elif j == len(lst_p_to_c[0])-1 and lst_p_to_c[0][j] > lst_c[j]:
                return False
            elif j == len(lst_p_to_c[0])-1 and lst_p_to_c[0][j] < lst_c[j]:
                deja_mis = lst_p_to_c[0][j]+1
            else:
                deja_mis += lst_p_to_c[0][j]+1
                deja +=1
        
        already = 0
        if len(lst_p_to_c[0]) == len(lst_c):
            already = (1+lst_p_to_c[0][-1])
        else:
            already = -1
        # on teste en rajoutant une case a partir de la derniere case
        if lst_p_to_c[2] == []:
            print(lst_p_to_c)
            print(lst_c)
            attente(10)
        for i in range(lst_p_to_c[2][-1], len(lst_p)):
            lst_p_copy = []
            lst_p_copy[:] = lst_p
            lst_p_copy[i] = 1
            lst_p_to_c_copy = affiche_cont(lst_p_copy)
            
            if (lst_c[-1] - already) <= lst_p_to_c_copy[1]+1:
                return True
                
        return False
    
    
def super_texte(x, y, str, c, t, a, ta):
    texte(x+1, y+1, str, couleur="black", taille=t, ancrage=a, tag=ta)
    texte(x+1, y, str, couleur="black", taille=t, ancrage=a, tag=ta)
    texte(x+1, y-1, str, couleur="black", taille=t, ancrage=a, tag=ta)
    texte(x, y+1, str, couleur="black", taille=t, ancrage=a, tag=ta)
    texte(x, y-1, str, couleur="black", taille=t, ancrage=a, tag=ta)
    texte(x-1, y+1, str, couleur="black", taille=t, ancrage=a, tag=ta)
    texte(x-1, y, str, couleur="black", taille=t, ancrage=a, tag=ta)
    texte(x-1, y-1, str, couleur="black", taille=t, ancrage=a, tag=ta)
    texte(x, y, str, couleur=c, taille=t, ancrage=a, tag=ta)
    
def superr_texte(x, y, str, c, a, t, ta):
    texte(x+1, y+1, str, couleur="black", ancrage=a, taille=t, tag=ta)
    texte(x+1, y, str, couleur="black", ancrage=a, taille=t, tag=ta)
    texte(x+1, y-1, str, couleur="black", ancrage=a, taille=t, tag=ta)
    texte(x, y+1, str, couleur="black", ancrage=a, taille=t, tag=ta)
    texte(x, y-1, str, couleur="black", ancrage=a, taille=t, tag=ta)
    texte(x-1, y+1, str, couleur="black", ancrage=a, taille=t, tag=ta)
    texte(x-1, y, str, couleur="black", ancrage=a, taille=t, tag=ta)
    texte(x-1, y-1, str, couleur="black", ancrage=a, taille=t, tag=ta)
    texte(x, y, str, couleur=c, ancrage=a, taille=t, tag=ta)
    
        
        
        
#=========================================================================================================================================================================================
    
# Les fonctions suivantes permettent d'afficher les differents menus du jeu
# cela permet de simplifier grandement le code principale.
# Certaines sont par ailleurs dans la classe "Logimage"

            
            
def fonct_info():
    """
    Creee et affiche la fenetre d'aide
    """
    ferme_fenetre()
    cree_fenetre(800,300)
    image(0, 0, 'image/jeu.pgm', ancrage='nw', tag='texte')
    super_texte(400, 40, "Informations", "green", 40, "center", "")
    super_texte(400, 90, "Clic gauche pour cocher une case", "blue", 25, "center", "")
    super_texte(400, 130, "Clic droit pour placer une croix aide", "blue", 25, "center", "")
    super_texte(400, 170, "Le bouton S pour le solveur, pour finir sans rien faire!", "gold", 22, "center", "")
    super_texte(400, 210, "Le bouton Q pour sauvegarder la partie et quitter", "red", 22, "center", "")
    super_texte(400, 270, "Echap pour quitter la fenêtre d'informations", "black", 22, "center", "")
    
    

    
    
def fonct_editeur():
    """
    Elle permet de preparer le mode editage, en initialisant des variables importantes au code
    et de creer la fenetre pour l'editage
    """
    
    editeur = True
    nom = True
    case_rouge = []
    direc = {"Up": (0,-1), "Down": (0,1), "Left": (-1,0), "Right": (1,0)}
    picross.lst_dessin = os.listdir(picross.les_dossiers[picross.index_dossier])
    
    # les deux listes ci-dessous contiennent les seuls caracteres possibles pour renommer notre nouveau dessin
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    nombre = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    ferme_fenetre()
    cree_fenetre(500,500)
    plat = plateau(25*[0],25*[0])
    plat_inv = plateau(25*[0],25*[0])
    secours = []

    rectangle(0,0,500,500,couleur="white",remplissage="white")
    for i in range(0,501,20):
        ligne(0, i, 501, i, couleur='black', epaisseur=1, tag="editeur") # lignes horizontales
    for i in range(0,501,20):
        ligne(i, 0, i, 501, couleur='black', epaisseur=1, tag="editeur") # lignes verticales
        
    rempli([25, 25], element="damier")
        
    return editeur, nom, case_rouge, direc, alphabet, nombre, plat, plat_inv, secours
    
    
    
def fonct_edimon():
    """
    Elle permet de preparer le mode editage, en initialisant des variables importantes au code
    et de creer la fenetre pour l'editage
    """
    
    edimon = True
    nom = True
    picross.index_dossier = 1
    picross.lst_dessin = os.listdir(picross.les_dossiers[picross.index_dossier])
    
    # les deux listes ci-dessous contiennent les seuls caracteres possibles pour renommer notre nouveau dessin
    alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    nombre = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    ferme_fenetre()
    cree_fenetre(600,500)
    image(0, 0, 'image/edimon.pgm', ancrage='nw', tag='')
        
    return edimon, nom, alphabet, nombre
    
        
        
def fonct_editeur_choix(nom, case_rouge, direc, alphabet, nombre, plat, plat_inv, secours, editeur):
    """
    Elle permet de gerer les actions pendant l'editage
    """
    
    ev = donne_ev()
    ty = type_ev(ev)
    
    if ty == "ClicGauche":
    
        case = pixel_en_coordonne(abscisse(ev),ordonnee(ev))
        x, y = case
        c1, c2 = pour_carre(abscisse(ev),ordonnee(ev))
        
        if case not in case_rouge:
            rectangle(c1+1, c2+1, c1+19, c2+19, couleur='white', remplissage='red', epaisseur=1, tag='rouge')
            case_rouge.append(case)
            plat[y][x] = 1
            plat_inv[x][y] = 1
            
        else:
            rempli([c1/20, c2/20], element="damier_1")
            case_rouge.remove(case)
            plat[y][x] = 0
            plat_inv[x][y] = 0
    
    # quand on valide notre dessin et qu'il n'est pas vide
    elif ty == 'Touche' and touche(ev) == "Return" and case_rouge != []:
        
        image(0, 0, 'image/noir.pgm', ancrage='nw', tag='texte')
        superr_texte(250, 100, "Donnez un nom à votre dessin", 'blue', 'center',  20, 'texte')
        superr_texte(250, 150, "Pour valider appuyez sur Entrée", 'blue', 'center',  15, 'texte')
        superr_texte(250, 170, "Sinon pour quitter, appuyez sur Esc", 'blue', 'center',  15, 'texte')
        name = ""
        
        # la boucle pour pouvoir renommer
        while nom:

            ev_2 = donne_ev()
            ty_2 = type_ev(ev_2)
            
            efface("name")
            superr_texte(250, 250, str(name), 'yellow', 'center',  30, 'name')
            
            if ty_2 == 'Touche':
            
                # supprimer avec backspace le dernier element du nom
                if touche(ev_2) == "BackSpace" and name != "":
                    name = name[0:len(name)-1]
                
                # validation et creation du fichier texte
                elif touche(ev_2) == "Return" and name != "" and str(name) not in picross.lst_dessin:
                    colonne_etat = affiche_contrainte(plat_inv)
                    ligne_etat = affiche_contrainte(plat)
                    l1 = aide(ligne_etat)
                    l1 += "\n"
                    l2 = aide(colonne_etat)
                    
                    # on l'ecrit dans le dossier
                    with open(picross.les_dossiers[picross.index_dossier]+"/{}.txt".format(name), "w") as f:
                        f.write(l1)
                        f.write(l2)
                    
                    nom = False
                
                # on annule le renommage et on revient dans l'editage
                elif touche(ev_2) == "Escape":
                    nom = False
                
                # pour le renommage, met la majuscule pour le premier caractere si c'est une lettre
                elif touche(ev_2) in alphabet or touche(ev_2) in nombre:
                    if name == "" and touche(ev_2) in alphabet:
                        name += chr(ord(touche(ev_2))-ord("a")+ord("A"))
                    else:
                        name += touche(ev_2)
                    
                else:
                    pass
                    
            else:
                pass
                
            mise_a_jour()
            
        efface("name")
        efface("texte")
        continuer = True
        nom = True
        
        # si on veut continuer a editer ou pas
        while continuer:
            image(0, 0, 'image/noir.pgm', ancrage='nw', tag='choix_fin')
            superr_texte(250, 250, "Entrée pour continuer, Echap pour quitter", 'yellow', 'center',  18, 'choix_fin')
            editeur, continuer = fonct_editeur_fin(editeur, continuer)
            efface("choix_fin")
    
    # on peut faire bouger tout le dessin, a chaque mouvement on verifie si aucune case deborde de la fenetre
    elif ty == 'Touche' and touche(ev) in direc:
        efface("rouge")
        efface("blanc")
        secours [:]= case_rouge
        for i in range(len(case_rouge)):
            case_rouge[i] = (case_rouge[i][0]+direc[touche(ev)][0],case_rouge[i][1]+direc[touche(ev)][1])
            if case_rouge[i][0] > 24 or case_rouge[i][0] < 0 or case_rouge[i][1] > 24 or case_rouge[i][1] < 0:
                case_rouge [:]= secours
                break
        for i in range(len(case_rouge)):
            rectangle(case_rouge[i][0]*20+1, case_rouge[i][1]*20+1, case_rouge[i][0]*20+19, case_rouge[i][1]*20+19, couleur='white', remplissage='red', epaisseur=1, tag='rouge')
    
    # pour quitter l'editage
    elif ty == 'Touche' and touche(ev) == "Escape":
        editeur = False
        
    else:
        pass
        
    mise_a_jour()
    
    return nom, case_rouge, direc, alphabet, nombre, plat, plat_inv, secours, editeur
    
    
def fonct_edimon_choix(nom, alphabet, nombre, edimon, text, pseudo_text, index, phase, mode, affiche_ancien):
    """
    Elle permet de gerer les actions pendant l'editage
    """
    
    ev = donne_ev()
    ty = type_ev(ev)
    
    efface("text")
    superr_texte(300, 230, pseudo_text, 'dodger blue', 'center',  25, 'text')
    superr_texte(30, 30, index, 'green', 'center',  25, 'text')
    if text[phase] != []:
        superr_texte(300, 300, "Avant il y a: " + str(affiche_ancien), 'gold', 'center',  10, 'text')
    if phase == 0:
        superr_texte(300, 60,"Les lignes de haut en bas", 'gold', 'center', 30, 'text')
        superr_texte(300, 460,"La flèche de gauche permet de revenir sur la ligne précédente", 'blue', 'center', 15, 'text')
        if pseudo_text != []:
            superr_texte(300, 360,"Pour valider cette ligne, appuyez sur le bouton Entrée", 'green', 'center',  15, 'text')
    else:
        superr_texte(300, 60,"Les colonnes de gauche à droite", 'gold', 'center', 30, 'text')
        superr_texte(300, 460,"La flèche de gauche permet de revenir sur la colonne précédente", 'blue', 'center', 15, 'text')
        if pseudo_text != []:
            superr_texte(300, 360,"Pour valider cette colonne, appuyez sur le bouton Entrée", 'green', 'center',  15, 'text')
    if mode:
        superr_texte(300, 100,"Mode 'contrainte supérieurs à 9' activé", 'blue', 'center',  15, 'text')
        superr_texte(300, 120,"Pour valider cette contrainte, appuyez sur le bouton Entrée", 'blue', 'center',  15, 'text')
    if text[phase] != []:
        if phase == 0:
            superr_texte(300, 400,"Si pour les lignes c'est ok, appuyez sur la flèche de droite", 'red', 'center',  15, 'text')
        else:
            superr_texte(300, 400,"Pour terminer, appuyez sur la flèche de droite", 'red', 'center',  15, 'text')
    superr_texte(300, 485,"En cas d'erreur, le retour arrière peut venir à ta rescousse", 'blue', 'center',  15, 'text')
    
    
    # on rentre une contrainre de la ligne/colonne
    if ty == 'Touche' and touche(ev) in nombre:
        pseudo_text.append(touche(ev))
        if not mode:
            pseudo_text.append("/")
        
    # on separe les contraintes entre elles par un espace
    # ou on passe a l'autre ligne/colonne si on appuie deux fois sur Entree
    elif ty == 'Touche' and touche(ev) == "Return" and pseudo_text != []:
        if pseudo_text[-1] != "/":
            pseudo_text.append("/")
        else:
            del pseudo_text[-1]
            text[phase].append(pseudo_text)
            pseudo_text = []
            index += 1
            
            affiche_ancien = ""
            for i in text[phase][-1]:
                if i != "/":
                    affiche_ancien += str(i) + "-"
            affiche_ancien = affiche_ancien[:-1]
    
    # supprimer avec backspace le dernier element du nom
    elif ty == 'Touche' and touche(ev) == "BackSpace" and pseudo_text != []:
        if pseudo_text[-1] == "/":
            del pseudo_text[-1]
        del pseudo_text[-1]
                
    # on revient a l'ancienne ligne/colonne
    elif ty == 'Touche' and touche(ev) == "Left" and text[phase] != []:
        del text[phase][-1]
        pseudo_text = []
        index -= 1
        
        affiche_ancien = ""
        if text[phase] != []:
            for i in text[phase][-1]:
                if i != "/":
                    affiche_ancien += str(i) + "-"
            affiche_ancien = affiche_ancien[:-1]
        
    # pour creer des contraintes superieurs a 9
    elif ty == 'Touche' and touche(ev) == "space":
        mode = not mode
    
    # quand on valide notre dessin et qu'il n'est pas vide
    elif ty == 'Touche' and touche(ev) == "Right" and text[phase] != []:
    
        if phase == 0:
        
            if pseudo_text != []:
                text[phase].append(pseudo_text)
            pseudo_text = []
            index = 1
            phase +=1
            affiche_ancien = ""
        
        else:
            
            l1 = ""
            l2 = ""
            for colonne_ou_ligne in text:
                index = 0
                for contraintes in colonne_ou_ligne:
                    index += 1
                    pseudo_l = ""
                    for contrainte in contraintes:
                        if contrainte in nombre:
                            pseudo_l += str(contrainte)
                        else:
                            pseudo_l += "-"
                    if text.index(colonne_ou_ligne) == 0:
                        l1 += pseudo_l + "/"
                    else:
                        l2 += pseudo_l + "/"
                        
            l1 = l1[:len(l1)-1] + "\n"
            l2 = l2[:len(l2)-1]
            
            # on l'ecrit dans le dossier "my_pokemon"
            if "creation.txt" in os.listdir("my_pokemon"):
                os.remove("my_pokemon/creation.txt")
            with open("my_pokemon/creation.txt", "w") as f:
                f.write(l1)
                f.write(l2)
            
            
            # boucle de jeu
            
            alors = True
            picross.fonct_jeu()
            
            while picross.jeu:
                picross.fonct_jeu_choix()
        
            picross.fonct_fin()
            picross.jeu = True
            
            while picross.fin:

                alors = picross.fonct_fin_choix()
        
            picross.fin = True
            
            if alors:
            
                ferme_fenetre()
                cree_fenetre(400,400)
                
                image(0, 0, 'image/edimon.pgm', ancrage='nw', tag='')
                image(0, 0, 'image/noir.pgm', ancrage='nw', tag='texte')
                superr_texte(200, 40, "Donnez un nom à votre dessin", 'blue', 'center',  15, 'texte')
                superr_texte(200, 80, "Pour valider appuyez sur Entrée", 'blue', 'center',  12, 'texte')
                superr_texte(200,360, "Sinon pour quitter, appuyez sur Esc", 'blue', 'center',  12, 'texte')
                name = ""
                mega = False
                
                # la boucle pour pouvoir renommer
                while nom:

                    ev_2 = donne_ev()
                    ty_2 = type_ev(ev_2)
                    
                    efface("name")
                    superr_texte(200, 200, str(name), 'yellow', 'center',  30, 'name')
                    if mega:
                        superr_texte(200, 380, "Oh c'est un méga! Mmh je vois je vois...", 'red', 'center',  15, 'name')
                    else:
                        superr_texte(200, 380, "Ton poké est un méga évolué? Si oui appuie sur Espace", 'blue', 'center',  10, 'name')
                    
                    if ty_2 == 'Touche':
                    
                        # supprimer avec backspace le dernier element du nom
                        if touche(ev_2) == "BackSpace" and name != "":
                            name = name[0:len(name)-1]
                            
                        # si c'est un mega
                        if touche(ev_2) == "space":
                            mega = not mega
                        
                        # validation et creation du fichier texte
                        elif touche(ev_2) == "Return" and name != "" and str(name) not in picross.lst_dessin:
                            
                            if mega:
                                os.rename("my_pokemon/creation.txt", "my_pokemon/Mega-"+str(name)+".txt")
                            else:
                                os.rename("my_pokemon/creation.txt", "my_pokemon/"+str(name)+".txt")
                            nom = False
                            edimon = False
                        
                        # pour le renommage, met la majuscule pour le premier caractere si c'est une lettre
                        elif touche(ev_2) in alphabet or touche(ev_2) in nombre:
                            if name == "" and touche(ev_2) in alphabet:
                                name += chr(ord(touche(ev_2))-ord("a")+ord("A"))
                            else:
                                name += touche(ev_2)
                            
                        else:
                            pass
                            
                    else:
                        pass
                        
                    mise_a_jour()
                    
            else:
                os.remove("my_pokemon/creation.txt")
                edimon = False
    
    # pour quitter l'editage
    elif ty == 'Touche' and touche(ev) == "Escape":
        edimon = False
        
    else:
        pass
        
    mise_a_jour()
    
    return edimon, text, pseudo_text, index, phase, mode, affiche_ancien
    
    
def fonct_editeur_fin(editeur, continuer):
    """
    Cette fonction gere les evenements apres d'avoir nommé un dessin,
    si on veut creer un autre dessin ou si on veut retourner a l'ecran titre
    """
    ev = donne_ev()
    ty = type_ev(ev)
    
    # retourner au menu principal
    if ty == 'Touche' and touche(ev) == "Escape":
        editeur = False
        continuer = False
    
    # continuer a editer
    elif ty == 'Touche' and touche(ev) == "Return":
        editeur = True
        continuer = False
        
    else:
        pass
        
    mise_a_jour()
    
    return editeur, continuer
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
#=========================================================================================================================================================================================

# La classe "Logimage" permet d'avoir des variables globales recuperable depuis n'importe où dans le code


class Logimage:

    def __init__(self, case_rouge, croix, matrice_contraintes, boucle_info, lst_dessin, plat, plat_inv, plat_vide, plat_inv_vide,
                 longueur_fenetre_jeu, largeur_fenetre_jeu, t0, tf, max_largeur, max_longueur):
        self.case_rouge = case_rouge
        self.croix = croix
        self.matrice_contraintes = matrice_contraintes
        self.debut = True
        self.jeu = True
        self.boucle_info = boucle_info
        self.programme = True
        self.fin = True
        self.lst_dessin = lst_dessin
        self.dessin = 0
        self.index_dossier = 0
        self.les_dossiers = ["my_picross", "my_pokemon"]
        self.plat = plat
        self.plat_vide = plat_inv_vide
        self.plat_inv = plat_inv
        self.plat_inv_vide = plat_inv_vide
        self.longueur_fenetre_jeu = longueur_fenetre_jeu
        self.largeur_fenetre_jeu = largeur_fenetre_jeu
        self.t0 = t0
        self.tf = tf
        self.max_largeur = max_largeur
        self.max_longueur = max_longueur
        self.curseur_mode = 0
        self.curseur_x = 0
        self.curseur_y = 0
        self.list_direc = {"Right": 1, "Left": -1, "Up": -1, "Down":1}
        self.coul = ["black", "gold"]


    def fonct_ecran_titre(self):
        """
        Elle permet d'afficher l'ecran titre, et d'initialiser des variables importantes au code
        """
        
        pygame.mixer.init()
        pygame.mixer.music.load("music/zelda shrine.mp3")
        pygame.mixer.music.play(loops=-1)
        
        image(0, 0, "image/ecran_titre.pgm", ancrage='nw')
        
        # lst_dessin est une liste contenant tous les fichiers de grilles dans my_picross
        self.lst_dessin = os.listdir(self.les_dossiers[self.index_dossier])
        # on enleve ".txt" pour bien afficher dans le programme
        self.lst_dessin = [file.split(".")[0] for file in self.lst_dessin]
        
        if "suspend.json" in os.listdir():
            with open("suspend.json","r") as f:
                dico = json.load(f)
            self.debut = False
            self.case_rouge = dico["case_rouge"]
            self.index_dossier = dico["dossier"]
            self.croix = dico["croix"]
            self.dessin = dico["dessin"]
            
        elif "creation.txt" in os.listdir("my_pokemon"):
            self.debut = False
        
        else:
        
            # on creer la fenetre et le quadrillage
            for i in range(0,151,20): # lignes horizontales
                ligne(0, i, 680, i, couleur='black', epaisseur=1, tag="jeu")
            for i in range(0,680,20): # lignes verticales
                ligne(i, 0, i, 140, couleur='black', epaisseur=1, tag="jeu")
            rempli([34, 7], element="damier")
            
            # pour afficher le titre "logimage" sur l'ecran titre
            self.case_rouge = [(1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (2, 5), (4, 2), (4, 3), (4, 4), (5, 5), 
                          (6, 4), (6, 3), (6, 2), (5, 1), (8, 2), (8, 3), (8, 4), (9, 5), (10, 5), (11, 4), 
                          (11, 3), (10, 3), (9, 1), (10, 1), (13, 1), (13, 3), (13, 4), (13, 5), (15, 5), 
                          (15, 4), (15, 3), (15, 2), (15, 1), (16, 2), (17, 3), (18, 2), (19, 1), (19, 2), 
                          (19, 3), (19, 4), (19, 5), (21, 5), (21, 4), (21, 3), (21, 2), (22, 1), (23, 2), 
                          (23, 3), (23, 4), (23, 5), (22, 3), (25, 2), (25, 3), (25, 4), (26, 5), (27, 5), 
                          (28, 4), (28, 3), (27, 3), (26, 1), (27, 1), (30, 1), (30, 2), (30, 3), (30, 4), 
                          (30, 5), (31, 5), (32, 5), (31, 3), (31, 1), (32, 1)]
            rempli(self.case_rouge)
            
            # liste pour les croix a placer comme aide
            self.croix = []

            super_texte(340, 165, "Sélectionnez votre niveau avec les flèches, puis Entrée", 'blue', "20", "center", "")
            super_texte(340, 255, "Flèche haut pour l'éditeur", 'steel blue', "16", "center", "")
            super_texte(340, 280, "Flèche bas pour l'éditeur spécial Pokémon Picross", 'steel blue', "16", "center", "")

    
    def son(self, sono):
        current = pygame.mixer.Sound("sound/"+sono)
        current.play()
        while pygame.mixer.get_busy():
            pass
    
    
    def fonct_ecran_titre_choix(self):
        """
        Cette fonction gere les evenements sur l'ecran titre, pour choisir le dessin pour jouer
        ou pour choisir le mode editeur
        """
        
        ev = donne_ev()
        ty = type_ev(ev)

        # affiche le nom du dessin
        efface("dessin")
        if "Mega-" in str(self.lst_dessin[self.dessin]):
            super_texte(340, 205, "Méga-" + str(self.lst_dessin[self.dessin])[5:], 'deep sky blue', "24", "center", "dessin")
        else:
            super_texte(340, 205, str(self.lst_dessin[self.dessin]), 'deep sky blue', "24", "center", "dessin")
        
        # gere les clics sur l'ecran titre, sur le quadrillage
        if (ty == "ClicGauche" or ty == "ClicDroit") and (0 < abscisse(ev) < 680) and (0 < ordonnee(ev) < 140):

            case = pixel_en_coordonne(abscisse(ev),ordonnee(ev))
            c1, c2 = pour_carre(abscisse(ev),ordonnee(ev))
            
            if ty == "ClicGauche":
                if case not in self.case_rouge:
                    rectangle(c1+1, c2+1, c1+19, c2+19, couleur='white', remplissage='red', epaisseur=1, tag='')
                    self.case_rouge.append(case)
                    
                else:
                    rempli([c1/20, c2/20], element="damier_1")
                    self.case_rouge.remove(case)
                    
                if case in self.croix:
                    self.croix.remove(case)
                    
            else:
                if case not in self.croix:
                    rempli([c1/20, c2/20], element="damier_1")
                    ligne(c1+1, c2+1, c1+20, c2+20, couleur='black', epaisseur=2, tag='')
                    ligne(c1+1, c2+20, c1+20, c2+1, couleur='black', epaisseur=2, tag='')
                    self.croix.append(case)
                    
                else:
                    rempli([c1/20, c2/20], element="damier_1")
                    self.croix.remove(case)
                    
                if case in self.case_rouge:
                    self.case_rouge.remove(case)
                    
        # permet de changer le dessin auquel on veut y jouer
        elif ty == 'Touche' and touche(ev) == "Right":
            self.son("curseur.wav")
            self.dessin = (self.dessin + 1) % len(self.lst_dessin)
            
        elif ty == 'Touche' and touche(ev) == "Left":
            self.son("curseur.wav")
            self.dessin = (self.dessin - 1) % len(self.lst_dessin)
            
        # permet de changer de dossier de dessin
        elif ty == 'Touche' and touche(ev) == "space":
            self.son("dossier.wav")
            self.index_dossier = abs(self.index_dossier-1)
            self.dessin = 0
            # lst_dessin est une liste contenant tous les fichiers de grilles dans 'my_picross' ou 'my_pokemon'
            self.lst_dessin = os.listdir(self.les_dossiers[self.index_dossier])
            # on enleve ".txt" pour bien afficher dans le programme
            self.lst_dessin = [file.split(".")[0] for file in self.lst_dessin]
            
        # editeur dessin
        elif ty == 'Touche' and touche(ev) == "Up":
            editeur = True
            editeur, nom, case_rouge, direc, alphabet, nombre, plat, plat_inv, secours = fonct_editeur()
            while editeur:
                nom, case_rouge, direc, alphabet, nombre, plat, plat_inv, secours, editeur = fonct_editeur_choix(nom, 
                case_rouge, direc, alphabet, nombre, plat, plat_inv, secours, editeur)
            ferme_fenetre()
            cree_fenetre(680,300)
            self.fonct_ecran_titre()
            
        # editeur pokemon
        elif ty == 'Touche' and touche(ev) == "Down":
            edimon = True
            editeur, nom, alphabet, nombre = fonct_edimon()
            text = [[],[]]
            pseudo_text = []
            index = 1
            phase = 0
            mode = False
            affiche_ancien = ""
            while edimon:
                edimon, text, pseudo_text, index, phase, mode, affiche_ancien = fonct_edimon_choix(nom, 
                 alphabet, nombre, edimon, text, pseudo_text, index, phase, mode, affiche_ancien)
            ferme_fenetre()
            cree_fenetre(680,300)
            self.fonct_ecran_titre()
            
        # pour valider le dessin pour jouer
        elif ty == 'Touche' and touche(ev) == "Return":
            self.son("valider.wav")
            self.debut = False
            
        else:
            pass
            
        mise_a_jour()
        
        
    def fonct_jeu(self):
        """
        On prepare le jeu:
        -on charge le fichier texte du dessin
        -on va creer un plateau sous forme de matrice longueur x largeur de la taille du dessin
        -on va creer le quadrillage a la taille du dessin
        -on reinitialise certaines variables pour qu'elles soient operationelles pour le jeu
        """
            
        # On charge le fichier, contraines_str est une liste contenant 
        # les contraintes horizontale et verticale du dessin qui sont en str
        # cela depend s'il y a une partie interrompue et du dossier du dessin
        if "suspend.json" not in os.listdir():
            # Liste qui contient les coordonnées des cases coloriées,
            # quand une case est reappuyée, on enlève sa coordonnée
            self.case_rouge = []
            self.croix = []
            
        else:
            with open("suspend.json","r") as f:
                dico = json.load(f)
            self.case_rouge = dico["case_rouge"]
            self.croix = dico["croix"]
            self.dessin = dico["dessin"]
            self.index_dossier = dico["dossier"]
            self.lst_dessin = os.listdir(self.les_dossiers[self.index_dossier])
            self.lst_dessin = [file.split(".")[0] for file in self.lst_dessin]
        
        
        # si on est dans l'editeur pokemon
        if "creation.txt" in os.listdir("my_pokemon"):
            print("CREATION.TXT EN COURS")
            with open("my_pokemon/creation.txt","r") as f:
                contraines_str = f.readlines()
            
        else:
            with open(self.les_dossiers[self.index_dossier]+"/"+(self.lst_dessin[self.dessin])+".txt","r") as f:
                contraines_str = f.readlines()
        
        # matrices_contraintes est une matrice contenant les contraintes horizantales et verticales
        # pour chaques colonnes la/les contrainte(s) sont dans une liste, de meme pour chaques lignes
        self.matrice_contraintes = contraintes(contraines_str)
        
        print(self.matrice_contraintes)
        
        """
        plat represente le plateau par une matrice de taille longueur x largeur du dessin,
        et chaques cases sont symbolysés par [0], [1] si la case est cochée
        cela permet d'avoir l'etat du plateau fait par le joueur, on exploitera ce plateau pour
        connaitre les contraintes actuels du plateau et les comparer avec matrices_contraintes
        plateau_inverse permet de mieux connaitre les contraintes actuelles pour les colonnes
        """
        self.plat = plateau(self.matrice_contraintes[0],self.matrice_contraintes[1])
        self.plat_vide = deepcopy(self.plat)
        self.plat_inv = plateau_inverse(self.matrice_contraintes[0],self.matrice_contraintes[1])
        self.plat_inv_vide = deepcopy(self.plat_inv)
        
        # on cree la nouvelle fenetre sur mesure
        ferme_fenetre()
        for i in self.matrice_contraintes[0]:
            if len(i) > self.max_longueur:
                self.max_longueur = len(i)
        for i in self.matrice_contraintes[1]:
            if len(i) > self.max_largeur:
                self.max_largeur = len(i)
        if self.max_largeur < 2:
            self.max_largeur = 2
        self.longueur_fenetre_jeu = len(self.matrice_contraintes[1])*20 + self.max_longueur*20+20
        self.largeur_fenetre_jeu = len(self.matrice_contraintes[0])*20 + self.max_largeur*20+20
        if self.longueur_fenetre_jeu < 200:
            self.longueur_fenetre_jeu = 200
        cree_fenetre(self.longueur_fenetre_jeu, self.largeur_fenetre_jeu)
        image(0, 0, 'image/jeu.pgm', ancrage='nw', tag='texte')

        # on creer les quadrillages sur mesure
        for i in range(0,len(self.matrice_contraintes[0])*20+1,20):
            ligne(0, i, len(self.matrice_contraintes[1])*20+1, i, couleur='black', epaisseur=1, tag="jeu") # lignes horizontales
        for i in range(0,len(self.matrice_contraintes[1])*20+1,20):
            ligne(i, 0, i, len(self.matrice_contraintes[0])*20+1, couleur='black', epaisseur=1, tag="jeu") # lignes verticales
        
        # on creer les contraines
        for i in range(0, len(self.plat)):
            texte(len(self.plat_inv)*20 + 10, (i * 20), self.matrice_contraintes[0][i], couleur="red", taille=16, tag="cont")
        for i in range (0,len(self.plat_inv)):
            if len(self.matrice_contraintes[1][i]) > 1:
                for j in range(len(self.matrice_contraintes[1][i])):
                    texte((i * 20) + 10, len(self.plat)*20 + 16 + j*20, self.matrice_contraintes[1][i][j], couleur="blue", ancrage='center', taille=16, tag="cont")
            else:
                texte((i * 20) + 10, len(self.plat)*20 + 16, self.matrice_contraintes[1][i], couleur="blue", ancrage='center', taille=16, tag="cont")

        rectangle(self.longueur_fenetre_jeu-30, self.largeur_fenetre_jeu-30,
                  self.longueur_fenetre_jeu, self.largeur_fenetre_jeu,
                  couleur='green', remplissage='lightgreen', epaisseur=1, tag='cont')
        texte(self.longueur_fenetre_jeu-15, self.largeur_fenetre_jeu-15,
              "i", couleur='green', ancrage='center', police='Arial', taille=24, tag='cont')
        
        if "suspend.json" in os.listdir():
            rempli(self.case_rouge)
            rempli(self.croix, "croix")
            os.remove("suspend.json")
            
        self.curseur_x = 0
        self.curseur_y = 0
        self.curseur_mode = 0
        
        rempli([len(self.plat_inv), len(self.plat)], element='damier')
    
    
    def affiche_curseur(self):
        # affichage du curseur
        if self.curseur_mode == 1:
            self.coul = ["black", "gold"]
        elif self.curseur_mode == 2:
            self.coul = ["gold", "gold"]
        elif self.curseur_mode == 3:
            self.coul = ["black", "black"]
        else:
            self.coul = ["gray30", "gray30"]
        x, y = self.curseur_x*20+10 , self.curseur_y*20+10
        cercle(x, y, 12, couleur=self.coul[0], epaisseur=4, tag="c")
        cercle(x, y, 1, couleur=self.coul[0], epaisseur=2, tag="c")
        ligne(x-20, y, x-5, y, couleur=self.coul[1], epaisseur=2, tag="c")
        ligne(x+20, y, x+5, y, couleur=self.coul[1], epaisseur=2, tag="c")
        ligne(x, y-20, x, y-5, couleur=self.coul[1], epaisseur=2, tag="c")
        ligne(x, y+20, x, y+5, couleur=self.coul[1], epaisseur=2, tag="c")
        
        
    def move(self, type, ou):
        # affichage du curseur
        if type == "x":
            handicap_x = 1
            handicap_y = 0
        else:
            handicap_x = 0
            handicap_y = 1
        if ou > 0:
            lst = [1, 21, 1]
        else:
            lst = [1, -21, -1]
        for i in range(lst[0], lst[1], lst[2]):
            efface("c")
            x, y = (self.curseur_x*20+10)+(handicap_x*i) , (self.curseur_y*20+10)+(handicap_y*i)
            cercle(x, y, 12, couleur=self.coul[0], epaisseur=4, tag="c")
            cercle(x, y, 1, couleur=self.coul[0], epaisseur=2, tag="c")
            ligne(x-20, y, x-5, y, couleur=self.coul[1], epaisseur=2, tag="c")
            ligne(x+20, y, x+5, y, couleur=self.coul[1], epaisseur=2, tag="c")
            ligne(x, y-20, x, y-5, couleur=self.coul[1], epaisseur=2, tag="c")
            ligne(x, y+20, x, y+5, couleur=self.coul[1], epaisseur=2, tag="c")
            attente(0)
        
        
    def mouvement(self, direction):
        # gere les directions du curseur
        if direction == "Right" or direction == "Left":
            self.move("x", self.list_direc[direction])
            self.curseur_x = self.curseur_x + self.list_direc[direction]
        elif direction == "Up" or direction == "Down":
            self.move("y", self.list_direc[direction])
            self.curseur_y = self.curseur_y + self.list_direc[direction]
        else:
            pass
            
            
    def torique(self):
        # modulo permet à l'arène d'être torique
        # lors du placement des bateaux, jeu = False
        # chacun place les bateaux sur son arene dans un premier temps
        # puis pendant le jeu, jeu = True, et notre curseur se trouve sur l'arène adverse
        self.curseur_x = self.curseur_x % (len(self.plat_inv))
        self.curseur_y = self.curseur_y % (len(self.plat))
        
        
    def restreint(self, x, y):
        # après qu'on ait choisi l'endoit où placé le bateau, il faut redéplacé le curseur pour choisir sa longueur
        # cette fonction va permettre d'éviter au curseur d'aller aux bords, de faire des diagonales ou de faire des bateaux trop longs
        if (not (-1 < x < len(self.plat_inv))) or (not (-1 < y < len(self.plat_inv))) or (self.curseur_x != x and self.curseur_y != y):
            self.curseur_x = x
            self.curseur_y = y
        
        
    def place_case(self, ty, touche, abscisse, ordonnee):
        # la fonction qui gere lorsqu'on place une case ou une croix
        if self.curseur_mode:
            x, y = self.curseur_x, self.curseur_y
            case = [x, y]
            c1, c2 = self.curseur_x * 20, self.curseur_y * 20
        else:
            case = pixel_en_coordonne(abscisse,ordonnee)
            x, y = case
            c1, c2 = pour_carre(abscisse,ordonnee)
        
        # pour cocher ou decocher une case
        if touche == "Return" or ty == "ClicGauche":
        
            if case not in self.case_rouge:
                rectangle(c1+1, c2+1, c1+19, c2+19, couleur='white', remplissage='red', epaisseur=1, tag='')
                self.case_rouge.append(case)
                self.plat[y][x] = 1
                self.plat_inv[x][y] = 1
                
            elif self.curseur_mode != 2:
                rempli([x, y], element='damier_1')
                self.case_rouge.remove(case)
                self.plat[y][x] = 0
                self.plat_inv[x][y] = 0
                
            if case in self.croix:
                self.croix.remove(case)
        
        #pour placer une croix aide ou l'enlever
        elif touche == "BackSpace" or ty == "ClicDroit":
        
            if case not in self.croix:
                rempli([x, y], element='damier_1')
                ligne(c1+1, c2+1, c1+20, c2+20, couleur='black', epaisseur=2, tag='cro')
                ligne(c1+1, c2+20, c1+20, c2+1, couleur='black', epaisseur=2, tag='cro')
                self.croix.append(case)
                
            elif self.curseur_mode != 3:
                rempli([x, y], element='damier_1')
                self.croix.remove(case)
                
            if case in self.case_rouge:
                self.plat[y][x] = 0
                self.plat_inv[x][y] = 0
                self.case_rouge.remove(case)
                
        else:
            if case in self.croix:
                self.croix.remove(case)
            elif case in self.case_rouge:
                self.case_rouge.remove(case)
                self.plat[y][x] = 0
                self.plat_inv[x][y] = 0
            rempli([x, y], element='damier_1')
        
        # contraintes actuels horizontales
        ligne_etat = affiche_contrainte(self.plat)

        # contraintes actuels verticales
        colonne_etat = affiche_contrainte(self.plat_inv)
        
        # on verifie si les contraintes actuels du plateau correspondent aux contraintes du dessin
        self.verifie(ligne_etat, colonne_etat)
        
        
    def fonct_jeu_choix(self):
        """
        Cette fonction gere les evenements pendant le jeu, quand on coche ou on place des croix,
        et verifie si on a reussi le puzzle ou pas
        """
        
        ev = donne_ev()
        ty = type_ev(ev)
        
        if self.curseur_mode != 0:
        
            efface("c")
            self.affiche_curseur()
            
            if ty == 'Touche' and touche(ev) in self.list_direc:
                self.mouvement(touche(ev))
                self.torique()
                if self.curseur_mode > 1:
                    curseur_dico = {2:"Return",3:"BackSpace",4:"erase"}
                    self.place_case(ty, curseur_dico[self.curseur_mode], 0, 0)
                
            elif ty == 'Touche' and (touche(ev) == "Return" or touche(ev) == "BackSpace"):
                self.place_case(ty, touche(ev), 0, 0)
        
        if ty == 'Touche' and touche(ev) in ["0","1","2","3","4"]:
            if touche(ev) == "0":
                self.curseur_mode = 0
            elif self.curseur_mode == int(touche(ev)):
                self.curseur_mode = 1
            else:
                self.curseur_mode = int(touche(ev))
                if self.curseur_mode > 1:
                    curseur_dico = {2:"Return",3:"BackSpace",4:"erase"}
                    self.place_case(ty, curseur_dico[self.curseur_mode], 0, 0)
            efface("c")
        
        elif not self.curseur_mode and (ty == "ClicGauche" or ty == "ClicDroit") and (0 < abscisse(ev) < len(self.matrice_contraintes[1])*20) and (0 < ordonnee(ev) < len(self.matrice_contraintes[0])*20):
            self.place_case(ty, touche(ev), abscisse(ev), ordonnee(ev))
        
        # les infos
        elif ty == 'Touche' and touche(ev) == "i":
            self.boucle_info = True
            fonct_info()
            while self.boucle_info:
                self.fonct_info_choix()
        
        # LE SOLVEUR!!!
        elif ty == 'Touche' and touche(ev) == "s":
            self.son("recover.wav")
            self.t0 = time.time()
            rempli(self.case_rouge, "blanc")
            rempli(self.croix, "blanc")
            self.case_rouge = []
            self.croix = []
            print("SOLVEUR GO!")
            self.solveur(0,0, ["debut"], [], [], False)
        
        # pour mettre le jeu en suspend
        elif ty == 'Touche' and touche(ev) == "q":
            # open("my_picross/{}.json".format("suspend"), "w+") as f:
            with open("suspend.json".format("suspend"), "w+") as f:
                dico = {
                "case_rouge": self.case_rouge,
                "croix" : self.croix,
                "dessin": self.dessin,
                "dossier": self.index_dossier}
                json.dump(dico, f)
            ferme_fenetre()
            
        else:
            pass
            
        mise_a_jour()



    def fonct_info_choix(self):
        """
        Attend que la touche echap est entree pour quitter la fenetre
        Elle permet aussi de retablir le plateau comme avant
        """
        ev = donne_ev()
        ty = type_ev(ev)
        
        if ty == 'Touche' and touche(ev) == "Escape":
            self.boucle_info = False
            # on cree la nouvelle fenetre sur mesure
            ferme_fenetre()
            self.longueur_fenetre_jeu = len(self.matrice_contraintes[1])*20 + self.max_longueur*20+20
            self.largeur_fenetre_jeu = len(self.matrice_contraintes[0])*20 + self.max_largeur*20+20
            cree_fenetre(self.longueur_fenetre_jeu, self.largeur_fenetre_jeu)
            image(0, 0, 'image/jeu.pgm', ancrage='nw', tag='texte')

            # on creer les quadrillages sur mesure
            for i in range(0,len(self.matrice_contraintes[0])*20+1,20):
                ligne(0, i, len(self.matrice_contraintes[1])*20+1, i, couleur='black', epaisseur=1, tag="jeu") # lignes horizontales
            for i in range(0,len(self.matrice_contraintes[1])*20+1,20):
                ligne(i, 0, i, len(self.matrice_contraintes[0])*20+1, couleur='black', epaisseur=1, tag="jeu") # lignes verticales
                
            for i in range(0, len(self.plat)):
                texte(len(self.plat_inv)*20 + 10, (i * 20), self.matrice_contraintes[0][i], couleur="red", taille=16, tag="cont")
            for i in range (0,len(self.plat_inv)):
                if len(self.matrice_contraintes[1][i]) > 1:
                    for j in range(len(self.matrice_contraintes[1][i])):
                        texte((i * 20) + 10, len(self.plat)*20 + 16 + j*20, self.matrice_contraintes[1][i][j], couleur="blue", ancrage='center', taille=16, tag="cont")
                else:
                    texte((i * 20) + 10, len(self.plat)*20 + 16, self.matrice_contraintes[1][i], couleur="blue", ancrage='center', taille=16, tag="cont")
                    
            rempli(self.case_rouge)
            rempli(self.croix, "croix")
            
            rectangle(self.longueur_fenetre_jeu-30, self.largeur_fenetre_jeu-30,
                      self.longueur_fenetre_jeu, self.largeur_fenetre_jeu,
                      couleur='green', remplissage='lightgreen', epaisseur=1, tag='cont')
            texte(self.longueur_fenetre_jeu-15, self.largeur_fenetre_jeu-15,
                  "i", couleur='green', ancrage='center', police='Arial', taille=24, tag='cont')
                  
            rempli([len(self.plat_inv), len(self.plat)], element='damier')

        else:
            pass
            
        mise_a_jour()
                
                
                
    def listen_fin(self,t):	#Joue music en entier
        pygame.mixer.music.stop()
        pygame.mixer.music.load(t)
        pygame.mixer.music.play()
        pygame.mixer.music.fadeout(10000)
        lst_bravo = os.listdir('bravo')
        while pygame.mixer.music.get_busy():
            for j in range(1,16):
                efface("bravo")
                image(self.longueur_fenetre_jeu/2, self.largeur_fenetre_jeu-(self.max_largeur*15), "bravo/bravo"+str(j)+".gif", ancrage='center', tag="bravo")
                attente(0.05)
                
                
                
    def fonct_fin(self):
        """
        Cette fonction permet d'afficher l'interface
        quand on a reussi
        """
        efface("cont")
        efface("cro")
        efface("c")
        
        if self.jeu == False:
            self.tf = time.time()
            print(self.tf-self.t0)
            self.listen_fin("music/mario rpg win.mp3")

        else:
            texte(12,12,"Ce puzzle est impossible?!", couleur='dark', ancrage='nw', police='Arial', taille=20)
        attente(1)
        efface("bravo")
        if "creation.txt" in os.listdir("my_pokemon"):
            image(self.longueur_fenetre_jeu/2, self.largeur_fenetre_jeu-(self.max_largeur*15), "bravo/fin_pokemon.gif", ancrage='center', tag="bravo")
        else:
            image(self.longueur_fenetre_jeu/2, self.largeur_fenetre_jeu-(self.max_largeur*15), "bravo/fin.gif", ancrage='center', tag="bravo")
        
        
        
    def fonct_fin_choix(self):
        """
        Cette fonction gere les evenements pendant la fin,
        si on veut faire un autre dessin ou si on veut arreter
        """
        ev = donne_ev()
        ty = type_ev(ev)
        
        if "creation.txt" not in os.listdir("my_pokemon"):
        
            if ty == 'Touche' and touche(ev) == "Escape":
                self.programme = False
                self.fin = False
                
            elif ty == 'Touche' and touche(ev) == "Return":
                self.fin = False
                self.debut = True
                self.jeu = True
                ferme_fenetre()
                cree_fenetre(680,300)
            
        elif "creation.txt" in os.listdir("my_pokemon"):
        
            if ty == 'Touche' and touche(ev) == "Escape":
                self.fin = False
                return False
                
            elif ty == 'Touche' and touche(ev) == "Return":
                self.fin = False
                return True
            
        else:
            pass
            
        mise_a_jour()


        
        
    def solveur(self, x, y, lst, e_plat, e_plat_inv, decal):
        """
        La fonction permet de resoudre un logimage de facon recursive avec les contraintes du dessin.
        x est la coordonnée correspondante aux lignes, y pour les colonnes.
        lst est la matrice avec les contraintes, les blocs placés ont leur contrainte enlevés.
        e_plat est une copie du plateau avec le dernier bloc placé.
        e_plat_inv est une copie du plateau retourné (pour mieux gerer les contraintes des colonnes) avec le dernier bloc placé
        """
        
        delai = 0
        
        # on regarde si le dessin correspond pour les contraintes verticales et horizontales
        # quand jeu est False, ca veut dire que le puzzle est fini
        ligne_etat = affiche_contrainte(e_plat)
        colonne_etat = affiche_contrainte(e_plat_inv)
        self.verifie(ligne_etat,colonne_etat)
        
        # ces deux lignes sont a mettre a chaque fois qu'on appelle la fonction solveur,
        # elles pourront intervenir dès que le dessin est fini
        if self.jeu == False:
            return False
            
        # au debut du solveur, on definit les copies des plateaux et des contraintes
        elif lst == ["debut"]:
                lst = deepcopy(self.matrice_contraintes[0])
                e_plat = []
                e_plat_inv = []
                e_plat[:] = self.plat_vide
                e_plat_inv[:] = self.plat_inv_vide
                
        # lst, la liste des contraintes dont les blocs placés ont leur contrainte enlevée est complexe
        # dans certains cas le solveur retourne en arriere sur les anciens blocs, il faut qu'on sache sur la
        # ligne quelles contraintes horizontales sont deja placées, le bout de code s'en charge, en comparant
        # l'etat du plateau et des contraintes existantes
        else:
            lst = deepcopy(self.matrice_contraintes[0])
            a = affiche_cont(e_plat[x])[0]
            if len(a) == 1 and a[0] != 0:
                lst[x].remove(a[0])
            elif len(a) > 1:
                for i in a:
                    lst[x].remove(i)
                
        # si etape_2 devient True, on va aller dans l'etape deux, qui gere lorsqu'il a etait impossible
        # de placer les blocs dans certaines conditions
        etape_2 = False
        
        # le bloc a placer, de longueur k
        k = lst[x][0]
        
        # pour chaque case que prend la taille du bloc, on regarde si...
        for i in range(k):
        
            # ...la position (x, y) ne depasse pas les limites du plateau
            if y > len(self.plat[0]):
                etape_2 = "stop"
                break
                
            # ...il est encore possible de placer les blocs restants depuis la position (x,y)
            if (y + sum(lst[x]) + len(lst[x]) -1) > len(self.plat[0]):
                etape_2 = "stop"
                break
                
            # sinon on colorie la case, dans les pseudo plateaux
            rempli([(y+i, x)])
            e_plat[x][y+i] = 1
            e_plat_inv[y+i][x] = 1
            attente(delai)
            
            # si le bloc vient d'etre decale, et en decalant ce dernier, les contraintes verticales derrieres deviennent possibles
            if decal == True and i == 0:
                for j in range(y-1,-1,-1):
                    if e_plat[x][j] == 0 and (affiche_cont(e_plat_inv[j])[0] != self.matrice_contraintes[1][j]):
                        test_plat_inv = deepcopy(e_plat_inv[j])
                        if x+1 < len(e_plat_inv[0]):
                            test_plat_inv[x+1] = 1
                            if completable(self.matrice_contraintes[1][j], test_plat_inv) != True:
                                rempli([y+i, x], element='damier_1')
                                e_plat[x][y+i] = 0
                                e_plat_inv[y+i][x] = 0
                                attente(delai)
                                decal = False
                                return None
            
            # de plus on verifie si on satisfait les contraintes verticales
            if completable(self.matrice_contraintes[1][y+i], e_plat_inv[y+i]) == False:
                etape_2 = True
                break
            elif completable(self.matrice_contraintes[1][y+i], e_plat_inv[y+i]) == "stop":
                etape_2 = "stop"
                for i in range(k):
                    rempli([y+i, x], element='damier_1')
                    e_plat[x][y+i] = 0
                    e_plat_inv[y+i][x] = 0
                    attente(delai)
                break
                
        # si le bloc a bien ete placé (pour l'instant c'est pas sur a 100%), on regarde s'il
        # reste des blocs a placer, si oui la liste de containtes lst se voit retirer la contrainte
        # concernant le bloc placé, sinon on passe sur la ligne du dessous
        if not etape_2:
            del(lst[x][0])
            if lst[x] != []:
                self.jeu = self.solveur(x, y + k + 1, lst, e_plat, e_plat_inv, False)
            else:
                self.jeu = self.solveur(x + 1, 0, lst, e_plat, e_plat_inv, False)
                
        if self.jeu == False:
            return False
                
        # etape deux c'est par ici! quand le bloc a placer est impossible depuis la case (x,y)
        
        # le fait de ne pas pouvoir remplir le reste des blocs sur la ligne est un probleme important
        # il faut quitter la fonction solveur où on etait, pour qu'on puisse passer a l'etape deux de
        # la fonction solveur precedente
        elif etape_2 == "stop":
            return None
            
        # etape_2 == True
        else:
            # on efface ce qu'on a colorié en tentant de placer le bloc depuis la case (x,y)
            for i in range(k):
                rempli([y+i, x], element='damier_1')
                e_plat[x][y+i] = 0
                e_plat_inv[y+i][x] = 0
                attente(delai)
            e_plat[:] = self.plat_vide
            e_plat_inv[:] = self.plat_inv_vide
            # tentons cette fois depuis la case (x, y+1), la case a gauche
            self.jeu = self.solveur(x, y+1, lst, e_plat, e_plat_inv, True)
            if self.jeu == False:
                return False

            
        
        
    def verifie(self, a, b):
        """
        En prenant comme paramètres les contraintes actuels et les contraintes de l'image qu'on
        cherche à trouver, on regarde si elles sont identiques, si oui, on retourne False.
        """
        if self.matrice_contraintes[0] == a and self.matrice_contraintes[1] == b:
            self.jeu = False 
        else:
            self.jeu = True
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
#=========================================================================================================================================================================================
    
# programme principal

cree_fenetre(680,300)
picross = Logimage([], [], [], True, [], [], [], [], [], 0, 0, 0, 0, 0, 0)

while picross.programme:

    picross.fonct_ecran_titre()
    
    while picross.debut:
    
        picross.fonct_ecran_titre_choix()
    
    picross.fonct_jeu()
            
    while picross.jeu:

        picross.fonct_jeu_choix()
        
    picross.fonct_fin()
        
    while picross.fin:

        picross.fonct_fin_choix()
        
    picross.fin = True
        
ferme_fenetre()