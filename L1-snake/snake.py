from upemtk import *
from time import sleep
from random import randint
import sys

log = open("voir.txt","w")
sys.stderr = log


# dimensions du jeu
taille_case = 15
largeur_plateau = 40  # en nombre de cases
hauteur_plateau = 30  # en nombre de cases


def case_vers_pixel(case):
    """
	Fonction recevant les coordonnées d'une case du plateau sous la 
	forme d'un couple d'entiers (ligne, colonne) et renvoyant les 
	coordonnées du pixel se trouvant au centre de cette case. Ce calcul 
	prend en compte la taille de chaque case, donnée par la variable 
	globale taille_case.
    """
    i, j = case
    return (i + .5) * taille_case, (j + .5) * taille_case

def pomme(apparition,menu,rock,a,b,a2,b2):
    global ap
    global bp
    if apparition == 0:
        ap = randint (0,39)
        bp = randint (0,29)
        if menu != 3:
            while ap == a and bp == b:
                ap = randint (0,39)
                bp = randint (0,29)
        else:
            while ap == a2 and bp == b2 and ap == a and bp == b:
                ap = randint (0,39)
                bp = randint (0,29)
        apparition = 1
    # on efface pour le clignotement
    efface("pomm")
    if menu == 4 and rock == 0:
        affiche_roches(ap, bp)
    elif menu != 4 or rspe == 1:
        affiche_pommes(ap, bp, c)
    return ap, bp, apparition

def affiche_pommes(ap, bp, c):
    x, y = case_vers_pixel((ap, bp))
    rectangle(x-2, y-taille_case*.4, x+2, y-taille_case*.7,
                couleur='darkgreen', remplissage='darkgreen', tag='pomm')
    if c%2 == 0:
        cercle(x, y, taille_case/2,
            couleur='orange', remplissage='yellow', tag='pomm')
    else:
        cercle(x, y, taille_case/2,
            couleur='yellow', remplissage='white', tag='pomm')
            
def aff_roch(menu, w, atr, ap, bp, c, frequencer):
    global ar
    global br
    global lar
    global lbr
    if menu == 1:
        frequencer = w + 2
    elif menu == 2:
        frequencer = 1
    elif menu == 3:
        frequencer = 0
    else:
        frequencer = w
    if atr == 0 and w > 0:
        for i in range (frequencer):
            ar = randint (0,39)
            br = randint (0,29)
            while a - 4 < ar < a + 4 and b - 4 < br < b + 4 and ar == ap and br == bp:
                ar = randint (0,39)
                br = randint (0,29)
            lar.append(ar)
            lbr.append(br)
            affiche_rochesmortels(ar, br,menu)
            atr = 1
    if menu == 1 and c % 100 == 0:
        efface("rockr")
        atr = 0
        lar = []
        lbr = []
    return ar, br, atr, frequencer
            
def affiche_roches(ap, bp):
    x, y = case_vers_pixel((ap, bp))
    cercle(x, y, taille_case/2, couleur='black', remplissage='black', tag='cassable')
    return x, y
    
def affiche_rochesmortels(ap, bp,menu):
    x, y = case_vers_pixel((ap, bp))
    if menu == 1 or menu == 4:
        cercle(x, y, taille_case/2, couleur='black', remplissage='black', tag='rockr')
    else:
        rectangle(x-2, y-taille_case*.4, x+2, y-taille_case*.7,
                couleur='darkgreen', remplissage='darkgreen', tag='rockr')
        cercle(x, y, taille_case/2,
            couleur='orange', remplissage='yellow', tag='rockr')
    return x, y

def affiche_serpent(a,b):
    x, y = case_vers_pixel((a, b))
    cercle(x, y, taille_case/2 + 1,
           couleur='darkgreen', remplissage='green', tag='serp')
    polygone([(x-6,y),(x,y+6),(x+6,y),(x,y-6)],
             couleur='pink', remplissage='pink', tag='serp')
             
def affiche_serpent2(a,b):
    x, y = case_vers_pixel((a, b))
    cercle(x, y, taille_case/2 + 1,
           couleur='darkblue', remplissage='blue', tag='serp')
    polygone([(x-6,y),(x,y+6),(x+6,y),(x,y-6)],
             couleur='orange', remplissage='orange', tag='serp')
             
def bord(a,b):
    if (not (-1 < a < 40) or not (-1 < b < 30)) and menu !=3:
        return a, b, False
    elif menu == 3:
        return a % 40, b % 30, True
    else:
        return a, b, True
        
def aff_serpentt(w):
    if w >= 1:
        for i in range(w):
            la[i+1], lb[i+1], jeu = bord(la[i+1],lb[i+1])
            affiche_serpentt(la[i+1],lb[i+1])

def affiche_serpentt(a,b):
    x, y = case_vers_pixel((a, b))
    cercle(x, y, taille_case/2 + 1,
           couleur='darkgreen', remplissage='green', tag='serp')
           
def affiche_serpentt2(a,b):
    x, y = case_vers_pixel((a, b))
    cercle(x, y, taille_case/2 + 1,
           couleur='darkblue', remplissage='blue', tag='serp')

def change_direction(direction, touche, u):
    if touche == 'Up' and u != 1:
        u = 2
        return "haut", u
    elif touche == 'Down' and u != 2:
        u = 1
        return "bas", u
    elif touche == 'Right' and u != 3:
        u = 4
        return "droite", u
    elif touche == 'Left' and u != 4:
        u = 3
        return "gauche", u
    return direction, u
    
def change_direction2(direction, touche, u):
    if touche == 's' and u != 1:
        u = 2
        return "haut", u
    elif touche == 'x' and u != 2:
        u = 1
        return "bas", u
    elif touche == 'c' and u != 3:
        u = 4
        return "droite", u
    elif touche == 'z' and u != 4:
        u = 3
        return "gauche", u
    return direction, u
    
def entre_choque(menu, w, a, b, w2, a2, b2):
    for i in range(w):
        if a == la[i+1] and b == lb[i+1]:
            print("AAAAAAAAAAA")
            return 2, False
        else:
            return 9, True
    if menu == 3:
        for i in range(w2):
            if a == la2[i+1] and b == lb2[i+1]:
                return 2, False
            else:
                return 9, True
                
        for i in range(w2):
            if a2 == la2[i+1] and b2 == lb2[i+1]:
                return 1, False
            else:
                return 9, True
                
        for i in range(w):
            if a2 == la[i+1] and b2 == lb[i+1]:
                return 0, False
            else:
                return 9, True
    return 9, True
                
    if menu == 3:
        if a == a2 and b == b2:
            return 0, False
    



# programme principal

programme = True

if __name__ == "__main__":

    cree_fenetre(taille_case * largeur_plateau, taille_case * hauteur_plateau)
    
    while programme:
    
        # initialisation du jeu
        
        mode = True
        regle = True
        jeu = True
                    
        framerate = 10    # taux de rafraîchissement du jeu en images/s

        # (a,b) (a2,b2) coordonnées respectifs du serpent 1 et 2 
        a = 0
        b = 0
        a2 = 39
        b2 = 29
        apparition = 0
        direction = "debut"
        direction2 = "debut"
        # liste des anciennes coordonnées du serpent
        la = []
        lb = []
        la2 = []
        lb2 = []
        # nombre de tetes supplementaires / pommes mangees 
        w = 0
        w2 = 0
        uturn = 4
        uturn2 = 3
        efface_tout()
        # nombre de frames dans la partie, permettant de faire clignoter la pomme
        c = 0
        # variable permettant de determiner le vainqueur dans le mode 3
        v = 0
        # dans le mode 4 pour savoir l'etat du rocher
        rock = 0
        lrock = ["grey", "brown", "pink"]
        rspe = 0
        # les rochers mortels aparaissent si cette variable est egale a 0:
        atr = 0
        lar = []
        lbr = []
        ar = 6
        br = 6
        
        
        efface_tout()
    
        image(0, 0, 'debut.pgm', ancrage='nw')
        
        texte(300, 30, "DIAMOND SNAKE", couleur = "red",
              ancrage = "n", taille = 40, police = "berlin sans fb demi")
              
        texte(300, 140, "Clic gauche sur le mode de jeu souhaité!", couleur = "black",
              ancrage = "n", police = "berlin sans fb demi", taille = '18')
              
        rectangle(20, 186, 210, 286, remplissage='green', epaisseur=0)
        
        texte(20, 230, "         1\n L'original", ancrage = "w", 
        taille = 30, police = "berlin sans fb demi", couleur = "light green")
        
        rectangle(20, 326, 210, 426, remplissage='pink', epaisseur=0)
        
        texte(22, 370, "         3\n Mode Duo", ancrage = "w",
        taille = 28, police = "berlin sans fb demi", couleur = "red")
        
        rectangle(390, 186, 580, 286, remplissage='blue', epaisseur=0)
        
        texte(394, 230, "        2\nCave gelée", ancrage = "w", 
        taille = 28, police = "berlin sans fb demi", couleur = "lightblue")
        
        rectangle(390, 326, 580, 426, remplissage='yellow', epaisseur=0)
        
        texte(394, 370, "        4\n   Désert", ancrage = "w", 
        taille = 30, police = "berlin sans fb demi", couleur = "orange")
        
        
        while mode:
            
            ev = donne_ev()
            tev = type_ev(ev)
            
            if tev == "ClicGauche":
                
                if tev == "ClicGauche" and (20 <= abscisse(ev) <= 210) and (186 <= ordonnee(ev) <= 286):
                    efface_tout()
                    image(0, 0, '1.pgm', ancrage='nw')
                    texte(290, 10, "Les Règles:\n\n- Prendre le plus de pommes\n- Attention aux bords\n- Pas de demi tour\n- On ne doit pas mordre soi-même\n- Obstacles noirs aléatoires"
                    , ancrage = "n", police = "berlin sans fb demi")
                    menu = 1
                    mode = False
                    
                elif tev == "ClicGauche" and (390 <= abscisse(ev) <= 580) and (186 <= ordonnee(ev) <= 286):
                    efface_tout()
                    image(0, 0, '2.pgm', ancrage='nw')
                    texte(290, 10, "Les Règles:\n\n- Prendre le plus de pommes\n- Attention aux bords\n- Pas de demi tour\n- On ne doit pas mordre soi-même\n- Lumière faible\n- Obstacle jaune aléatoire"
                    , ancrage = "n", police = "berlin sans fb demi")
                    menu = 2
                    mode = False

                elif tev == "ClicGauche" and (20 <= abscisse(ev) <= 210) and (326 <= ordonnee(ev) <= 426):
                    efface_tout()
                    image(0, 0, '3.pgm', ancrage='nw')
                    texte(290, 10, "Les Règles:\n\n- Coincer l'adversaire\n- Pas de demi tour\n- On ne doit pas mordre soi-même\n- J2 joue avec les touches SZXC"
                    , ancrage = "n", police = "berlin sans fb demi")
                    menu = 3
                    mode = False
                    
                elif tev == "ClicGauche" and (390 <= abscisse(ev) <= 580) and (326 <= ordonnee(ev) <= 426):
                    efface_tout()
                    image(0, 0, '4.pgm', ancrage='nw')
                    texte(290, 10, "Les Règles:\n- Prendre le plus de pommes\n- Pas de demi tour\n- On ne doit pas mordre soi-même\n- Clic gauche quatre fois sur la pomme\n   pour la rendre comestible\n- Obstacles noirs aléatoires\n- Attention aux bords"
                    , ancrage = "n", police = "berlin sans fb demi")
                    menu = 4
                    mode = False
                    
                else:
                    pass
                    
            else:
                pass
                
            mise_a_jour()
                    
                    
        
        texte(310, 410, " Appuyez sur Entrée\n   pour commencer,\nclic droit pour quitter",
                couleur = "blue", ancrage = "s", tag = "fin", taille = 20, police = "berlin sans fb demi")
        if menu == 1:
            frequencer = w + 2
        elif menu == 2:
            frequencer = 1
        elif menu == 3:
            frequencer = 0
        else:
            frequencer = w

                    
        while regle:
        
            ev = donne_ev()
            tev = type_ev(ev)
            
            if tev == "Touche" and touche(ev) == "Return":
                regle = False
                
            elif tev == "ClicDroit":
                regle = False
                jeu = False
                
            else:
                pass
                
            mise_a_jour()
            
        image(0, 0, str(menu)+'fond.pgm', ancrage='nw')
            
        while jeu:
               
        # boucle principale
                    
            # liste des coordonnés de la tete
            # la contient tout les points en abscisse où la tete est passé
            # la contient tout les points en ordonné où la tete est passé
            la.insert(0,a)
            lb.insert(0,b)
            la2.insert(0,a2)
            lb2.insert(0,b2)
            
            efface("serp")
            efface("cassable")
            
            # afficher de la tete du serpent
            a, b, jeu = bord(a,b)
            affiche_serpent(a,b)
            if jeu == False:
                break
            
            # afficher de la tete du serpent2
            if menu == 3:
                a2, b2, jeu = bord(a2,b2)
                affiche_serpent2(a2,b2)
            
            # afficher si corps en + pour serpent
            aff_serpentt(w)

            # afficher si corps en + pour serpent 2
            if menu == 3:
                aff_serpentt(w2)
                        
            # pour le menu 4 il faut que ce soit placé avant l'affectation de ap et bp sinon
            # quand on prend la pomme ap et bp changent de valeur et on ne rentre pas dans le deuxieme if
            # le c > 1 permet d'attendre qu'on affecte la valeur a ap et bp sinon il y a un bug
            if (menu == 4 or menu == 2) and c > 1:
                if a == ap and b == bp:
                    efface("rockr")
                    atr = 0
                    lar = []
                    lbr = []

            # afficher la pomme cassable / pomme normale
            ap, bp, apparition = pomme(apparition,menu,rock,a,b,a2,b2)
                
            # afficher les rochers mortels
            ar, br, atr, frequencer = aff_roch(menu, w, atr, ap, bp, c, frequencer)
                    
            # si le serpent touche un rocher mortel
            for i in range(len(lar)):
                if a == lar[i]:
                    if b == lbr[i]:
                        jeu = False
            if jeu == False:
                break
            
            # afficher cercle noir
            if menu == 2:
                n1, n2 = case_vers_pixel((a, b))
                cercle(n1, n2, 600, couleur='black', epaisseur=1000, tag='serp')
            
            # connaitre si la position de la tete ne se
            # trouve pas sur une des positions du corps
            # ou de son adversaire en mode deux joueurs
            v, jeu = entre_choque(menu, w, a, b, w2, a2, b2)
            if jeu == False:
                break
            
            # gestion de la direction
            if direction == "debut":
                a=a+1
            if direction == "droite":
                a=a+1
            if direction == "gauche":
                a=a-1
            if direction == "haut":
                b=b-1
            if direction == "bas":
                b=b+1
                
            # gestion de la direction 2    
            if menu == 3:
                if direction2 == "debut":
                    a2=a2-1
                if direction2 == "droite":
                    a2=a2+1
                if direction2 == "gauche":
                    a2=a2-1
                if direction2 == "haut":
                    b2=b2-1
                if direction2 == "bas":
                    b2=b2+1


            # dans le menu 4, si la pomme a casser n'est pas completement cassé
            if menu == 4 and a == ap and b == bp and rspe == 0:
                w = w - 1
                jeu = False
                break
                
                
            # pomme si elle a été mangé
            if a == ap and b == bp:
                efface('pomm')
                apparition = 0
                if menu != 3:
                    framerate = framerate + .5
                w = w + 1
                rock = 0
                rspe = 0
                mise_a_jour()
                
            # pomme si elle a été mangé 2
            if a2 == ap and b2 == bp:
                efface('pomm')
                apparition = 0
                framerate = framerate + 0.5
                w2 = w2 + 1
                mise_a_jour()
                
                
            # gestion des événements
            ev = donne_ev()
            ty = type_ev(ev)
            if ty == 'Quitte':
                jeu = False
            elif ty == 'Touche':
                direction, uturn = change_direction(direction, touche(ev), uturn)
                direction2, uturn2 = change_direction2(direction2, touche(ev), uturn2)
            elif ty == "ClicGauche" and menu == 4:
                if (ap-1 <= abscisse(ev)/15 <= ap+1) and (bp-1 <= ordonnee(ev)/15 <= bp+1):
                    rock = rock + 1
                    if rock < 4:
                        efface("rock")
                        cercle(ap*15+7.5, bp*15+7.5, taille_case/2, couleur='black', remplissage=lrock[rock-1], tag='rock')
                        mise_a_jour()
                    else:
                        efface("rock")
                        rspe = 1
                        
            

            # attente avant rafraîchissement
            sleep(1/framerate)
            c = c + 1
            
            mise_a_jour()
            

        #partie finie
        if menu != 3:
        
            f = open("record"+str(menu)+".txt", "r")
            r1 = f.read()
            f.close()
            if w > int(r1):
                f = open("record"+str(menu)+".txt", "w")
                f.write(str(w))
                r1 = str(w)
                f.close()

            efface("pomm")  
            texte(300, 20, "Meilleur score: " + r1, couleur = "yellow",
                      ancrage = "center", tag = "fin", taille = 25, police = "berlin sans fb demi")
            if w <= 1:
                texte(300, 200, "Votre score est de:\n       " + str(w) + " pomme", couleur = "red",
                      ancrage = "center", tag = "fin", taille = 40, police = "berlin sans fb demi")
            else:
                texte(300, 200, "Votre score est de:\n      " + str(w) + " pommes", couleur = "red",
                      ancrage = "center", tag = "fin", taille = 40, police = "berlin sans fb demi")
                      
                texte(300, 430, "Appuyez sur Entrée pour recommencer,\n ou clic droit sur la fenêtre pour quitter",
                      couleur = "blue", ancrage = "s", tag = "fin", taille = 20, police = "berlin sans fb demi")
                  
        else:
            if v == 0:
                texte(300, 200, "Egalité", couleur = "red", ancrage = "center", tag = "fin", taille = 40, police = "berlin sans fb demi")
            if v == 1:
                texte(300, 200, "Serpent Vert gagne !", couleur = "red", ancrage = "center", tag = "fin", taille = 40, police = "berlin sans fb demi")
            if v == 2:
                texte(300, 200, "Serpent Bleu gagne !", couleur = "red", ancrage = "center", tag = "fin", taille = 40, police = "berlin sans fb demi")
        texte(300, 430, "Appuyez sur Entrée pour recommencer,\n ou clic droit sur la fenêtre pour quitter", couleur = "blue", ancrage = "s", tag = "fin", taille = 20, police = "berlin sans fb demi")
                          
                          
                          
                          


    # fermeture et sortie
    ferme_fenetre()
