#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <MLV/MLV_all.h>
#include "editeur.h"


/*initialise et renvoie le Tableau d'Objets qui servira pour l'édition*/
Tab_Edition* initialisation_Tab_Edit(){
	int i, j;
	Tab_Edition* edit = (Tab_Edition*)malloc(sizeof(Tab_Edition));
	if(edit == NULL)
		exit(EXIT_FAILURE);
	for(i=0; i<Y ;i++){
		for(j=0; j<X ;j++)
			edit->tab[i][j]='!';}
	return edit;
}

/*libère la mémoire de Tab_Edition*/
void free_Tab_Edit(Tab_Edition* edit){
	free(edit);
	edit = NULL;
}

/* Affiche tout l'éditeur de niveau */
void edit_affiche(char elem, Tab_Edition* edit, int* oui_p, int* oui_d, int etendu_x, int etendu_y){
	int i, j;
	MLV_Image *image;
	
	MLV_clear_window(MLV_COLOR_BLACK);

	image = MLV_load_image("image/editeur.png");
	MLV_draw_image(image, 0, 0);
	MLV_free_image(image);
	*oui_p=0; *oui_d=0;

	/* Affichage du plateau et on compte le nombre de depart et d'arrivee*/
	for(i=0; i<Y ;i++){
		for(j=0; j<X ;j++){
			switch(edit->tab[i][j]){
				/* Debut */
				case 'P':
				image = MLV_load_image("image/debut.png"); *oui_p=*oui_p+1; break;

				/* Sol */
				case '.': image = MLV_load_image("image/sol_1.png"); break;

				/* Sol fissure */
				case ',': image = MLV_load_image("image/sol_2.png"); break;

				/* Sol champi */
				case ';': image = MLV_load_image("image/sol_3.png"); break;

				/* Sol os */
				case ':': image = MLV_load_image("image/sol_4.png"); break;

				/* Mur */
				case '#': image = MLV_load_image("image/mur.png"); break;

				/* Lanceur */
				case 'O': image = MLV_load_image("image/obstacle.png"); break;

				/* Fin */
				case 'D': image = MLV_load_image("image/fin.png"); *oui_d=*oui_d+1; break;

				/* Cle */
				case 'K': image = MLV_load_image("image/cle.png"); break;

				/* Porte */
				case 'T': image = MLV_load_image("image/cle_mur.png"); break;

				/* Rocher */
				case '*': image = MLV_load_image("image/pousse.png"); break;

				/* Piques */
				case '+': image = MLV_load_image("image/pique.png"); break;

				/* Glace */
				case '-': image = MLV_load_image("image/glace.png"); break;

				/* Vide */
				case '!': break;		
			}
			if(edit->tab[i][j]!='!'){
				MLV_draw_image(image, j*48, i*48);
				MLV_free_image(image);}
		}
	}

	/* On affiche le curseur */
	image = MLV_load_image("image/curseur.png");
	switch(elem){
		/* Debut */
		case 'P': MLV_draw_image(image, 1104, 22); break;

		/* Sol */
		case '.': MLV_draw_image(image, 1076, 109); break;

		/* Sol fissure */
		case ',': MLV_draw_image(image, 1131, 109); break;

		/* Sol champi */
		case ';': MLV_draw_image(image, 1076, 164); break;

		/* Sol os */
		case ':': MLV_draw_image(image, 1131, 164); break;

		/* Mur */
		case '#': MLV_draw_image(image, 1076, 248); break;

		/* Lanceur */
		case 'O': MLV_draw_image(image, 1104, 334); break;

		/* Fin */
		case 'D': MLV_draw_image(image, 1104, 430); break;

		/* Cle */
		case 'K': MLV_draw_image(image, 470, 682); break;

		/* Porte */
		case 'T': MLV_draw_image(image, 470, 737); break;

		/* Pousse */
		case '*': MLV_draw_image(image, 306, 682); break;

		/* Pique */
		case '+': MLV_draw_image(image, 1131, 248); break;

		/* Glace */
		case '-': MLV_draw_image(image, 306, 737); break;

		/* Gomme */
		case '!': MLV_draw_image(image, 1104, 545); break;
	}
	MLV_free_image(image);

	/* On affiche le dammier du mode étendu */
	image = MLV_load_image("image/etendue.png");
	for(i=0; i<etendu_y ;i++){
		for(j=0; j<etendu_x ;j++){
			if(!(i==0 && j==0))
				MLV_draw_image(image, 790+(j*14), 680+(i*14));}
	}
	MLV_free_image(image);
	MLV_actualise_window();
}

/*vérifie si on a le droit de décaler tout le Plateau dans une direction
on procéde comme suit :
pour la direction DROITE 'd': on regarde la dernière ligne tout à droite et si elle ne colle
pas au bord du plateau alors on peut déplacer ce dernier
c'est la même méthode pour les autres directions*/
int edit_verif(char direction, Tab_Edition* edit){
	int i;
	switch(direction){
		case 'd': 
		for(i=0; i<Y ; i++){
			if(edit->tab[i][X-1]!='!')
				return 0;}
		break;
		
		case 'g': 
		for(i=0; i<Y ; i++){
			if(edit->tab[i][0]!='!')
				return 0;}
		break;
		
		case 'h': 
		for(i=0; i<X ; i++){
			if(edit->tab[0][i]!='!')
				return 0;}
		break;
		
		case 'b': 
		for(i=0; i<X ; i++){
			if(edit->tab[Y-1][i]!='!')
				return 0;}
		break;
	}
	return 1;
}


/*si la vérification effectuée avec edit_verif() est concluante alors on bouge tout le plateau
dans la direction demandée*/
int edit_move(char direction, Tab_Edition* edit){
	int i, j;
	if(edit_verif(direction,edit)){
		switch(direction){	
			case 'g':
			for(i=0; i<Y ; i++){
				for(j=1 ; j<X ; j++){
					edit->tab[i][j-1] = edit->tab[i][j];
					edit->tab[i][j]='!';}
			}
			break;
			
			case 'd': 
			for(i=0; i<Y ; i++){
				for(j=X-2 ; j>-1 ; j--){
					edit->tab[i][j+1] = edit->tab[i][j];
					edit->tab[i][j]='!';}
			}
			break;
			
			case 'b': 
			for(j=0 ; j<X ; j++){
				for(i=Y-2; i>-1 ; i--){
					edit->tab[i+1][j] = edit->tab[i][j];
					edit->tab[i][j]='!';}
			}
			break;
			
			case 'h': 
			for(j=0 ; j<X ; j++){
				for(i=1; i<Y ; i++){
					edit->tab[i-1][j] = edit->tab[i][j];
					edit->tab[i][j]='!';}
			}
			break;
		}
		return 1;
	}
	return 0;
}

/*détecte les clics de l'utilisateur et renvoie le charactère associé à l'Objet
que l'on souhaite placer sur l'éditeur*/
/*si aucun clic ne convient alors ça veux dire qu'on ne veut pas changer d'Objet*/
char edit_select(int x, int y, int* etendu_x, int* etendu_y, char elem){
	/* Debut */
	if((x>1107 && x<1154) && (y>25 && y<72)){ *etendu_x=1; *etendu_y=1; return 'P';}

	/* Sol */
	if((x>1079 && x<1126) && (y>112 && y<159)) return '.';

	/* Sol fissure */
	if((x>1134 && x<1181) && (y>112 && y<159)) return ',';

	/* Sol champi */
	if((x>1079 && x<1126) && (y>167 && y<214)) return ';';

	/* Sol os */
	if((x>1134 && x<1181) && (y>167 && y<214)) return ':';

	/* Mur */
	if((x>1079 && x<1126) && (y>251 && y<298)) return '#';

	/* Lanceur */
	if((x>1107 && x<1154) && (y>337 && y<384)) return 'O';

	/* Fin */
	if((x>1107 && x<1154) && (y>433 && y<480)){ *etendu_x=1; *etendu_y=1; return 'D';}

	/* Cle */
	if((x>473 && x<520) && (y>685 && y<732)) return 'K';

	/* Porte */
	if((x>473 && x<520) && (y>740 && y<787)) return 'T';

	/* Pousse */
	if((x>309 && x<356) && (y>685 && y<732)) return '*';

	/* Pique */
	if((x>1134 && x<1181) && (y>251 && y<298)) return '+';

	/* Glace */
	if((x>309 && x<356) && (y>740 && y<787)) return '-';

	/* Gomme */
	if((x>1107 && x<1154) && (y>548 && y<595)) return '!';

	return elem;
}

/*paramètre les données utilisées par le mode étendu
plus ces données sont grosses, plus on va placer d'Objets sur le plateau en même temps*/
void edit_etendue(int x, int y, int* etendu_x, int* etendu_y){
	*etendu_x = (x-790)/14+1;
	*etendu_y = (y-680)/14+1;
}

/*Place l'élément elem dans le tableau à la position a,b
si le mode étendu est activé alors on en place plusieurs d'un coup*/
void edit_tiles(int a, int b, char elem, Tab_Edition* edit, int etendu_x, int etendu_y){
	int i, j;
	/* Si le mode étendu n'est pas activé */
	if(etendu_x==1 && etendu_y==1){
		if(edit->tab[b/48][a/48] != elem)
			edit->tab[b/48][a/48] = elem;
		else /*on peut effacer un élément similaire qu'on avait déjà sur le plateau*/
			edit->tab[b/48][a/48] = '!';
	}

	/*si le mode étendu est activé*/
	else{
		for(i=0; i<etendu_y ;i++){
			for(j=0; j<etendu_x ;j++){
				if(b/48+i<Y && a/48+j<X)
					edit->tab[b/48+i][a/48+j] = elem;
			}
		}
	}

}

/*Remplace tout le plateau d'édition par le fichier demandé*/
/*utilisé dans edit_import()*/
void edit_remplace(Tab_Edition* edit, FILE* fichier){
	int i, j;
	char carac;
	/*nitialise le plateau*/
	for(i=0; i<Y ;i++){
		for(j=0; j<X ;j++)
			edit->tab[i][j] = '!';}

	i=0;j=0;
	do{ /*lecture et remplissage*/
		carac = fgetc(fichier);
		if(carac == '\n' || carac == EOF){
			j=0;
			i++;}
		else{
			/* Si le caractere ne correspond pas a un element, on le remplace par un sol simple */
			if(carac!='P' && carac!='.' && carac!=';' && carac!=':' && carac!=',' &&
				carac!='O' && carac!='#' && carac!='D' && carac!='!' && carac!='K' &&
				carac!='T' && carac!='*' && carac!='+' && carac!='-')
					carac='.';
			edit->tab[i][j]=carac;
			j++;
		}
	}while(carac != EOF);
}

/*Permet d'importer un Niveau déjà créé dans l'éditeur*/
void edit_import(Tab_Edition* edit){
	char nom[45], nom_final[60];
	MLV_Image *image;
	FILE* fichier = NULL;

	image = MLV_load_image("image/message_import.png");
	MLV_draw_image(image, 0, 0);
	MLV_free_image(image);
	MLV_actualise_window();

	printf("\nRentrez le nom du niveau que vous voulez importer, sans extension:\n(Attention si vous importez un niveau vous perdrez ce que vous avez fait actuellement)\n");

	do{
		scanf("%s", nom);
		if(strcmp(nom, "nope") != 0){ /*si on importe vraiment*/
			sprintf(nom_final,"niveaux/%s.txt",nom);
			fichier = fopen(nom_final, "r"); /*ouverture du fichier*/
			if(fichier != NULL){
				sprintf(nom_final,"niveaux/%s.txt",nom);
				fichier = fopen(nom_final, "r");
				edit_remplace(edit, fichier); /*importation*/
				fclose(fichier);
				printf("\nImportation reussi!\nVeuillez retourner sur la fenetre de jeu.\n");
			}
			else
				printf("\nLe fichier %s n'existe pas dans le dossier 'niveaux'.\nRentrez le nom du niveau que vous voulez importer, sans extension:\n(Attention si vous importez un niveau vous perdrez ce que vous avez fait actuellement)\n", nom);
		}
	}while(strcmp(nom, "nope") != 0 && fichier == NULL);

	if((strcmp(nom, "nope") == 0))
		printf("Importation annulée, veuillez retourner sur la fenetre de jeu.\n");
}

/*Copie le tableau d'édition dans le fixhier texte*/
/*utilisé pour la sauvegarde*/
void edit_ecrit(FILE* fichier, Tab_Edition* edit, int* max_x, int* max_y){
	int i, j;
	for(i=0; i<*max_y ;i++){
		for(j=0; j<*max_x ;j++)
			fputc(edit->tab[i][j], fichier);
		if(i!=*max_y-1)
			fputc('\n', fichier);
	}
	fputc('\n', fichier);
}


/*calcule la taille exacte maximale du niveau d'édition*/
/*utilisé pour la sauvegarde*/
void edit_tailleMax(Tab_Edition* edit, int* max_x, int* max_y){
	int i, j;
	for(i=0; i<Y ;i++){
		for(j=0; j<X ;j++){
			if(edit->tab[i][j]!='!'){
				*max_y=i;
				if(j>*max_x)
					*max_x=j;
			}
		}
	}
	*max_x=*max_x+1; *max_y=*max_y+1;
}


/*vérifie si le tableau d'édition est vide,
si c'est le cas on renvoie true sinon on renvoie false*/
/*utilisé pour la sauvegarde*/
bool edit_vide(Tab_Edition* edit){
	int i, j;
	for(i=0; i<Y ;i++){
		for(j=0; j<X ;j++){
			if(edit->tab[i][j]!='!')
				return false;
		}
	}
	return true;
}


/*permet de décaler tout le tableau d'édition le plus à gauche possible*/
/*permet de créer un fichier texte correct pour la sauvegarde*/
/*utilisé pour la sauvegarde*/
void edit_fin(Tab_Edition* edit, int* max_x, int* max_y, FILE * fichier){
	int i;
	int decal_h, decal_g;
	int rep;

	decal_h=-1; decal_g=-1;
	
	do{
		rep = edit_move('h',edit);
		decal_h++;
	}while(rep == 1);
	
	do{
		rep = edit_move('g',edit);
		decal_g++;
	}while(rep == 1);

	edit_tailleMax(edit, max_x, max_y);

	edit_ecrit(fichier, edit, max_x, max_y);

	for(i=0; i<decal_h; i++)
		edit_move('b', edit);

	for(i=0; i<decal_g; i++)
		edit_move('d', edit);
}


/*fonction principale de sauvegarde d'un Niveau*/
/*renvoie 1 si la sauvegarde a été un succès et 0 sinon*/
int edit_sauv(Tab_Edition* edit, int oui_p, int oui_d){
	int x, y, max_x, max_y;
	int boucle = 1, succes = 0;
	char nom[45], nom_final[60];
	MLV_Image *image;
	FILE* fichier = NULL;

	/* Si le plateau n'est pas vide, on peut sauvegarder */
	if(!edit_vide(edit) && oui_p == 1 && oui_d == 1){
		image = MLV_load_image("image/message_sauvegarde.png");
		MLV_draw_image(image, 0, 0);
		MLV_free_image(image);
		MLV_actualise_window();

		printf("\nEntrez un nom pour votre niveau inférieur a 40 caracteres sans extension:\n");
		do{
			scanf("%s", nom);
		}while(strlen(nom) > 40);

		if(strcmp(nom, "nope") != 0){
			sprintf(nom_final,"niveaux/%s.txt", nom);
			printf("\nVotre niveau %s a bien ete enregistre dans le dossier 'niveaux'\nVeuillez retourner sur la fenetre de jeu.\n", nom);
			fichier = fopen(nom_final, "w");
			edit_fin(edit, &max_x, &max_y, fichier);
			fclose(fichier);
			succes = 1;
		}
		else
			printf("\nPas sauvegarde, veuillez retourner sur la fenetre de jeu.\n");
	}

	/* Si le plateau est vide, ou ne contient pas depart ou pas de fin, on affiche le message d'erreur */
	else{
		image = MLV_load_image("image/message_erreur.png");
		MLV_draw_image(image, 0, 0);
		MLV_free_image(image);
		MLV_actualise_window();

		do{
			MLV_wait_mouse(&x, &y);
			if((x>509 && x<690) && (y>531 && y<600))
				boucle=0;
		}while(boucle==1);
	}
	return succes;
}

/*permet de quitter l'éditeur de niveaux*/
int edit_quitter(){
	int x, y;
	int tmp, boucle = 1;
	MLV_Image *image;

	image = MLV_load_image("image/message_quitter.png");
	MLV_draw_image(image, 0, 0);
	MLV_free_image(image);
	MLV_actualise_window();

	do{
		MLV_wait_mouse(&x, &y);

		/* On souhaite vraiment quitter */
		if((x>657 && x<1136) && (y>53 && y<635)){
			boucle=0;
			tmp=0;}

		/* Finalement, on ne veut plus quitter */
		if((x>63 && x<542) && (y>535 && y<635)){
			boucle=0;
			tmp=1;}
	}while(boucle == 1);

	return tmp;
}

/*fonction principale de l'éditeur de niveaux*/
/*x et y sont les coordonnes des clics
- oui_p et oui_d servent pour vérifier si on a bien un seul début et une seule arrivée 
- boucle permet d'effectuer la boucle do while de la fonction
- etendu_x et etendu_y representent la dimension du carré permettant le mode étendu
- elem est le char qui va etre placé, qui represente l'element a placer dans l'editeur
- tab represente le plateau d'édition*/
/*MODE ETENDU :*/
/*le mode étendu permet de placer plusieurs éléments d'un coup dans le plateau d'édition, il est
paramètrable par l'utilisateur en cliquant sur le carré "étendu" en bas de l'éditeur*/
int editeur(){
	int x=-1, y=-1, oui_p=0, oui_d=0, boucle=1, etendu_x=1, etendu_y=1;
	char elem = 'P';
	Tab_Edition* edit = initialisation_Tab_Edit();
	MLV_Keyboard_button touche = MLV_KEYBOARD_z;
	MLV_Sound* son;
	son = MLV_load_sound("music/save.wav");

	/* Permet de tout afficher, on verra cette fonction souvent */
	edit_affiche(elem, edit, &oui_p, &oui_d, etendu_x, etendu_y);

	/* Boucle d'édition */
	do{
		MLV_wait_keyboard_or_mouse(&touche, NULL, NULL, &x, &y);

		/* Si on appuie sur les touches fléchées */
		if(touche != MLV_KEYBOARD_z){
			if(touche == MLV_KEYBOARD_UP)
				edit_move('h' ,edit);
			if(touche == MLV_KEYBOARD_DOWN)
				edit_move('b' ,edit);
			if(touche == MLV_KEYBOARD_LEFT)
				edit_move('g' ,edit);
			if(touche == MLV_KEYBOARD_RIGHT)
				edit_move('d' ,edit);
			edit_affiche(elem, edit, &oui_p, &oui_d, etendu_x, etendu_y);
			touche = MLV_KEYBOARD_z;
		}

		/* Si on clique sur l'ecran */
		if(x>=0 && y>=0){
			/*Sélection d'un élément principaux (à droite)*/
			if((x>1079 && x<1181) && (y>25 && y<595))
				elem=edit_select(x, y, &etendu_x, &etendu_y, elem);

			/*Sélection des éléments rocher, glace, clé et porte*/
			if((x>309 && x<520) && (y>685 && y<787))
				elem=edit_select(x, y, &etendu_x, &etendu_y, elem);

			/*Change les paramètres du mode étendu*/
			if((x>790 && x<901) && (y>680 && y<791))
				edit_etendue(x, y, &etendu_x, &etendu_y);

			/*Placement d'un élément sur le plateau d'édition*/
			if((x>0 && x<1055) && (y>0 && y<671))
				edit_tiles(x, y, elem, edit, etendu_x, etendu_y);

			/*Importer*/
			if((x>1106 && x<1153) && (y>634 && y<681))
				edit_import(edit);

			/*Sauvegarder*/
			if((x>1107 && x<1154) && (y>723 && y<770)){
				if(edit_sauv(edit, oui_p, oui_d) == 1)
					MLV_play_sound(son, 1.0);}

			/*Quitter*/
			if((x>990 && x<1037) && (y>723 && y<770))
				boucle = edit_quitter();

			edit_affiche(elem, edit, &oui_p, &oui_d, etendu_x, etendu_y);
			x = -1; y = -1;
		}
	}while(boucle==1);

	free_Tab_Edit(edit);
	return 0;
}
