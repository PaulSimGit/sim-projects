#ifndef __editeur__
#define __editeur__
#define X 22
#define Y 14


typedef struct tabedition {
    char tab[Y][X];
} Tab_Edition;

Tab_Edition* initialisation_Tab_Edit();
void free_Tab_Edit(Tab_Edition* edit);
void edit_affiche(char elem, Tab_Edition* edit, int* oui_p, int* oui_d, int etendu_x, int etendu_y);
int edit_verif(char direction, Tab_Edition* edit);
int edit_move(char direction, Tab_Edition* edit);
char edit_select(int x, int y, int* etendu_x, int* etendu_y, char elem);
void edit_etendue(int x, int y, int* etendu_x, int* etendu_y);
void edit_tiles(int a, int b, char elem, Tab_Edition* edit, int etendu_x, int etendu_y);
void edit_remplace(Tab_Edition* edit, FILE* fichier);
void edit_import(Tab_Edition* edit);
void edit_ecrit(FILE* fichier, Tab_Edition* edit, int* max_x, int* max_y);
void edit_tailleMax(Tab_Edition* edit, int* max_x, int* max_y);
bool edit_vide(Tab_Edition* edit);
void edit_fin(Tab_Edition* edit, int* max_x, int* max_y, FILE * fichier);
int edit_sauv(Tab_Edition* edit, int oui_p, int oui_d);
int edit_quitter();
int editeur();


#endif
