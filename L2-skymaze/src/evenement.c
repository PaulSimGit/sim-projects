#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "evenement.h"


/*alloue la mémoire et crée un Tas*/
Tas* malloc_Tas(){
    Tas* tas = (Tas*)malloc(sizeof(Tas));
    if(tas != NULL){
        tas->capacite = CAPACITE_INITIALE;
        tas->taille = 0;
        tas->valeurs = (Evenement*)malloc(sizeof(Evenement) * CAPACITE_INITIALE);
        if(tas->valeurs == NULL)
            exit(EXIT_FAILURE);
    }
    else
        exit(EXIT_FAILURE);
    return tas;
}

/*détruit un Tas*/
void free_Tas(Tas* tas){
    free(tas->valeurs);
    tas->valeurs = NULL;
    free(tas);
    tas = NULL;
}

/*revoie -1 si le noeud représenté par la case indice n'a pas de fils, 
sinon renvoie l'indice du fils contenant la plus petite étiquette */
int Fils(Tas* tas, int indice){
    unsigned int fils;
    fils = (indice*2 + 1);
    if(fils >= tas->taille)
        return -1;
    if(fils == (tas->taille)-1)
        return fils;
    if(tas->valeurs[fils].moment > tas->valeurs[fils+1].moment)
        fils+=1;
    return (int)fils;
}

/*remplace l'Evenement de la case 'indice' par l'Evenement 'new_Ev' et effectue diverses échange de case*/
void change(Tas* tas, int indice, Evenement new_Ev){
    int fils_min, parent;
    if(indice < (int)tas->taille){
        tas->valeurs[indice].coo_obj = new_Ev.coo_obj;
        tas->valeurs[indice].moment = new_Ev.moment;

        fils_min = Fils(tas, indice);
        while(fils_min != -1 && tas->valeurs[indice].moment > tas->valeurs[fils_min].moment){
            tas->valeurs[indice].coo_obj = tas->valeurs[fils_min].coo_obj;
            tas->valeurs[indice].moment = tas->valeurs[fils_min].moment;
            tas->valeurs[fils_min].coo_obj = new_Ev.coo_obj;
            tas->valeurs[fils_min].moment = new_Ev.moment;
            indice = fils_min;
            fils_min = Fils(tas, indice);
        }
        parent = (indice -1)/2;
        while(indice > 0 && tas->valeurs[indice].moment < tas->valeurs[parent].moment){
            tas->valeurs[indice].coo_obj = tas->valeurs[parent].coo_obj;
            tas->valeurs[indice].moment = tas->valeurs[parent].moment;
            tas->valeurs[parent].coo_obj = new_Ev.coo_obj;
            tas->valeurs[parent].moment = new_Ev.moment;
            indice = parent;
            parent = (indice -1)/2;
        }
    }
}

/*renvoie 'true' si un événement doit être effectué*/
bool un_evenement_est_pret(Tas* tas){
    int i;
    unsigned long now = maintenant();
    if(tas->taille != 0){
        for(i=0; (unsigned int)i<tas->taille; i++){
            if(now >= tas->valeurs[i].moment)
                return true;
        }
    }
    return false;
}

/*renvoie et retire de 'tas' l'Evenement dont le 'moment' est le plus petit*/
Evenement ote_minimum(Tas* tas){
    Evenement min;
    min = tas->valeurs[0];
    tas->taille--;
    change(tas, 0, tas->valeurs[tas->taille]);
    return min;
}

/*retire de 'tas' l'Evenement dont les coordonnées sont (x,y)*/
/*fonction utilisée pour qu'un ROCHER puisse détruire des PROJECTILES*/
void ote_coordo(Tas* tas, unsigned int Cx, unsigned int Cy){
    int i;
    for(i=0; i<(int)tas->taille; i++){
        if(tas->valeurs[i].coo_obj.x == Cx && tas->valeurs[i].coo_obj.y == Cy)
            break;
    }
    tas->taille--;
    change(tas, i, tas->valeurs[tas->taille]);
}

/*ajoute un Evenement a un Tas*/
void ajoute_evenement(Tas* tas, Evenement n){
    if (tas->taille == tas->capacite){ /*augmente la mémoire si on a plus de place*/
        tas->valeurs = (Evenement*)realloc(tas->valeurs, sizeof(Evenement) * (tas->taille+1));
        if(tas->valeurs == NULL)
            exit(EXIT_FAILURE);
        tas->capacite ++;
    }
    tas->taille++; /*ajoute*/
    change(tas, tas->taille-1, n);
}

/*construit un Tas et initialise les Evenements initiaux d'un Niveau*/
Tas* construit_Tas(Niveau* niveau){
    Tas* tas = malloc_Tas();
    Generation* gen;
    Piege* piege;
    Evenement init;
    unsigned int i, j;
    for(j=0; j<niveau->taille.y; j++){
        for(i=0; i<niveau->taille.x; i++){
            switch(niveau->objets[j][i][0].type){
                case LANCEUR: gen = (Generation*)niveau->objets[j][i][0].donnee_suppl;
                                init.moment = maintenant() + gen->intervalle;
                                init.coo_obj.x = i; init.coo_obj.y = j;
                                ajoute_evenement(tas, init); break;
                case PIQUES: piege = (Piege*)niveau->objets[j][i][0].donnee_suppl;
                                init.moment = maintenant() + piege->intervalle;
                                init.coo_obj.x = i; init.coo_obj.y = j;
                                ajoute_evenement(tas, init); break;
                default: break;
            }
        }
    }
    return tas;
}

/*affiche le contenu d'un tas, pour tester*/
void affiche_Tas(Tas* tas){
    int i;
    printf("Tas event:\n");
    if(tas->taille == 0)
        printf("TAS VIDE\n");
    for(i=0; (unsigned int)i<tas->capacite+30; i++){
        printf("i %d   momment:%ld ticks / ", i, tas->valeurs[i].moment);
        printf("coo_obj:(%d,%d)\n", tas->valeurs[i].coo_obj.y, tas->valeurs[i].coo_obj.x);
    }
}

/*ajoute un projectile (ou un rocher) dans le niveau à la position (x,y) en fonction de sa direction 'd' et son allure
ajoute également les événements liés à ce projectile (ou ce rocher si il glisse)*/
void generation_objet(Tas* tas, Niveau* niv, TypeObjet objet, Direction d, unsigned long allure, int x, int y){
    Evenement e;
    Deplacement* depl = (Deplacement*)malloc(sizeof(Deplacement));
    niv->objets[y][x][1].type = objet; /*création objet*/
    depl->direction = d;
    depl->allure = allure;
    niv->objets[y][x][1].donnee_suppl = depl;

    TypeSol* type_sol = (TypeSol*)niv->objets[y][x][0].donnee_suppl;

    if(objet == PROJECTILE || (objet == ROCHER && type_sol->type == GLACE)){
        e.coo_obj.x = x; /*création events déplacement*/
        e.coo_obj.y = y;
        e.moment = maintenant() + allure;
        ajoute_evenement(tas, e);
    }
}

/*déplace le projectile (ou le rocher) demandé dans le niveau si il ne rencontre pas un autre objet
le projectile (ou le rocher) se déplace de (c.x, c.y) a (newX, newY)
renvoie 0 si le projectile touche le personnage et 1 sinon*/
/*Coordonnees c : ancienne position   ///   newX/newY : nouvelle position*/
int deplacement_Objet(Tas* tas, Niveau *niveau, Deplacement* depl, Coordonnees c, unsigned int newX, unsigned int newY){
    int etat = 1; bool rocher_bouge_pas = false;
    unsigned int max_X = niveau->taille.x - 1;
    unsigned int max_Y = niveau->taille.y - 1;
    TypeObjet rencontre_sol, rencontre_mouvant;
    TypeObjet objet = niveau->objets[c.y][c.x][1].type;
    Deplacement d;
    d.direction = depl->direction;
    d.allure = depl->allure;

    /*coordonnées en dehors du plateau, on ne fait rien*/
    if(newX > max_X || newY > max_Y || (int)newX < 0 || (int)newY < 0){
        if(objet == ROCHER)
            rocher_bouge_pas = true;
    }
    else{
        rencontre_sol = niveau->objets[newY][newX][0].type;
        rencontre_mouvant = niveau->objets[newY][newX][1].type;
        /*le projectile se déplace sur du SOL, on ajoute un nouveau projectile*/
        if(objet == PROJECTILE && rencontre_sol == SOL && (rencontre_mouvant == VIDE || rencontre_mouvant == PERSONNAGE)){
            generation_objet(tas, niveau, PROJECTILE, d.direction, d.allure, newX, newY);
            if(rencontre_mouvant == PERSONNAGE) /*le projectile touche le personnage*/
                etat = 0;
        }
        /*le rocher se déplace sur du sol et/ou écrase un projectile*/
        else if(objet == ROCHER && rencontre_sol == SOL  && (rencontre_mouvant == VIDE || rencontre_mouvant == PROJECTILE)){
            if(rencontre_mouvant == PROJECTILE)
                ote_coordo(tas, newX, newY); /*on supprime l'event du projectile*/
            generation_objet(tas, niveau, ROCHER, d.direction, d.allure, newX, newY);
        }
        /*le rocher ne bouge pas car il est bloqué par quelque chose*/
        else if(objet == ROCHER && (rencontre_sol != SOL || rencontre_mouvant == ROCHER || rencontre_mouvant == PERSONNAGE))
            rocher_bouge_pas = true;
    }

    if(!rocher_bouge_pas){
        niveau->objets[c.y][c.x][1].type = VIDE;
        free(niveau->objets[c.y][c.x][1].donnee_suppl);
    }

    return etat;
}

/*ajoute 4 nouveaux projectiles dans le Niveau 'niv' à partir du lanceur aux coordonnées 'coordo'
ajoute également le prochain événement du lanceur
renvoie 0 si le personnage a été tué pendant cette action et 1 sinon*/
int creation_Projectiles(Tas* tas, Niveau* niv, Coordonnees coordo){
	unsigned int x = coordo.x, y = coordo.y;
    unsigned int max_X = niv->taille.x - 1;
    unsigned int max_Y = niv->taille.y - 1;
    Generation* gen = (Generation*)niv->objets[y][x][0].donnee_suppl;
    Evenement new_lanceur;

    /*si le personnage se trouve sur un projectile pendant sa création, on ne fait rien*/
    if((y != 0 && niv->objets[y-1][x][1].type == PERSONNAGE) || (y != max_Y && niv->objets[y+1][x][1].type == PERSONNAGE) 
        || (x != 0 && niv->objets[y][x-1][1].type == PERSONNAGE) || (x != max_X && niv->objets[y][x+1][1].type == PERSONNAGE)){
            return 0;
    }

    /*sinon, on génère les projectiles (seulement si ils sont créés sur une place libre)*/
    if(y != 0 && niv->objets[y-1][x][0].type == SOL && niv->objets[y-1][x][1].type == VIDE)
        generation_objet(tas, niv, PROJECTILE, HAUT, gen->allure_proj, x, y-1);
    if(y != max_Y && niv->objets[y+1][x][0].type == SOL && niv->objets[y+1][x][1].type == VIDE)
        generation_objet(tas, niv, PROJECTILE, BAS, gen->allure_proj, x, y+1);
    if(x != 0 && niv->objets[y][x-1][0].type == SOL && niv->objets[y][x-1][1].type == VIDE)
        generation_objet(tas, niv, PROJECTILE, GAUCHE, gen->allure_proj, x-1, y);
    if(x != max_X && niv->objets[y][x+1][0].type == SOL && niv->objets[y][x+1][1].type == VIDE)
        generation_objet(tas, niv, PROJECTILE, DROITE, gen->allure_proj, x+1, y);
    
    /*ajout du prochain événement du lanceur*/
    new_lanceur.coo_obj.x = x;
    new_lanceur.coo_obj.y = y;
    new_lanceur.moment = maintenant() + gen->intervalle;
    ajoute_evenement(tas, new_lanceur);

    return 1;
}

/*vérifie la prochaine position du personnage si il se déplace après l'appuie
d'une touche par l'utilisateur
renvoie true si le mouvement est possible et false sinon*/
bool deplacement_perso_possible(Niveau* niveau, MLV_Button_state up, MLV_Button_state left, MLV_Button_state down, MLV_Button_state right){
    unsigned int max_X = niveau->taille.x - 1;
    unsigned int max_Y = niveau->taille.y - 1;
    Coordonnees perso = niveau->coo_perso, roc = niveau->coo_perso;
    TypeObjet new_pos_perso0, new_pos_perso1, new_pos_roc0, new_pos_roc1;
    int verif_roc = 1;

    /*coordonnées en dehors du plateau*/
    if(up == MLV_PRESSED){
        perso.y --; roc.y -= 2;
        if((int)perso.y < 0)
            return false;
        if((int)roc.y < 0)
            verif_roc = 0;
    }
    else if(left == MLV_PRESSED){
        perso.x --; roc.x -= 2;
        if((int)perso.x < 0)
            return false;
        if((int)roc.x < 0)
            verif_roc = 0;
    }
    else if(down == MLV_PRESSED){
        perso.y ++; roc.y += 2;
        if(perso.y > max_Y)
            return false;
        if(roc.y > max_Y)
            verif_roc = 0;
    }
    else if(right == MLV_PRESSED){
        perso.x ++; roc.x += 2;
        if(perso.x > max_X)
            return false;
        if(roc.x > max_X)
            verif_roc = 0;
    }
    else
        return false;

    new_pos_perso0 = niveau->objets[perso.y][perso.x][0].type;
    new_pos_perso1 = niveau->objets[perso.y][perso.x][1].type;

    /*on touche un ROCHER, on vérifie si on peut déplacer ce rocher ou non*/
    if(new_pos_perso1 == ROCHER && verif_roc == 1){
        new_pos_roc0 = niveau->objets[roc.y][roc.x][0].type;
        new_pos_roc1 = niveau->objets[roc.y][roc.x][1].type;
        if(new_pos_roc0 != SOL || new_pos_roc1 == ROCHER || new_pos_roc1 == PERSONNAGE)
            return false;
    }
    else if(new_pos_perso1 == ROCHER && verif_roc == 0)
        return false;

    /*on touche un autre obstacle, déplacement impossible*/
    else if(new_pos_perso0 == MUR || new_pos_perso0 == LANCEUR
        || (new_pos_perso0 == PORTE && niveau->nb_cle <= 0))
            return false;
    /*déplacement normal*/
    return true;
}

/*déplace le personnage dans le Niveau*/
/*dans cette fonction, le tas ne sert que à déplacer le ROCHER*/
/*niveau->coo_perso = nouvelle position   ///   c = ancienne position*/
/*return 1 si tout se passe normalement, 2 si on a perdu et 3 si on a gagné*/
int deplacement_Personnage(Tas* tas, Niveau *niveau, Coordonnees c){
    int etat = 1;
    MLV_Sound* son;
    Coordonnees new = niveau->coo_perso;
    TypeObjet contact_sol = niveau->objets[new.y][new.x][0].type;
    TypeObjet contact_mouvant = niveau->objets[new.y][new.x][1].type;

    if(contact_mouvant == PROJECTILE) /*mort par PROJECTILE*/
        etat = 2;
    else if(contact_sol == DESTINATION) /*arrivé*/
        etat = 3;
    else if(contact_sol == CLE){ /*récupère une clé*/
        son = MLV_load_sound("music/key.wav");
        MLV_play_sound(son, 1.0);
        niveau->nb_cle ++;
        creer_Sol(niveau, new.x, new.y, NORMAL);
        /*MLV_free_sound(son);*/
    }
    else if(contact_sol == PORTE){ /*ouvre une porte*/
        son = MLV_load_sound("music/door.wav");
        MLV_play_sound(son, 1.0);
        niveau->nb_cle --;
        creer_Sol(niveau, new.x, new.y, NORMAL);
        /*MLV_free_sound(son);*/
    }
    else if(contact_sol == PIQUES){ /*mort si PIQUES ACTIVE*/
        Piege* piege = (Piege*)niveau->objets[new.y][new.x][0].donnee_suppl;
        if(piege->activite == ACTIVE)
            etat = 2;
    }
    else if(contact_mouvant == ROCHER){ /*pousse un rocher, on défini donc sa direction et on le déplace*/
        Deplacement* roc = (Deplacement*)niveau->objets[new.y][new.x][1].donnee_suppl;
        if(new.x == c.x && new.y == c.y-1){
            roc->direction = HAUT;
            deplacement_Objet(tas, niveau, roc, new, new.x, new.y-1);}
        else if(new.x == c.x-1 && new.y == c.y){
            roc->direction = GAUCHE;
            deplacement_Objet(tas, niveau, roc, new, new.x-1, new.y);}
        else if(new.x == c.x && new.y == c.y+1){
            roc->direction = BAS;
            deplacement_Objet(tas, niveau, roc, new, new.x, new.y+1);}
        else if(new.x == c.x+1 && new.y == c.y){
            roc->direction = DROITE;
            deplacement_Objet(tas, niveau, roc, new, new.x+1, new.y);}
    }

    niveau->objets[c.y][c.x][1].type = VIDE;
    niveau->objets[new.y][new.x][1].type = PERSONNAGE;
    return etat;
}

/*change l'état des Piques aux Coordonnées c du Niveau et ajoute
l'Evenement correspondant dans le tas
renvoie 0 si les piques ont tué le personnage et 1 sinon*/
int changement_etat_Piques(Tas* tas, Niveau* niveau, Coordonnees c, Activite activite){
    Evenement pik;
    Piege* piege = (Piege*)niveau->objets[c.y][c.x][0].donnee_suppl;
    piege->activite = activite;
    pik.coo_obj.x = c.x; pik.coo_obj.y = c.y;
    pik.moment = maintenant() + piege->intervalle;
    ajoute_evenement(tas, pik);
    if(activite == ACTIVE && niveau->objets[c.y][c.x][1].type == PERSONNAGE) /*le personnage se trouve sur les piques*/
        return 0;
    return 1;
}

/*exécute un Evenement, ce qui peut consister à déplacer un objet dans le Niveau,
vérifier si la partie est terminée, ajouter de nouveaux événements au Tas, ...*/
/*return 0 si le personnage est mort pendant l'exécution de l'événement et 1 sinon*/
int execute_evenement(Tas* tas, Niveau* niveau, Evenement e){
    int etat_perso = 1;
    Coordonnees c = e.coo_obj;
    TypeObjet objet_sol = niveau->objets[c.y][c.x][0].type;
    TypeObjet objet_mouvant = niveau->objets[c.y][c.x][1].type;

    /*créations des projectiles par les lanceurs*/
    if(objet_sol == LANCEUR)
        etat_perso = creation_Projectiles(tas, niveau, c);

    /*déplacement d'un projectile ou glissement d'un rocher sur la glace*/
    if(objet_mouvant == PROJECTILE || objet_mouvant == ROCHER){
        Deplacement* depl = (Deplacement*)niveau->objets[c.y][c.x][1].donnee_suppl;
        switch(depl->direction){
            case HAUT: etat_perso = deplacement_Objet(tas, niveau, depl, c, c.x, c.y-1); break;
            case BAS: etat_perso = deplacement_Objet(tas, niveau, depl, c, c.x, c.y+1); break;
            case GAUCHE: etat_perso = deplacement_Objet(tas, niveau, depl, c, c.x-1, c.y); break;
            default: etat_perso = deplacement_Objet(tas, niveau, depl, c, c.x+1, c.y); break; /*DROITE*/
        }
    }

    /*activation - désactivation des piques*/
    if(objet_sol == PIQUES){
        Piege* piege = (Piege*)niveau->objets[c.y][c.x][0].donnee_suppl;
        switch(piege->activite){
            case ACTIVE: etat_perso = changement_etat_Piques(tas, niveau, c, DESACTIVE); break;
            case DESACTIVE: etat_perso = changement_etat_Piques(tas, niveau, c, ACTIVE); break;
        }
    }

    return etat_perso;
}