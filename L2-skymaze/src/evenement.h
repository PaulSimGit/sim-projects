#ifndef __evenement__
#define __evenement__
#include "niveau.h"
#define CAPACITE_INITIALE 5


typedef struct event {
    unsigned long moment; /*moment auquel l'événement doit avoir lieu en
                          nombre de 'tick' du processeur*/
    Coordonnees coo_obj; /*objet affecté*/
} Evenement;

typedef struct tas {
    unsigned taille; /*nombre d'Evenements contenus dans le tas*/
    unsigned capacite; /*nombre de cases allouées pour les 'valeurs' (taille max)*/
    Evenement* valeurs; /*tableau d'Evenements de taille 'capacite' et dont
                        les 'taille' premières cases sont utilisées*/
} Tas;

Tas* malloc_Tas();
void free_Tas(Tas* tas);
int Fils(Tas* tas, int indice);
void change(Tas* tas, int indice, Evenement valeur);
bool un_evenement_est_pret(Tas* tas);
Evenement ote_minimum(Tas* tas);
void ote_coordo(Tas* tas, unsigned int Cx, unsigned int Cy);
void ajoute_evenement(Tas* tas, Evenement n);
Tas* construit_Tas(Niveau* niveau);
void affiche_Tas(Tas* tas);
void generation_objet(Tas* tas, Niveau* niv, TypeObjet objet, Direction d, unsigned long allure, int x, int y);
int deplacement_Objet(Tas* tas, Niveau *niveau, Deplacement* proj, Coordonnees c, unsigned int newX, unsigned int newY);
int creation_Projectiles(Tas* tas, Niveau* niv, Coordonnees coordo);
bool deplacement_perso_possible(Niveau* niveau, MLV_Button_state up, MLV_Button_state left, MLV_Button_state down, MLV_Button_state right);
int deplacement_Personnage(Tas* tas, Niveau *niveau, Coordonnees c);
int changement_etat_Piques(Tas* tas, Niveau* niveau, Coordonnees c, Activite activite);
int execute_evenement(Tas* tas, Niveau* niveau, Evenement e);


#endif