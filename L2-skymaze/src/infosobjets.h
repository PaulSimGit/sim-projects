#ifndef __infosobjets__
#define __infosobjets__


typedef enum typeobj {
    SOL = 0,
    MUR,
    LANCEUR,
    PROJECTILE,
    PERSONNAGE,
    DESTINATION,
    VIDE,
    PORTE,
    CLE,
    PIQUES,
    ROCHER
} TypeObjet;

typedef enum variantesol {
    NORMAL = 0,
    CASSE,
    CHAMPI,
    OS,
    GLACE,
    START
} VarianteSol;

typedef struct typesol {
    VarianteSol type; /*variante du sol*/
} TypeSol;

typedef enum direc {
    HAUT = 8,
    GAUCHE = 4,
    BAS = 2,
    DROITE = 6
} Direction;

typedef struct deplacement {
    Direction direction; /*direction du déplacement*/
    unsigned long allure; /*allure du déplacement*/
} Deplacement;

typedef struct genera {
    unsigned long intervalle; /*intervalle entre deux envois de projectiles*/
    unsigned long allure_proj; /*allures des projectiles envoyés*/
} Generation;

typedef enum activ {
    ACTIVE,
    DESACTIVE
} Activite;

typedef struct piege {
    Activite activite; /*état des piques*/
    unsigned long intervalle; /*intervalle d'activation des piques*/
} Piege;


#endif