#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <MLV/MLV_all.h>
#include "intro.h"


/*Affiche une image demandé pour l'Intro
On redimensionne l'image si demandé*/
/*int sleep permet de faire une pause de 'sleep' millisecondes*/
void affiche_image_Intro(const char* img, int x, int y, bool resize, int sleep){
	MLV_Image* image;
	image = MLV_load_image(img);
	if(resize)
		MLV_resize_image(image, 350, 350);
	MLV_draw_image(image, x, y);
	MLV_free_image(image);
	MLV_actualise_window();
	millisleep(sleep);
}

/*Affiche un rectangle noir pour cacher les éléments affichés de l'Intro*/
void affiche_cache_Intro(){
	MLV_draw_filled_rectangle(240, 600, 800, 200, MLV_COLOR_BLACK);
	MLV_actualise_window();
	millisleep(1200);
}

/*Affiche une succession d'images pour leur donner l'air de bouger
Utilisé pour afficher une image de l'Intro*/
void affiche_mouvement_Intro(const char* img, int x1, int x2){
	int i;
	for(i=x1; i>x2; i-=8){
		MLV_draw_filled_rectangle(i+100, 200, 300, 300, MLV_COLOR_BLACK);
		affiche_image_Intro(img, i, 200, true, 12);}
}

/*Affiche l'Intro quand on joue au mode Histoire*/
void Intro(){
	MLV_Music* music;
	music = MLV_load_music("music/story.mp3");
	MLV_play_music(music, 1.0, -1);

	MLV_draw_filled_rectangle(0, 0, 1200, 800, MLV_COLOR_BLACK);
	affiche_image_Intro("image/perso.png", 440, 150, true, 1);
	affiche_cache_Intro();

	affiche_image_Intro("image/intro/intro0.PNG", 370, 650, false, 4500);
	affiche_cache_Intro();

	affiche_image_Intro("image/intro/intro1.PNG", 230, 630, false, 4500);
	affiche_cache_Intro();

	affiche_image_Intro("image/intro/intro2.PNG", 250, 610, false, 50);

	affiche_mouvement_Intro("image/fin_alpha.png", 1199, 700);

	affiche_image_Intro("image/intro/intro3.PNG", 315, 680, false, 4000);
	affiche_cache_Intro();

	affiche_image_Intro("image/intro/intro4.PNG", 430, 650, false, 3000);

	MLV_free_music(music);
}