#ifndef __intro__
#define __intro__
#include "temps.h"


void affiche_image_Intro(const char* img, int x, int y, bool resize, int sleep);
void affiche_cache_Intro();
void affiche_mouvement_Intro(const char* img, int x1, int x2);
void Intro();


#endif