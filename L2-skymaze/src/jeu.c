#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <stdbool.h>
#include <MLV/MLV_all.h>
#include "jeu.h"
 

int jeu_libreInit(int* musik){
	int x, y; /*coordonnees de la souris*/
	int boucle; /*boucle de jeu, quand on perd et on souhaite recommencer*/
	FILE* fichier = NULL;
	char rep[40] = "niveaux/"; /*nom du dossier des niveaux*/
	char nom[40]; /*nom du fichier*/

	/*Niveau et sa taille*/
	Niveau* niveau;
	MLV_Image* image;
	MLV_Music* music;

	/*On affiche l'ecran qui demande de rentrer le nom du niveau*/
	image = MLV_load_image("image/message_libre.png");
	MLV_draw_image(image, 0, 0);
	MLV_free_image(image);
	MLV_actualise_window();

	boucle=1;

	do{

		/*On rentre dans un premier temps le nom du fichier auquel on souhaite lire
		sans extension .txt qui se trouve dans le dossier 'niveaux'*/
		printf("\nRentrez le nom du fichier.txt contenant le niveau:\n");
		scanf("%s", nom);

		if(strcmp(nom, "nope") != 0){
		    	strcat(rep, nom);
		    	strcat(rep, ".txt");
			fichier = fopen(rep, "r");

			/*Si le fichier existe*/
			if(fichier != NULL){
				niveau = lecture_Fichier(fichier);
				/*Si le niveau est conforme (taille, debut, fin)*/
				if(niveau!=NULL){
					sprintf(nom, "nope");
				}
				sprintf(rep, "niveaux/");
			}
			/*Si le fichier n'existe pas*/
			else{
				printf("\nLe fichier %s n'existe pas dans le dossier niveaux.", rep);
				sprintf(rep, "niveaux/");
			}

		}

		/*Si on veut quitter en rentrant le mot "nope" dans le terminal*/
		else{
			printf("\nRetour au menu principal.\nVeuillez retourner sur la fenetre de jeu.\n");
			return 0;
		}
	
	}while(strcmp(nom, "nope") != 0);

	image = MLV_load_image("image/message_libre2.png");
	MLV_draw_image(image, 0, 0);
	MLV_free_image(image);
	MLV_actualise_window();
	/*Message pour dire que le niveau a bien ete charge*/
	do{
		MLV_wait_mouse(&x, &y);
		/*Si tout compte fait on veut quitter*/
		if((x>63 && x<542) && (y>535 && y<635))
			return 0;
	}while(!( (x>657 && x<1136) && (y>535 && y<635) ));

	/*On joue la musique*/
	if(*musik==0){
		music = MLV_load_music("music/jeu0.mp3");
	}
	if(*musik==1){
		music = MLV_load_music("music/jeu1.mp3");
	}
	if(*musik==2){
		music = MLV_load_music("music/jeu2.mp3");
	}
	MLV_play_music(music, 1.0, -1);

	/*Le jeu commence*/
	do{
		boucle=jeu_partie(niveau);
		rewind(fichier);
		niveau = lecture_Fichier(fichier);
	}while(boucle==1);

	MLV_free_music(music);
	fclose(fichier);

	music = MLV_load_music("music/menu.mp3");
	MLV_play_music(music, 1.0, -1);

	return 0;
}


int jeu_histoireInit(int* musik){
	int i; /*parcours l'indice des niveaux en mode histoire*/
	int x, y; /*coordonnees de la souris*/
	int boucle_i; /*boucle qui permet de traverser tout les niveaux en mode histoire*/
	int boucle; /*boucle de jeu, quand on perd et on souhaite recommencer*/
	FILE* fichier = NULL;
	char rep[40]; /*nom du niveau dans mode histoire*/

	/*Niveau et sa taille*/
	Niveau* niveau;
	MLV_Image* image;
	MLV_Music* music;

	/*On affiche a l'ecran si on veut jouer ou pas*/
	image = MLV_load_image("image/message_histoire.png");
	MLV_draw_image(image, 0, 0);
	MLV_free_image(image);
	MLV_actualise_window();

	i=0;
	boucle=1;
	boucle_i=1;

	/*Clic si on veut jouer ou pas*/
	do{
		MLV_wait_mouse(&x, &y);
		/*Si tout compte fait on veut quitter*/
		if((x>63 && x<542) && (y>535 && y<635))
			return 0;
	}while(!( (x>657 && x<1136) && (y>535 && y<635) ));

	Intro(); /*Intro du mode Histoire*/

	/*On traverse tout les niveaux en mode histoire*/
	do{
		sprintf(rep, "niveaux/histoire/niveau%d.txt", i);
		fichier = fopen(rep, "r");

		if(fichier != NULL){
			/*On initialise le plateau*/
			niveau = lecture_Fichier(fichier);
			/*Si le niveau n'est pas conforme (taille, debut, fin)*/
			if(niveau==NULL){
				boucle_i=0;
			}

			/*Si le niveau est conforme (taille, debut, fin)*/
			else{
				/*On joue la musique une seule fois, sauf si le niveau0 n'est pas conforme*/
				if(i==0){
					if(*musik==0){
						music = MLV_load_music("music/jeu0.mp3");
					}
					if(*musik==1){
						music = MLV_load_music("music/jeu1.mp3");
					}
					if(*musik==2){
						music = MLV_load_music("music/jeu2.mp3");
					}
					MLV_play_music(music, 1.0, -1);
				}

				/*Le jeu commence*/
				do{
					boucle=jeu_partie(niveau);
					rewind(fichier);
					niveau = lecture_Fichier(fichier);
				}while(boucle==1);

				/*Si on a pas quitté la partie*/
				if(boucle != -1){
					i++;
					boucle=1;
				}
				else{
					boucle_i=0;
				}

			}

			fclose(fichier);

		}

		else{
			boucle_i=0;
		}

	}while(boucle_i==1);
	MLV_free_music(music);
	boucle_i=1;

	/* Reussi au moins un niveau, et si on a lu tous les niveaux du dossier Histoire */
	if(i>0 && boucle!=-1){
		MLV_clear_window( MLV_COLOR_BLACK );
		music = MLV_load_music("music/hateno.mp3");
		MLV_play_music(music, 1.0, -1);
		image = MLV_load_image("image/credits.png");
		MLV_draw_image(image, 0, 0);
		MLV_free_image(image);
		MLV_actualise_window();
		do{
			MLV_wait_mouse(&x, &y);
			if((x>509 && x<690) && (y>694 && y<763))
				boucle_i=0;
		}while(boucle_i==1);
		MLV_free_music(music);
	}

	/* Bug des le niveau0.txt */
	if(i==0 && boucle!=-1){
		image = MLV_load_image("image/message_bug.png");
		MLV_draw_image(image, 0, 0);
		MLV_free_image(image);
		MLV_actualise_window();
		do{
			MLV_wait_mouse(&x, &y);
			if((x>509 && x<690) && (y>531 && y<600))
				boucle_i=0;
		}while(boucle_i==1);
	}

	music = MLV_load_music("music/menu.mp3");
	MLV_play_music(music, 1.0, -1);

	return 0;
}

/*boucle principale d'une partie d'un Niveau
renvoie -1 si on veut quitter, 1 si on veut recommencer et 0 si on a fini le niveau actuel*/
int jeu_partie(Niveau* level){
	int x = -1, y = -1; /*coordonnees de la souris*/
	int boucle = 1; /*boucle de jeu*/
	int verif_perso = 1; /*vérifie l'état du perso*/
	MLV_Image* image;
	MLV_Sound* son;
	MLV_Button_state souris, up, left, down, right;
	unsigned long dernier_mv_perso;
	Coordonnees perso;

	son = MLV_load_sound("music/start.wav");
	MLV_play_sound(son, 1.0);

	MLV_clear_window(MLV_COLOR_BLACK);
	image = MLV_load_image("image/jeu.png");
	MLV_draw_image(image, 0, 0);
	MLV_free_image(image);
	Tas* tas = construit_Tas(level);

	do{
		/*affiche_Niveau_terminal(level);*/
		affiche_Niveau(level);
		MLV_actualise_window();
		if(verif_perso == 2 || verif_perso == 3){
			if(verif_perso == 3) /* si on rentre ici c'est qu'on a déjà fini la partie*/
				boucle = 0;
			break;
		}
		souris = MLV_RELEASED; up = MLV_RELEASED; left = MLV_RELEASED;
		down = MLV_RELEASED; right = MLV_RELEASED;

		if(un_evenement_est_pret(tas)){
			Evenement e = ote_minimum(tas); /*exécute un évenement*/
			if(execute_evenement(tas, level, e) == 0)
				verif_perso = 2; /*on a perdu*/
		}
		else{
			millisleep(10);
			MLV_get_mouse_position(&x, &y);
			souris = MLV_get_mouse_button_state(MLV_BUTTON_LEFT);
			if((x>6 && x<137) && (y>764 && y<793) && souris == MLV_PRESSED){
				verif_perso = 2; boucle = 2; break;} /*on veut quitter pendant la partie*/

			/* Si on appuie sur les touches POUR BOUGER */
			up = MLV_get_keyboard_state(MLV_KEYBOARD_UP);
			left = MLV_get_keyboard_state(MLV_KEYBOARD_LEFT);
			down = MLV_get_keyboard_state(MLV_KEYBOARD_DOWN);
			right = MLV_get_keyboard_state(MLV_KEYBOARD_RIGHT);

			if(level->depl_perso_autorise && deplacement_perso_possible(level, up, left, down, right)){
				perso = level->coo_perso;
				if(up == MLV_PRESSED)
					level->coo_perso.y --;
				else if(left == MLV_PRESSED)
					level->coo_perso.x --;
				else if(down == MLV_PRESSED)
					level->coo_perso.y ++;
				else if(right == MLV_PRESSED)
					level->coo_perso.x ++;

				/*déplacement du personnage*/
				verif_perso = deplacement_Personnage(tas, level, perso);
				dernier_mv_perso = maintenant();
				level->depl_perso_autorise = false;
			}
			else{
				if(maintenant() >= dernier_mv_perso + level->allure_perso)
					level->depl_perso_autorise = true;
			}
		}
	}while(boucle==1);

	MLV_free_sound(son);
	millisleep(500); /*petit délai pour montrer au joueur qu'il a vraiment fini le jeu*/
	free_Tas(tas);
	free_Niveau(level);

	if(boucle == 0){
		/* Gagné */
		son = MLV_load_sound("music/clear.wav");
		MLV_play_sound(son, 1.0);
		image = MLV_load_image("image/message_fin.png");
		MLV_draw_image(image, 0, 0);
		MLV_free_image(image);
		MLV_actualise_window();
		do{
			MLV_wait_mouse(&x, &y);
			if((x>509 && x<690) && (y>531 && y<600)){
				MLV_free_sound(son); return 0;}
		}while(1);
	}
	else{
		son = MLV_load_sound("music/death.wav");
		MLV_play_sound(son, 1.0);
		if(boucle == 2) /*abandon*/
			image = MLV_load_image("image/message_abandon.png");
		else /*perdu*/
			image = MLV_load_image("image/message_mort.png");
		MLV_draw_image(image, 0, 0);
		MLV_free_image(image);
		MLV_actualise_window();
		do{
			MLV_wait_mouse(&x, &y);
			/* On souhaite recommencer */
			if((x>657 && x<1136) && (y>53 && y<635)){
				MLV_free_sound(son); return 1;}
			/* On souhaite quitter */
			if((x>63 && x<542) && (y>535 && y<635)){
				MLV_free_sound(son); return -1;}
		}while(1);
	}
}