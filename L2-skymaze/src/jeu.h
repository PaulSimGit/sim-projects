#ifndef __jeu__
#define __jeu__
#include "evenement.h"
#include "intro.h"


int jeu_libreInit(int* musik);
int jeu_histoireInit(int* musik);
int jeu_partie(Niveau* level);


#endif
