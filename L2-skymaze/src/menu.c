#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <MLV/MLV_all.h>
#include "menu.h"


/* Menu */
void ecran_titre(int *main, int *musik){
    int x=0, y=0, ecran=1;
	char song[25];

	MLV_Music* music;
	MLV_Sound* son;
	MLV_Image* image;

	image = MLV_load_image("image/ecran_titre.png");
	MLV_draw_image(image, 0, 0);
	MLV_free_image(image);

	/* Boucle */
	do{
		/* Affichage */
		image = MLV_load_image("image/music_non.png");
		MLV_draw_image(image, 408, 472);
		MLV_draw_image(image, 564, 472);
		MLV_draw_image(image, 720, 472);
		MLV_free_image(image);
		image = MLV_load_image("image/music_ok.png");
		switch(*musik){
			case 0:MLV_draw_image(image, 408, 472);break;
			case 1:MLV_draw_image(image, 564, 472);break;
			default:MLV_draw_image(image, 720, 472);break;
		}
		MLV_free_image(image);
		sprintf(song,"music/jeu%d.mp3", *musik);
	  	MLV_actualise_window();

		/* On attend un clic */
		MLV_wait_mouse(&x, &y);

		son = MLV_load_sound("music/select.wav");
		MLV_play_sound(son, 1.0);

		/* Mode histoire */
		if((x>190 && x<573) && (y>232 && y<304))
			ecran = jeu_histoireInit(musik);

		/* Mode libre */
		if((x>626 && x<1009) && (y>232 && y<304))
			ecran = jeu_libreInit(musik);

		/* Mode editeur */
		if((x>408 && x<792) && (y>352 && y<424)){
			music = MLV_load_music("music/editeur.mp3");
			MLV_play_music(music, 1.0, -1);
			ecran = editeur();
			MLV_free_music(music);
			music = MLV_load_music("music/menu.mp3");
			MLV_play_music(music, 1.0, -1);
		}

		/* Choisit la musique 0 */
		if((x>408 && x<480) && (y>472 && y<544))
			*musik=0;

		/* Choisit la musique 1 */
		if((x>564 && x<636) && (y>472 && y<544))
			*musik=1;

		/* Choisit la musique 2 */
		if((x>720 && x<792) && (y>472 && y<544))
			*musik=2;
		
		/* On quitte */
		if((x>408 && x<792) && (y>592 && y<664)){
			*main=0;
			ecran=0;}
			
		MLV_free_sound(son);
	}while(ecran==1);
}
