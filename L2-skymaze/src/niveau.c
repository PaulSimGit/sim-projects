#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <MLV/MLV_all.h>
#include "niveau.h"


/* Effectue les malloc's pour obtenir un Niveau* de la taille donnee */
Niveau* malloc_Niveau(Coordonnees dimension){
	Niveau* level;
	level = (Niveau*)malloc(sizeof(Niveau));

	if(level != NULL){
		level->taille.x = dimension.x;
		level->taille.y = dimension.y;
		level->objets = init_Objets(dimension.x, dimension.y);
	}	
	else
        exit(EXIT_FAILURE);
	return level;
}

/* Libere la memoire prise par un Niveau */
void free_Niveau(Niveau* niveau){
    unsigned int i, j, k;
	TypeObjet type;
	for(i=0; i<niveau->taille.y; i++){
		for(j=0; j<niveau->taille.x; j++){
			for(k=0; k<2; k++){
				type = niveau->objets[i][j][k].type;
				if(type == LANCEUR || type == PROJECTILE || type == SOL || type == ROCHER)
					free(niveau->objets[i][j][k].donnee_suppl);
			}
			free(niveau->objets[i][j]);
		}
		free(niveau->objets[i]);
	}
	free(niveau->objets);
	free(niveau);
    niveau = NULL;
}

/* Affiche dans le terminal le Niveau donnee en argument */
void affiche_Niveau_terminal(Niveau* niveau){
	int verif = 0;
	unsigned int i, j;
	for(i=0; i<niveau->taille.y; i++){
		for(j=0; j<niveau->taille.x; j++){
			switch(niveau->objets[i][j][1].type){
				case PERSONNAGE: printf("P"); verif=1; break;
				case PROJECTILE: affiche_Projectile_terminal(niveau->objets[i][j][1]); verif=1; break;
				case ROCHER: printf("*"); verif=1; break;
				default: break;
			}
			if(verif != 1){
				switch(niveau->objets[i][j][0].type){
					case SOL: affiche_Sol_terminal(niveau->objets[i][j][0]); break;
					case LANCEUR: printf("O"); break;
					case MUR: printf("#"); break;
					case DESTINATION: printf("D"); break;
					case PORTE: printf("T"); break;
					case CLE: printf("K"); break;
					case PIQUES: affiche_Piques_terminal(niveau->objets[i][j][0]); break;
					default: printf("#"); break;
				}
			}
			verif = 0;
		}
		printf("\n");
	}
}

/* Affiche le Niveau dans l'interface MLV*/
void affiche_Niveau(Niveau* niveau){
	MLV_Image* image;
	unsigned int i, j, decal_x, decal_y;
	int obj1 = 0;

	/* Pour centrer le plateau */
	decal_x = (1200-(niveau->taille.x)*48)/2;
	decal_y = (800-(niveau->taille.y)*48)/2;

	MLV_draw_filled_rectangle(decal_x-24, decal_y-24, (niveau->taille.x)*48+48, (niveau->taille.y)*48+48, MLV_COLOR_BLACK);
	
	/* On parcourt le plateau et affiche toutes les images */
	for(i=0; i<niveau->taille.y; i++){
		for(j=0; j<niveau->taille.x; j++){
			switch(niveau->objets[i][j][0].type){ /*objets fixes*/
				case SOL: image = affiche_Sol(niveau->objets[i][j][0]); break;
				case MUR: image = MLV_load_image("image/mur.png"); break;
				case LANCEUR: image = MLV_load_image("image/obstacle.png"); break;
				case DESTINATION: image = MLV_load_image("image/fin.png"); break;
				case PORTE: image = MLV_load_image("image/cle_mur.png"); break;
				case CLE: image = MLV_load_image("image/cle.png"); break;
				case PIQUES: image = affiche_Piques(niveau->objets[i][j][0]); break;
				default: image = MLV_load_image("image/mur.png"); break;
			}
			MLV_draw_image(image, j*48+decal_x, i*48+decal_y);
			MLV_free_image(image);

			switch(niveau->objets[i][j][1].type){ /*objets mouvants*/
				case PERSONNAGE: image = MLV_load_image("image/perso.png"); obj1 = 1; break;
				case PROJECTILE: image = affiche_Projectile(niveau->objets[i][j][1]); obj1 = 1; break;
				case ROCHER: image = MLV_load_image("image/pousse_alpha.png"); obj1 = 1; break;
				default: obj1 = 0; break;
			}
			if(obj1 == 1){
				MLV_draw_image(image, j*48+decal_x, i*48+decal_y);
				MLV_free_image(image);}
		}
	}
}

/* On calcule la dimension du Niveau lu dans le fichier texte */
Coordonnees lecture_Dimension(FILE* fichier){
	char charActuel;
	int x = -1, x_max = -1, y_max = 0;
	Coordonnees coord;

	do{
		charActuel = fgetc(fichier);
		x++;
		if(charActuel == '\n'){
			if(x > x_max)
				x_max = x;
			x = -1;
			y_max++;
		}
	}while(charActuel != EOF);

	/*En soit la creation du niveau peut prendre une taille infini tant qu'on peut
	allouer de la place, cependant la fenetre n'est pas assez grande, donc on
	doit restreindre le niveau a la taille max d'un niveau qu'on peut creer dans
	l'editeur, c'est a dire 22 cases en longeur et 14 en hauteur
	Si ca depasse, on renvoie des coordonnees negatives*/
	if(x_max>22 || y_max>14){
		coord.x = -1;
		coord.y = -1;}
	else{
		coord.x = x_max;
		coord.y = y_max;}

	return coord;
}

/*crée un Sol du type demandé dans le niveau*/
void creer_Sol(Niveau* level, int x, int y, VarianteSol type){
	TypeSol* info = (TypeSol*)malloc(sizeof(TypeSol));
	info->type = type;
	level->objets[y][x][0].type = SOL;
	level->objets[y][x][0].donnee_suppl = info;
}

/*On lit bien chaque caractere du fichier pour creer le Niveau 
On fait attention s'il y a bien 1 seul depart et au moins une seule arrivee*/
/*les paramètres unsigned long servent à donner les informations générales des lanceurs*/
/*renvoie 1 si le Niveau est créé correctement et 0 sinon*/
int creation_Niveau(FILE* fichier, Niveau* niv){
	char charActuel;
	int x = -1, y = 0;
	int ok_p = 0, ok_d = 0;

	do{ /*lecture caractères*/
		charActuel = fgetc(fichier);
		x++;
		switch(charActuel){ /*ajout des objets*/
			case '.': creer_Sol(niv, x, y, NORMAL); break;
            case ' ': creer_Sol(niv, x, y, NORMAL); break;
			case ',': creer_Sol(niv, x, y, CASSE); break;
			case ';': creer_Sol(niv, x, y, CHAMPI); break;
			case ':': creer_Sol(niv, x, y, OS); break;
			case '-': creer_Sol(niv, x, y, GLACE); break;
			case '#': niv->objets[y][x][0].type = MUR; break;
			case '!': niv->objets[y][x][0].type = MUR; break;
			case 'D': niv->objets[y][x][0].type = DESTINATION; ok_d++; break;
			case 'K': niv->objets[y][x][0].type = CLE; break;
			case 'T': niv->objets[y][x][0].type = PORTE; break;
			case 'O': niv->objets[y][x][0].type = LANCEUR; /*informations générales d'un lanceur*/
						Generation* gen = (Generation*)malloc(sizeof(Generation));
						gen->intervalle = une_seconde;
						gen->allure_proj = une_milliseconde*300;
						niv->objets[y][x][0].donnee_suppl = gen; break;
			case 'P': niv->objets[y][x][1].type = PERSONNAGE; ok_p++; /*initialisation du perso*/
						niv->coo_perso.x = x; niv->coo_perso.y = y;
						creer_Sol(niv, x, y, START); break;
			case '+': niv->objets[y][x][0].type = PIQUES; /*informations générales des piques*/
						Piege* piege = (Piege*)malloc(sizeof(Piege));
						piege->activite = DESACTIVE;
						piege->intervalle = une_milliseconde*(700+(100*(rand()%8)));
						niv->objets[y][x][0].donnee_suppl = piege; break;
			case '*': niv->objets[y][x][1].type = ROCHER;
						Deplacement* depl = (Deplacement*)malloc(sizeof(Deplacement));
						depl->allure = une_milliseconde*300;
						niv->objets[y][x][1].donnee_suppl = depl;
						creer_Sol(niv, x, y, NORMAL); break;
			case '\n': x=-1; y++; break;
            case EOF: break;
			default: niv->objets[y][x][0].type = MUR; break;
		}
	}while(charActuel != EOF);

	/*informations du personnage*/
	niv->allure_perso = une_milliseconde*140;
	niv->depl_perso_autorise = true;
	niv->nb_cle = 0;

	/*vérifie si on a une unique sortie et un unique perso*/
	if(ok_p != 1 || ok_d != 1){
		free_Niveau(niv);
		return 0;}
	return 1;
}

/* Lit un fichier texte pour le convertir en Niveau */
Niveau* lecture_Fichier(FILE* fichier){
	
	int rep; /*  Si le Niveau possede un seul depart et une seule arrivee  */

	Niveau* level = NULL;
	Coordonnees dimension;

	/*On calcule la dimension du Niveau lu par dans le fichier texte
	On fait attention si le niveau a une taille ne depassant pas la fenetre*/
	dimension = lecture_Dimension(fichier);
	/* Verification si le Niveau possede des coordoonees ne depassant pas la fenetre */
	if(dimension.x!=(unsigned)-1 && dimension.y!=(unsigned)-1){
		/* On alloue de la place en fonction de la dimension calculee juste avant */
		level = malloc_Niveau(dimension);

		/*On lit bien chaque caractere du fichier pour creer le Niveau 
		On fait attention s'il y a bien 1 seul depart et 1 seule arrivee*/
		rewind(fichier);
		rep = creation_Niveau(fichier, level);

		/* Construction du plateau et verification si le Niveau possede un seul depart et une seule arrivee */
		if(rep == 0){
			printf("\nVotre niveau ne possede pas un seul debut (P) ou au moins une arrivee (D)\nImpossible de jouer.\nVeuillez retourner sur la fenetre de jeu.\n");
			return NULL;}
		printf("\nLecture reussie! Veuillez retourner sur la fenetre de jeu.\n");
	}
	else
		printf("\nVotre niveau est trop grand pour etre affiche dans la fenetre.\nIl ne doit pas depasser 22 cases de longueur et 14 cases en hauteur.\nVeuillez retourner sur la fenetre de jeu.\n");

	return level;
}