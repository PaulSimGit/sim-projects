#ifndef __niveau__
#define __niveau__
#include <MLV/MLV_all.h>
#include "objet.h"
#include "temps.h"


typedef struct coordo {
    unsigned x;
    unsigned y;
} Coordonnees;

typedef struct level {
    Coordonnees taille; /*taille du niveau*/
    Objet*** objets; /*tableau bi-dimensionnel de largeur taille.x, de hauteur taille.y,
                    et dont chaque case contient un Objet*/
    Coordonnees coo_perso; /*position actuelle du personnage*/
    unsigned long allure_perso; /*allure du personnage*/
    bool depl_perso_autorise; /*vaut 'true' si le temps écoulé depuis le dernier déplacement
                              du personnage est supérieur à son allure*/
    int nb_cle; /*nb de clés que possède le personnage*/
} Niveau;


Niveau* malloc_Niveau(Coordonnees dimension);
void free_Niveau(Niveau* niveau);
void affiche_Niveau_terminal(Niveau* niveau);
void affiche_Niveau(Niveau* niveau);
Coordonnees lecture_Dimension(FILE* fichier);
void creer_Sol(Niveau* level, int x, int y, VarianteSol type);
int creation_Niveau(FILE* fichier, Niveau* niv);
Niveau* lecture_Fichier(FILE* fichier);


#endif