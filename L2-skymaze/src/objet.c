#include <stdio.h>
#include <stdlib.h>
#include <MLV/MLV_all.h>
#include "objet.h"

/* On initialise le tableau tri-directionnel du Niveau concernant les objets */
Objet*** init_Objets(int x, int y){
	int i, j;
	Objet ***tab;
	tab = (Objet ***)malloc(sizeof(Objet **) * y);
	if(tab == NULL)
		exit(EXIT_FAILURE);
    
	for(i=0; i<y; i++){
		tab[i] = (Objet **)malloc(sizeof(Objet*) * x);
		if(tab[i] == NULL)
			exit(EXIT_FAILURE);
		for(j=0; j<x; j++){
			tab[i][j] = (Objet *)malloc(sizeof(Objet) * 2);
			if(tab[i][j] == NULL)
				exit(EXIT_FAILURE);
			tab[i][j][0].type = MUR; /*objets de bases qui seront modifiés par la suite*/
			tab[i][j][1].type = VIDE;
		}
	}
	return tab;
}

/*affiche le Sol dans le terminal*/
void affiche_Sol_terminal(Objet sol){
	TypeSol* type_sol = (TypeSol*)sol.donnee_suppl;
	switch(type_sol->type){
		case GLACE: printf("-"); break;
		default: printf("."); break; /*tous les autres types de sols*/
	}
}

/*affiche un projectile en fonction de sa direction dans le terminal*/
void affiche_Projectile_terminal(Objet proj){
	Deplacement* depla = (Deplacement*)proj.donnee_suppl;
	switch(depla->direction){
		case HAUT: printf("^"); break;
		case GAUCHE: printf("<"); break;
		case BAS: printf("v"); break;
		default: printf(">"); break; /*DROITE*/
	}
}

/*affiche les piques en fonction de son activité dans le terminal*/
void affiche_Piques_terminal(Objet pik){
	Piege* piege = (Piege*)pik.donnee_suppl;
	switch(piege->activite){
		case ACTIVE: printf("+"); break;
		default: printf("."); break; /*DESACTIVE*/
	}
}

/*charge l'image du sol demandé et la renvoie*/
MLV_Image* affiche_Sol(Objet sol){
	MLV_Image* image;
	TypeSol* typeSol = (TypeSol*)sol.donnee_suppl;
	switch(typeSol->type){
		case NORMAL: image = MLV_load_image("image/sol_1.png"); break;
		case CASSE: image = MLV_load_image("image/sol_2.png"); break;
		case CHAMPI: image = MLV_load_image("image/sol_3.png"); break;
		case OS: image = MLV_load_image("image/sol_4.png"); break;
		case GLACE: image = MLV_load_image("image/glace.png"); break;
		case START: image = MLV_load_image("image/debut.png"); break;
	}
	return image;
}

/*charge l'image du projectile demandé et la renvoie*/
MLV_Image* affiche_Projectile(Objet proj){
	MLV_Image* image;
	Deplacement* depla = (Deplacement*)proj.donnee_suppl;
	switch(depla->direction){
		case HAUT: image = MLV_load_image("image/fleche_h.png"); break;
		case GAUCHE: image = MLV_load_image("image/fleche_g.png"); break;
		case BAS: image = MLV_load_image("image/fleche_b.png"); break;
		case DROITE: image = MLV_load_image("image/fleche_d.png"); break;
	}
	return image;
}

/*charge l'image des Piques demandé et la renvoie*/
MLV_Image* affiche_Piques(Objet pik){
	MLV_Image* image;
	Piege* piege = (Piege*)pik.donnee_suppl;
	switch(piege->activite){
		case ACTIVE: image = MLV_load_image("image/pique.png"); break;
		default: image = MLV_load_image("image/pique_off.png"); /*DESACTIVE*/
	}
	return image;
}