#ifndef __objet__
#define __objet__
#include "infosobjets.h"


typedef struct obj {
    TypeObjet type; /*type de l'objet*/
    void* donnee_suppl; /*Donnée supplémentaire: son type dépend de 'type' ci-dessus:
                        - Deplacement si type == PROJECTILE ou BLOC
                        - Generation si type == LANCEUR
                        - VarianteSol si type == SOL
                        - Piege si type == PIQUES
                        - donnee_suppl vaut NULL pour les autres types*/
} Objet;


Objet*** init_Objets(int x, int y);
void affiche_Sol_terminal(Objet sol);
void affiche_Projectile_terminal(Objet proj);
void affiche_Piques_terminal(Objet pik);
MLV_Image* affiche_Sol(Objet sol);
MLV_Image* affiche_Projectile(Objet proj);
MLV_Image* affiche_Piques(Objet pik);


#endif