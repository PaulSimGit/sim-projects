#ifndef __temps__
#define __temps__
#include <sys/select.h>
#include <time.h>


long unsigned maintenant();
static const long unsigned une_seconde = CLOCKS_PER_SEC;
static const long unsigned une_milliseconde = une_seconde/1000;
void millisleep(unsigned long i);


#endif