#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <MLV/MLV_all.h>
#include "menu.h"


int main(void){
	int global = 1, musik = 0;
	MLV_Music* music;
	srand(time(NULL));

	MLV_create_window("The Sky Maze", "The Sky Maze", 1200, 800);
	MLV_init_audio();
	music = MLV_load_music("music/menu.mp3");
	MLV_play_music(music, 1.0, -1);

	do{
		ecran_titre(&global, &musik);
		MLV_clear_window(MLV_COLOR_BLACK);
	}while(global == 1);

	MLV_free_music(music);
	MLV_free_audio();
	MLV_free_window();
  	return 0;
}