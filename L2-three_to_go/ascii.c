#include <stdio.h>
#include <stdlib.h>
#include <time.h>


// La nouvelle variable Token
typedef struct{

	int couleur;
	int forme;

}Token;



// La fonction renvoie un nombre au hasard entre 0 et 3
int hasard(void){

	int a;
  
	a = rand() % 4;

return a;

}



// Convertie un nombre en premiere lettre de la forme du Token
int nbr_forme(int x){

	char a;
  
	switch(x){
    	case 0 : a='O'; break;
    	case 1 : a='C'; break;
    	case 2 : a='T'; break;
    	case 3 : a='D'; break;
    	default : a='9'; break;
	}

return a;

}



// Convertie un nombre en premiere lettre de la couleur du Token
int nbr_couleur(int x){

	char a;
  
	switch(x){
    	case 0 : a='r'; break;
    	case 1 : a='v'; break;
    	case 2 : a='b'; break;
    	case 3 : a='j'; break;
    	default : a='9'; break;
	}

return a;

}



// Corps du programme
int main(int argc, char *argv[]){

	/*
	INT:
	- i : pour l'incrementation en general
	- j : pareil que i, utile dans des boucles ou il y a deja i
	- jeu : pour savoir quand c'est fini (=1 en cours, =0 fini)
	- combo_coul : si =3, alors il y a 3 tokens consecutifs de la meme couleur
	- combo_val_coul : la valeur verifiee pour les combos lies a la couleur
	- combo_form : si =3, alors il y a 3 tokens consecutifs de la meme  forme
	- combo_val_form : la valeur verifiee pour les combos lies a la forme
	- combo_ind : l'indice du dernier token similaire
	- combo : =0 si pas de combo, sinon =x avec x le nombre de combo
	- rep2 : pour choisir l'index du token auquel on veut faire une inversion
	
	CHAR:
	- rep : pour savoir si on met le token a gauche ou a droite
	
	TOKEN:
	- tab_att : le tableau de tokens en attente
	- tab_jeu1 : le tableau de tokens qui se trouvent sur le plateau de jeu
	- tab_jeu2 : un tableau de copie, qui permet a la liste tab_jeu1 d'avoir un token au debut
	- inversion : token de stockage lors d'une inversion
	*/
	int i, j, jeu=1, nbr=1, combo_coul, combo_val_coul, combo_ind, combo_form, combo_val_form, combo, rep2=-1;
	char rep='n';
	Token tab_att[5];
	Token* tab_jeu1 = NULL;
	Token* tab_jeu2 = NULL;
	Token inversion;
	
	// La fonction aleatoire a initialisee
	srand(time (NULL));
	
	// On initialise la liste des tokens du plateau et sa copie 
	tab_jeu1 = malloc(nbr * sizeof(Token));
	tab_jeu2 = malloc(nbr * sizeof(Token));
	
	// On initialise les tokens en attente
	for(i=0;i<5;i++){
		tab_att[i].couleur=hasard();
		tab_att[i].forme=hasard();
	}
	
	// On initialise le premier token sur le plateau de jeu
	tab_jeu1[0].couleur=hasard();
	tab_jeu1[0].forme=hasard();
	
	// Que le jeu commence !
	do{
		
		printf("Tokens a venir :\n");
		
		// On affiche les tokens en attente
		for(i=0;i<5;i++){
			printf("%c ", nbr_forme(tab_att[i].forme));
			printf("%c ", nbr_couleur(tab_att[i].couleur));
			if(i!=4){
				printf("--> ");
			}
			else{
				printf("<<< PROCHAIN TOKEN\n\nPlateau de jeu :\n");
			}
		}
		
		// On affiche les tokens sur le plateau de jeu
		for(i=0;i<nbr;i++){
			printf("%c ", nbr_forme(tab_jeu1[i].forme));
			printf("%c ", nbr_couleur(tab_jeu1[i].couleur));
			if(i!=nbr-1){
				printf("--> ");
			}
			else{
				printf("\n\nQue voulez vous faire ?\n");
				printf("Pour placer le prochain token a gauche du plateau, rentrez 'a'.\n");
				printf("Pour placer le prochain token a droite du plateau, rentrez 'z'.\n");
				printf("Pour permuter des tokens de meme couleur, rentrez 'c'.\n");
				printf("Pour permuter des tokens de meme forme, rentrez 'f'.\n");
			}
		}
		
		// Saisie controlee, pour choisir entre mettre le prochain token a droite ou a gauche
		do{
			scanf("%c", &rep);
		}while(rep!='a' && rep!='z' && rep!='c' && rep!='f');
		
		do{
			
			// Si on place un token
			if(rep=='a' || rep=='z'){
			
				/* La taille du plateau de jeu augmente vu qu'on rajoute un token, donc la liste de copie
				augmente aussi grace a malloc*/	
				nbr++;
				tab_jeu2 = malloc(nbr * sizeof(Token));
				
				// On place le token au debut de la table
				if(rep=='a'){
					tab_jeu2[0]=tab_att[4];
					for(i=1;i<nbr;i++){
						tab_jeu2[i]=tab_jeu1[i-1];
					}	
				}
				// On place le token a la fin de la table
				if(rep=='z'){
					for(i=0;i<nbr-1;i++){
						tab_jeu2[i]=tab_jeu1[i];
					}
					tab_jeu2[nbr-1]=tab_att[4];
				}
				
				// On decale les tokens en attente
				for(i=4;i>0;i--){
					tab_att[i]=tab_att[i-1];
				}
				// On place le nouveau token en premier dans la table en attente
				tab_att[0].couleur=hasard();
				tab_att[0].forme=hasard();
			}
			
			// Si on effectue juste une permutation
			if(rep=='c' || rep=='f'){
				
				tab_jeu2 = malloc(nbr * sizeof(Token));
				
				printf("Choisissez l'index d'un token : \n");
				
				do{
					scanf("%d",&rep2);
				}while(rep2<0 || rep2>nbr-1);
				
				j=0;
				
				if(rep=='f'){
					combo_val_form=tab_jeu1[rep2].forme;
					for(i=0;i<nbr;i++){
						if(tab_jeu1[i].forme==combo_val_form){
							tab_jeu2[j]=tab_jeu1[i];
							j++;
							tab_jeu1[i].couleur=9;
							tab_jeu1[i].forme=9;
						}
					}
				}
				
				if(rep=='c'){
					combo_val_coul=tab_jeu1[rep2].couleur;
					for(i=0;i<nbr;i++){
						if(tab_jeu1[i].couleur==combo_val_coul){
							tab_jeu2[j]=tab_jeu1[i];
							j++;
							tab_jeu1[i].couleur=9;
							tab_jeu1[i].forme=9;
						}
					}
				}

				rep2=j-1;
				j=rep2-1;
				for(i=0;i<nbr;i++){
					if(tab_jeu1[i].couleur==9 && tab_jeu1[i].forme==9){
						j++;
						if(j>rep2){
							j=0;
						}
						tab_jeu1[i]=tab_jeu2[j];
					}
				}
				
				tab_jeu2 = malloc(nbr * sizeof(Token));
				for(i=0;i<nbr;i++){
					tab_jeu2[i]=tab_jeu1[i];
				}
				
			}
			

				
			// On verifie s'il y a 3 ou + de tokens consecutifs qui ont la meme forme ou meme couleur
			combo_form=1;
			combo_coul=1;
			combo=0;
			combo_val_form=tab_jeu2[0].forme;
			combo_val_coul=tab_jeu2[0].couleur;
			for(i=1;i<nbr;i++){
				// Verification de combos de forme
				if(combo_val_form==tab_jeu2[i].forme){
					combo_form++;
				}
				else{
					combo_form=1;
					combo_val_form=tab_jeu2[i].forme;
				}
				if(combo_form>=3){
					combo=combo_form;
					combo_ind=i;
				}
				// Verification de combos de couleurs
				if(combo_val_coul==tab_jeu2[i].couleur){
					combo_coul++;
				}
				else{
					combo_coul=1;
					combo_val_coul=tab_jeu2[i].couleur;
				}
				if(combo_coul>=3){
					combo=combo_coul;
					combo_ind=i;
				}
			}
			
			// S'il y a eu un combo
			if(combo>=3){
				printf("\nCombo Token!\n");
				// Il y a eu un combo, on remplace ces tokens par des 9
				for(i=combo_ind-combo+1;i<combo_ind+1;i++){
					tab_jeu2[i].couleur=9;
					tab_jeu2[i].forme=9;
				}
				tab_jeu1 = malloc((nbr-combo) * sizeof(Token));
			}
			else{
				tab_jeu1 = malloc(nbr * sizeof(Token));
			}
			
			/* Maintenant que la table de copie est correct et apres que la vrai table du plateau a ete 
			reinitialise ci-dessus, la vrai table du plateau prend les memes valeurs que la table copie
			sauf les tokens 9 qui sont des tokens combo a faire disparaitre */
			combo_ind=0;
			for(i=0;i<nbr;i++){
				if(tab_jeu2[i].couleur==9 || tab_jeu2[i].forme==9){
					combo_ind++;
				}
				else{
					tab_jeu1[i-combo_ind]=tab_jeu2[i];
				}
			}
				
			// On remet rep par une valeur quelconque
			rep='n';
			
			/* Si on vient de faire un combo, on verifie si apres d'avoir enleve des tokens de nouveaux
			combo de tokens sont possible, donc on refait une verification, on diminue le nombre d'elements dans le plateau*/
			if(combo>=3){
				nbr=nbr-combo;
				for(i=0;i<nbr;i++){
					tab_jeu2[i]=tab_jeu1[i];
				}
			}
			else{
				// On peut desallouer la place de la table copie
				free(tab_jeu2);
			}
			
		}while(combo!=0);
		
		printf("\n\n---------------------------------------------------------------------------------\n\n\n");
		
		//jeu=0;
		
	}while(jeu==1);

}
