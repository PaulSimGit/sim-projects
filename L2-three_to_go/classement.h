#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void decroissant(int tab[], int len)
{
     int x, i, j;
 
     for (i=0; i<len; i++)
     {
        for(j=i; j<len; j++)
        {
            if(tab[j]>tab[i])
            {
                x = tab[i];
                tab[i] = tab[j];
                tab[j] = x;
            }
        }
 
     }
}



int classement(int score){

      FILE* fichier = NULL;
      int caractereActuel, nbr=1, i=0, liste[11], rep, ok=0;
      char record[10];
  
      fichier = fopen("record.txt", "r");
  
      if(fichier != NULL){
      
          do{
              liste[nbr]=0;
                caractereActuel = fgetc(fichier);
              if(caractereActuel<=47 && caractereActuel>=58 && caractereActuel!=10){
                  printf("erreur\n");
                  break;
            }
            else{
                record[i]=caractereActuel;
                i++;
            }
              if(caractereActuel==10){
                rep=atoi(record);
                liste[nbr-1]=rep;
                  nbr++;
                  i=0;
                  sprintf(record,"0");
            }
        }while(caractereActuel != EOF);
          
        fclose(fichier);
        
    }
    
    else{
        printf("Impossible d'ouvrir le fichier test.txt\n");
    }
      
    decroissant(liste, nbr-2);

	if(score>=0){
		for(i=0;i<nbr-1;i++){
			if(score>=liste[i]){
				ok=1;
			}			
		}
		if(nbr==1){
			ok=1;
		}
		if(ok==1){
			fichier = fopen("record.txt", "w");
			for(i=0;i<nbr-2;i++){			
				fprintf(fichier,"%d\n",liste[i]);	
			}
			fprintf(fichier,"%d\nx",score);
			fclose(fichier);
		}
	}
	else{
		for(i=0;i<nbr-2;i++){
			MLV_draw_text(530, 192+i*48, "RECORD %d:       %d", MLV_COLOR_BLACK, i+1, liste[i]);
		}
	}

    return 0;

}
