#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "jeu.h"
#include "classement.h"


void ecran_titre(int *main){

    	int x=0, y=0, ecran=1, score=-1, musik=0;
	char song[25];


	MLV_init_audio();
	MLV_Music* music = MLV_load_music("music/ecran.wav");
	MLV_play_music(music, 1.0, -1);

	MLV_Image *image;
	image = MLV_load_image("image/ecran_titre.png");
	MLV_draw_image(image, 0, 0);


	do{

		image = MLV_load_image("image/music_non.png");
		MLV_draw_image(image, 408, 472);
		MLV_draw_image(image, 564, 472);
		MLV_draw_image(image, 720, 472);
		image = MLV_load_image("image/music_ok.png");
		switch(musik){
			case 0:MLV_draw_image(image, 408, 472);break;
			case 1:MLV_draw_image(image, 564, 472);break;
			default:MLV_draw_image(image, 720, 472);break;
		}
		sprintf(song,"music/jeu%d.mp3",musik);
	  	MLV_actualise_window();

		MLV_wait_mouse(&x, &y);

		if((x>408 && x<792) && (y>232 && y<304)){

			image = MLV_load_image("image/regle.png");
			MLV_draw_image(image, 0, 0);
			MLV_actualise_window();
			MLV_wait_mouse(&x, &y);

			MLV_Music* music = MLV_load_music(song);
			MLV_play_music(music, 1.0, -1);
			score=0;

			jeu(&score);
			classement(score);
			image = MLV_load_image("image/fin.png");
			MLV_draw_image(image, 0, 0);
			MLV_draw_text(590, 400, "%d",MLV_COLOR_BLACK, score);
			MLV_actualise_window();
			x=999;y=999;
			do{
				MLV_wait_mouse(&x, &y);
			}while(!(x>24 && x<104 && y>708 && y<788));
			ecran=0;
		}
		if((x>408 && x<792) && (y>352 && y<424)){
			image = MLV_load_image("image/classement.png");
			MLV_draw_image(image, 0, 0);
			MLV_Music* music = MLV_load_music("music/classement.wav");
			MLV_play_music(music, 1.0, -1);
			classement(score);

			MLV_actualise_window();

			do{
				MLV_wait_mouse(&x, &y);
			}while(!(x>24 && x<104 && y>648 && y<728));

			ecran=0;

		}
		if((x>408 && x<480) && (y>472 && y<544)){
			musik=0;
		}
		if((x>564 && x<636) && (y>472 && y<544)){
			musik=1;
		}
		if((x>720 && x<792) && (y>472 && y<544)){
			musik=2;
		}
		if((x>408 && x<792) && (y>592 && y<664)){
			*main=0;
			ecran=0;
		}


	}while(ecran==1);

}
