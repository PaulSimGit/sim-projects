#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


// La nouvelle variable Token
typedef struct{

	int couleur;
	int forme;

}Token;





void temps(int* chrono, int *jeu){

	char chrono_char[3];

	*chrono=*chrono+1;
	MLV_Image *image;
	image = MLV_load_image("image/time.png");
	MLV_draw_image(image, 0, 0);

	if(*chrono%60<10){
		sprintf(chrono_char,":0");
	}
	else{
		sprintf(chrono_char,":");
	}

	if(*chrono>120){
		*jeu=0;
	}

	if(*chrono==120){
		MLV_draw_text(207, 10, "Temps 2:00 ",MLV_COLOR_LAVENDER);
	}

	if(*chrono<60){
		MLV_draw_text(207, 10, "Temps 0%s%d ",MLV_COLOR_LAVENDER, chrono_char, *chrono%60);
	}

	if(*chrono>=60 && *chrono<120){
		MLV_draw_text(207, 10, "Temps 1%s%d ",MLV_COLOR_LAVENDER, chrono_char, *chrono%60);
	}

}






// La fonction renvoie un nombre au hasard entre 0 et 3
int hasard(void){

	int a;
  
	a = rand() % 4;

return a;

}



// Convertie un nombre en premiere lettre de la forme du Token
int nbr_forme(int x){

	char a;
  
	switch(x){
    	case 0 : a='o'; break;
    	case 1 : a='c'; break;
    	case 2 : a='t'; break;
    	case 3 : a='d'; break;
    	default : a='9'; break;
	}

return a;

}



// Convertie un nombre en premiere lettre de la couleur du Token
int nbr_couleur(int x){

	char a;
  
	switch(x){
    	case 0 : a='r'; break;
    	case 1 : a='v'; break;
    	case 2 : a='b'; break;
    	case 3 : a='j'; break;
    	default : a='9'; break;
	}

return a;

}



// Corps du jeu
void jeu(int* score){

	/*
	INT:
	- i : pour l'incrementation en general
	- j : pareil que i, utile dans des boucles ou il y a deja i
	- jeu : pour savoir quand c'est fini (=1 en cours, =0 fini)
	- combo_coul : si =3, alors il y a 3 tokens consecutifs de la meme couleur
	- combo_val_coul : la valeur verifiee pour les combos lies a la couleur
	- combo_form : si =3, alors il y a 3 tokens consecutifs de la meme  forme
	- combo_val_form : la valeur verifiee pour les combos lies a la forme
	- combo_ind : l'indice du dernier token similaire
	- combo : =0 si pas de combo, sinon =x avec x le nombre de combo
	- rep2 : pour choisir l'index du token auquel on veut faire une inversion
	- x, y : coordonne de la souris
	- coord : coordonne du token placé
	- index_inv : table des index des tokens presents sur la fenetre
	- coord_inv : table des coordonnes des tokens presents sur la fenetre
	- chrono : le temps écoulé depuis l'ouverture de la fenetre de jeu
	- mode_inv : pour savoir si on est train de choisir de faire une inversion 
	
	CHAR:
	- rep : pour savoir si on met le token a gauche ou a droite
	- qui : pour savoir quelle image chercher
	
	TOKEN:
	- tab_att : le tableau de tokens en attente
	- tab_jeu1 : le tableau de tokens qui se trouvent sur le plateau de jeu
	- tab_jeu2 : un tableau de copie, qui permet a la liste tab_jeu1 d'avoir un token au debut
	- inversion : token de stockage lors d'une inversion

	MLV_Image:
	- image : variable permettant de stocker puis afficher une image
	*/

	int i, j, jeu=1, nbr=1, combo_coul, combo_val_coul, combo_ind, combo_form, combo_val_form, combo, rep2=-1, x, y, coord, index_inv[16], coord_inv[16], chrono=0, mode_inv=-1;
	char rep='n', qui[20];
	Token tab_att[5];
	Token* tab_jeu1 = NULL;
	Token* tab_jeu2 = NULL;
	MLV_Image *image;

	
	// La fonction aleatoire a ete initialisee
	srand(time (NULL));
	
	// On initialise la liste des tokens du plateau et sa copie 
	tab_jeu1 = malloc(nbr * sizeof(Token));
	tab_jeu2 = malloc(nbr * sizeof(Token));
	
	// On initialise les tokens en attente
	for(i=0;i<5;i++){
		tab_att[i].couleur=hasard();
		tab_att[i].forme=hasard();
	}
	
	// On initialise le premier token sur le plateau de jeu
	tab_jeu1[0].couleur=hasard();
	tab_jeu1[0].forme=hasard();
	
	// Que le jeu commence !
	do{

		MLV_clear_window( MLV_COLOR_BLACK );

		// Les fonctions d'image ont ete initialisees
		image = MLV_load_image("image/jeu.png");
		MLV_draw_image(image, 0, 0);
		MLV_draw_text(10, 10, "SCORE %d", MLV_COLOR_LIGHT_BLUE, *score);
	  	MLV_actualise_window();
		
		// On affiche les tokens en attente
		for(i=0;i<5;i++){
			sprintf(qui,"symbole/%c%c.png",nbr_forme(tab_att[i].forme),nbr_couleur(tab_att[i].couleur));
			image = MLV_load_image(qui);
			MLV_draw_image(image, 1181-(i*98), 12);
		  	MLV_actualise_window();
		}


		/*On initialise les tables pour les index et coordonnes des tokens sur la fenetre, cela pemet de savoir où cliquer pour faire des inversions*/
		for(i=0;i<14;i++){
			index_inv[i]=-1;
			coord_inv[i]=-1;
		}

		j=0;

		// On affiche les tokens sur le plateau de jeu
		for(i=0;i<nbr;i++){

			sprintf(qui,"symbole/%c%c.png",nbr_forme(tab_jeu1[i].forme),nbr_couleur(tab_jeu1[i].couleur));
			image = MLV_load_image(qui);
			if(nbr%2==0){
				coord=(600-40-(nbr/2*80-10)+((i+1)*80))-45;
			}
			else{
				coord=600-50-(nbr/2*80-10)+(i*80);
			}
			MLV_draw_image(image, coord, 584);

			// Si le token est dans la fenetre
			if((coord>-20 && coord<1200 && nbr%2!=0) || (coord>0 && coord<1119 && nbr%2==0)){
				index_inv[j]=i;
				coord_inv[j]=coord;
				j++;
			}
		  	MLV_actualise_window();
		}


		// Saisie controlee, pour choisir entre mettre le prochain token a droite ou a gauche
		do{

			temps(&chrono, &jeu);
			x=-999;
			y=-999;

			MLV_actualise_window();

			MLV_wait_mouse_or_seconds(&x, &y, 1);
			
			// Si on souhaite placer le token a gauche
			if((x>408 && x<552) && (y>696 && y<776) && mode_inv==-1){
				rep='a';
			}

			// Si on souhaite placer le token a droite
			if((x>648 && x<792) && (y>696 && y<776) && mode_inv==-1){
				rep='z';
			}

			/* Si on souhaite faire une inversion, on verifie que le clic s'est fait sur quelle token*/
			if(mode_inv==-1){
				for(i=0;i<j;i++){
					// Si on clique sur un token dans la fenetre
					if((x>coord_inv[i] && x<coord_inv[i]+80) && (y>584 && y<664)){
						mode_inv=i;
						x=-999;
						y=-999;
						break;
					}
				}
			}

			if(mode_inv>=0){

				image = MLV_load_image("image/permutation.png");
				MLV_draw_image(image, coord_inv[mode_inv], 394);
				
				// Si on a fait une inversion par forme
				if((x>coord_inv[mode_inv] && x<coord_inv[mode_inv]+80) && (y>484 && y<564)){
					rep='f';
					mode_inv=-1;
					break;
				}
				// Si on a fait une inversion par couleur
				if((x>coord_inv[mode_inv] && x<coord_inv[mode_inv]+80) && (y>394 && y<474)){
					rep='c';
					mode_inv=-1;
					break;
				}

				// Si on a clique autre part
				if(x>=0 && y>=0){
					mode_inv=-1;
					rep='n';
				}

				if(mode_inv==-1){
					image = MLV_load_image("image/transition3.png");
					MLV_draw_image(image, 0, 0);
					MLV_actualise_window();
				}
			}

				

		}while(rep!='a' && rep!='z' && rep!='c' && rep!='f' && jeu==1);

		if(jeu==1){

			do{
				
				// Si on place un token
				if((rep=='a' || rep=='z') && jeu==1){
				
					/* La taille du plateau de jeu augmente vu qu'on rajoute un token, donc la liste de copie
					augmente aussi grace a malloc*/	
					nbr++;
					tab_jeu2 = malloc(nbr * sizeof(Token));
					
					// On place le token au debut de la table
					if(rep=='a'){
						tab_jeu2[0]=tab_att[4];
						for(i=1;i<nbr;i++){
							tab_jeu2[i]=tab_jeu1[i-1];
						}	
					}
					// On place le token a la fin de la table
					if(rep=='z'){
						for(i=0;i<nbr-1;i++){
							tab_jeu2[i]=tab_jeu1[i];
						}
						tab_jeu2[nbr-1]=tab_att[4];
					}
					
					// On decale les tokens en attente
					for(i=4;i>0;i--){
						tab_att[i]=tab_att[i-1];
					}
					// On place le nouveau token en premier dans la table en attente
					tab_att[0].couleur=hasard();
					tab_att[0].forme=hasard();
				}
				
				// Si on effectue juste une permutation
				if((rep=='c' || rep=='f') && jeu==1){
					
					tab_jeu2 = malloc(nbr * sizeof(Token));
					
					rep2=index_inv[i];
					
					j=0;
					
					if(rep=='f'){
						combo_val_form=tab_jeu1[rep2].forme;
						for(i=0;i<nbr;i++){
							if(tab_jeu1[i].forme==combo_val_form){
								tab_jeu2[j]=tab_jeu1[i];
								j++;
								tab_jeu1[i].couleur=9;
								tab_jeu1[i].forme=9;
							}
						}
					}
					
					if(rep=='c'){
						combo_val_coul=tab_jeu1[rep2].couleur;
						for(i=0;i<nbr;i++){
							if(tab_jeu1[i].couleur==combo_val_coul){
								tab_jeu2[j]=tab_jeu1[i];
								j++;
								tab_jeu1[i].couleur=9;
								tab_jeu1[i].forme=9;
							}
						}
					}

					rep2=j-1;
					j=rep2-1;
					for(i=0;i<nbr;i++){
						if(tab_jeu1[i].couleur==9 && tab_jeu1[i].forme==9){
							j++;
							if(j>rep2){
								j=0;
							}
							tab_jeu1[i]=tab_jeu2[j];
						}
					}
					
					tab_jeu2 = malloc(nbr * sizeof(Token));
					for(i=0;i<nbr;i++){
						tab_jeu2[i]=tab_jeu1[i];
					}
					
				}
				


				// On verifie s'il y a 3 ou + de tokens consecutifs qui ont la meme forme ou meme couleur
					    combo_form=1;
					    combo_coul=1;
					    combo=0;
					    combo_val_form=tab_jeu2[0].forme;
					    combo_val_coul=tab_jeu2[0].couleur;
					    for(i=1;i<nbr;i++){
						// Verification de combos de forme
						if(combo_val_form==tab_jeu2[i].forme){
						    combo_form++;
						}
						else{
						    combo_form=1;
						    combo_val_form=tab_jeu2[i].forme;
						}
						if(combo_form>=3){
						    combo=combo_form;
						    combo_ind=i;
						}
						// Verification de combos de couleurs
						if(combo_val_coul==tab_jeu2[i].couleur){
						    combo_coul++;
						}
						else{
						    combo_coul=1;
						    combo_val_coul=tab_jeu2[i].couleur;
						}
						if(combo_coul>=3){
						    combo=combo_coul;
						    combo_ind=i;
						}
					    }





				// S'il y a eu un combo
				if(combo>=3){

					MLV_draw_text(10, 10, "SCORE %d", MLV_COLOR_LIGHT_BLUE, *score);

					if(rep=='a' || rep=='z'){
						*score+=(combo)*100;
						MLV_draw_text(100, 10, "+ %d", MLV_COLOR_GREEN, (combo)*100);
					}
					else{
						*score+=(combo)*100+50*combo;
						MLV_draw_text(100, 10, "+ %d", MLV_COLOR_GREEN, (combo)*100+50*combo);
					}


					// On affiche pendant deux secondes le barmen content, le score en + et les tokens concernes
					image = MLV_load_image("image/combo.png");
					MLV_draw_image(image, 0, 0);
					MLV_draw_text(642, 215, "x %d", MLV_COLOR_RED, combo);

					// On affiche les tokens en attente
					for(i=0;i<5;i++){
						sprintf(qui,"symbole/%c%c.png",nbr_forme(tab_att[i].forme),nbr_couleur(tab_att[i].couleur));
						image = MLV_load_image(qui);
						MLV_draw_image(image, 1181-(i*98), 12);
					}

					// On affiche les tokens sur le plateau de jeu
					for(i=0;i<nbr;i++){
						if(tab_jeu2[i].couleur!=9 && tab_jeu2[i].forme!=9){
							if(nbr%2==0){
								coord=(600-40-(nbr/2*80-10)+((i+1)*80))-45;
							}
							else{
								coord=600-50-(nbr/2*80-10)+(i*80);
							}
							sprintf(qui,"symbole/%c%c.png",nbr_forme(tab_jeu2[i].forme),nbr_couleur(tab_jeu2[i].couleur));
							image = MLV_load_image(qui);
							MLV_draw_image(image, coord, 584);
						}
					}

					// On actualise et on attend
					MLV_actualise_window();
					MLV_wait_seconds(2);

					image = MLV_load_image("image/jeu_2.png");
					MLV_draw_image(image, 0, 0);

					// Il y a eu un combo, on remplace ces tokens par des 9
					for(i=combo_ind-combo+1;i<combo_ind+1;i++){
						tab_jeu2[i].couleur=9;
						tab_jeu2[i].forme=9;
					}
					tab_jeu1 = malloc((nbr-combo) * sizeof(Token));
				}
				else{
					tab_jeu1 = malloc(nbr * sizeof(Token));
				}
				
				/* Maintenant que la table de copie est correct et apres que la vrai table du plateau a ete 
				reinitialise ci-dessus, la vrai table du plateau prend les memes valeurs que la table copie
				sauf les tokens 9 qui sont des tokens combo a faire disparaitre */
				combo_ind=0;
				for(i=0;i<nbr;i++){
					if(tab_jeu2[i].couleur==9 || tab_jeu2[i].forme==9){
						combo_ind++;
					}
					else{
						tab_jeu1[i-combo_ind]=tab_jeu2[i];
					}
				}
					
				// On remet rep par une valeur quelconque
				rep='n';
				
				/* Si on vient de faire un combo, on verifie si apres d'avoir enleve des tokens de nouveaux
				combo de tokens sont possible, donc on refait une verification, on diminue le nombre d'elements dans le plateau*/
				if(combo>=3){
					nbr=nbr-combo;
					for(i=0;i<nbr;i++){
						tab_jeu2[i]=tab_jeu1[i];
					}
				}
				else{
					// On peut desallouer la place de la table copie
					free(tab_jeu2);
				}
				
			}while(combo!=0);

		}		

	}while(jeu==1);

}
