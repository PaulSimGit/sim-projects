%{
#include "tpcas.tab.h"
#include "../src/tree.h"
unsigned int lineno=1;
%}

%option nounput
%option noinput
%option noyywrap

%x COM
%x COM2

%%

("int"|"char") 		{return TYPE;}
"=="|"!=" 		{return EQ;}
"<"|"<="|">"|">=" 	{return ORDER;}
[+-] 			{return ADDSUB;}
[*/%] 			{return DIVSTAR;}
"||" 			{return OR;}
"&&" 			{return AND;}
"while" 		{return WHILE;}
"if" 			{return IF;}
"else" 			{return ELSE;}
"void" 			{return VOID;}
"switch" 		{return SWITCH;}
"case" 			{return CASE;}
"default" 		{return DEFAULT;}
"break" 		{return BREAK;}
"return" 		{return RETURN;}
[0-9]+ 			{return NUM;}
\'(.|\\[nt\\'])\' 	{return CHARACTER;}
[;:(){},!=] 		{return yytext[0];}
[a-zA-Z_][a-zA-Z0-9_]* 	{return IDENT;}

"/*" BEGIN COM;
<COM>. ;
<COM>"\n" { lineno++; };
<COM>"*/" BEGIN INITIAL;

"//" BEGIN COM2;
<COM2>. ;
<COM2>"\n" { lineno++; BEGIN INITIAL; }

\n { lineno++; }
[ \t] ;
. { return ERR;}
<<EOF>> { return 0; }

%%
