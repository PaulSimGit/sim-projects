%{
#include <stdio.h>
#include <string.h>
#include "../src/tree.h"
void yyerror(char* msg);
int yylex();
int yyparse();
char* yytext;
extern unsigned int lineno;
int bool_tree;
%}

%precedence ')'
%precedence ELSE

%union{struct Node* node; struct Node* tree;}

%type<node> Prog DeclVars Declarateurs DeclFoncts DeclFonct EnTeteFonct Parametres ListTypVar Corps SuiteInstr Instr Exp TB FB M E T F LValue Arguments ListExp case_instr case_next Case case_end

%token<tree> CHARACTER NUM IDENT TYPE EQ ORDER ADDSUB DIVSTAR OR AND WHILE IF ELSE VOID SWITCH CASE DEFAULT BREAK RETURN ERR


%%

Prog:
	DeclVars DeclFoncts {
	    $$=makeNode(Prog);
	    addChild($$,$1);
	    addChild($$,$2);
	    if(bool_tree){
		printTree($$);
	    }
	    deleteTree($$);         
	};

DeclVars:
	DeclVars TYPE Declarateurs ';' {
	   $$=makeNode(DeclVars);
	   addChild($$,$1);
	   addChild($$,makeNode(type));
	   addChild($$,$2);
	}
	|
	{
            $$=makeNode(DeclVars);
        };

Declarateurs:
	Declarateurs ',' IDENT {
	   $$=makeNode(Declarateurs);
	   addChild($$,$1);
	   addChild($$,makeNode(ident));
	}
    	|
	IDENT {
            $$=makeNode(Declarateurs);
            addChild($$,makeNode(ident));
        };

DeclFoncts:
	DeclFoncts DeclFonct {
		$$=makeNode(DeclFoncts);
		addChild($$,$1);
		addChild($$,$2);
	}
	|
	DeclFonct {
	$$=makeNode(DeclFonct);
		addChild($$,$1);
	};

DeclFonct:
	EnTeteFonct Corps {
		$$=makeNode(DeclFoncts);
		addChild($$,$1);
		addChild($$,$2);
	};

EnTeteFonct:
	TYPE IDENT '(' Parametres ')' {
		$$=makeNode(EnTeteFonct);
		addChild($$,makeNode(type));
		addChild($$,makeNode(ident));
		addChild($$,$4);
	}
	|
	VOID IDENT '(' Parametres ')' {
		$$=makeNode(EnTeteFonct);
		addChild($$,makeNode(tok_void));
		addChild($$,makeNode(ident));
		addChild($$,$4);
	};

Parametres:
	VOID {
		$$=makeNode(Parametres);
		addChild($$,makeNode(tok_void));
	}
	|
	ListTypVar {
		$$=makeNode(Parametres);
		addChild($$,$1);
	};

ListTypVar:
	ListTypVar ',' TYPE IDENT {
		$$=makeNode(ListTypVar);
		addChild($$,$1);
		addChild($$,makeNode(type));
		addChild($$,makeNode(ident));
	}
	|
	TYPE IDENT {
		$$=makeNode(ListTypVar);
		addChild($$,makeNode(type));
		addChild($$,makeNode(ident));
	};

Corps:
	'{' DeclVars SuiteInstr '}' {
		$$=makeNode(Corps);
		addChild($$,$2);
		addChild($$,$3);
	};

SuiteInstr:
	SuiteInstr Instr {
		$$=makeNode(SuiteInstr);
		addChild($$,$1);
		addChild($$,$2);
	}
	|   
	{
		$$=makeNode(SuiteInstr);
	};

Instr:
	LValue '=' Exp ';' {
		$$=makeNode(Instr);
		addChild($$,$1);
		addChild($$,$3);
	}
	|
	IF '(' Exp ')' Instr {
		$$=makeNode(Instr);
		addChild($$,makeNode(tok_if));
		addChild($$,$1);
		addChild($$,$3);
	}
	|
	IF '(' Exp ')' Instr ELSE Instr {
		$$=makeNode(Instr);
		addChild($$,makeNode(tok_if));
		addChild($$,$1);
		addChild($$,makeNode(tok_else));
		addChild($$,$3);
	}
	|
	WHILE '(' Exp ')' Instr {
		$$=makeNode(Instr);
		addChild($$,makeNode(tok_while));
		addChild($$,$1);
		addChild($$,$3);
	}
  	| 
	SWITCH '(' Exp ')' '{' case_instr '}'{
		$$=makeNode(Instr);
		addChild($$,makeNode(tok_switch));
		addChild($$,$3);
		addChild($$,$6);
	}
	|
	IDENT '(' Arguments  ')' ';' {
		$$=makeNode(Instr);
		addChild($$,makeNode(ident));
		addChild($$,$1);
	}
	|
	RETURN Exp ';' {
		$$=makeNode(Instr);
		addChild($$,makeNode(tok_return));
		addChild($$,$1);
	}
	|
	RETURN ';' {
		$$=makeNode(Instr);
		addChild($$,makeNode(tok_return));
	}
	|
	'{' SuiteInstr '}' {
		$$=makeNode(Instr);
		addChild($$,$2);
	}
	|
	';' {
		$$=makeNode(Instr);
	};

Exp:  
	Exp OR TB {
		$$=makeNode(Exp);
		addChild($$,$1);
		addChild($$,makeNode(OR));
		addChild($$,$2);
	}
	|
	TB {
		$$=makeNode(Exp);
		addChild($$,$1);
	};

TB:  
	TB AND FB {
		$$=makeNode(TB);
		addChild($$,$1);
		addChild($$,makeNode(AND));
		addChild($$,$2);
	}
	|
	FB {
		$$=makeNode(TB);
		addChild($$,$1);
	};

FB:
	FB EQ M {
		$$=makeNode(FB);
		addChild($$,$1);
		addChild($$,makeNode(eq));
		addChild($$,$2);
	}
	|
	M {
		$$=makeNode(FB);
		addChild($$,$1);
	};

M:
	M ORDER E {
		$$=makeNode(M);
		addChild($$,$1);
		addChild($$,makeNode(order));
	}
	|
	E {
		$$=makeNode(M);
		addChild($$,$1);
	};

E:  
	E ADDSUB T {
	$$=makeNode(E);
		addChild($$,$1);
		addChild($$,makeNode(addsub));
		addChild($$,$3);
	}
	|
	T {
		$$=makeNode(E);
		addChild($$,$1);
	};
    
T:
	T DIVSTAR F {
		$$=makeNode(T);
		addChild($$,$1);
		addChild($$,makeNode(divstar));
		addChild($$,$3);
	}
	|
	F {
		$$=makeNode(T);
		addChild($$,$1);
	};

F:
	ADDSUB F {
		$$=makeNode(F);
		addChild($$,makeNode(addsub));
		addChild($$,$1);
	}
	|
	'!' F {
		$$=makeNode(F);
		addChild($$,$2);
	}
	|  
	'(' Exp ')' {
		$$=makeNode(F);
		addChild($$,$2);
	}
	|  
	NUM {
		$$=makeNode(F);
		addChild($$,makeNode(num));
	}
	|  
	CHARACTER {
		$$=makeNode(F);
		addChild($$,makeNode(character));
	}
	|
	LValue {
		$$=makeNode(F);
		addChild($$,$1);
	}
	|  IDENT '(' Arguments  ')' {
		$$=makeNode(F);
		addChild($$,makeNode(ident));
		addChild($$,$1);
	};

LValue:
	IDENT {
		$$=makeNode(LValue);
		addChild($$,makeNode(ident));
	};

Arguments:
	ListExp {
		$$=makeNode(Arguments);
		addChild($$,$1);
	}
	|   
	{
		$$=makeNode(Exp);
	};

ListExp:
	ListExp ',' Exp {
		$$=makeNode(ListExp);
		addChild($$,$1);
		addChild($$,$3);
	}
	|  
	Exp {
		$$=makeNode(ListExp);
		addChild($$,$1);
	};

case_instr:
    SuiteInstr case_next {
         $$=makeNode(case_instr);
         addChild($$,$1);
         addChild($$,$2);}
    ;

Case:
    CASE NUM ':' SuiteInstr {
         $$=makeNode(Case);
         addChild($$,makeNode(tok_case));
         addChild($$,makeNode(num));
         addChild($$,$4);}
    |   CASE CHARACTER ':' SuiteInstr {
         $$=makeNode(Case);
         addChild($$,makeNode(tok_case));
         addChild($$,makeNode(character));
     addChild($$,$4);}
    |   DEFAULT ':' SuiteInstr {
         $$=makeNode(Case);
         addChild($$,makeNode(tok_default));
         addChild($$,$3);}
    ;
case_next:
    case_next Case case_end {
         $$=makeNode(case_next);
         addChild($$,$1);
         addChild($$,$2);
         addChild($$,$3);}
    | {
         $$=makeNode(case_next);}
    ;
case_end:
    BREAK ';' {
         $$=makeNode(case_end);
         addChild($$,makeNode(tok_break));}
     | {
         $$=makeNode(case_end);}
    ;

%%


void yyerror(char* msg){
	fprintf(stderr, "%s à la ligne %d.\n", msg, lineno);
}


int main(int argc, char *argv[]){

	if( argc==2 ){

		if( strcmp("-h",argv[1])==0 || strcmp("--help",argv[1])==0 ){
			printf("\nCe programme analyse un fichier .tpc et renvoie si il est syntaxiquement correct.\n\n");
			printf("Lancer l'executable de cette façon:\n");
			printf("$ ./bin/tpcas [OPTION] < [FICHIER] \n\n");
			printf("[OPTION]\n");
			printf("-t ou --tree: permet d'afficher l'arbre si le fichier .tpc est correct\n\n");
			printf("[FICHIER]\n");
			printf("Le nom du fichier .tpc à analyser.\n");
			printf("Il doit se trouver dans le dossier test.\n\n");
			printf("Les valeurs de retour sont les suivantes:\n");
			printf("0: il n'y a pas d'erreur dans le fichier .tpc.\n");
			printf("1: il y a au moins une erreur, on va en plus afficher la ligne de la première erreur lue.\n");
			printf("2: il y a une erreur sur la ligne de commande ou autre.\n\n");
			return 2;
		}

		if( strcmp("-t",argv[1])==0 || strcmp("--tree",argv[1])==0 ){
			bool_tree = 1;
		}

		else{
			printf("Erreur sur la ligne de commande ou autre.\n");
			return 2;
		}

	}

	switch(yyparse()){

		case 0:
		if(bool_tree==0){
			printf("Code test correct!\n");
		}
		return 0;

		/*erreur*/
		case 1:
		return 1;

		default:
		printf("Erreur sur la ligne de commande ou autre.\n");
		return 2;

	}

	return 2;

}
