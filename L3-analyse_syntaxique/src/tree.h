/* tree.h */

typedef enum {
Prog,
  DeclVars,
  Declarateurs,
  DeclFoncts,
  DeclFonct,
  EnTeteFonct,
  Parametres,
  ListTypVar,
  Corps,
  SuiteInstr,
  Instr,
  Exp,
  TB,
  FB,
  M,
  E,
  T,
  F,
  LValue,
  Arguments,
  ListExp,
  character,
  num,
  ident,
  type,
  eq,
  order,
  addsub,
  divstar,
  tok_while,
  or,
  and,
  tok_if,
  tok_else,
  tok_void,
  tok_switch,
  tok_case,
  tok_default,
  tok_break,
  tok_return,
  case_instr,
  case_next,
  Case,
  case_end
	/* list all other node labels, if any */
	/* The list must coincide with the string array in tree.c */
	/* To avoid listing them twice, see https://stackoverflow.com/a/10966395 */
} label_t;

typedef struct Node {
	label_t label;
	struct Node *firstChild, *nextSibling;
	int lineno;
} Node;

Node *makeNode(label_t label);
void addSibling(Node *node, Node *sibling);
void addChild(Node *parent, Node *child);
void deleteTree(Node*node);
void printTree(Node *node);

#define FIRSTCHILD(node) node->firstChild
#define SECONDCHILD(node) node->firstChild->nextSibling
#define THIRDCHILD(node) node->firstChild->nextSibling->nextSibling
