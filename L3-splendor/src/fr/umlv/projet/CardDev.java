package fr.umlv.projet;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Record about the cards
 * @author Paul SIM
 * @author Paul LE SAGER
 */
public record CardDev(int level, GemBag bonus, int prestige, String image, GemBag price) {

	/**
	 * Initialization of the class 'CarteDev'
	 * @param level
	 * 			Level of the card
	 * @param bonus
	 * 			Bonus given after being paid
	 * @param prestige
	 * 			Prestige point given after being paid
	 * @param image
	 * 			Image of the card
	 * @param price
	 * 			Price of the card
	 */
	public CardDev{
		
		Objects.requireNonNull(bonus, "La carte de développement ne possède pas de bonus.");
		Objects.requireNonNull(image, "La carte de développement ne possède pas d'image.");
		Objects.requireNonNull(price, "La carte de développement ne possède pas de prix.");
		
		if(level<1 || level>5){
			throw new IllegalAccessError("ERROR: niveau de la carte de développement incorrect.");
		}
		
		if(prestige<0){
			throw new IllegalAccessError("ERROR: une carte de développement ne doit pas donner des points de prestige négatif.");
		}
		
	}
	
	/**
	 * Process a String to find the bonus of a card
	 * @param line
	 * 		The String wich has been read
	 * @param bonus
	 * 		The bonus of the card
	 * @return
	 * 		The new version of the bonus
	 */
	private static GemBag treatBonusDoc(String line, GemBag bonus) {
		switch(line) {
			case "\"blue\"" -> bonus.add('u', 1);
			case "\"white\"" -> bonus.add('w', 1);
			case "\"green\"" -> bonus.add('g', 1);
			case "\"red\"" -> bonus.add('r', 1);
			default -> bonus.add('k', 1);
		}
		return bonus;
	}
	
	/**
	 * Process a String to find the prestige of a card
	 * @param line
	 * 		The String wich has been read
	 * @return
	 * 		The number of prestige meant by the document
	 */
	private static int treatPrestigeDoc(String line){				
		switch(line) {
			case "\"1\"" -> {return 1;}
			case "\"2\"" -> {return 2;}
			case "\"3\"" -> {return 3;}
			case "\"4\"" -> {return 4;}
			case "\"5\"" -> {return 5;}
			default -> {return 0;}
		}
	}
	
	/**
	 * Process a String to find the price of a card
	 * @param line
	 * 		The String wich has been read
	 * @param price
	 * 		The price of the card
	 * @param i
	 * 		The type of Gem wich is process
	 * @return
	 * 		The new version of price
	 */
	private static GemBag treatPriceDoc(String line, GemBag price, int i){
		switch(i) {
			case 0 -> price.add('w', Character.getNumericValue(line.charAt(1)));
			case 1 -> price.add('u', Character.getNumericValue(line.charAt(1)));
			case 2 -> price.add('g', Character.getNumericValue(line.charAt(1)));
			case 3 -> price.add('r', Character.getNumericValue(line.charAt(1)));
			case 4 -> price.add('k', Character.getNumericValue(line.charAt(1)));
		}
		return price;
	}
	
	/**
	 * Initialization of all the cards from a csv file
	 * @param deck
	 * 			HashMap to add all the cards
	 */
	public static void initCardDev(HashMap<Integer, ArrayList<CardDev>> deck) {
		var path = Path.of("Splendor_cards_list.csv");
		var level1 = new ArrayList<CardDev>();
		var level2 = new ArrayList<CardDev>();
		var level3 = new ArrayList<CardDev>();
		try(var reader = Files.newBufferedReader(path)){
			String line;
			String image;
			int level = 0;
			int prestige;
			GemBag price;
			GemBag bonus = null;
			while((line = reader.readLine()) != null) {
				if(line.equals("\"\"") == false) {
					level = Character.getNumericValue(line.charAt(1));
				}
				line = reader.readLine();
				if(line.equals("\"\"") == false) {
					bonus = new GemBag();
					bonus = treatBonusDoc(line, bonus);
				}
				line = reader.readLine();
				prestige = treatPrestigeDoc(line);
				line = reader.readLine();
				line = reader.readLine();
				image = line;
				price = new GemBag();
				for(var i = 0; i < 5; i ++) {
					line = reader.readLine();
					if(line.equals("\"\"") == false) {
						price = treatPriceDoc(line, price, i);
					}
				}
				switch(level) {
					case 1 -> level1.add(new CardDev(level, bonus, prestige, image, price));
					case 2 -> level2.add(new CardDev(level, bonus, prestige, image, price));
					case 3 -> level3.add(new CardDev(level, bonus, prestige, image, price));
				}
			}
			deck.put(1, level1);
			deck.put(2, level2);
			deck.put(3, level3);
		}catch(IOException e){
			System.err.println(e.getMessage());
			System.exit(1);
			return;
		}
		
	}

	/**
	 * Method to display the cards on the board game
	 * @return 
	 * 		All the information as a string
	 */
	public String afficheCarteDev() { 
		var builder = new StringBuilder();
		int index = 4 - price.Gems.size();
		builder.append("|" + prestige + " " + bonus.Gems.keySet() + "|\n");
		for(int i = 0; i < index; i ++) {
			builder.append("|   |\n");
		}
		for(var element: price.Gems.entrySet()) {
			builder.append("|" + element.getValue() + element.getKey() + " |\n");
		}
		builder.append("+---+\n");
		return builder.toString();
	}
	
}