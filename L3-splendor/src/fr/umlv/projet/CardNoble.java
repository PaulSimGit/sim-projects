package fr.umlv.projet;

import java.util.ArrayList;


import java.util.Objects;

/**
 * Record about the Noble cards
 * @author Paul SIM
 * @author Paul LE SAGER
 */
public record CardNoble(String name, String image, GemBag price, int prestige) {
	
	/**
	 * Prestige points given after the visit of a Noble
	 */
	public static final int PRESTIGE = 3;

	/** 
	 * Initialization of the class 'CarteNob'  
	 * @param name  
	 * 			Name of the Noble   
	 * @param image  
	 * 			Image of the Noble  
	 * @param price   
	 * 			Price of the Noble 
	 * @param prestige
	 * 			Prestige points given after the visit 
	 */
	public CardNoble{
		Objects.requireNonNull(name, "La carte noble ne poss�de pas de name.");
		//Objects.requireNonNull(image, "La carte noble ne poss�de pas d'image.");
		Objects.requireNonNull(price, "La carte noble ne poss�de pas de prix.");
	}
	
	/**
	 * Initialization of all the Noble cards
	 * @param nobList
	 * 				Array to add all the new Nobles
	 */
	public static void initCardNob(ArrayList<CardNoble> nobList) {
		nobList.add(new CardNoble("Catherine de Medici", null, initpriceNob(0, 3, 3, 3, 0), PRESTIGE));
		nobList.add(new CardNoble("Elisabeth of Austria", null, initpriceNob(3, 3, 0, 0, 3), PRESTIGE));
		nobList.add(new CardNoble("Isabella I of Castile", null, initpriceNob(4, 0, 0, 0, 4), PRESTIGE));
		nobList.add(new CardNoble("Niccolo Machiavelli", null, initpriceNob(4, 4, 0, 0, 0), PRESTIGE));
		nobList.add(new CardNoble("Suleiman The Magnificient", null, initpriceNob(0, 4, 4, 0, 0), PRESTIGE));
		nobList.add(new CardNoble("Anne of Britanny", null, initpriceNob(3, 3, 3, 0, 0), PRESTIGE));
		nobList.add(new CardNoble("Charles V", null, initpriceNob(3, 0, 0, 3, 3), PRESTIGE));
		nobList.add(new CardNoble("Francis I of France", null, initpriceNob(0, 0, 3, 3, 3), PRESTIGE));
		nobList.add(new CardNoble("Henry VII", null, initpriceNob(0, 0, 0, 4, 4), PRESTIGE));
		nobList.add(new CardNoble("Mary Stuart", null, initpriceNob(0, 0, 4, 4, 0), PRESTIGE));
	}
	
	/**
	 * Give the price to a Noble Card
	 * @param w
	 * 		Amount of white gem required
	 * @param u
	 * 		Amount of blue gem required
	 * @param g
	 * 		Amount of green gem required
	 * @param r
	 * 		Amount of red gem required
	 * @param k
	 * 		Amount of black gem required
	 * @return
	 * 		The price as a Gem
	 */
	private static GemBag initpriceNob(int w, int u, int g, int r, int k) {
		var price = new GemBag();
		if(w>0) {
			price.add('w', w);
		}
		if(u>0) {
			price.add('u', u);
		}
		if(g>0) {
			price.add('g', g);
		}
		if(r>0) {
			price.add('r', r);
		}
		if(k>0) {
			price.add('k', k);
		}
		return price;
	}
	
	/**
	 * Method to display the information of a noble
	 * @return 
	 * 		All the information as a string
	 */
	@Override
	public String toString(){
			return name + ": " + price;
	}

}