package fr.umlv.projet;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

/**
 * Class about the game
 * @author Paul SIM
 * @author Paul LE SAGER
 */

public class Game{
		
	
	/*
	------------------------------------------------------------------------------------------------------
	Initialisation du type Jeu, et son affichage
	------------------------------------------------------------------------------------------------------
	*/
		
	/**
	 * All the nobles
	 */
	public static ArrayList<CardNoble> nobleList;
	/**
	 * Nobles on the board game
	 */
	public static ArrayList<CardNoble> nobleBoard;
	
	/**
	 * All the (development) cards
	 */
	public static HashMap<Integer, ArrayList<CardDev>> devList;
	/**
	 * (development) cards on the board game
	 */
	public static HashMap<Integer, ArrayList<CardDev>> devBoard;
	
	/**
	 * Array of the players
	 */
	public static ArrayList<Player> playerList;
	
	/**
	 * Stack of gems on the board game
	 */
	public static GemBag tokenStack;
	/**
	 * Type of game to play
	 */
	public static int type;
	
	
	/**
	 * Initialization of the class 'Jeu'
	 */
	public Game() {		
		//On initialise nobleList, les 10 nobles de l'�nonc�
		//Dans nobleList on prend 3 nobles, qui pourront rendre visite
		nobleList = new ArrayList<CardNoble>();
		nobleBoard = new ArrayList<CardNoble>();
		CardNoble.initCardNob(nobleList);	
		//On cr�e devList, les 40 cartes de d�vellopement depuis le fichier csv
		//Dans devList on prend 12 cartes qui sera dans devBoard, les cartes du plateau
		devList = new HashMap<Integer, ArrayList<CardDev>>();
		devBoard = new HashMap<Integer, ArrayList<CardDev>>();
		CardDev.initCardDev(devList);
		GameInit.initDev(devBoard);//Initialise les joueurs
		playerList = new ArrayList<Player>();//Initialise les piles de jetons
		tokenStack = new GemBag();
		tokenStack.add('w',7);
		tokenStack.add('u',7);
		tokenStack.add('g',7);
		tokenStack.add('r',7);
		tokenStack.add('k',7);
		tokenStack.add('j',5);
	}

	/**
	 * Method to display the information of the game
	 */
	@Override
	public String toString() {
		
		var displayString = new StringBuilder();

		//Affiche les joueurs
		for(var element: playerList){
			displayString.append(element);
			displayString.append("\n");
		}
		//Affiche les piles de jetons
		displayString.append("Pile de jetons:\n");
		displayString.append(tokenStack.Gems.toString());
		//Affiche les nobles
		displayString.append("\n\nlist de nobles:\n");
		for(var element: nobleBoard){
			displayString.append(element);
			displayString.append("\n");
		}
		return displayString.toString();
	}
	
	
	
	
	
	/*
	------------------------------------------------------------------------------------------------------
	Cr�ation des joueurs, et m�thode d'ajout de gems
	------------------------------------------------------------------------------------------------------
	*/

	/**
	 * Will change the amount of the stack of gems
	 * @param list
	 * 			Which list of gems the method will change its amount
	 * @param checkGem
	 * 			Which gems its amount will change.
	 * 			If it's 'A', it will be all the kind of gems expect the gold one 'j'
	 * @param toAdd
	 * 			The amount of gems to add (if it's positive) or remove (if negative)
	 */
	public static void addTokens(GemBag list, char checkGem, int toAdd){ /* Questionner sur l'interet de dupliquer cette fonction en une d'initialisation et une d'ajout */
		if(checkGem == 'A') {
			for(var element: list.Gems.entrySet()) {
				if(element.getKey()!='j') {
					list.add(element.getKey(), element.getValue() + toAdd);
				}
			}
		}else {
			var tempo = list.Gems.get(checkGem);
			list.Gems.remove(checkGem);
			list.Gems.put(checkGem, tempo + toAdd);
		}
	}
	
	
	
	
	/*
	------------------------------------------------------------------------------------------------------
	Action du jeu:
	- g�re les actions du joueur
	- v�rifie si on peut piocher 2 jetons du même type
	- g�re la pioche de 2 jetons du même type
	- v�rifie si on peut piocher 3 jetons de type diff�rent
	- g�re la pioche de 3 jetons de type diff�rent
	------------------------------------------------------------------------------------------------------
	*/
	
	

	/**
	 * Check if among all the gems in the stack, how many
	 * stacks have at least 4 gems, it is used when a player want
	 * to take 2 gems of the same type in the stack
	 * @return
	 * 		'0': all the gems in the stack have less than 4 gems, so you can't take 2 gems
	 * 		'unique': the only type of gem which have at least 4 gems in the stack
	 * 		'1': there are at least 2 types of gem which have at least 4 gems in the stack
	 */
	public static char checkStack2(){
		int stackState=0;
		char unique='0';
		for(var element: tokenStack.Gems.entrySet()){
			if(element.getValue() >= 4 && element.getKey() != 'j'){
				unique = element.getKey();
				stackState++;
			}
		}
		switch(stackState){
			case 0:
				return '0';
			case 1:
				return unique;
			default:
				// Il y a au moins deux piles de pierres qui a au moins 4 jetons
				return '1';
		}
	}
	
	/**
	 * Check if among all the gems in the stack, how many
	 * stacks have at least 1 gem, it is used when a player want
	 * to take 3 gems of different type in the stack
	 * @return
	 * 		"0": the stack is empty
	 * 		"unique": the only type of gem which have at least 1 gem in the stack, it could be 1, 2 or 3 stacks
	 * 		"1": there are at least 4 types of gem which have at least 1 gem in the stack
	 */
	public static String checkStack3(){	
		
		int stackState=0;
		var unique= new StringBuilder();

		for(var element: tokenStack.Gems.entrySet()){
			if(element.getValue() >= 1 && element.getKey() != 'j'){
				unique.append(element.getKey());
				stackState++;
			}
		}
		if(stackState==0)
			return "0";
		
		if(stackState<=3)
			return unique.toString();
		
		return "1";
	}	
	
	/*
	------------------------------------------------------------------------------------------------------
	Autres v�rifications
	------------------------------------------------------------------------------------------------------
	*/
	

	/**
	 * Check how many gems a player has
	 * @param i
	 * 		The # of the player to check
	 * @return
	 * 		The amount of gems a player has
	 */
	public static int countTokens(int i){	
		
		int cmpt=0;
		
		for(var element: playerList.get(i).tokens.Gems.entrySet()){
			cmpt = cmpt + element.getValue();
		}
		
		return cmpt;
				
	}
	
	/**
	 * Check if a player has 15 prestige points or more
	 * @return
	 * 		The # of player who win, if nobody, return -1
	 */
	public int win(){
		
		for(var element: playerList){
			if( element.prestige>=15 ){
				return element.num;
			}
		}
		
		return -1;
				
	}
	
	/**
	 * If a player has enough bonus, he can have the
	 * visit of one or more nobles who are on the board game 
	 * @param i
	 * 		The # of the player who can have the visit of a noble
	 * @param scan
	 * 		For choosing a noble or the next one
	 */
	public static void checkNob(int i, Scanner scan){
		boolean loop=false, possibleNob;
		String input;
		// Boucle, tant qu'il y a au moins un noble possible et choisi
		do{
			// On v�rifie chaque noble sur le plateau s'il peut rendre visite
			for(var element: nobleBoard){
				possibleNob=true;
				for(var price: element.price().Gems.entrySet()) {
					if( price.getValue() > playerList.get(i).bonus.Gems.get(price.getKey()) ) {
						possibleNob=false;
						break;
					}
				}	
				if(possibleNob) {
					loop=true;
					System.out.println("\n" + element.name() + " vous rend visite, voulez-vous accepter ces 3 points de prestige, voici les bonus requis:\n" + element.price());
					System.out.println("Rentrer oui pour accepter, non pour refuser.");
					do {
						input = scan.next();
						if(input.equals("oui")) {
							System.out.println("Noble combl�, prestige re�u.");
							playerList.get(i).prestige = playerList.get(i).prestige + CardNoble.PRESTIGE;
							nobleBoard.remove(element);
							GameInit.initNob(1);
							loop=false;
						}						
					}while(!input.equals("oui") && !input.equals("non"));
				}
				if(!loop) {
					break;
				}	
			}
		}while( loop );
	}	
	
	/*
	------------------------------------------------------------------------------------------------------
	Fonctions sur les cartes de d�veloppement
	------------------------------------------------------------------------------------------------------
	*/	
	
	/**
	 * Allow the player to choose which reserved card to pay
	 * @param joueur
	 * 			The # of player who is paying a card that he has reserved
	 * @param scan
	 * 			For choosing which card to pay
	 * @return
	 * 			The card chosen
	 */
	private static CardDev chooseHandCard(Player joueur, Scanner scan) {
		String string = null;
		joueur.affichehand();
		System.out.println("Veuillez choisir une carte � acheter\nPour quitter, appuyer sur Q.\n" + joueur.hand.size() + "\n|\n|\n|\n1");
		while(true) {
			string = scan.nextLine();
			if(string.equals("1")) 
				return joueur.hand.get(0);				
			else if(string.equals("2") && joueur.hand.size() >= 2)
				return joueur.hand.get(1);				
			else if(string.equals("3") && joueur.hand.size() >= 3)
				return joueur.hand.get(2);
			else if(string.equals("Q") || string.equals("q"))
				return null;
			else
				System.out.println("Erreur, cette saisie ne correspond � aucune action possible.");
		}
	}
	
	/**
	 * Remove and add card on the board game after being paid
	 */
	public static void updateDevBoard() {
		var random = new Random();
		CardDev card;
		if(devBoard.get(1).size() != 4) {
			card = devList.get(1).get(random.nextInt(devList.get(1).size()));
			devBoard.get(1).add(card);
			devList.get(1).remove(card);
		}else if(devBoard.get(2).size() != 4) {
			card = devList.get(2).get(random.nextInt(devList.get(2).size()));
			devBoard.get(2).add(card);			
			devList.get(2).remove(card);
		}else if(devBoard.get(3).size() != 4) {
			card = devList.get(3).get(random.nextInt(devList.get(3).size()));
			devBoard.get(3).add(card);
			devList.get(3).remove(card);
		}else {
			throw new AssertionError("Probleme de suppression d'une des cartes.");
		}
	}
	
	/**
	 * Update the state of tokenStack according to how many did a player spent
	 * @param realCost
	 * 			The quantity of tokens spent by a player, and so the quantity returning in tokenStack
	 */	
	public static void miseAJourJeu(GemBag realCost) {
		int value;
		for(var element: realCost.Gems.entrySet()) {
			value = tokenStack.Gems.get(element.getKey());
			tokenStack.Gems.remove(element.getKey());
			tokenStack.Gems.put(element.getKey(), value + element.getValue());
		}
	}
	/**
	 * Treat the player will by analysing the key pressed
	 * 
	 * @param string
	 * 		The key that the player entered
	 * @param scan
	 * 		For choosing which card to pay
	 * @param card
	 * 		The variable that take the card that the player choose
	 * @param i
	 * 		The number that designed the player choosing a card
	 * @return
	 * 		0 if the player choose to quite this loop of events, 1 if the player choose to keep going
	 */
	public static CardDev treatChooseCard(String string, Scanner scan, CardDev card, int i) {
		if(string.equals("M") && playerList.get(i).hand.size() > 0) {
			card = Game.chooseHandCard(playerList.get(i), scan);
			if(card == null) 
				return null;
		}else if(string.equals("J")) {
			card = GameDisplay.ChooseCard(false, scan);
			if(card == null) {
				return null;
			}
		}else if(string.equals("q") || string.equals("Q")) {
			return null;
		}else {
			System.out.println("Erreur, cette saisie ne correspond � aucune des options possible. ");
			string = null;
		}
		return card;
	}
	
	/**
	 * Ask the player if he wants to pay a reserved card, or on the board.
	 * Also check if the player can buy it, and update the stack
	 * @param i
	 * 		The # of the player who is paying
	 * @param scan
	 * 		For choosing which card to pay
	 * @return
	 * 		True a card has been paid, false the card cannot be paid
	 */
	public static boolean buyCardDev(int i, Scanner scan) {
		String string = null;
		CardDev card = null;
		GemBag realCost = null;
		System.out.println("Voulez vous acheter une carte provenant de votre hand ou une case encore en jeu? (M / J)\n\nAppuyer sur Q pour quitter.\n\n");
		while(string == null) {
			string = scan.nextLine();
			card = treatChooseCard(string, scan, card, i);
			if(card == null)
				return false;
		}
		if(playerList.get(i).buyable(card) == false) {
			System.out.println("Erreur, vous ne pouvez pas acheter cette carte.");
			return false;
		}
		realCost = playerList.get(i).miseAJourAchat(card);
		playerList.get(i).miseAJourBonus(card);
		Game.miseAJourJeu(realCost);
		if(string.equals("M")) {
			playerList.get(i).hand.remove(card);
		}else {
			if(devBoard.get(1).remove(card) || devBoard.get(2).remove(card) || devBoard.get(3).remove(card)) {
				updateDevBoard();
			}else {
				devList.get(1).remove(card);
				devList.get(2).remove(card);
				devList.get(3).remove(card);
				updateDevBoard();
			}
		}
		playerList.get(i).prestige += card.prestige();
		return true;
	}
}