package fr.umlv.projet;

import java.util.Scanner;


/**
 * Class about the application of the action of a player
 * @author Paul SIM
 * @author Paul LE SAGER
 */

public class GameAction {

	/**
	 * Allow the player to choose the action to do
	 * @param game
	 * 		The game with all the variables (cards, players...)
	 * @param i
	 * 		The # of the player playing right now
	 * @param scan
	 * 		For choosing the action to do
	 */
	public static void action(Game game, int i, Scanner scan){
		String string;
		CardDev card;
		boolean action = false;
		System.out.println(game);
		do{
			System.out.println("\nAu tour du joueur " + (i+1) + " " + (Game.playerList).get(i).name + " de jouer, veuillez rentrez une action:\nTouche '3': Prendre 3 jetons pierre pr�cieuse de couleur diff�rente.\nTouche '2': Prendre 2 jetons pierre pr�cieuse de la m�me couleur (si il reste au moins 4 jetons de la couleur choisie).\nTouche 'a': Acheter une carte de d�veloppement.\nTouche 'e': R�server une carte d�veloppement.");
			string = scan.nextLine();			
			switch(string){// Voir si on garde ce switch ou si on change pour un switch a fleche (->)
				case "2" -> {action = select2Tokens(i, scan); break;}
				case "3" ->{action = select3Tokens(i, scan); break;}
				case "a" -> {action = Game.buyCardDev(i, scan); break;}
				case "e" -> {if(Game.playerList.get(i).hand.size() >= 3) {
						System.out.println("Impossible de r�server une nouvelle carte, vous en poss�d� d�j� le nombre maximum.");
						break;
					}else {
						card = GameDisplay.ChooseCard(true, scan);
						if(card != null) {
							Game.playerList.get(i).hand.add(card);
							action = true;
						}else 
							action = false;
						break;
					}}
				default -> System.out.println("Erreur: action innexistante!");
			}
		}while( action==false );
		Game.checkNob(i, scan);
		string = scan.nextLine();
	}
	
	/**
	 * Allow the player to pick 2 gems of the same type in the stack
	 * @param i
	 * 		The # of the player who is picking
	 * @param scan
	 * 		For choosing the type of gem to pick
	 * @return
	 * 		True if the player could pick, else false
	 */
	private static boolean select2Tokens(int i, Scanner scan){
		/*
		- input:
		  c'est le type de pierre qu'on a rentr�
		- action:
		  prend diff�rtente valeur en fonction de la pierre choisie et de l'�tat des jetons:
		  0 = la pierre rentr�e n'existe pas
		  1 = la pierre rentr�e existe mais il n'y en a pas assez, au moins 4
		  2 = la pierre rentr�e existe et il y en a au moins 4 pour qu'on puisse retirer 2 jetons de la pile
		*/
		char input;
		int action = 0;
		/*
		checkStack2() v�rifie l'�tat des piles de jetons � disposition:
		'0' = aucune pile vide ayant 4 jetons ou plus
		'1' = deux ou plus de piles ayant 4 jetons ou plus
		'char d'une pierre' = une seule pile ayant 4 jetons ou plus
		*/
		switch(input=Game.checkStack2()){
			case '0' -> {System.out.println("Erreur: aucune pile ne contient au moins 4 pierres!"); return false;}
			case '1' -> {break;}
			default -> {System.out.println("Vous avez pris deux jetons " + input + ", le seul type de pierre qui contenait au moins 4 jetons.\nRentrer quelque chose pour continuer.");
				Game.addTokens(Game.tokenStack , input, -2);
				Game.addTokens(Game.playerList.get(i).tokens, input, 2);
				scan.next().charAt(0); return true;}
		}		
		// Boucle pour choisir une pierre
		do{
			System.out.println("Rentrer une type de pierre � prendre:");
			input = scan.next().charAt(0);
			// On v�rifie si l'�l�ment existe, puis s'il y en a on v�rifie qu'il y en assez
			for(var element: Game.tokenStack.Gems.entrySet()){
				if(element.getKey()==input){
					if(element.getValue()>=4){
						Game.addTokens(Game.tokenStack , input, -2);
						Game.addTokens(Game.playerList.get(i).tokens, input, 2);
						action=2;
						break;
					}
					else{
						System.out.println("Erreur: on peut prendre 2 pierres d'un m�me type seulement s'il reste au moins 4.");
						action=1;
						break;
					}
				}
			}
			if(action==0)
				System.out.println("Erreur: Ce type de pierre n'existe pas.");
		}while( action!=2 );
		return true;
	}
	
	/**
	 * Allow the player to pick 3 gems of different type in the stack 
	 * @param i
	 * 		The # of the player who is picking
	 * @param scan
	 * 		For choosing the type of gem to pick
	 * @return
	 * 		True if the player could pick, else false
	 */
	private static boolean select3Tokens(int i, Scanner scan){
		/*
		- input:
		  c'est le type de pierre qu'on a rentr�
		- action:
		  prend diff�rtente valeur en fonction de la pierre choisie et de l'�tat des jetons:
		  0 = la pierre rentr�e n'existe pas
		  1 = la pierre rentr�e existe mais il n'y en a pas assez, m�me pas 1
		  2 = la pierre rentr�e existe et il y en a au moins 1 pour qu'on puisse retirer 1 jeton de la pile
		  3 = la pierre rentr�e existe mais on l'a d�ja choisie, or il faut selectionner 3 pierres diff�rentes
		- select:
		  compte le namebre de jeton choisi
		- selectArray:
		  tableau contenant les pierres choisies, dans le cas où il y a 4 piles ou plus ayant au moins un jeton
		- restriGem:
		  string contenant les pierres choisies, dans le cas où il y a 3 piles ou moins ayant au moins un jeton
		*/
		char input;
		int action = 0;
		int select = 0;
		char selectArray[] = {0,0,0};
		String restriGem;
		int iter;
		/*
		checkStack3() v�rifie l'�tat des piles de jetons � disposition:
		"0" = pile vide
		"1" = il y a quatre ou plus de piles ayant au moins un jeton
		"string" = une, deux ou trois piles ont au moins 1 jeton
		*/
		switch(restriGem=Game.checkStack3()){
		case "0":
			System.out.println("Erreur: aucune pile ne contient au moins 1 pierre!");
			return false;
		case "1":
			break;
		default:
			System.out.println("Voici le(s) type(s) de pierre restante(s) que vous pouvez piocher:\n");
			for(iter=0; iter<restriGem.length(); iter++){
				Game.addTokens(Game.tokenStack , restriGem.charAt(iter), -1);
				Game.addTokens(Game.playerList.get(i).tokens, restriGem.charAt(iter), 1);
				System.out.print(restriGem.charAt(iter)+ " ");
			}
			System.out.println("Rentrer quelque chose pour continuer.");
			scan.next().charAt(0);
			return true;
		}
		do{
			System.out.println("Rentrer une type de pierre � prendre:");
			input = scan.next().charAt(0);
			for(var element: Game.tokenStack.Gems.entrySet()){
				if(element.getKey()==input){
					for(iter=0; iter<select; iter++){
						if(element.getKey()==selectArray[iter]){
							action=3;
							break;
						}
					}
					if(action==3){
						System.out.println("Erreur: ce type de pierre a d�ja �t� s�lectionn�.");
						break;
					}
					if(element.getValue()>=1){
						Game.addTokens(Game.tokenStack , input, -1);
						Game.addTokens(Game.playerList.get(i).tokens, input, 1);
						selectArray[select]=input;
						select++;
						action=2;
						break;
					}
					else{
						System.out.println("Erreur: il n'y a plus de pierre dans cette pile.");
						action=1;
						break;
					}
				}
			}
			if(action==0){
				System.out.println("Erreur: Ce type de pierre n'existe pas.");
			}
			action = 0;
		}while(Game.checkStack3().equals("0")==false && select!=3 );
		return true;
	}
	
	/**
	 * If a player has more than 10 gems on him, this method
	 * will allow him to choose which gems he wants to discard
	 * @param i
	 * 		The # of the player to check
	 * @param scan
	 * 		For choosing the gems to discard 
	 */
	public static void checkMore10(int i, Scanner scan){	
		char input;
		int nbr;
		while((nbr=Game.countTokens(i))>10 ){
			System.out.println("Attention: un joueur ne peut pas avoir plus de 10 jetons!\nVous avez " + nbr + " jetons, veuillez en d�barasser un,\nVoici vos jetons en ce moment: " + Game.playerList.get(i).tokens);
			input = scan.next().charAt(0);
			// On v�rifie si l'�l�ment existe, puis s'il y en a on v�rifie qu'il y en assez
			for(var element: Game.playerList.get(i).tokens.Gems.entrySet()){
				if(element.getKey()==input){
					if(element.getValue()==0){
						System.out.println("Erreur: vous avez 0 jeton du type " + input + "."); break;
					}
					else{
						Game.addTokens(Game.tokenStack , input, 1);
						Game.addTokens(Game.playerList.get(i).tokens, input, -1);break;
					}
				}
			}
		}
	}
}