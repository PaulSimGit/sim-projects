package fr.umlv.projet;

import java.util.ArrayList;

import java.util.Random;
import java.util.Scanner;

/**
 * Class about the display of informations about a game
 * @author Paul SIM
 * @author Paul LE SAGER
 */

public class GameDisplay {

	/**
	 * Allow the player to choose which card to take on the board or random
	 * @param choix
	 * 			If true it means the card chosen will be reserved, false it will be paid
	 * @param scan
	 * 			For choosing which card to take and from where
	 * @return
	 * 			The card chosen
	 */
	public static CardDev ChooseCard(boolean choix, Scanner scan) { /* Ajouter fonctionnalit� pour empecher de piocher une carte si la pioche est vide, et de prendre une carte sur le terrain si il n'y en a plus(même si tres improbable) */
		///Choix est true quand on reserve une carte dev, et false quand on veut selectionner une carte pout l'acheter, ce qui supprimme ou non la carte de la liste devBoard
		var random = new Random();
		String string;
		CardDev card = null;
		int x = 4;
		int y = 4;
		GameDisplay.DisplayCardDev();
		System.out.printf("\nEntrer les coordonn�es de la carte (X puis Y)\n\nVous pouvez �galement tenter votre chance en piochant une carte al�atoirement sur un des tas de cartes\n\nAppuyer sur Q pour quitter.\n\nLevel 1: L1\nLevel 2: L2\nLevel 3: L3\n\n3\n^\n|\n|\n1---->4\n\n");
		while(x == 4) {
			string = scan.nextLine();
			switch(string){
				case "1" -> x = 0;
				case "2" -> x = 1;
				case "3" -> x = 2;
				case "4" -> x = 3;
				case "L1" -> {return Game.devList.get(1).get(random.nextInt(Game.devList.get(1).size()));}
				case "L2" -> {return Game.devList.get(2).get(random.nextInt(Game.devList.get(2).size()));}
				case "L3" -> {return Game.devList.get(3).get(random.nextInt(Game.devList.get(3).size()));}
				case "Q", "q" -> {return null;}
				default -> System.out.println("Erreur, cette saisie ne peut correspondre � aucune carte.");
			}
		}
		System.out.printf("Maintenant, Y\n\n3\n^\n|\n|\n0---->3\n\n");
		while(y == 4) {
			string = scan.nextLine();
			switch(string){
				case "1" -> y = 1;
				case "2" -> y = 2;
				case "3" -> y = 3;
				default -> System.out.println("Erreur, cette saisie ne peut correspondre � aucune carte.");
			}
		}
		card = Game.devBoard.get(y).get(x);
		if(choix == true) {
			Game.devBoard.get(y).remove(card);
			Game.updateDevBoard();
		}
		return card;
	}
	
	/**
	 * Initialize niveau, a representation of a state of a game
	 * @return
	 * 		The representation just initialized
	 */		
	private static ArrayList<ArrayList<ArrayList<String>>> initNiveau(){
		var niveau = new ArrayList<ArrayList<ArrayList<String>>>();
		for(int i = 0; i < 3; i ++) {
			niveau.add(new ArrayList<ArrayList<String>>());
			for(int y = 0; y < 4; y ++) {
				niveau.get(i).add(new ArrayList<String>());
				niveau.get(i).add(new ArrayList<String>());
				niveau.get(i).add(new ArrayList<String>());
				niveau.get(i).add(new ArrayList<String>());
				niveau.get(i).get(0).add("  ");
				niveau.get(i).get(1).add("  ");
				niveau.get(i).get(2).add("  ");
				niveau.get(i).get(3).add("  ");
			}
		}
		return niveau;
	}
	
	/**
	 * Construct a builder object from a devBoard and print it
	 * 
	 * @param niveau
	 * 			niveau is the level to construct a builder from
	 */
	private static void builderConstruct(ArrayList<ArrayList<ArrayList<String>>> niveau){
		var builder = new StringBuilder();
		for(int i = 3; i >= 1; i --) {
			builder.append("+---+---+---+---+\n");
			builder.append("|" + Game.devBoard.get(i).get(0).prestige() + " " + Game.devBoard.get(i).get(0).bonus().toString().charAt(0));
			builder.append("|" + Game.devBoard.get(i).get(1).prestige() + " " + Game.devBoard.get(i).get(1).bonus().toString().charAt(0));
			builder.append("|" + Game.devBoard.get(i).get(2).prestige() + " " + Game.devBoard.get(i).get(2).bonus().toString().charAt(0));
			builder.append("|" + Game.devBoard.get(i).get(3).prestige() + " " + Game.devBoard.get(i).get(3).bonus().toString().charAt(0) + "|\n");
			for(int z = 3; z >= 0; z --) {
				builder.append("|" + niveau.get(i-1).get(0).get(z) + " |");
				builder.append(niveau.get(i-1).get(1).get(z) + " |");
				builder.append(niveau.get(i-1).get(2).get(z) + " |");
				builder.append(niveau.get(i-1).get(3).get(z) + " |\n");
			}
		}
		builder.append("+---+---+---+---+\n");
		System.out.println(builder.toString());
	}
	
	/**
	 * Display the cards on the board game
	 */
	public static void DisplayCardDev() {
		var niveau = initNiveau();
		for(int i = 1; i <= 3; i ++) {
			for(int y = 0; y < 4; y++) {
				if(Game.devBoard.get(i).get(y).price().Gems.get('w') != null) {
					niveau.get(i-1).get(y).remove(0);
					niveau.get(i-1).get(y).add(Game.devBoard.get(i).get(y).price().Gems.get('w') + "w");
				}
				if(Game.devBoard.get(i).get(y).price().Gems.get('u') != null) {
					niveau.get(i-1).get(y).remove(0);
					niveau.get(i-1).get(y).add(Game.devBoard.get(i).get(y).price().Gems.get('u') + "u");
				}
				if(Game.devBoard.get(i).get(y).price().Gems.get('g') != null) {
					niveau.get(i-1).get(y).remove(0);
					niveau.get(i-1).get(y).add(Game.devBoard.get(i).get(y).price().Gems.get('g') + "g");
				}
				if(Game.devBoard.get(i).get(y).price().Gems.get('r') != null) {
					niveau.get(i-1).get(y).remove(0);
					niveau.get(i-1).get(y).add(Game.devBoard.get(i).get(y).price().Gems.get('r') + "r");
				}
				if(Game.devBoard.get(i).get(y).price().Gems.get('k') != null) {
					niveau.get(i-1).get(y).remove(0);
					niveau.get(i-1).get(y).add(Game.devBoard.get(i).get(y).price().Gems.get('k') + "k");
				}
			}
		}
		builderConstruct(niveau);
	}
}