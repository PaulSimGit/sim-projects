package fr.umlv.projet;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

/**
 * Class about the initiation of a game
 * @author Paul SIM
 * @author Paul LE SAGER
 */

public class GameInit{

	/**
	 * Initialization of the players who will play
	 * @param scan
	 * 			For writing your name
	 * @param game
	 * 			The main variable of the game, with the players, the cards...
	 */
	public static void initJoueur(Scanner scan, Game game){
		int numeroPlayer=1;
		String namePlayer;
		do{
			do{
				switch(numeroPlayer) {
					case 1 -> {System.out.println("\nRentrer le pr�nom du joueur " + numeroPlayer + " (celui le plus jeune):"); break;}
					case 2 -> {if(Game.type==2) {initJoueurOrdi(scan, game); return; } System.out.println("\nRentrer le pr�nom du joueur " + numeroPlayer + ":"); break;}
					default -> {System.out.println("\nSi vous voulez jouer une partie �" + (numeroPlayer-1) + " joueurs, appuyez sur Entr�e,\nSinon rentrer le pr�nom du joueur " + numeroPlayer + ":"); }
				}
				namePlayer = scan.nextLine();
			}while( namePlayer.equals("")==true && numeroPlayer<=2 );
			if( namePlayer.equals("")==false )
				Game.playerList.add( new Player(namePlayer,numeroPlayer,0,false) );			
			numeroPlayer++;
		}while( (namePlayer.equals(""))==false && numeroPlayer<=4 );
	}
	
	/**
	 * Create players controlled by the computer, with IA
	 * @param scan
	 * 			For selecting the number of players
	 * @param game
	 * 			The main variable of the game, with the players, the cards...
	 */
	private static void initJoueurOrdi(Scanner scan, Game game){	
		int i;
		String type;	
		System.out.println("Contre combien d'ordinateurs voulez-vous vous battre? Entre 1 et 3 inclus:");
		do{		
			type = scan.nextLine();
		}while(type.charAt(0)<'1' || type.charAt(0)>'3' || type.length()!=1);
		for(i=0; i<type.charAt(0)-48 ; i++) {
			Game.playerList.add( new Player("Robot"+(i+1),i+2,0,true) );	
		}
	}
	
	/**
	 * Initialization of the nobles on the game board
	 * @param numberNob
	 * 		Number of nobles to initialize to the board
	 */
	public static void initNob(int numberNob) {
		for(var i = 0; i < numberNob; i ++) {
			if(Game.nobleList.size()!=0) {
				CardNoble tmp;
				Random random = new Random();
				tmp = Game.nobleList.remove(random.nextInt(Game.nobleList.size()));
				Game.nobleBoard.add(tmp);
			}
		}
	}
	
	/**
	 * Initialization of the cards on the game board
	 * @param devBoard
	 * 			The Hashmap to add the cards for the board game
	 */
	public static void initDev(HashMap<Integer, ArrayList<CardDev>> devBoard) {
		var random = new Random();
		int index;
		for(int i = 1; i <= 3; i++) {
			devBoard.put(i, new ArrayList<CardDev>());
			for(int y = 0; y < 4; y++) {
				index = random.nextInt(Game.devList.get(i).size());
				devBoard.get(i).add(Game.devList.get(i).get(index));
			}
		}
	}
	
	/**
	 * Choose a type of game
	 * @param scan
	 * 			For choosing the type of game to play
	 * @return
	 * 			The type of game choosen
	 */
	public static int initType(Scanner scan){	
		String type;	
		System.out.println("Choisissez le type de jeu que vous voulez jouer:");
		System.out.println("0 = version ASCII.");
		System.out.println("1 = version graphique.");
		System.out.println("2 = version graphique, seul contre l'ordinateur.");
		do{		
			type = scan.nextLine();
		}while(type.charAt(0)<'0' || type.charAt(0)>'2' || type.length()!=1);
		
		/*
		 * On vide le buffer de scan, si ce n'est pas fait au moment d'appuyer sur Entr�e
		 * pour valider le type de jeu le Entr�e va aussi s'appliquer sur le prochain scan.something
		 * dans notre cas le nom du Joueur 1, ce qui est tr�s g�nant.		 
		scan.nextLine();*/
		
		return type.charAt(0)-48;
	}
	
}