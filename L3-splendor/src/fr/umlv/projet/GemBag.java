package fr.umlv.projet;

import java.util.LinkedHashMap;
//import java.util.Objects;

/**
 * Class about the Gem
 * @author Paul SIM
 * @author Paul LE SAGER
 */
public class GemBag{
	LinkedHashMap<Character, Integer> Gems;
	
	/**
	 * Initialization of the class 'Gem' as an LinkedHashMap
	 */
	public GemBag(/*char couleur, int amount*/){
		this.Gems = new LinkedHashMap<Character, Integer>();
	}
	
	/**
	 * Allow to add a type of gem for any GemBag
	 * @param type
	 * 			Type of gem
	 * @param amount
	 * 			Amount of the gem
	 */
	public void add(Character type, Integer amount) {
		Gems.put(type, amount);
	}
	
	/**
	 * Add to a type GemBag a certain cost from a specific color
	 * @param type
	 * 			The color to add
	 * @param amount
	 * 			The amount of the cost to add
	 */
	public void giveCost(char type, int amount) {
		int value;
		if(Gems.get(type) == null) {
			Gems.put(type, amount);
		}else {
			value = Gems.get(type);
			Gems.remove(type);
			Gems.put(type, amount + value);
		}				
	}
	
	/**
	 * Method to display the amount of each gem
	 * @return 
	 * 		All the information as a string
	 */
	@Override
	public String toString(){
		var builder = new StringBuilder();
		for(var element: Gems.entrySet()) {
			builder.append(element.getKey()).append("=").append(element.getValue()).append(" ");
		}
		return builder.toString();
	}
	
}