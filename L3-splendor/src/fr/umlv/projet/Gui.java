package fr.umlv.projet;

import java.awt.Color;

import java.awt.Font;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import fr.umlv.zen5.Application;
import fr.umlv.zen5.ApplicationContext;
import fr.umlv.zen5.Event;
import fr.umlv.zen5.ScreenInfo;
import fr.umlv.zen5.Event.Action;

/**
 * Class which will create a graphic user interface
 * 
 * @author Paul SIM
 * @author Paul LE SAGER
 */
public class Gui {

	/**
	 * Class wich allows to draw
	 * 
	 * @author Paul SIM
	 * @author Paul LE SAGER
	 */
	static class Area {
		private Ellipse2D.Float ellipse = new Ellipse2D.Float(0, 0, 0, 0);
		private Rectangle2D.Float rectangle = new Rectangle2D.Float(0, 0, 0, 0);

		/**
		 * Draw circle with a number in the middle, for the tokens of the board or
		 * players' bonus/tokens
		 * 
		 * @param context class allowing to draw
		 * @param x       coordinate x to draw on the window
		 * @param y       coordinate y to draw on the window
		 * @param value   number to display in the middle of the circle
		 * @param color   color depending of the type of the gem to display
		 * @param size    it will divide by this value the size of the circle
		 */
		void jeton(ApplicationContext context, float x, float y, int value, char color, int size) {
			context.renderFrame(graphics -> {
				Color cercle;
				Color texte;
				// Initialise les couleurs du cercle et du texte
				switch (color) {
				case 'w' -> {cercle = Color.WHITE; texte = Color.BLACK; break;}
				case 'u' -> {cercle = Color.BLUE; texte = Color.WHITE; break;}
				case 'g' -> {cercle = Color.GREEN; texte = Color.WHITE; break;}
				case 'r' -> {cercle = Color.RED; texte = Color.WHITE; break;}
				case 'k' -> {cercle = Color.BLACK; texte = Color.WHITE; break; }
				default -> {cercle = Color.YELLOW; texte = Color.BLACK; break;}
				}
				graphics.setColor(cercle);
				ellipse = new Ellipse2D.Float(x - 30 / size, y - 30 / size, 60 / size, 60 / size);
				graphics.fill(ellipse);
				graphics.setFont(new Font("Times New Roman", Font.PLAIN, 40 / size));
				graphics.setColor(texte);
				graphics.drawString(String.valueOf(value), x - 10, y + 10);
			});
		}

		/**
		 * Draw rectangle, for cards
		 * 
		 * @param context class allowing to draw
		 * @param x       coordinate x to draw on the window
		 * @param y       coordinate y to draw on the window
		 */
		void card(ApplicationContext context, float x, float y) {
			context.renderFrame(graphics -> {
				graphics.setColor(Color.MAGENTA);
				rectangle = new Rectangle2D.Float(x, y, 120, 180);
				graphics.fill(rectangle);
			});
		}
		
		/**
		 * Draw rectangle, for noble cards
		 * 
		 * @param context class allowing to draw
		 * @param x       coordinate x to draw on the window
		 * @param y       coordinate y to draw on the window
		 */
		void cardNob(ApplicationContext context, float x, float y) {
			context.renderFrame(graphics -> {
				graphics.setColor(Color.CYAN);
				rectangle = new Rectangle2D.Float(x, y, 160, 80);
				graphics.fill(rectangle);
			});
		}

	}

	/*
	 * -----------------------------------------------------------------------------
	 * ------------------------- Affichage graphique du jeu
	 * -----------------------------------------------------------------------------
	 * -------------------------
	 */

	/**
	 * Display the game
	 * 
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 */
	public static void gameGui(Game game, Area area, ApplicationContext context, int numPlayer) {
		ScreenInfo screenInfo = context.getScreenInfo();
		float length = screenInfo.getWidth();
		float height = screenInfo.getHeight();

		// Affiche tout le jeu
		backgroundActionGui(area, context, length, height);
		playersGui(game, area, context);
		cardsGui(game, area, context);
		nobGui(game, area, context);
		stackGui(game, area, context);

		// Affiche c'est le tour de qui
		context.renderFrame(graphics -> {
			graphics.setColor(Color.GRAY);
			graphics.fill(new Rectangle2D.Float(0, 680, 650, 120));
			graphics.setColor(Color.WHITE);
			graphics.setFont(new Font("Times New Roman", Font.PLAIN, 20));
			graphics.drawString("Tour de " + Game.playerList.get(numPlayer).name + ":", 5, 700);
			graphics.drawString("Veuillez choisir une ACTION.", 5, 730);
			graphics.drawString("Appuyez sur une touche du clavier pour quitter le jeu.", 5, 750);
		});
	}

	/**
	 * Display the background and the actions
	 * 
	 * @param area    class allowing to draw
	 * @param context class allowing to draw
	 * @param length  length of the rectangle
	 * @param height  height of the rectangle
	 */
	private static void backgroundActionGui(Area area, ApplicationContext context, float length, float height) {
		context.renderFrame(graphics -> {
			String text;
			graphics.setColor(Color.ORANGE);
			graphics.fill(new Rectangle2D.Float(0, 0, length, height));
			graphics.setColor(Color.BLACK);
			graphics.fill(new Rectangle2D.Float(550, 0, 100, 580));
			graphics.setColor(Color.WHITE);
			graphics.setFont(new Font("Times New Roman", Font.PLAIN, 20));
			graphics.drawString("ACTIONS", 560, 65);
			graphics.setFont(new Font("Times New Roman", Font.PLAIN, 40));
			int i;
			for (i = 0; i < 5; i++) {
				switch (i) {
					case 0 -> {graphics.setColor(Color.RED); text = "2"; break;}
					case 1 -> {graphics.setColor(Color.GREEN); text = "3"; break;}
					case 4 -> {graphics.setColor(Color.BLUE); text = "p"; break;}
					default -> {graphics.setColor(Color.YELLOW); text = "r" + (i - 1); break;}
				}
				graphics.fill(new Rectangle2D.Float(575, 100 + 40 * i + 50 * i, 50, 50));
				graphics.setColor(Color.BLACK);
				graphics.drawString(text, 580, 140 + 40 * i + 50 * i);
			}
		});
	}

	/**
	 * Display players
	 * 
	 * @param game    the game with all the variables (cards, players...)
	 * @param area    class allowing to draw
	 * @param context class allowing to draw
	 */
	private static void playersGui(Game game, Area area, ApplicationContext context) {
		context.renderFrame(graphics -> {
			int i;
			for (i = 0; i < Game.playerList.size(); i++) {
				graphics.setColor(Color.GRAY);
				graphics.fill(new Rectangle2D.Float(660, i * 180 + i * 20, 730, 180));
				// Carte r�serv�
				handGui(Game.playerList.get(i), area, context, i);
				// Nom du joueur et point prestige
				playersNameGui(Game.playerList.get(i), area, context, i);
				// Jetons
				graphics.setColor(Color.WHITE);
				graphics.setFont(new Font("Times New Roman", Font.PLAIN, 20));
				graphics.drawString("Jetons", 680, i * 180 + i * 20 + 80);
				bagGui(Game.playerList.get(i).tokens, area, context, 680, i * 180 + i * 20, 2);
				graphics.drawString("Bonus", 830, i * 180 + i * 20 + 80);
				bagGui(Game.playerList.get(i).bonus, area, context, 810, i * 180 + i * 20, 2);
			}
		});
	}

	/**
	 * Display players' name and prestige points
	 * 
	 * @param player  player to display
	 * @param area    class allowing to draw
	 * @param context class allowing to draw
	 * @param num     # of the player
	 */
	private static void playersNameGui(Player player, Area area, ApplicationContext context, int num) {
		context.renderFrame(graphics -> {
			graphics.setFont(new Font("Times New Roman", Font.PLAIN, 40));
			graphics.setColor(Color.WHITE);
			graphics.drawString(player.name + " " + player.prestige + "pts", 670, num * 180 + num * 20 + 40);
		});
	}

	/**
	 * Display players' reserved cards
	 * 
	 * @param player  player to display
	 * @param area    class allowing to draw
	 * @param context class allowing to draw
	 * @param num     # of the player
	 */
	private static void handGui(Player player, Area area, ApplicationContext context, int num) {
		context.renderFrame(graphics -> {
			int j = 0;
			for (var card : player.hand) {
				area.card(context, 950 + j * 120 + j * 20, num * 180 + num * 20); // rectangle carte
				graphics.setFont(new Font("Times New Roman", Font.PLAIN, 20));
				graphics.setColor(Color.WHITE);
				graphics.drawString(card.prestige() + "pts", 950 + j * 120 + j * 20 + 5, num * 180 + num * 20 + 25); // prestige
				bagGui(card.bonus(), area, context, 950 + j * 120 + j * 20 + 100, num * 180 + num * 20 - 80, 2); // bonus
				bagGui(card.price(), area, context, 950 + j * 120 + j * 20 + 20, num * 180 + num * 20, 2); // prix
				j++;
			}
		});
	}

	/**
	 * Display development cards
	 * 
	 * @param game    the game with all the variables (cards, players...)
	 * @param area    class allowing to draw
	 * @param context class allowing to draw
	 */
	private static void cardsGui(Game game, Area area, ApplicationContext context) {
		context.renderFrame(graphics -> {
			int i = 0, j = 0;
			for (var level : Game.devBoard.entrySet()) {
				for (var card : level.getValue()) {
					area.card(context, j * 120 + j * 20, i * 180 + i * 20);
					graphics.setFont(new Font("Times New Roman", Font.PLAIN, 20));
					graphics.setColor(Color.WHITE);
					graphics.drawString(card.prestige() + "pts", j * 120 + j * 20 + 5, i * 180 + i * 20 + 25); // prestige
					bagGui(card.bonus(), area, context, j * 120 + j * 20 + 100, i * 180 + i * 20 - 80, 2); // bonus
					bagGui(card.price(), area, context, j * 120 + j * 20 + 20, i * 180 + i * 20, 2); // prix
					j++;
				}
				i++;
				j = 0;
			}
		});
	}
	
	/**
	 * Display noble cards
	 * 
	 * @param game    the game with all the variables (cards, players...)
	 * @param area    class allowing to draw
	 * @param context class allowing to draw
	 */
	private static void nobGui(Game game, Area area, ApplicationContext context) {
		context.renderFrame(graphics -> {
			int i = 0;
			for (var nob : Game.nobleBoard) {
				area.cardNob(context, 1400, i * 80 + i * 20);
				graphics.setFont(new Font("Times New Roman", Font.PLAIN, 20));
				graphics.setColor(Color.WHITE);
				graphics.drawString(nob.name(), 1400, i * 80 + i * 20 + 20); // prestige
				bagGui(nob.price(), area, context, 1420, i * 80 + i * 20 - 60, 2); // prix
				i++;
			}
		});
	}

	/**
	 * Display stack of tokens on board
	 * 
	 * @param game    the game with all the variables (cards, players...)
	 * @param area    class allowing to draw
	 * @param context class allowing to draw
	 */
	private static void stackGui(Game game, Area area, ApplicationContext context) {
		int i = 0;
		for (var element : Game.tokenStack.Gems.entrySet()) {
			area.jeton(context, 80 * i + 40, 620, element.getValue(), element.getKey(), 1);
			i = i + 1;
		}
	}

	/**
	 * Display tokens for cards and players' bonus/tokens
	 * 
	 * @param bag     list of bag of gems
	 * @param area    class allowing to draw
	 * @param context class allowing to draw
	 * @param x       where to draw on x
	 * @param y       where to draw on y
	 * @param size    size of token to divide
	 */
	private static void bagGui(GemBag bag, Area area, ApplicationContext context, int x, int y, int size) {
		context.renderFrame(graphics -> {
			int i = 0;
			for (var element : bag.Gems.entrySet()) {
				if (i < 3)
					area.jeton(context, 40 * i + x, y + 100, element.getValue(), element.getKey(), size);
				else
					area.jeton(context, 40 * (i - 3) + x, y + 140, element.getValue(), element.getKey(), size);
				i++;
			}
		});
	}

	/**
	 * Display message for the player for what to do
	 * 
	 * @param message  message to display
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param paragraph where to write in the box
	 * @param last      if last phrase, we add the phrase to exit
	 */
	public static void textGui(String message, Area area, ApplicationContext context, int paragraph, boolean last) {
		context.renderFrame(graphics -> {
			graphics.setColor(Color.GRAY);
			graphics.fill(new Rectangle2D.Float(0, 690 + (paragraph + 1) * 20, 650, 60));
			graphics.setColor(Color.WHITE);
			graphics.setFont(new Font("Times New Roman", Font.PLAIN, 20));
			graphics.drawString(message, 5, 730 + paragraph * 20);
			if (last) {
				graphics.drawString("Appuyez sur touche du clavier pour choisir une autre action", 5,
						730 + (paragraph + 1) * 20);
			}
		});
	}

	/*
	 * -----------------------------------------------------------------------------
	 * ------------------------- Actions
	 * -----------------------------------------------------------------------------
	 */

	/**
	 * Check which action the player chose
	 * 
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param location  the coordinate of the click
	 * @param numPlayer number of the player who is playing
	 * @return -1 if no action has been selected, else an action has been choose
	 */
	private static int checkClick(Game game, Area area, ApplicationContext context, Point2D.Float location,
			int numPlayer) {
		int i, action = -1;
		// On regarde si on a cliqu� sur les carr�s actions
		for (i = 0; i < 5; i++) {
			if (location.x > 575 && location.x < 625 && location.y > 100 + 40 * i + 50 * i
					&& location.y < 100 + 40 * i + 50 * i + 50) {
				action = i;
				break;
			}
		}
		switch (action) {
		case 0 -> {action = checkClickJetons2(game, area, context, numPlayer); break;}
		case 1 -> {action = checkClickJetons3(game, area, context, numPlayer); break;}
		case -1 -> {break;}
		default ->{
			// 2: reserver, 3: reserver aleatoirement, 4: acheter
			action = checkClickCarte(game, area, context, numPlayer, i); break;}
		}
		return action;
	}
	
	/**
	 * Check which action the player chose
	 * 
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 */
	private static void checkClickIa(Game game, Area area, ApplicationContext context, int numPlayer) {
		Ia.iaChoose(game, area, context, numPlayer);
		textGui("Cliquez pour continuer.", area, context, 1, false);
		waitClick(context);
		Ia.iaMore10(game, area, context, numPlayer);
	}

	/**
	 * Wait a click for the player
	 * 
	 * @param context class allowing to draw
	 * @return the coordinate of the click
	 */
	public static Point2D.Float waitClick(ApplicationContext context) {
		for (;;) {
			Event event = context.pollOrWaitEvent(10);
			if (event == null)// no event
				continue;
			Action action = event.getAction();
			Point2D.Float location = event.getLocation();
			if (action == Action.POINTER_DOWN)
				return location;
		}
	}

	/**
	 * Action when you choose 2 tokens of the same type
	 * 
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 * @return -1 if the action was impossible, else the action has been made
	 */
	private static int checkClickJetons2(Game game, Area area, ApplicationContext context, int numPlayer) {
		int i = 0;
		char input;
		Event event;

		switch (input = Game.checkStack2()) {
			case '0' -> { textGui("Erreur: aucune pile ne contient au moins 4 pierres!", area, context, 0, false);
						  textGui("Cliquez pour continuer.", area, context, 1, false); waitClick(context); return -1;}
			case '1' -> { break;}
			default -> { textGui("Vous avez pris le seul type de pierre qui contenait au moins 4 jetons.", area, context, 0, false);
						 textGui("Cliquez pour continuer.", area, context, 1, false);
						 Game.addTokens(Game.tokenStack, input, -2); Game.addTokens(Game.playerList.get(i).tokens, input, 2); waitClick(context); return 0;}
		}
		textGui("Choisissez une type de pierre (sauf or).", area, context, 0, true);
		for (;;) {
			event = context.pollOrWaitEvent(10);
			if (event == null) // no event
				continue;
			Action action = event.getAction();
			if (action == Action.KEY_RELEASED) 
				return -1;
			Point2D.Float location = event.getLocation();

			if (action == Action.POINTER_DOWN) {
				for (var element : Game.tokenStack.Gems.entrySet()) {
					// On v�rifie si on a cliqu� sur un des jetons de la pile
					if (location.x > 80 * i + 10 && location.x < 80 * i + 70 && location.y > 590 && location.y < 650) {
						if (element.getKey() == 'j')
							break;
						if (element.getValue() >= 4) {
							Game.addTokens(Game.tokenStack, element.getKey(), -2);
							Game.addTokens(Game.playerList.get(numPlayer).tokens, element.getKey(), 2);
							return 0;
						} else {
							textGui("Erreur: on peut prendre 2 pierres d'un m�me type s'il reste au moins 4!", area, context, 0, false);
							textGui("Choisissez une type de pierre (sauf or).", area, context, 1, true);
							break;
						}
					}
					i++;
				}
				i = 0;
			}
		}
	}

	/**
	 * Action when you choose 3 tokens of different type
	 * 
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 * @return -1 if the action was impossible, else the action has been made
	 */
	private static int checkClickJetons3(Game game, Area area, ApplicationContext context, int numPlayer) {
		int iter, i = 0;
		String restriGem;
		Event event;
		int select = 0;
		char selectArray[] = { 0, 0, 0 };

		switch (restriGem = Game.checkStack3()) {
		case "0":
			textGui("Erreur: aucune pile ne contient au moins 1 pierre!", area, context, 0, false);
			textGui("Cliquez pour continuer.", area, context, 1, false);
			waitClick(context);
			return -1;
		case "1":
			break;
		default:
			textGui("Vous avez pris les jetons qui restait.", area, context, 0, false);
			textGui("Cliquez pour continuer.", area, context, 1, false);
			for (iter = 0; iter < restriGem.length(); iter++) {
				Game.addTokens(Game.tokenStack, restriGem.charAt(iter), -1);
				Game.addTokens(Game.playerList.get(numPlayer).tokens, restriGem.charAt(iter), 1);
			}
			waitClick(context);
			return 1;
		}

		textGui("Choisissez trois type de pierre (sauf or).", area, context, 0, true);
		for (;;) {

			event = context.pollOrWaitEvent(10);
			if (event == null) { // no event
				continue;
			}
			Action action = event.getAction();
			if (action == Action.KEY_RELEASED && select < 1) {
				return -1;
			}
			Point2D.Float location = event.getLocation();

			if (action == Action.POINTER_DOWN) {
				// On v�rifie si on a cliqu� sur un des jetons de la pile
				for (var element : Game.tokenStack.Gems.entrySet()) {
					if (location.x > 80 * i + 10 && location.x < 80 * i + 70 && location.y > 590 && location.y < 650) {
						if (element.getKey() == 'j') {
							break;
						}

						for (iter = 0; iter < select; iter++) {
							if (element.getKey() == selectArray[iter]) {
								iter = 99;
								break;
							}
						}
						if (iter == 99) {
							textGui("Erreur: Vous avez d�j� choisi cette pierre.", area, context, 0, false);
							textGui("", area, context, 1, false);
							break;
						}
						if (element.getValue() < 1) {
							if (select == 0)
								textGui("Erreur: cette pile est vide.", area, context, 0, true);
							else {
								textGui("Erreur: cette pile est vide.", area, context, 0, false);
								textGui("", area, context, 1, false);
							}
							break;
						} else {
							selectArray[select] = element.getKey();
							select++;
							Game.addTokens(Game.tokenStack, element.getKey(), -1);
							Game.addTokens(Game.playerList.get(numPlayer).tokens, element.getKey(), 1);
							gameGui(game, area, context, numPlayer);
							textGui("Choisissez " + (3 - select) + " type de pierre (sauf or).", area, context, 0,
									false);
							textGui("", area, context, 1, false);
							if (select == 3)
								return 1;
							else
								break;
						}
					}
					i++;
				}
				i = 0;
			}

		}

	}

	/**
	 * Action when you select a card
	 * 
	 * @param game       the game with all the variables (cards, players...)
	 * @param area       class allowing to draw
	 * @param context    class allowing to draw
	 * @param numPlayer  number of the player who is playing
	 * @param typeAction 2 for a reservation of card, 3 for a reservation of a
	 *                   random card, 4 for buying a card
	 * @return -1 if the action was impossible, else the action has been made
	 */
	private static int checkClickCarte(Game game, Area area, ApplicationContext context, int numPlayer, int typeAction) {
		int i = 0, j = 0;
		CardDev card;
		GemBag realCost = null;

		// v�rification pour les reservations
		if ((typeAction == 2 || typeAction == 3) && Game.playerList.get(numPlayer).hand.size() >= 3) {
			textGui("Erreur: vous avez d�ja 3 cartes r�serv�es dans votre main!", area, context, 0, false);
			textGui("Cliquez pour continuer.", area, context, 1, false);
			waitClick(context);
			return -1;
		}

		// message
		switch (typeAction) {
		case 2:
			textGui("Cliquez sur une carte pour la reserver", area, context, 0, true);
			break;
		case 3:
			textGui("Cliquez sur une action de reservation ci-dessus parmi les 3.", area, context, 0, true);
			context.renderFrame(graphics -> {
				int iter;
				for (iter = 0; iter < 3; iter++) {
					if (Game.devBoard.get(iter + 1).size() > 0) {
						graphics.setColor(Color.GRAY);
						graphics.fill(new Rectangle2D.Float(0, iter * 180 + iter * 20, 540, 180));
						graphics.setColor(Color.WHITE);
						graphics.setFont(new Font("Times New Roman", Font.PLAIN, 30));
						graphics.drawString("R�server une carte al�atoire de niveau " + (iter + 1), 25,
								iter * 180 + iter * 20 + 90);
					} else {
						graphics.setColor(Color.BLACK);
						graphics.fill(new Rectangle2D.Float(0, iter * 180 + iter * 20, 540, 180));
					}

				}
			});
			break;
		default:
			textGui("Cliquez sur une carte pour l'acheter.", area, context, 0, true);
			break;
		}
		i = 0;

		// boucle on choisit la carte
		for (;;) {

			Event event = context.pollOrWaitEvent(10);
			if (event == null) { // no event
				continue;
			}
			Action action = event.getAction();
			if (action == Action.KEY_RELEASED) {
				return -1;
			}
			Point2D.Float location = event.getLocation();
			if (action == Action.POINTER_DOWN) {
				for (i = 0; i < 4; i++) {
					for (j = 0; j < 3; j++) {

						// reserver
						if (typeAction == 2 && location.x > i * 120 + i * 20 && location.x < i * 120 + i * 20 + 120
								&& location.y > j * 180 + j * 20 && location.y < j * 180 + j * 20 + 180
								&& Game.devBoard.get(j + 1).size() > 0) {
							card = Game.devBoard.get(j + 1).get(i);
							if (Game.devBoard.get(1).remove(card) || Game.devBoard.get(2).remove(card)
									|| Game.devBoard.get(3).remove(card)) {
								Game.updateDevBoard();
							} else {
								Game.devList.get(1).remove(card);
								Game.devList.get(2).remove(card);
								Game.devList.get(3).remove(card);
								Game.updateDevBoard();
							}
							if (Game.tokenStack.Gems.get('j') > 0) {
								Game.addTokens(Game.tokenStack, 'j', -1);
								Game.addTokens(Game.playerList.get(numPlayer).tokens, 'j', 1);
							}
							Game.playerList.get(numPlayer).hand.add(card);
							gameGui(game, area, context, numPlayer);
							textGui("Vous avez reserv� la carte avec succ�s!", area, context, 0, false);
							textGui("Cliquez pour continuer.", area, context, 1, false);
							waitClick(context);
							return 2;
						}

						// reserver aleatoirement
						if (typeAction == 3 && location.x > 0 && location.x < 540 && location.y > j * 180 + j * 20
								&& location.y < j * 180 + j * 20 + 180 && Game.devList.get(j + 1).size() > 0) {
							var random = new Random();
							card = Game.devList.get(j + 1).get(random.nextInt(Game.devList.get(j + 1).size()));
							Game.devList.get(j + 1).remove(card);
							if (Game.tokenStack.Gems.get('j') > 0) {
								Game.addTokens(Game.tokenStack, 'j', -1);
								Game.addTokens(Game.playerList.get(numPlayer).tokens, 'j', 1);
							}
							Game.playerList.get(numPlayer).hand.add(card);
							gameGui(game, area, context, numPlayer);
							textGui("Vous avez reserv� la carte avec succ�s!", area, context, 0, false);
							textGui("Cliquez pour continuer.", area, context, 1, false);
							waitClick(context);
							return 3;
						}

						// achat d'une carte du plateau
						if (location.x > i * 120 + i * 20 && location.x < i * 120 + i * 20 + 120
								&& location.y > j * 180 + j * 20 && location.y < j * 180 + j * 20 + 180) {
							card = Game.devBoard.get(j + 1).get(i);
							if (Game.playerList.get(numPlayer).buyable(card)) {
								realCost = Game.playerList.get(numPlayer).miseAJourAchat(card);
								Game.playerList.get(numPlayer).miseAJourBonus(card);
								Game.miseAJourJeu(realCost);
								if (Game.devBoard.get(1).remove(card) || Game.devBoard.get(2).remove(card)
										|| Game.devBoard.get(3).remove(card)) {
									Game.updateDevBoard();
								} else {
									Game.devList.get(1).remove(card);
									Game.devList.get(2).remove(card);
									Game.devList.get(3).remove(card);
									Game.updateDevBoard();
								}
								Game.playerList.get(numPlayer).prestige += card.prestige();
								gameGui(game, area, context, numPlayer);
								textGui("Vous avez pay� la carte avec succ�s!", area, context, 0, false);
								textGui("Cliquez pour continuer.", area, context, 1, false);
								waitClick(context);
								return 4;
							} else {
								textGui("Erreur: Pas assez de jetons, cliquez sur une autre carte.", area, context, 0,
										true);
								break;
							}
						}
					}
				}

				// achat d'une carte r�serv�e
				if (typeAction == 4) {
					for (i = 0; i < Game.playerList.get(numPlayer).hand.size(); i++) {

						if (location.x > 950 + i * 120 + i * 20 && location.x < 950 + i * 120 + i * 20 + 120
								&& location.y > numPlayer * 180 + numPlayer * 20
								&& location.y < numPlayer * 180 + numPlayer * 20 + 180) {
							card = Game.playerList.get(numPlayer).hand.get(i);
							if (Game.playerList.get(numPlayer).buyable(card)) {
								realCost = Game.playerList.get(numPlayer).miseAJourAchat(card);
								Game.playerList.get(numPlayer).miseAJourBonus(card);
								Game.miseAJourJeu(realCost);
								Game.playerList.get(numPlayer).hand.remove(card);
								Game.playerList.get(numPlayer).prestige += card.prestige();
								gameGui(game, area, context, numPlayer);
								textGui("Vous avez pay� la carte avec succ�s!", area, context, 0, false);
								textGui("Cliquez pour continuer.", area, context, 1, false);
								waitClick(context);
								return 4;
							} else {
								textGui("Erreur: Pas assez de jetons, cliquez sur une autre carte.", area, context, 0,
										true);
								break;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Check if a player has more than 10 tokens, if yes, he has to put it back on
	 * the stack
	 * 
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 */
	private static void checkMore10Gui(Game game, Area area, ApplicationContext context, int numPlayer) {
		int nbr, i = 0;
		Point2D.Float location;
		nbr = Game.countTokens(numPlayer);
		if (nbr <= 10)
			return;
		gameGui(game, area, context, numPlayer);
		textGui("Attention: un joueur ne peut pas avoir plus de 10 jetons au total!", area, context, 0, false);
		while ((nbr = Game.countTokens(numPlayer)) > 10) {
			textGui("Vous avez " + nbr + " jetons, veuillez d�poser sur les piles du plateau.", area, context, 1,
					false);
			location = waitClick(context);
			for (var element : Game.tokenStack.Gems.entrySet()) {
				if (location.x > 80 * i + 10 && location.x < 80 * i + 70 && location.y > 590 && location.y < 650) {
					if (Game.playerList.get(numPlayer).tokens.Gems.get(element.getKey()) > 0) {
						Game.addTokens(Game.tokenStack, element.getKey(), 1);
						Game.addTokens(Game.playerList.get(numPlayer).tokens, element.getKey(), -1);
						gameGui(game, area, context, numPlayer);
						break;
					}
				}
				i++;
			}
			i = 0;
		}
	}
	
	/**
	 * Check if a player has more enough bonus to receive a noble
	 * 
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 */
	private static void checkNobGui(Game game, Area area, ApplicationContext context, int numPlayer) {	
		boolean loop=false, possibleNob;
		Point2D.Float location;
		do{
			for(var element: Game.nobleBoard){			
				possibleNob=true;
				for(var price: element.price().Gems.entrySet()) {
					if( price.getValue() > Game.playerList.get(numPlayer).bonus.Gems.get(price.getKey()) ) {
						possibleNob=false; break;
					}
				}		
				if(possibleNob) {
					loop=true;
					textGui(element.name() + " vous rend visite, cliquez � droite de l'�cran pour accepeter.", area, context, 0, false);
					location = waitClick(context);
					if(location.x >= 1400) {
						loop=false;
						Game.playerList.get(numPlayer).prestige = Game.playerList.get(numPlayer).prestige + CardNoble.PRESTIGE;
						Game.nobleBoard.remove(element);
						break;
					}
				}
			}	
		}while( loop );
	}	

	/*
	 * -----------------------------------------------------------------------------
	 * ------------------------- Boucle du jeu
	 * -----------------------------------------------------------------------------
	 * -------------------------
	 */

	/**
	 * Loop of the game with a graphic user interface
	 * 
	 * @param game the game with all the variables (cards, players...)
	 */
	public static void mainBoucleGui(Game game) {
		Application.run(Color.ORANGE, context -> {
			int i = 0, numberWinner = -1;
			Area area = new Area();
			do {
				for (i = 0; i < (Game.playerList).size(); i++) {
					gameGui(game, area, context, i);
					if(!Game.playerList.get(i).computer) {				
						for (;;) {
							Event event = context.pollOrWaitEvent(10);
							if (event == null) { // no event
								continue;
							}
							Action action = event.getAction();
							if (action == Action.KEY_RELEASED) {
								System.out.println("Jeu interrompu.");
								context.exit(0);
								return;
							}
							Point2D.Float location = event.getLocation();
							if (action == Action.POINTER_DOWN) {
								if (checkClick(game, area, context, location, i) != -1) {
									gameGui(game, area, context, i);
									checkNobGui(game, area, context, i);
									numberWinner = game.win();
									break;
								} else
									gameGui(game, area, context, i);
							}
						}
					}
					else {
						checkClickIa(game, area, context, i);
						Ia.checkNobIa(game, area, context, i);
						numberWinner = game.win();
					}
					if (numberWinner != -1)
						break;
					checkMore10Gui(game, area, context, i);
				}
			} while (numberWinner == -1);
			gameGui(game, area, context, i);
			textGui("Le joueur " + Game.playerList.get(numberWinner - 1).name + " a gagn�!", area, context, 0, false);
			textGui("Cliquez pour quitter.", area, context, 1, false);
			waitClick(context);
			context.exit(0);
		});
	}
}