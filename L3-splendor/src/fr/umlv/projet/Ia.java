package fr.umlv.projet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

import fr.umlv.projet.Gui.*;
import fr.umlv.zen5.ApplicationContext;

/**
 * Class about Ia
 * @author Paul SIM
 * @author Paul LE SAGER
 */
public class Ia {
	
	/*
	 * -----------------------------------------------------------------------------
	 * ------------------------- Actions
	 * -----------------------------------------------------------------------------
	 */
	
	/**
	 * The action chosen by the Ia
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 */
	public static void iaChoose(Game game, Area area, ApplicationContext context, int numPlayer) {
		if( iaChooseBuy(game, area, context, numPlayer) )
			return;
		if( iaChooseToken2(game, area, context, numPlayer) )
			return;
		if( iaChooseToken3(game, area, context, numPlayer) )
			return;
		Gui.textGui("Erreur: Aucune action possible, on passe le tour.", area, context, 0, false);
	}
	
	/**
	 * Action to buy by the Ia
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 * @return			true if the action has been made, else false
	 */
	private static boolean iaChooseBuy(Game game, Area area, ApplicationContext context, int numPlayer) {
		CardDev card = iaCheckCard(game, numPlayer);
		if(card!=null) {
			iaPay(game, area, context, numPlayer, card);
			Gui.textGui("L'ordi a pay� une carte.", area, context, 0, false);
			return true;
		}
		return false;
	}
	
	/**
	 * Action to choose 2 token of the same type by the Ia
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 * @return			true if the action has been made, else false
	 */
	private static boolean iaChooseToken2(Game game, Area area, ApplicationContext context, int numPlayer) {
		String gemPossible;
		String gemPossibleChoose;
		char input=Game.checkStack2();
		if(input!='0') {
			gemPossible = gemPossibleCheck(game, area, context, numPlayer, 4);
			gemPossibleChoose = gemPossibleChoose(gemPossible, 1);
			gemPossibleTake(game, numPlayer, gemPossibleChoose, 2);
			Gui.textGui("L'ordi a pris des jetons identiques: " + gemPossibleChoose, area, context, 0, false);
			return true;
		}
		return false;
	}
	
	/**
	 * Action to take 3 tokens of different type by the Ia
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 * @return			true if the action has been made, else false
	 */
	private static boolean iaChooseToken3(Game game, Area area, ApplicationContext context, int numPlayer) {
		String gemPossible;
		String gemPossibleChoose;
		String restriGem=Game.checkStack3();
		if(!restriGem.equals("0")) {
			gemPossible = gemPossibleCheck(game, area, context, numPlayer, 1);
			gemPossibleChoose = gemPossibleChoose(gemPossible, 3);
			gemPossibleTake(game, numPlayer, gemPossibleChoose, 1);
			Gui.textGui("L'ordi a pris des jetons diff�rents: " + gemPossibleChoose, area, context, 0, false);
			return true;
		}
		return false;
	}
	
	
	
	
	/*
	 * -----------------------------------------------------------------------------
	 * ------------------------- G�re les jetons � prendre ou � donner
	 * -----------------------------------------------------------------------------
	 */
	
	/**
	 * Select the tokens in the stack which their amount or more or equal than a value
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 * @param atLeast   the amount of token to check must be superior than this value
	 * @return			a string with the tokens in the stack which their amount or more or equal than a value
	 */
	private static String gemPossibleCheck(Game game, Area area, ApplicationContext context, int numPlayer, int atLeast) {
		var chaine = new StringBuilder();
		for(var element: Game.tokenStack.Gems.entrySet()){
			if(element.getValue() >= atLeast && element.getKey() != 'j'){
				chaine.append(element.getKey());
			}
		}
		return chaine.toString();
	}
	
	/**
	 * Choose tokens randomly in a string
	 * @param chaineFinal	the token chosen
	 * @param size			how many will be chosen
	 * @return				a string with the tokens chose randomly
	 */
	private static String gemPossibleChoose(String chaineFinal, int size) {
		ArrayList<Character> chars = (ArrayList<Character>) chaineFinal.chars()
									 .mapToObj(e -> (char)e)
									 .collect(Collectors.toList());

		Collections.shuffle(chars);

		var chaine = new StringBuilder();
		int i;
		for(i=0; i<chars.size() ; i++){
			if(i==size)
				break;
			chaine.append(chars.get(i));
		}

		return chaine.toString();
	}
	
	/**
	 * Take token from the stack to the player, or vice versa
	 * @param game      the game with all the variables (cards, players...)
	 * @param numPlayer number of the player who is playing
	 * @param chaine	the tokens to take
	 * @param addPlayer the number of tokens to add to the player, and remove to the stack
	 */
	private static void gemPossibleTake(Game game, int numPlayer, String chaine, int addPlayer) {
		ArrayList<Character> chars = (ArrayList<Character>) chaine.chars()
									 .mapToObj(e -> (char)e)
									 .collect(Collectors.toList());
		for(var pierre: chars){
			Game.addTokens(Game.playerList.get(numPlayer).tokens, pierre, addPlayer);
			Game.addTokens(Game.tokenStack, pierre, -addPlayer);
		}
	}
	
	/**
	 * Take randomly a token from a Ia player when he has more than 10 tpkens
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 */
	public static void iaMore10(Game game, Area area, ApplicationContext context, int numPlayer) {
		String gemPossibleChoose;
		while (Game.countTokens(numPlayer) > 10) {
			var chaine = new StringBuilder();
			for(var element: Game.playerList.get(numPlayer).tokens.Gems.entrySet()){
				if(element.getValue() >= 1 && element.getKey() != 'j'){
					chaine.append(element.getKey());
				}
			}
			gemPossibleChoose = gemPossibleChoose(chaine.toString(), 1);
			Game.addTokens(Game.tokenStack, gemPossibleChoose.charAt(0), 1);
			Game.addTokens(Game.playerList.get(numPlayer).tokens, gemPossibleChoose.charAt(0), -1);
			Gui.gameGui(game, area, context, numPlayer);
			Gui.textGui("L'ordi a a rendu " + gemPossibleChoose, area, context, 0, false);
			Gui.waitClick(context);
		}
	}

	
	
	
	/*
	 * -----------------------------------------------------------------------------
	 * ------------------------- G�re le paiement des cartes
	 * -----------------------------------------------------------------------------
	 */

	/**
	 * Pay a card for the ia
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 * @param card		card to pay
	 */
	private static void iaPay(Game game, Area area, ApplicationContext context, int numPlayer, CardDev card) {
		GemBag realCost;
		realCost = Game.playerList.get(numPlayer).miseAJourAchat(card);
		Game.playerList.get(numPlayer).miseAJourBonus(card);
		Game.miseAJourJeu(realCost);
		if (Game.devBoard.get(1).remove(card) || Game.devBoard.get(2).remove(card)
				|| Game.devBoard.get(3).remove(card)) {
			Game.updateDevBoard();
		} else {
			Game.devList.get(1).remove(card);
			Game.devList.get(2).remove(card);
			Game.devList.get(3).remove(card);
			Game.updateDevBoard();
		}
		Game.playerList.get(numPlayer).hand.remove(card);
		Game.playerList.get(numPlayer).prestige += card.prestige();
	}
	
  	/**
	 * Check the most expensive and buyable card that a player can afford
	 * @param game      the game with all the variables (cards, players...)
	 * @param numPlayer number of the player who is playing
	 * @return			the most expensive and buyable card that a player can afford
	 */
	private static CardDev iaCheckCard(Game game, int numPlayer) {
		CardDev possible = null;
		for (var level : Game.devBoard.entrySet()) {
			for (var card : level.getValue()) {
				if(Game.playerList.get(numPlayer).buyable(card)) {
					possible = card;
				}
			}
		}
		for (var reserve : Game.playerList.get(numPlayer).hand) {
			if(Game.playerList.get(numPlayer).buyable(reserve)) {
				possible = reserve;
			}
		}
		return possible;
	}
	
	/**
	 * Check if an Ia player has more enough bonus to receive a noble
	 * 
	 * @param game      the game with all the variables (cards, players...)
	 * @param area      class allowing to draw
	 * @param context   class allowing to draw
	 * @param numPlayer number of the player who is playing
	 */
	public static void checkNobIa(Game game, Area area, ApplicationContext context, int numPlayer) {	
		boolean possibleNob;
		for(var element: Game.nobleBoard){			
			possibleNob=true;
			for(var price: element.price().Gems.entrySet()) {
				if( price.getValue() > Game.playerList.get(numPlayer).bonus.Gems.get(price.getKey()) ) {
					possibleNob=false; break;
				}
			}		
			if(possibleNob) {
				Gui.textGui(element.name() + " rend visite � " + Game.playerList.get(numPlayer).name + ".", area, context, 0, false);
				Gui.textGui("Cliquez pour continuer.", area, context, 1, false);
				Gui.waitClick(context);
				Game.playerList.get(numPlayer).prestige = Game.playerList.get(numPlayer).prestige + CardNoble.PRESTIGE;
				Game.nobleBoard.remove(element);
				break;
			}
		}	
	}	
	
}
