package fr.umlv.projet;

import java.util.ArrayList;

import java.util.Objects;

/**
 * Class about players
 * @author Paul SIM
 * @author Paul LE SAGER
 */
public class Player{
	
	/**
	 * If the player is controlled by an IA
	 */
	public final boolean computer;
	/**
	 * Name of the player
	 */
	public final String name;
	/**
	 * Number (#) of the player
	 */
	public final int num;
	/**
	 * Prestige point at the beginning, zero
	 */
	public int prestige;
	/**
	 * Gems and bonus of the player
	 */
	public final GemBag tokens, bonus;
	/**
	 * Cards that the player has reserved
	 */
	public ArrayList<CardDev> hand;
	/**
	 * Cards that the player paid
	 */
	public ArrayList<CardDev> deck;
	
	/**
	 * Initialization of the class 'Player'
	 * @param name
	 * 			The name of the player
	 * @param num
	 * 			The # of the player
	 * @param prestige
	 * 			The amount of prestige point of the player
	 * @param computer 
	 * 			If the player is controlled by an Ia
	 */
	public Player(String name, int num, int prestige, boolean computer){
		Objects.requireNonNull(name, "Le joueur ne poss�de pas de nom.");
		if(num<0 || num>4)
			throw new IllegalAccessError("ERROR: num�ro de joueur incorrect.");
		this.tokens = new GemBag();
		tokens.add('w', 0);
		tokens.add('u', 0);
		tokens.add('g', 0);
		tokens.add('r', 0);
		tokens.add('k', 0);
		tokens.add('j', 0);		
		this.bonus = new GemBag();
		bonus.add('w', 0);
		bonus.add('u', 0);
		bonus.add('g', 0);
		bonus.add('r', 0);
		bonus.add('k', 0);
		this.computer = computer;
		this.num = num;
		this.name = name;
		this.prestige = prestige;
		this.deck = new ArrayList<CardDev>();
		this.hand = new ArrayList<CardDev>();
	}
	
	/**
	 * Update the parameter bonus of a player after purchasing a card
	 * @param carte
	 * 			Card which its bonus will be given to the player
	 */
	public void miseAJourBonus(CardDev carte) { 
		int value;
		for(var element: carte.bonus().Gems.entrySet()) {
			value = bonus.Gems.get(element.getKey());
			bonus.Gems.remove(element.getKey());
			bonus.add(element.getKey(), element.getValue() + value);
		}
	}
	
	/**
	 * Display on the terminal the cards that the player has reserved
	 */
	public void affichehand() {
		var builder = new StringBuilder();
		builder.append("+---+\n");
		for(int i = 0; i < hand.size(); i++) {
			builder.append(hand.get(i).afficheCarteDev());
		}
		System.out.println(builder.toString());
	}
	
	/**
	 * Check if a card can be buy by the player
	 * @param carte
	 * 			Card to pay
	 * @return
	 * 			True if the player can pay the card, else false
	 */
	public boolean buyable(CardDev carte) { // verification de l'ajout
		int orValue = 0;
		for(var element: carte.price().Gems.entrySet()) {
			if(element.getValue() > tokens.Gems.get(element.getKey()) + bonus.Gems.get(element.getKey()) + tokens.Gems.get('j')) {
				return false;
			}
			if(element.getValue() - (tokens.Gems.get(element.getKey()) + bonus.Gems.get(element.getKey())) > 0) {
				orValue += element.getValue() - (tokens.Gems.get(element.getKey()) + bonus.Gems.get(element.getKey()));
			}
			if(orValue > tokens.Gems.get('j')) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Update the gems of the player after the purchase of a card
	 * @param carte
	 * 			Card paid
	 * @return
	 * 			The price of the card, with the gold taken
	 */
	public GemBag miseAJourAchat(CardDev carte) {
		var coutCarte = new GemBag();
		int cout, valueOr;
		deck.add(carte);
		for(var element: carte.price().Gems.entrySet()) {
			cout = element.getValue() - bonus.Gems.get(element.getKey());
			if(cout > 0) {
				if(cout > tokens.Gems.get(element.getKey())) {
					valueOr = tokens.Gems.get('j');
					coutCarte.giveCost(element.getKey(), tokens.Gems.get(element.getKey()));
					coutCarte.giveCost('j', cout - tokens.Gems.get(element.getKey()));
					tokens.Gems.remove('j');
					tokens.Gems.put('j', valueOr - (cout - tokens.Gems.get(element.getKey())));
					cout = tokens.Gems.get(element.getKey());
				}
				coutCarte.giveCost(element.getKey(), cout);
				cout = tokens.Gems.get(element.getKey()) - cout;
				tokens.Gems.remove(element.getKey());
				tokens.Gems.put(element.getKey(), cout);
			}
		}
		return coutCarte;
	}
	
	/**
	 * Method to display the information of the player
	 * @return 
	 * 		All the information as a string
	 */
	@Override
	public String toString(){
		String tokensString = tokens.Gems.toString();
		String bonusString = bonus.Gems.toString();
		return "----------------- Joueur " + num + " -----------------\nPr�nom: " + name + "\nPoints prestige=" + prestige + "\nJetons: " + tokensString + "\nJetons Bonus: " + bonusString + "\n";
	}
	
}