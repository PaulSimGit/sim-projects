package fr.umlv.projet.main;

import java.util.Scanner;


import fr.umlv.projet.Game;
import fr.umlv.projet.GameAction;
import fr.umlv.projet.GameInit;
import fr.umlv.projet.Gui;



/**
 * A program which allow you to play the Splendor game (ASCII version)
 * @author Paul SIM
 * @author Paul LE SAGER
 */
public class Main {
	
	/**
	 * The main method of the program
	 * @param args
	 * 			Unused
	 */
	public static void main(String[] args){		
		var game = new Game();
		Scanner scan;
		scan = new Scanner(System.in);

		System.out.println("\nBienvenue au jeu du Splendor!\n");
		Game.type = GameInit.initType(scan);
		GameInit.initJoueur(scan, game);
		GameInit.initNob((Game.playerList).size() + 1);
		tokenStackInit();
		
		if(Game.type>=1)
			Gui.mainBoucleGui(game);
		else
			mainBoucle(game, scan);
		
		scan.close();	
	}


	/**
	 * The loop of the game, exit until there is a winner
	 * @param game
	 * 			The variable with all the cards and informations about the players
	 * @param scan
	 * 			For writing the actions
	 */
	private static void mainBoucle(Game game, Scanner scan){
		int numberWinner;
		do{		
			for(var i=0; i<(Game.playerList).size(); i++){
				GameAction.action(game, i, scan);
				GameAction.checkMore10(i, scan);
				System.out.println("\n");
				numberWinner = game.win();
				if( numberWinner!=-1 ) {
					break;
				}
			}
			numberWinner = game.win();		
		}while( numberWinner==-1 );	
		System.out.println("\nLe joueur " + (numberWinner) + " " + (Game.playerList).get(numberWinner-1).name + " a gagn�!!\n");
	}
	
	
	/**
	 * Remove all tokens (except gold) on the stack depending of the number of players
	 */
	private static void tokenStackInit(){
		switch(Game.playerList.size()) {
			case 2:
				Game.addTokens(Game.tokenStack, 'A', -3);
				break;
			case 3:
				Game.addTokens(Game.tokenStack, 'A', -2);
				break;
			default:
				break;
		}
	}
	
}