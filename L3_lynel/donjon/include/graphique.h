#ifndef __graphique__
#define __graphique__
#include <MLV/MLV_all.h>
#include "image.h"

#define MONSTER_ON_MAP 0 /*1 pour afficher les monstres sur la map et 0 sinon*/

/* Fonctions graphiques complexes (affichage long, grosse boucle, etc...) */

void affiche_jeu(MLV_Image* images[], Floor floor, Player player, int decal_x, int decal_y, int frame,
                    int decal_x_monstre, int decal_y_monstre, int frame_monstre, Coordo* mstr_actuel);
void animation_terrain(MLV_Image* images[], Floor floor, Player player, int decal_x, int decal_y, int run);
int deplacement_perso(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player player);
void deplacement_monstre(MLV_Image* images[], Floor* floor, Player player, Direction direc, Coordo origine);

void affiche_vision(MLV_Image* images[], Floor floor, int decal_x, int decal_y, int type_vision);
void affiche_carte(MLV_Image* images[], MLV_Sound* sounds[], Floor floor, Player player);
void animation_carte(MLV_Image* images[], Floor floor, Player player, int mode);
void animation_transition(MLV_Image* images[], Floor floor, Player player);

void animation_perso(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player player, bool chance);
void animation_attaque_boss(MLV_Image* images[], Floor* floor, Player player, Coordo c_boss);

int animation_combat(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player* player, Monster* monster,
                        Coordo c_mstr, int ordre);
void game_over(MLV_Image* images[], Floor* floor, Player* player);


#endif