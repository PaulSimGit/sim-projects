#ifndef __image__
#define __image__
#include <MLV/MLV_all.h>
#include "terrain_generation.h"

#define NBR_IMG 12
#define NBR_MUS 4
#define NBR_SON 17


/* Fonctions graphiques simples (affichage rapide, boucles rapides,
simple affichage d'image ou quelque chose de basique, etc...) */

void affiche_UI(MLV_Image* images[], Player player);
void nombre_UI(MLV_Image* images[], int nombre, int x, int y, int color);
void nombre_stats(MLV_Image* images[], int nombre, int x, int y, int color);
void nombre_degats(MLV_Image* images[], int nombre, int x, int y, bool chance);
void nombre_exp(MLV_Image* images[], int nombre, int x, int y);

int affiche_inventaire(MLV_Image* images[], MLV_Sound* sounds[], Player* player, Item* potion, Item trezieme);
int affiche_all_items(MLV_Image* images[], Player player);
void affiche_item(MLV_Image* images[], Item item, int x, int y, int mode_affichage);

int affiche_stats(MLV_Image* images[], MLV_Sound* sounds[], Player player);
int affiche_save(MLV_Image* images[], MLV_Sound* sounds[]);
int navigation_menus();

void affiche_perso(MLV_Image* images[], Player player, int frame);
void affiche_monstre(MLV_Image* images[], int x, int y, Monster mstr, int frame);
void affiche_message(MLV_Image* images[], int i_message);
void affiche_wall(MLV_Image* images[], Floor floor, int i_draw, int j_draw, int decal_x, int decal_y, Wallstype type);

void donne_exp(MLV_Image* images[], MLV_Sound* sounds[], Player* player, int mstr_exp);
void montee_niveau(MLV_Image* images[], MLV_Sound* sounds[], Player* player);
void montee_niveau_stats(MLV_Image* images[], Player* player);

void initialisation_images(MLV_Image* images[]);
void initialisation_musiques(MLV_Music* musics[]);
void initialisation_sounds(MLV_Sound* sounds[]);
void free_all(MLV_Image* images[], MLV_Music* musics[], MLV_Sound* sounds[]);


#endif
