#ifndef __intro__
#define __intro__
#include "jeu.h"


/* Fonctions concernant l'avant-jeu (introduction, écran titre, étage 0). */

int ecran_titre(MLV_Image* images[], MLV_Music* musics[], MLV_Sound* sounds[]);
void animation_lancement_jeu(MLV_Image* images[], MLV_Sound* sounds[]);

void easteregg_ecran_titre(MLV_Image* images[]);
void affiche_tutoriel(MLV_Image* images[], Coordo c);
void animation_ouverture_porte(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player player);
void affiche_cristal(MLV_Image* images[], int angle, int rayon);

void lancement_intro(MLV_Image* images[], MLV_Music* musics[], MLV_Sound* sounds[]);
int evenements_intro(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player player);
Floor creation_terrain_intro(int zone, int coordo_persoX, int coordo_persoY);
void donne_forme_terrain_intro(Floor* floor, int zone);
void specialise_design_terrain_intro(Floor* floor, int zone);


#endif