#ifndef __jeu__
#define __jeu__
#include "graphique.h"

#define F1_STATE 0 /*activité du bouton F1: 1 pour pouvoir quitter une partie à tout moment en appuyant sur F1*/

/*structure utilisée pour stocker les informations en binaire lors de la sauvegarde*/
typedef struct data {
    unsigned int seed; /*seed du terrain*/
    Player player; /*joueur*/
    Coordo coordo_perso; /*position du joueur*/
    Coordo escalier; /*position de l'escalier descendant*/
    Lst_Coordo_Entity coordo_mstr; /*liste des positions des monstres*/
    Lst_Coordo_Entity coordo_treasure; /*listes des position des coffres*/
} Data;


/* Fonctions d'actions et d'événements du jeu. */

int lancement_jeu(MLV_Image* images[], MLV_Music* musics[], MLV_Sound* sounds[], int jeu);
int get_action(MLV_Keyboard_button* clavier, MLV_Button_state* etat, int* pressed, int* run);
int evenements_jeu(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player* player, Item* potion);
int actions_monstres(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player* player);

void passage_etage(MLV_Image* images[], Floor* floor, Player* player, Celltype escalier);
void sauvegarde(Floor floor, Player player, char* file_name);
void chargement(Floor* floor, Player* player, char* file_name);
char* fichier_etage(int etage);
void retablissement_stats(Player* player, Player temp);


#endif
