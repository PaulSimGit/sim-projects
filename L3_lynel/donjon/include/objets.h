#ifndef __objets__
#define __objets__
#include <math.h>
#include <time.h>

#define TAILLE_INVENTAIRE 12
#define PLAGE_OBJ 2 /*plage des images des objets, utilisé dans plage_item*/


typedef struct equipment {
	int quality;
} Equipment;

typedef struct potion {
	int duration;
} Potion;

typedef enum itemtype {
	NO_ITEM,
	POTION_SOIN,
	POTION_MAGIE,
	POTION_REGEN,
	POTION_PRECI,
	POTION_APPRENTI,
	WEAPON,
	ARMOR,
	MAGIC_STAFF
} Itemtype;

typedef union itementity {
	Potion potion;
	Equipment equipment;
} Itementity;

/*représente un objet qui possède un type et une entitée unique*/
typedef struct item {
	Itemtype type;
	Itementity entity;
} Item;

/*représente un Trésor qui contient un ou deux objets*/
typedef struct treasure {
	Item item;
	/*Item item2;*/
} Treasure;


/* Fonctions concernant les objets et les trésors */

Item new_item(Itemtype type, int etage, int i);
Treasure new_treasure(int etage, int i);
int use_potion(Item* item, int* pv, int* mp, int def, int intel, int* crit, int* bonus_exp);
void equip_equipment(Item inventaire[TAILLE_INVENTAIRE], int index, int* def, int* atk, int* intel, int* e_weapon, int* e_staff, int* e_armor);
void equip_auto(Item inventaire[TAILLE_INVENTAIRE], int index, int* def, int* atk, int* intel, int* e_weapon, int* e_staff, int* e_armor);
void jeter_equipment(Item inventaire[TAILLE_INVENTAIRE], int index, int* def, int* atk, int* intel, int* e_weapon, int* e_staff, int* e_armor);
void update_stat(Item item, int* stat, int mode);
int ajoute_dans_inventaire(Item inventaire[TAILLE_INVENTAIRE], Item item);
int plage_item(int qualite);


#endif