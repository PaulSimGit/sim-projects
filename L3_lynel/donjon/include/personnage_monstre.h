#ifndef __personnage_monstre__
#define __personnage_monstre__
#include <MLV/MLV_all.h>
#include <stdbool.h>
#include "objets.h"


typedef struct coordo {
    int x;
    int y;
} Coordo;

typedef enum type_attaque {
	PHYSIQUE,
	MAGIQUE
} Type_Attaque;

typedef enum direction {
	HAUT,
	BAS,
	GAUCHE,
	DROITE,
	FOUILLE, /*animation de fouille*/
	MELEE, /*animation de combat*/
	HERE /*aucune direction*/
} Direction;

typedef enum type_monstre {
	BLOB, /*le BLOB se déplace aléatoirement et n'attaque pas toujours le joueur*/
	LOBO, /*le LOBO va suivre le joueur jusqu'à sa mort et l'attaquera toujours*/
	BOSS /*le boss se comporte comme le LOBO mais il a une attaque spéciale, il est plus puissant et n'apparaît que tous les 5 étages*/
} Type_Monstre;

typedef struct monster {
    int pv; int pv_max;
    int atk; int crit;
	int exp;
	Direction direction;
	Type_Attaque type_attaque;
	Type_Monstre type;
} Monster;

typedef struct player {
    int pv; int mp;
	int lvl; int exp;
	int atk; int crit;
	int intel; int def;
	int etage; int last_etage; /*étage actuel et dernier étage visité*/
	int run; int bonus_exp; /*vitesse de déplacement et bonus d'exp*/
	Direction direction;
	Type_Attaque type_attaque;
	Item inventaire[TAILLE_INVENTAIRE];
	int e_weapon; int e_armor; int e_staff; /*équipements équipés*/
} Player;


/* Fonctions concernant le personnage, les monstres, les combats, les actions dans l'inventaire,
les conversions de directions ou les clics avec la souris. */

Player creation_joueur();
Monster creation_monstre(int etage, int i);
Monster creation_boss(int etage);
int combat(Player* player, Monster* monster, int ordre, int* degats, bool* chance);
void chance_critique(int* degats, int crit, bool* chance);
void switch_type_attaque(Player* player);
bool contact_joueur(Coordo perso, Direction d, Coordo monstre);

Direction donne_direction(MLV_Keyboard_button keyboard_direction);
void deplacement_index_inventaire(MLV_Sound* sounds[], MLV_Event event, MLV_Keyboard_button clavier, int mouseX, int mouseY, int* index, Itemtype treizieme);
int action_dans_inventaire(MLV_Sound* sounds[], MLV_Event event, MLV_Keyboard_button clavier, int mouseX, int mouseY, Player* player, Item* potion, Item item, Item treizieme, int* index, int vide);
Coordo donne_coordo_voisin(Coordo c, Direction d);
Direction conversion_direc(Direction d);
int mouse_clic(int mouseX, int mouseY, int infX, int infY, int supX, int supY);


#endif