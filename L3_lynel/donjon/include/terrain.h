#ifndef __terrain__
#define __terrain__
#include "personnage_monstre.h"

#define TAILLE_X 63 /*taille en X du terrain*/
#define TAILLE_Y 43 /*taille en Y du terrain*/
#define MAX_OBJ_C 50 /*nb maximum d'objet d'un seul type sur un étage*/


/*représente le champ de vision du personnage*/
typedef struct vision {
	Coordo* champ;
	int taille;
} Vision;

/*représente les différentes directions d'un Mur*/
typedef enum wallstype {
	NONE, NO, N, NE, O, E, SO, S, SE,
	E1, E2, E3, E4, E3_DEG, E4_DEG, E5,
	E6, E7, E8, E9, WALL_SP1, WALL_SP2
} Wallstype;

/*représente un Mur, certains peuvent avoir deux types*/
typedef struct walls {
	Wallstype direc;
	Wallstype direc2;
} Walls;

typedef enum celltype {
	WALL, /*mur*/
	GROUND, /*sol*/
	MONSTER, /*ennemi*/
	TREASURE, /*coffre*/
	PLAYER, /*joueur*/
	STAIR_UP, /*début de l'étage*/
	STAIR_DOWN, /*fin de l'étage*/
	NOTHING,
	TREASURE_OPEN, /*coffre ouvert*/
	GROUND_SP1, GROUND_SP2, /*représente des éléments utilisés uniquement dans l'intro (design spécial d'un sol)*/
	GROUND_SP3, GROUND_SP4
} Celltype;

typedef union entity {
	Monster monster;
	Treasure treasure;
	Walls wall;
} Entity;

typedef struct cell {
	Celltype type;
	Entity entity;
} Cell;

/*représente un étage du donjon*/
/*chaque élément non mouvant se situe dans [y][x][0]*/
/*les éléments qui se déplacent se trouvent dans [y][x][1]*/
typedef struct floor {
    Cell terrain[TAILLE_Y][TAILLE_X][2]; /*terrain*/
    unsigned int seed; /*graine aléatoire*/
    int theme; /*design du terrain*/
    Coordo coordo_perso; /*coordonnées du personnage*/
    Vision vision; /*champ de vision*/
} Floor;

/*met en lien une entitée (monster, treasure) avec ses coordonnées*/
typedef struct coordo_entity {
	Entity entity;
	Coordo coordo;
} Coordo_Entity;

/*structure utilisée pour stocker des listes de coordonnées/d'entitées*/
typedef struct lst_coordo_entity {
    Coordo_Entity c[MAX_OBJ_C];
    int taille;
} Lst_Coordo_Entity;


/* Le reste des fonctions liées au terrain (aucune génération aléatoire) */

Celltype check_case(Floor floor, Direction d, Coordo c, int place);
Entity* donne_entity(Floor* floor, Direction d, Coordo c, int place);
Lst_Coordo_Entity recupere_coordo_all_obj(Floor floor, Celltype obj, int place, Coordo* escalier);
void assigne_coordo_tab(Coordo** tab, int* taille, int vx, int vy);

void init_vision(Floor* floor);
void creation_vision(Floor* floor, Coordo check, int restant);
int dans_champ_vision(Floor floor, Celltype obj, int place, Coordo* lst_c);

Direction direction_monstre(Floor floor, int x, int y);
Direction direction_intelligente(Floor floor, int x, int y, int appel, Direction init, Direction last);

void donne_wall_direction(Floor* floor, Wallstype type, int i, int j);
void specialise_walls(Floor* floor);
void affiche_terrain_terminal(Floor floor);


#endif