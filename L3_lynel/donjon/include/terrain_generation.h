#ifndef __terrain_generation__
#define __terrain_generation__
#include "terrain.h"


/* Tout ce qui touche à la génération aléatoire du terrain se retrouve ici. */

Floor creation_terrain_complet(unsigned int seed, int etage);
void creation_terrain(Floor* floor, unsigned int seed);
void generation(Floor* floor, Coordo* toexpand, int taille, unsigned int seed);
int admi(Floor* floor, int y, int x);
void bout(Floor* floor, Celltype obj, int etage);
void generation_objet(Floor* floor, unsigned int seed, int etage, Celltype obj, int place, int distance, int mode, int num);
void placement_objet(Floor* floor, int etage, Celltype obj, Coordo choisi, int place, int num);
void placement_all_obj(Floor* floor, Celltype obj, Lst_Coordo_Entity lst, int place);
void remplissage_initial(Floor* floor);
void free_terrain(Floor* floor);


#endif