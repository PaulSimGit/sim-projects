#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/graphique.h"


/** Affiche une frame du jeu (terrain, personnage, monstres, ...). Les paramètres indiquent les 
 * données x y et les frames du joueur / monstres. Si on veut simplement afficher le jeu, on met tout à 0.
 * \param decal_x Décalage horizontal.
 * \param decal_y Décalage vertical.
 * \param frame Frames du personnage (image à afficher).
 * \param decal_x_monstre Décalage horizontal du monstre.
 * \param decal_y_monstre Décalage vertical du monstre.
 * \param frame_monstre Frames du monstre (image à afficher).
 * \param mstr_actuel Coordonnées X,Y du monstre qui se déplace actuellement ou NULL. */
void affiche_jeu(MLV_Image* images[], Floor floor, Player player, int decal_x, int decal_y, int frame,
					int decal_x_monstre, int decal_y_monstre, int frame_monstre, Coordo* mstr_actuel){
	int i, j, i_draw = -2, j_draw = -2, taille=1;
	Coordo* monstres = malloc(sizeof(Coordo));
	Coordo* monstres_draw = malloc(sizeof(Coordo));

	/* i_draw et j_draw sont les coordonnées pour savoir où dessiner sur la fenêtre
	  (0,0) etant le point en haut à gauche
	  on commence à -2 car on dessine aussi le terrain en dehors du cadre, pour l'afficahge pendant que le personnage bouge
	  et pour afficher les murs autour du sol */

	/* fond en arrière plan qui change en fonction du thème */
	MLV_draw_partial_image(images[7], 0, 864*floor.theme, 1824, 864, 0, 0);

	/* Terrain */
	for(i=floor.coordo_perso.y-6; i<floor.coordo_perso.y+7; i++){
		for(j=floor.coordo_perso.x-11; j<floor.coordo_perso.x+12; j++){

			if( i>=0 && i<TAILLE_Y && j>=0 && j<TAILLE_X ){
				switch(floor.terrain[i][j][0].type){
					case GROUND:
						MLV_draw_partial_image(images[6], 288*floor.theme, 0, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case STAIR_UP:
						MLV_draw_partial_image(images[6], 96+(288*floor.theme), 0, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case STAIR_DOWN:
						MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 0, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case TREASURE:
						MLV_draw_partial_image(images[6], 288*floor.theme, 0, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y);
						MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96*4, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case TREASURE_OPEN:
						MLV_draw_partial_image(images[6], 288*floor.theme, 0, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y);
						MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96*7, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case GROUND_SP1: /*sur le fichier, en position du mur impossible*/
						MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96*2, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case GROUND_SP2: /*sur le fichier, en position du mur E4*/
						MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96*6, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case GROUND_SP3: /*sur le fichier, en position du mur E3*/
						MLV_draw_partial_image(images[6], (288*floor.theme), 96*6, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case GROUND_SP4: /*sur le fichier, en bas à gauche*/
						MLV_draw_partial_image(images[6], (288*floor.theme), 96*8, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break;
					case WALL:
						affiche_wall(images, floor, i_draw, j_draw, decal_x, decal_y, floor.terrain[i][j][0].entity.wall.direc);
						affiche_wall(images, floor, i_draw, j_draw, decal_x, decal_y, floor.terrain[i][j][0].entity.wall.direc2);
					default: break;
				}
				switch(floor.terrain[i][j][1].type){
					case MONSTER:
						monstres[taille-1].x = j;
						monstres[taille-1].y = i;
						monstres_draw[taille-1].x = j_draw;
						monstres_draw[taille-1].y = i_draw;
						taille++;
						monstres = realloc(monstres, taille*sizeof(Coordo));
						monstres_draw = realloc(monstres_draw, taille*sizeof(Coordo));
					default: break;
				}
			}

			j_draw++;
		}
		i_draw++; j_draw = -2;
	}

	/*Affichage des monstres*/
	for(i=0; i<taille-1; i++){
		/*monstres mouvants*/
		if(mstr_actuel != NULL && mstr_actuel->x == monstres[i].x && mstr_actuel->y == monstres[i].y)
			affiche_monstre(images, (monstres_draw[i].x)*96+decal_x+decal_x_monstre, (monstres_draw[i].y)*96+decal_y+decal_y_monstre,
				floor.terrain[monstres[i].y][monstres[i].x][1].entity.monster, frame_monstre);
		else /*monstres non mouvants*/
			affiche_monstre(images, (monstres_draw[i].x)*96+decal_x, (monstres_draw[i].y)*96+decal_y,
				floor.terrain[monstres[i].y][monstres[i].x][1].entity.monster, 0);
	}

	if(floor.theme == 2) /*effet de lumière pour le labyrinthe thème 2(sombre)*/
		affiche_vision(images, floor, decal_x, decal_y, 0);

	affiche_perso(images, player, frame);

	/* avant plan (effets visuels) */
	MLV_draw_partial_image(images[7], 1824, 864*floor.theme, 1824, 864, 0, 0);

	affiche_UI(images, player);
}


/** Lance l'animation du terrain pour un affichage fluide
 * \param decal_x déplacement horizontal du plateau
 * \param decal_y déplacement vertical du plateau
 * \param run vitesse de déplacement */
void animation_terrain(MLV_Image* images[], Floor floor, Player player, int decal_x, int decal_y, int run){
	int i;
	for(i=0; i<96; i=i+5+run){
		affiche_jeu(images, floor, player, i*decal_x, i*decal_y, i/12, 0, 0, 0, NULL);
		MLV_actualise_window();
		MLV_wait_milliseconds(1);
	}
}


/** Affichage du jeu pendant que le personnage bouge
 * \return 1 si le déplacement a été effectué, 0 si le joueur a touché un mur
 * et 2 si un combat se lance. */
int deplacement_perso(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player player){
	int inverse = 1; Coordo p;
	/*si inverse == 1 on déplacera à gauche ou en haut
	mais si inverse == -1 on inverse ces directions (donc droite ou bas)*/

	p.x = floor->coordo_perso.x; p.y = floor->coordo_perso.y;

	/*Si le personnage se déplace vers un mur*/
	if(check_case(*floor, player.direction, p, 0) == WALL)
		return 0;
	if(check_case(*floor, player.direction, p, 1) == MONSTER)
		return 2;

	if(player.direction == DROITE || player.direction == BAS)
		inverse = -1;

	MLV_play_sound(sounds[7], 10.0);

	/*Droite(inverse=-1) et Gauche*/
	if(player.direction == DROITE || player.direction == GAUCHE){
		animation_terrain(images, *floor, player, inverse, 0, player.run);
		floor->coordo_perso.x = p.x - inverse;}

	/*Haut(inverse=-1) et Bas*/
	else{
		animation_terrain(images, *floor, player, 0, inverse, player.run);
		floor->coordo_perso.y = p.y - inverse;}

	free_terrain(floor); /*free champ de vision*/
	init_vision(floor); /*mise à jour champ de vision*/
	return 1;
}


/** Déplace le monstre dans le terrain avec une animation.
 * \param origine coordonnées du monstre qui se déplace */
void deplacement_monstre(MLV_Image* images[], Floor* floor, Player player, Direction direc, Coordo origine){
	int i, coefx, coefy, newX = origine.x, newY = origine.y;
	switch(direc){ /*nouvelles coordonnées*/
		case DROITE: newX++; coefx=1; coefy=0; break;
		case GAUCHE: newX--; coefx=-1; coefy=0; break;
		case BAS: newY++; coefx=0; coefy=1; break;
		default: newY--; coefx=0; coefy=-1; /*HAUT*/
	}
	/*déplacement*/
	floor->terrain[origine.y][origine.x][1].entity.monster.direction = direc;
	if(floor->terrain[origine.y][origine.x][1].entity.monster.type == BOSS){
		for(i=0; i<96; i=i+8){
			affiche_jeu(images, *floor, player, 0, 0, 0, i*coefx, i*coefy, i/12, &origine);
			MLV_actualise_window();
			MLV_wait_milliseconds(8);}
	}
	else{ /*un monstre normal*/
		for(i=0; i<96; i=i+11){
			affiche_jeu(images, *floor, player, 0, 0, 0, i*coefx, i*coefy, i%2, &origine);
			MLV_actualise_window();
			MLV_wait_milliseconds(10);}
	}
	floor->terrain[origine.y][origine.x][1].type = NOTHING;
	floor->terrain[newY][newX][1].type = MONSTER;
	floor->terrain[newY][newX][1].entity.monster = floor->terrain[origine.y][origine.x][1].entity.monster;
}




/** Permet d'obscursir les cases qui ne sont pas dans la vision du personnage.
 * \param type_vision 0 pour une vision sombre et 1 pour une vision très sombre */
void affiche_vision(MLV_Image* images[], Floor floor, int decal_x, int decal_y, int type_vision){
	int i, j, k, dans_vision, i_draw = -1, j_draw = -1;
	for(i=floor.coordo_perso.y-5; i<floor.coordo_perso.y+6; i++){
		for(j=floor.coordo_perso.x-10; j<floor.coordo_perso.x+11; j++){
			dans_vision = 0;
			for(k=0; k<floor.vision.taille; k++){
				if( floor.vision.champ[k].x == j && floor.vision.champ[k].y == i ){
					dans_vision=1; break;}
			}
			if(!dans_vision)
				MLV_draw_partial_image(images[6], 0+type_vision*96, 96*8, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y);
			j_draw++;
		}
		i_draw++; j_draw = -1;
	}
}


/*Affichage d'une mini carte*/
void affiche_carte(MLV_Image* images[], MLV_Sound* sounds[], Floor floor, Player player){
	MLV_Button_state etat;
	MLV_play_sound(sounds[9], 10.0);
	MLV_wait_milliseconds(450);
	
	/*Affichage de la carte de plus en plus grand*/
	animation_carte(images, floor, player, 0);
	MLV_flush_event_queue();

	/*On attend une action*/
	MLV_wait_event(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &etat);
	MLV_play_sound(sounds[10], 10.0);
	MLV_wait_milliseconds(400);

	/*Affichage de la carte de plus en plus petit*/
	animation_carte(images, floor, player, 1);
	MLV_flush_event_queue();
}


/** Animation de la mini carte
 * \param mode == 0 pour un aggrandissement ou == 1 pour un rétrécissement */
void animation_carte(MLV_Image* images[], Floor floor, Player player, int mode){
 	int i, iter, x, y, x_draw = 0, y_draw = 0; Coordo p;
	p.x = floor.coordo_perso.x;
	p.y = floor.coordo_perso.y;

	for(iter=0; iter<11; iter++){
		if(mode == 1){
			affiche_jeu(images, floor, player, 0, 0, 0, 0, 0, 0, NULL);
			i = 8-iter;}
		else i = iter;

		for(y=0; y<TAILLE_Y; y++){
			for(x=0; x<TAILLE_X; x++){
				if(floor.terrain[y][x][0].type == GROUND || floor.terrain[y][x][0].type == TREASURE_OPEN)
					MLV_draw_filled_rectangle(x_draw*i, y_draw*i, i, i, MLV_COLOR_INDIANRED4);
                if(floor.terrain[y][x][0].type == STAIR_DOWN)
					MLV_draw_filled_rectangle(x_draw*i, y_draw*i, i, i, MLV_COLOR_CYAN1);
				if(floor.terrain[y][x][0].type == TREASURE)
					MLV_draw_filled_rectangle(x_draw*i, y_draw*i, i, i, MLV_COLOR_YELLOW);
				if(floor.terrain[y][x][0].type == WALL)
					MLV_draw_filled_rectangle(x_draw*i, y_draw*i, i, i, MLV_COLOR_BLACK);
				if(MONSTER_ON_MAP && floor.terrain[y][x][1].type == MONSTER)
					MLV_draw_filled_rectangle(x_draw*i, y_draw*i, i, i, MLV_COLOR_WHITE);
				x_draw++;
			}
			x_draw = 0; y_draw++;
		}
		x_draw = 0; y_draw = 0;

		MLV_draw_filled_rectangle(TAILLE_X/2*i, TAILLE_Y/2*i, i, i, MLV_COLOR_INDIANRED3); /*STAIR_UP*/
		MLV_draw_filled_rectangle(p.x*i, p.y*i, i, i, MLV_COLOR_LIME_GREEN); /*PLAYER*/
		MLV_actualise_window();
		MLV_wait_milliseconds(10);
	}
}


/*Affiche une transition graphique entre deux étages*/
void animation_transition(MLV_Image* images[], Floor floor, Player player){
	int i, speed = 0;
	for(i=0; i<1824; i+=10){ /*apparition écran noir*/
		MLV_draw_filled_rectangle(0, 0, i, 864, MLV_COLOR_BLACK);
		MLV_actualise_window();
	}
	for(i=0; i<1824; i+=40){ /*apparition jeu*/
		/*speed: le jeu s'affiche tous les 5 tours de boucle (+optimisé, +rapide)*/
		if(speed == 0){affiche_jeu(images, floor, player, 0, 0, 0, 0, 0, 0, NULL); speed++;}
		else if(speed == 5) speed = 0;
		else speed++;
		MLV_draw_filled_rectangle(i, 0, 1824-i, 864, MLV_COLOR_BLACK);
		MLV_actualise_window();
	}
}




/** Gère l'affichage des actions spéciales du personnage (fouille de trésor et combat).
 * \param chance true si un coup critique est effectué, false sinon. Si on lance cette fonction pour une
 * autre animation que celle du combat, la valeur de ce paramètre n'influencera rien. */
void animation_perso(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player player, bool chance){
	int i; Item item;
	switch(player.direction){
		case FOUILLE:
			for(i=0; i<12; i++){
				affiche_jeu(images, *floor, player, 0, 0, i%2, 0, 0, 0, NULL);
				MLV_actualise_window();
				MLV_wait_milliseconds(60);
			}
			MLV_play_sound(sounds[4], 10.0);
			MLV_wait_milliseconds(100);
			item = floor->terrain[floor->coordo_perso.y][floor->coordo_perso.x][0].entity.treasure.item;
			floor->terrain[floor->coordo_perso.y][floor->coordo_perso.x][0].type = TREASURE_OPEN;
			affiche_jeu(images, *floor, player, 0, 0, 2, 0, 0, 0, NULL);
			affiche_item(images, item, 894, 292, 0);
			MLV_actualise_window();
			MLV_wait_seconds(1);
			break;

		case MELEE:
			if(player.type_attaque == PHYSIQUE){
				if(chance) MLV_play_sound(sounds[1], 10.0);
				MLV_play_sound(sounds[2], 10.0);
				MLV_wait_milliseconds(200);
				for(i=0; i<12; i++){
					affiche_jeu(images, *floor, player, 0, 0, i, 0, 0, 0, NULL);
					MLV_actualise_window();
					MLV_wait_milliseconds(40);
				}
			}
			else{ /*if MAGIQUE*/
				if(chance) MLV_play_sound(sounds[1], 10.0);
				MLV_play_sound(sounds[6], 10.0);
				MLV_wait_milliseconds(200);
				for(i=0; i<5; i++){
					affiche_jeu(images, *floor, player, 0, 0, i, 0, 0, 0, NULL);
					MLV_actualise_window();
					MLV_wait_milliseconds(10);}
				MLV_wait_milliseconds(60);
				for(i=0; i<24; i++){
					affiche_jeu(images, *floor, player, 0, 0, i%8+4, 0, 0, 0, NULL);
					MLV_actualise_window();
					MLV_wait_milliseconds(20);}
				for(i=4; i>-1; i--){
					affiche_jeu(images, *floor, player, 0, 0, i, 0, 0, 0, NULL);
					MLV_actualise_window();
					MLV_wait_milliseconds(10);}
			}
			break;
		default: break;
	}
	MLV_flush_event_queue();
}


/** Effectue l'animation d'attaque du boss.
 * \param c_boss Coordonnées du boss sur le terrain. */
void animation_attaque_boss(MLV_Image* images[], Floor* floor, Player player, Coordo c_boss){
	int i, x, y, teleport, end = 0; Coordo escalier;
	Direction direc_temp = floor->terrain[c_boss.y][c_boss.x][1].entity.monster.direction;

	if(rand()%100 <= 50){ /*50% de chances d'effectuer une attaque spéciale de téléportation*/
		floor->terrain[c_boss.y][c_boss.x][1].entity.monster.type_attaque = MAGIQUE;
		teleport = 1;}
		
	floor->terrain[c_boss.y][c_boss.x][1].entity.monster.direction = MELEE;
	for(i=0; i<12; i++){ /*animation d'attaque*/
		affiche_jeu(images, *floor, player, 0, 0, 0, 0, 0, i, &c_boss);
		MLV_actualise_window();
		MLV_wait_milliseconds(40);}
		
	if(teleport == 1){ /*action de téléportation*/
		for(y=0; y<TAILLE_Y; y++){ /*recherche de l'escalier*/
			for(x=0; x<TAILLE_X; x++){
				if(floor->terrain[y][x][0].type == STAIR_DOWN){
					escalier.x = x; escalier.y = y; end = 1;}
				if(end == 1) break;
			}
			if(end == 1) break;
		} /*téléportation sur l'escalier*/
		floor->terrain[c_boss.y][c_boss.x][1].type = NOTHING;
		floor->terrain[escalier.y][escalier.x][1].type = MONSTER;
		floor->terrain[escalier.y][escalier.x][1].entity.monster = floor->terrain[c_boss.y][c_boss.x][1].entity.monster;
		c_boss.x = escalier.x; c_boss.y = escalier.y;
		affiche_jeu(images, *floor, player, 0, 0, 0, 0, 0, 0, NULL); /*mise à jour de l'affichage pour la téléportation*/
		MLV_actualise_window();
	}
	floor->terrain[c_boss.y][c_boss.x][1].entity.monster.direction = direc_temp;
	floor->terrain[c_boss.y][c_boss.x][1].entity.monster.type_attaque = PHYSIQUE;
}




/** Cette fonction va lancer le combat et afficher les animations du combat une fois celui-ci terminé.
 * Descriptions des variables:
 *     degats = nb de dégâts infligés
 *     resultat = 1 si le joueur est mort, 2 si le monstre est mort ou 0 si ils sont encore vivants
 *     coefx/coefy = influence les coefficients de directions x/y pour une attaque d'un monstre
 *     deca = nb de frames à ignorer dans le fichier image pour récupérer les frames de clignotements d'un mosntre
 *     img_mstr = 8 si on s'occupe d'un monstre et 11 pour le boss
 *     dfm = découpeur de frames de monstre, 1 si on s'occupe d'un monstre et 0 pour un boss
 *     chance = vaut true si un coup critique a été effectué
 * \param player Pointeur sur le joueur concerné par le combat.
 * \param monster Pointeur sur le monstre concerné par le combat.
 * \param c_mstr Coordonnées du monstre concerné par le combat.
 * \param ordre Ordre du combat, 1 si le joueur attaque en premier ou 0 si le monstre attaque.
 * \return 1 si le joueur est mort et 0 sinon. */
int animation_combat(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player* player, Monster* monster, Coordo c_mstr, int ordre){
	int i, degats, resultat, coefx, coefy, deca = 2, img_mstr = 8, dfm = 1; bool chance = false;
	Direction d, temp_player = player->direction;
	Type_Monstre type_m = monster->type;

	if(player->type_attaque == MAGIQUE && player->mp < 20) /*vérifie si on a assez de MP pour une attaque magique*/
		switch_type_attaque(player);

	resultat = combat(player, monster, ordre, &degats, &chance); /*effectue le combat*/

	if(ordre == 1){ /*attaque du joueur*/
		player->direction = MELEE; /*animation d'attaque du joueur*/
		animation_perso(images, sounds, floor, *player, chance);

		if(type_m == BOSS){ /*adaptation données d'affichage si le combat concerne le BOSS*/
			img_mstr = 11; deca = 8; dfm = 0;}
		d = conversion_direc(monster->direction);
		for(i=0; i<10; i++){ /*clignotement du monstre*/
			if(c_mstr.x-1 == floor->coordo_perso.x && c_mstr.y == floor->coordo_perso.y){
				MLV_draw_partial_image(images[img_mstr], (96*(i%2+deca))+(96*4*type_m*dfm), 96*d, 96, 96, 864+96, 384-12);
				nombre_degats(images, degats, 864+35+96, 384-40, chance);} /*si monstre est à droite du joueur*/
			if(c_mstr.x+1 == floor->coordo_perso.x && c_mstr.y == floor->coordo_perso.y){
				MLV_draw_partial_image(images[img_mstr], (96*(i%2+deca))+(96*4*type_m*dfm), 96*d, 96, 96, 864-96, 384-12);
				nombre_degats(images, degats, 864+35-96, 384-40, chance);} /*si monstre est à gauche du joueur*/
			if(c_mstr.x == floor->coordo_perso.x && c_mstr.y-1 == floor->coordo_perso.y){
				MLV_draw_partial_image(images[img_mstr], (96*(i%2+deca))+(96*4*type_m*dfm), 96*d, 96, 96, 864, 384-12+96);
				nombre_degats(images, degats, 864+35, 384+96+60, chance);} /*si monstre est en bas du joueur*/
			if(c_mstr.x == floor->coordo_perso.x && c_mstr.y+1 == floor->coordo_perso.y){
				MLV_draw_partial_image(images[img_mstr], (96*(i%2+deca))+(96*4*type_m*dfm), 96*d, 96, 96, 864, 384-12-96);
				nombre_degats(images, degats, 864+35, 384-96-40, chance);} /*si monstre est au dessus du joueur*/
			MLV_actualise_window();
			MLV_wait_milliseconds(50);
		}
		player->direction = temp_player;
	}

	if(ordre == 0){ /*attaque du monstre*/
		if(chance) MLV_play_sound(sounds[1], 10.0);
		MLV_play_sound(sounds[0], 10.0);
		affiche_jeu(images, *floor, *player, 0, 0, 0, 0, 0, 0, NULL);
		if(type_m == BOSS) animation_attaque_boss(images, floor, *player, c_mstr);
		else{ /*animation d'attaque du monstre*/
			switch(monster->direction){
				case DROITE: coefx=1; coefy=0; break;
				case GAUCHE: coefx=-1; coefy=0; break;
				case BAS: coefx=0; coefy=1; break;
				default: coefx=0; coefy=-1;} /*HAUT*/
			for(i=0 ; i<52 ; i=i+11){
				affiche_jeu(images, *floor, *player, 0, 0, 0, i*coefx, i*coefy, i%2, &c_mstr);
				MLV_actualise_window();
				MLV_wait_milliseconds(10);
			}
		}

		d = conversion_direc(player->direction);
		for(i=0; i<10; i++){ /*clignotement du joueur*/
			MLV_draw_partial_image(images[5], 96*(i%2+8), d*96, 96, 96, 864, 370);
			nombre_degats(images, degats, 895, 345, chance);
			MLV_actualise_window();
			MLV_wait_milliseconds(50);
		}
	}

	if(resultat == 2){ /*monstre mort*/
		floor->terrain[c_mstr.y][c_mstr.x][1].type = NOTHING;
		donne_exp(images, sounds, player, monster->exp);}

	if(resultat == 1) /*joueur mort*/
		return 1;

	return 0;
}


/*Effectue l'animation de mort et quitte le jeu si le joueur est mort.*/
void game_over(MLV_Image* images[], Floor* floor, Player* player){
	int i;
	MLV_stop_music();

	for(i=0; i<40; i++){ /*fond noir progressif*/
		player->direction = BAS;
		affiche_perso(images, *player, 0);
		MLV_draw_partial_image(images[0], 0, 864*6, 1824, 864, 0, 0);
		MLV_actualise_window();
		MLV_wait_milliseconds(50);
	}
	for(i=0; i<20; i++){ /*le joueur tourne*/
		MLV_clear_window(MLV_COLOR_BLACK);
		switch(i%4){
			case 0: player->direction = 0; break;
			case 1: player->direction = 3; break;
			case 2: player->direction = 1; break;
			default: player->direction = 2; break;}
		affiche_perso(images, *player, 0);
		MLV_actualise_window();
		MLV_wait_milliseconds(100);
	}
	MLV_clear_window(MLV_COLOR_BLACK); /*le joueur est mort*/
	MLV_draw_partial_image(images[5], 0, 96*5, 96, 96, 864, 370);
	MLV_actualise_window();

	MLV_wait_milliseconds(500);
	for(i=0; i<40; i++){ /*message game over progressif*/
		MLV_draw_partial_image(images[0], 1824, 864*7, 1824, 300, 0, 0);
		MLV_actualise_window();
		MLV_wait_milliseconds(50);
	}

	MLV_wait_milliseconds(100); /*affichage des stats*/
	MLV_draw_partial_image(images[0], 1824, 864*7+300, 1824, 864-300, 0, 300);
	nombre_stats(images, player->last_etage, 500, 690, 5);
	nombre_stats(images, player->lvl, 1200, 690, 6);
	MLV_actualise_window();

	MLV_wait_milliseconds(300);
	MLV_wait_keyboard_or_mouse(NULL, NULL, NULL, NULL, NULL);
	free_terrain(floor);
	remove("saves/player_data.save");
}
