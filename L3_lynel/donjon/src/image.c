#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/image.h"


/*Affiche l'UI et les menus*/
void affiche_UI(MLV_Image* images[], Player player){
	MLV_draw_partial_image(images[0], 0, 864, 1824, 864, 0, 0); /*menus*/
	nombre_UI(images, player.etage, 40, 140, 3); /* numéro de l'étage */

	/* icones à gauche */
	if(player.etage != 0){
		MLV_draw_partial_image(images[0], 1824, 5930, 110, 118, 96, 258); /*map*/
		MLV_draw_partial_image(images[0], 2451, 5930, 110, 118, 1572, 558); /*stat*/
		MLV_draw_partial_image(images[0], 2563, 5930, 110, 118, 1684, 558); /*inventaire*/
		MLV_draw_partial_image(images[0], 2675, 5930, 110, 118, 1628, 686); /*save*/
	}
	if(player.type_attaque == PHYSIQUE)
		MLV_draw_partial_image(images[0], 2049, 5952, 96, 96, 36, 501);
	else /*MAGIQUE*/
		MLV_draw_partial_image(images[0], 2161, 5952, 96, 96, 148, 501);
	if(player.run > 3)
		MLV_draw_partial_image(images[0], 1934, 5930, 110, 118, 96, 662);

	/* points de vies */
	if(player.pv <= 0) nombre_UI(images, 0, 1580, 111, 1);
	else nombre_UI(images, player.pv, 1580, 111, 1);
	nombre_UI(images, player.def*10, 1580, 191, 1);

	/* points de magie */
	nombre_UI(images, player.mp, 1580, 363, 0);
	nombre_UI(images, 10*player.intel-50, 1580, 443, 0);
}


/*Affichage centré d'un petit nombre, utilisé dans affiche_UI().*/
void nombre_UI(MLV_Image* images[], int nombre, int x, int y, int color){
	char chaine[4]; int taille, i, decal;
	sprintf(chaine, "%d", nombre);
	taille = strlen(chaine);

	switch(taille){
		case 1: decal = 78; break;
		case 2: decal = 52; break;
		case 3: decal = 26; break;
		default: decal = 0;}

	for(i=0; i<taille; i++)
		MLV_draw_partial_image(images[2], (chaine[i]-48)*52, color*72, 52, 72, x+i*52+decal, y);
}


/*Affichage centré d'un grand nombre, utilisé dans affiche_stats().*/
void nombre_stats(MLV_Image* images[], int nombre, int x, int y, int color){
	char chaine[10]; int taille, i;
	if (nombre < 0)
		MLV_draw_partial_image(images[1], 720, color*96, 72, 96, x, y);
	else{
		sprintf(chaine, "%d", nombre);
		taille = strlen(chaine);

		for(i=0; i<taille; i++)
			MLV_draw_partial_image(images[1], (chaine[i]-48)*72, color*96, 72, 96, x+i*72, y);
	}
}


/** Affichage centré d'un nombre utilisé pour les dégâts.
 * \param chance true si un coup critique est effectué et false sinon (si true alors
 * un message Coup critique! s'affichera en plus des dégâts) */
void nombre_degats(MLV_Image* images[], int nombre, int x, int y, bool chance){
	char* chaine; int i, decal;
	chaine = malloc( (int)(log10(nombre)+2) * sizeof(char) ); 
	sprintf(chaine, "%d", nombre);
	decal = (strlen(chaine)-1)*13;
	for(i=0; i<(int)strlen(chaine); i++) /*dégâts*/
		MLV_draw_partial_image(images[9], (chaine[i]-48)*26, 0, 26, 36, x+(i*26)-decal, y);
	if(chance) /*message coup critique*/
		MLV_draw_partial_image(images[9], 10*26, 0, 4*26, 36, x-39, y-36);
	free(chaine);
}


/** Affichage du nombre d'exp donné avec un un symbole '+' au début.*/
void nombre_exp(MLV_Image* images[], int nombre, int x, int y){
	char* chaine; int i;
	chaine = malloc( (int)(log10(nombre)+2) * sizeof(char) ); 
	sprintf(chaine, "%d", nombre);
	for(i=0; i<(int)strlen(chaine)+1; i++){ 
		if(i == 0) /*symbole '+'*/
			MLV_draw_partial_image(images[9], 10*26, 36, 26, 36, x+(i*26), y);
		else
			MLV_draw_partial_image(images[9], (chaine[i-1]-48)*26, 36, 26, 36, x+(i*26), y);
	}
	free(chaine);
}




/** Affichage de l'inventaire du joueur et permet à ce dernier d'intéragir avec ses objets. 
 * Il peut changer son équipement, utiliser un objet et jeter un objet.
 * \param potion Pointeur vers un objet NO_ITEM qui sera rempli d'une potion si une potion a été utilisée.
 * \param treizieme Objet supplémentaire si on récupère un objet alors que l'inventaire est rempli ou NO_ITEM sinon.
 * \return Code d'action en int: 4(menu stats), 5(menu save) ou 0(quitter inventaire).
 * 		   Si on renvoie 0, plusieurs choses ont pu se produire: objet utilisé, objet équipé, objet jeté ou aucune action.
 * 		   L'action en question aura déjà été effectuée et l'inventaire du joueur sera donc à jour. */
int affiche_inventaire(MLV_Image* images[], MLV_Sound* sounds[], Player* player, Item* potion, Item treizieme){
	int action = 1, index = 0, vide = 0, affiche_qualite = 0, mouseX=0, mouseY=0;
	MLV_Keyboard_button clavier; Item item; MLV_Event event;
	MLV_play_sound(sounds[8], 10.0); MLV_wait_milliseconds(150);

	if(treizieme.type != NO_ITEM)
		MLV_draw_partial_image(images[0], 0, 864*7, 1824, 864, 0, 0); /*background inventaire quand inventaire plein*/
	else
		MLV_draw_partial_image(images[0], 0, 864*5, 1824, 864, 0, 0); /*background inventaire*/

	while(action == 1){
		MLV_draw_partial_image(images[0], 2024, 864*5, 1624, 864, 200, 0); /*menu(bulle) vide*/
		if(treizieme.type != NO_ITEM){
			MLV_draw_partial_image(images[0], 43, 6489, 196, 188, 43, 441); /*menu(bulle) vide (13e objet)*/
			affiche_item(images, treizieme, 70, 464, 1);} /*13e objet*/
		vide = affiche_all_items(images, *player); /*tous les objets*/

		if(!vide){ /*si l'inventaire n'est pas vide*/
			if(index != -1){ /*curseur inventaire*/
				if(index < 6)
					MLV_draw_partial_image(images[0], 1824, 864*5, 172, 172, 364+184*index, 448);
				else
					MLV_draw_partial_image(images[0], 1824, 864*5, 172, 172, 364+184*(index-6), 632);
			}
			else /*curseur (13e objet)*/
				MLV_draw_partial_image(images[0], 1824, 864*5, 172, 172, 54, 448);

			/*récupération de l'objet sur lequel se trouve le curseur*/
			if(index != -1) item = player->inventaire[index];
			else item = treizieme;

			if(item.type == WEAPON || item.type == ARMOR || item.type == MAGIC_STAFF){ /*équipement*/
				affiche_qualite = plage_item(item.entity.equipment.quality); /*type de l'equipement en fonction de la qualité*/
				switch(item.type){
					case WEAPON: 
						MLV_draw_partial_image(images[4], 1492, 300*affiche_qualite, 1824, 300, 0, 36); /*nom de l'équipement*/
						MLV_draw_partial_image(images[4], 1492*4, 300, 1824, 300, 0, 36); break; /*description de l'équipement*/
					case ARMOR: 
						MLV_draw_partial_image(images[4], 1492*2, 300*affiche_qualite, 1824, 300, 0, 36);
						MLV_draw_partial_image(images[4], 1492*4, 300*2, 1824, 300, 0, 36); break;
					default: /*MAGIC_STAFF*/
						MLV_draw_partial_image(images[4], 1492*3, 300*affiche_qualite, 1824, 300, 0, 36);
						MLV_draw_partial_image(images[4], 1492*4, 300*3, 1824, 300, 0, 36); break;
				}
				MLV_draw_partial_image(images[0], 1824, 5654, 1824, 140, 0, 341); /*message équiper (équipement)*/
				nombre_UI(images, item.entity.equipment.quality, 912-52*2, 200, 2); /*qualité équipement*/
			}
			else if(item.type != NO_ITEM){ /*potion*/
				MLV_draw_partial_image(images[4], 0, 300*item.type, 1824, 300, 0, 36); /*nom+description de la potion*/
				MLV_draw_partial_image(images[0], 1824, 864*6, 1824, 450, 0, 0);} /*messgae utiliser (potion)*/
		}
		MLV_actualise_window(); MLV_flush_event_queue();

		/*actions utilisateur dans l'inventaire*/
		event = MLV_wait_keyboard_or_mouse(&clavier, NULL, NULL, &mouseX, &mouseY);
		action = action_dans_inventaire(sounds, event, clavier, mouseX, mouseY, player, potion, item, treizieme, &index, vide);
	}
	if(action == 0 || action == 6){
		MLV_play_sound(sounds[8], 10.0); MLV_wait_milliseconds(150);
		return 0;}
	if(action == -1) return 0;
	return action;
}


/** Affiche tous les objets de l'inventaire du joueur.
 * \return renvoie 1 si l'inventaire est vide et 0 sinon. */
int affiche_all_items(MLV_Image* images[], Player player){
	int i, vide = 0;
	for(i=0; i<TAILLE_INVENTAIRE; i++){
		/*Affiche les objets*/
		if(i < 6){
			affiche_item(images, player.inventaire[i], 380+184*i, 464, 1);
			if(i == player.e_weapon || i == player.e_staff || i == player.e_armor) /*équipement équipé ?*/
				MLV_draw_partial_image(images[0], 2269, 6013, 35, 35, 102+380+184*i, 102+464);}
		else{
			affiche_item(images, player.inventaire[i], 380+184*(i-6), 648, 1);
			if(i == player.e_weapon || i == player.e_staff || i == player.e_armor) 
				MLV_draw_partial_image(images[0], 2269, 6013, 35, 35, 102+380+184*(i-6), 102+648);}

		/*Compte les objets NO_ITEM*/
		if(player.inventaire[i].type == NO_ITEM)
			vide++;
	}
	if(vide == TAILLE_INVENTAIRE)
		return 1;
	return 0;
}


/** Affiche un objet en fonction des coordonnées données et du mode d'affiche demandé.
 * \param item l'objet qui doit être affiché.
 * \param x affiche en x pixels.
 * \param y affiche en y pixels.
 * \param mode_affichage 1 pour un affichage grand et 0 pour un petit affichage. */
void affiche_item(MLV_Image* images[], Item item, int x, int y, int mode_affichage){
	int type_img, taille_img, affichage_x, affichage_y;
	/*Choix de l'image*/
	if(mode_affichage){
		type_img = 3; taille_img = 140;}
	else{
		type_img = 10; taille_img = 84;}
	/*Règle les X Y de l'affichage selon le type de l'objet*/
	if(item.type == WEAPON || item.type == ARMOR || item.type == MAGIC_STAFF ){
		affichage_x = plage_item(item.entity.equipment.quality);
		switch(item.type){
			case WEAPON: affichage_y = 1; break;
			case ARMOR: affichage_y = 2; break;
			default: affichage_y = 3; /*MAGIC_STAFF*/
		}
	}
	else{ /*POTION et NO_ITEM*/
		affichage_x = item.type; affichage_y = 0;}
	MLV_draw_partial_image(images[type_img], affichage_x*taille_img, affichage_y*taille_img, taille_img, taille_img, x, y);
}




/*Affichage des stats du joueur*/
int affiche_stats(MLV_Image* images[], MLV_Sound* sounds[], Player player){
	int action;
	MLV_play_sound(sounds[8], 10.0); MLV_wait_milliseconds(150);
	
	MLV_draw_partial_image(images[0], 0, 864*2, 1824, 864, 0, 0);
	nombre_stats(images, player.pv, 734, 48, 3);
	nombre_stats(images, -1, 734+(int)(log10(player.pv)+1)*72, 48, 3);
	nombre_stats(images, 10*player.def, 734+(int)(log10(player.pv)+2)*72, 48, 3);
	nombre_stats(images, player.mp, 734, 144, 2);
	nombre_stats(images, -1, 734+(int)(log10(player.mp)+1)*72, 144, 2);
	nombre_stats(images, 10*player.intel-50, 734+(int)(log10(player.mp)+2)*72, 144, 2);
	nombre_stats(images, player.lvl, 970, 288, 6);
	nombre_stats(images, player.exp, 970, 384, 5);
	nombre_stats(images, player.atk, 1066, 524, 0);
	nombre_stats(images, player.intel, 1066, 616, 1);
	nombre_stats(images, player.def, 1066, 712, 4);
	MLV_actualise_window();

	action = navigation_menus(sounds);

	if(action == 0 || action == 4){
		MLV_play_sound(sounds[8], 10.0); MLV_wait_milliseconds(150);
		return 0;}
	return action;
}


/*Affichage la confirmation de sauvegarde.
Si c'est le cas, on renvoie 7 et ça quittera le jeu.*/
int affiche_save(MLV_Image* images[], MLV_Sound* sounds[]){
	int action;
	MLV_play_sound(sounds[8], 10.0); MLV_wait_milliseconds(150);
	
	MLV_draw_partial_image(images[0], 0, 864*4, 1824, 864, 0, 0);
	MLV_actualise_window();

	action = navigation_menus(sounds);
	if(action == 0){
		MLV_play_sound(sounds[8], 10.0); MLV_wait_milliseconds(150);
		return 0;}
	else if(action == 5){
		MLV_play_sound(sounds[13], 10.0); MLV_wait_milliseconds(150);
		return 7;}
	return action;
}


/** Permet de naviguer entre les différentes options du jeu en pleine partie:
 * Sauvegarder, Inventaire ou Stats (renvoie son code en int). 
 * \return 4: stats, 5: sauvegarder, 6: inventaire, 0: quitter navigation */
int navigation_menus(){
	int action = 1, mouseX=0, mouseY=0; MLV_Event event;
	MLV_Keyboard_button clavier; MLV_Button_state etat; MLV_Mouse_button mouse_button;
	MLV_flush_event_queue();
	while(action == 1){
		event = MLV_wait_event(&clavier, NULL, NULL, NULL, NULL, &mouseX, &mouseY, &mouse_button, &etat);
		if(etat == MLV_PRESSED){
			if((event == MLV_KEY && clavier == MLV_KEYBOARD_r) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 38, 738, 315, 820)))
				action = 0;
			if((event == MLV_KEY && clavier == MLV_KEYBOARD_a) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 1572, 580, 1667, 675)))
				action = 4;
			if((event == MLV_KEY && clavier == MLV_KEYBOARD_s) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 1627, 709, 1723, 801)))
				action = 5;
			if((event == MLV_KEY && clavier == MLV_KEYBOARD_z) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 1684, 580, 1780, 675)))
				action = 6;
		}
	}
	return action;
}




/*Affiche le joueur en fonction de sa direction (déplacement, attaque, fouille, etc...).
Le joueur est toujours affiché visuellement au centre du terrain en (864,370).*/
void affiche_perso(MLV_Image* images[], Player player, int frame){
	int d;
	MLV_draw_partial_image(images[5], 5*96, 4*96, 96, 96, 864, 375); /*ombre*/
	switch(player.direction){
		case DROITE: d = 0; break;
		case GAUCHE: d = 1; break;
		case BAS: d = 2; break;
		case HAUT: d = 3; break;
		case FOUILLE: MLV_draw_partial_image(images[5], frame*96, 4*96, 96, 96, 864, 370); return;
		case MELEE: 
			if(player.type_attaque == PHYSIQUE)
				MLV_draw_partial_image(images[5], frame*96*3, 6*96, 96*3, 96*3, 864-96, 370-96);
			else /*MAGIQUE*/
				MLV_draw_partial_image(images[5], frame*96*3, 9*96, 96*3, 96*3, 864-96, 370-96);
			return;
		default: return;
	}
	MLV_draw_partial_image(images[5], frame*96, d*96, 96, 96, 864, 370); /*joueur*/
}


/** Affiche un mosntre en (x,y)pixels sur le terrain en fonction de sa direction, sa frame et son type.
 * \return 0 si l'animation a été effectuée normalement et 1 si l'animation a été effectuée par un BOSS
 * et qu'il vient de lancer son attaque spéciale de téléportation. */
void affiche_monstre(MLV_Image* images[], int x, int y, Monster mstr, int frame){
	mstr.direction = conversion_direc(mstr.direction);
	MLV_draw_partial_image(images[5], 5*96, 4*96, 96, 96, x, y-9); /*ombre*/
	if(mstr.type == BOSS){ /*pour un BOSS*/
		if(mstr.direction == MELEE){ /*position d'attaque*/
			if(mstr.type_attaque == PHYSIQUE) /*attaque normale*/
				MLV_draw_partial_image(images[11], frame*96*3, 6*96, 96*3, 96*3, x-96, y-12-96);
			else /*attaque spéciale de téléportation*/
				MLV_draw_partial_image(images[11], frame*96*3, 9*96, 96*3, 96*3, x-96, y-12-96);
		}
		else /*autre position*/
			MLV_draw_partial_image(images[11], 96*frame, 96*mstr.direction, 96, 96, x, y-12);
	}
	else MLV_draw_partial_image(images[8], (96*frame)+(96*4*mstr.type), 96*mstr.direction, 96, 96, x, y-12); /*pour un monstre normal*/
	MLV_draw_filled_rectangle(x, y-20, 96, 10, MLV_COLOR_BLACK);
	if(mstr.pv > 0) /*barre de vie*/
		MLV_draw_filled_rectangle(x+3, y-17, (int)((float)((float)mstr.pv/(float)mstr.pv_max)*90), 4, MLV_COLOR_RED);
}


/** Message qui apparait progressivement.
 * \param i_message indice du message à afficher, si i_message == 0 alors on affichera le message
 * d'escalier mais si i_message == 1 on affiche le message de lecture du tutoriel (intro seulement). */
void affiche_message(MLV_Image* images[], int i_message){
	int i;
	MLV_flush_event_queue();
	for(i=0; i<20; i++){
		if(i_message == 0) MLV_draw_partial_image(images[0], 1824, 864, 1824, 864, 0, 0); /*message d'escalier*/
		else MLV_draw_partial_image(images[0], 1824, 0, 1824, 864, 0, 0); /*message lecture stèle*/
		MLV_actualise_window();
		MLV_wait_milliseconds(12);
	}
	MLV_flush_event_queue();
}


/*affiche les murs en fonction de leur direction type*/
void affiche_wall(MLV_Image* images[], Floor floor, int i_draw, int j_draw, int decal_x, int decal_y, Wallstype type){
	switch(type){
		case NONE: break;
		case NO: MLV_draw_partial_image(images[6], 288*floor.theme, 96, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case N: MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case NE: MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case O: MLV_draw_partial_image(images[6], 288*floor.theme, 96*2, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E: MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96*2, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case SO: MLV_draw_partial_image(images[6], 288*floor.theme, 96*3, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case S: MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96*3, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case SE: MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96*3, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E1: MLV_draw_partial_image(images[6], 288*floor.theme, 96*4, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E2: MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96*4, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E3_DEG: MLV_draw_partial_image(images[6], 288*floor.theme, 96*7, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E3: MLV_draw_partial_image(images[6], 288*floor.theme, 96*6, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E4_DEG: MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96*7, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E4: MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96*6, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E5: MLV_draw_partial_image(images[6], 288*floor.theme, 96*5, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E6: MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96*5, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E7: MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96*6, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E8: MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96*5, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); break;
		case E9: MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96*3, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); /*S*/ 
			 MLV_draw_partial_image(images[6], 288*floor.theme, 96, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); /*NO*/
			 MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96, 96, 96, (j_draw)*96+decal_x, (i_draw)*96+decal_y); /*NE*/ break;
		case WALL_SP1: MLV_draw_partial_image(images[6], 96+(288*floor.theme), 96*8, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break; /*sur le fichier, en bas au milieu*/
		case WALL_SP2: MLV_draw_partial_image(images[6], 96*2+(288*floor.theme), 96*8, 96, 96, j_draw*96+decal_x, i_draw*96+decal_y); break; /*sur le fichier, en bas au droite*/
		default: break;
	}
}




/** Donne de l'exp au joueur après la défaite d'un ennemi en affichant une barre d'exp.
 * \param mstr_exp l'expérience à donner au joueur. */
void donne_exp(MLV_Image* images[], MLV_Sound* sounds[], Player* player, int mstr_exp){
	int i; int exp = mstr_exp*((float)(player->bonus_exp+100)/(float)100);
	MLV_draw_partial_image(images[0], 1824, 864*3, 1824, 864, 0, 0);
	MLV_draw_partial_image(images[0], 1824, 864*2, 1824, 864, 0, 0);
	nombre_stats(images, player->lvl, 710, 627, 6);
	nombre_exp(images, exp+1, 1200, 692);
	for(i=0; i<exp; i=i+2){
		player->exp = player->exp + 2;
		if( player->exp < 350+50*(player->lvl) ){ /*barre exp qui augmente*/
			MLV_draw_filled_rectangle(549, 745, (int)( (float)((float)player->exp / (float)(350+50*(player->lvl)) )*726.0), 36, MLV_COLOR_CYAN);
			MLV_actualise_window();
			MLV_wait_milliseconds(1);
		}
		else{ /*montée de niveau*/
			player->lvl = player->lvl + 1;
			/*rectangle 'niveau', barre exp rempli, nouveau niveau du joueur*/
			MLV_draw_partial_image(images[0], 1824, 864*2, 1824, 864, 0, 0);
			MLV_draw_filled_rectangle(549, 745, 726, 36, MLV_COLOR_CYAN);
			nombre_stats(images, player->lvl, 710, 627, 6);
			nombre_exp(images, exp+1, 1200, 692);
			MLV_actualise_window();
			/*interface choix de montée de stats, puis rectangle 'niveau',
			fenetre rose et niv du joueur*/
			montee_niveau(images, sounds, player);
			MLV_draw_partial_image(images[0], 1824, 864*2, 1824, 864, 0, 0);
			MLV_draw_partial_image(images[0], 1824, 864*4, 1824, 864, 0, 0);
			nombre_stats(images, player->lvl, 710, 627, 6);
			nombre_exp(images, exp+1, 1200, 692);
			MLV_actualise_window();
		}
	}
	MLV_wait_milliseconds(500);
}


/*Cette fonction va afficher l'écran de montée de niveau avec montee_niveau_stats()
et permettre un choix de gain de statistique.*/
void montee_niveau(MLV_Image* images[], MLV_Sound* sounds[], Player* player){
	int action = 0, mouseX=0, mouseY=0; MLV_Keyboard_button clavier; MLV_Event event;
	MLV_play_sound(sounds[5], 10.0); MLV_wait_milliseconds(350);

	player->exp = 0;
	montee_niveau_stats(images, player);

	while(action == 0){ /*choix de montée de stats*/
		event = MLV_wait_keyboard_or_mouse(&clavier, NULL, NULL, &mouseX, &mouseY);
		if((event == MLV_KEY && clavier == MLV_KEYBOARD_UP) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 270, 247, 347, 323))) /*attaque*/
			action = 1;
		if((event == MLV_KEY && clavier == MLV_KEYBOARD_RIGHT) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 270, 339, 347, 415))) /*intelligence*/
			action = 2;
		if((event == MLV_KEY && clavier == MLV_KEYBOARD_DOWN) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 270, 432, 347, 508))) /*defense*/
			action = 3;
	}

	switch(action){
		case 1: player->atk = player->atk+3; break;
		case 2: player->intel = player->intel+3; break;
		default: player->def = player->def+3; break; /*3*/
	}
	player->pv = 10*(player->def); /*régénération stats*/
	player->mp = 10*(player->intel)-50;
	
	montee_niveau_stats(images, player);
	MLV_wait_seconds(1);
}


/*Affiche l'écran de montée de niveau (avec les stats à augmenter)*/
void montee_niveau_stats(MLV_Image* images[], Player* player){
	MLV_draw_partial_image(images[0], 0, 864*3, 1824, 864, 0, 0);
	nombre_stats(images, player->atk, 1066, 524-287, 0);
	nombre_stats(images, player->intel, 1066, 616-287, 1);
	nombre_stats(images, player->def, 1066, 712-287, 4);
	MLV_actualise_window();
	MLV_flush_event_queue();
}




void initialisation_images(MLV_Image* images[]){
    images[0] = MLV_load_image("images/ecran.png");
	images[1] = MLV_load_image("images/nombre_grand.png");
	images[2] = MLV_load_image("images/nombre_petit.png");
	images[3] = MLV_load_image("images/objet.png");
	images[4] = MLV_load_image("images/objet_description.png");
	images[5] = MLV_load_image("images/perso.png");
	images[6] = MLV_load_image("images/sol.png");
	images[7] = MLV_load_image("images/sol_fond.png");
	images[8] = MLV_load_image("images/monstre.png");
	images[9] = MLV_load_image("images/nombre_degats.png");
	images[10] = MLV_load_image("images/objet_petit.png");
	images[11] = MLV_load_image("images/boss.png");
}


void initialisation_musiques(MLV_Music* musics[]){
	musics[0] = MLV_load_music("music/jeu.mp3");
	musics[1] = MLV_load_music("music/intro.mp3");
	musics[2] = MLV_load_music("music/title.mp3");
	musics[3] = MLV_load_music("music/boss.mp3");
}


void initialisation_sounds(MLV_Sound* sounds[]){
	sounds[0] = MLV_load_sound("sound/monstre_hit.wav");
	sounds[1] = MLV_load_sound("sound/crit.wav");
	sounds[2] = MLV_load_sound("sound/hit.wav");
	sounds[3] = MLV_load_sound("sound/intro.wav");
	sounds[4] = MLV_load_sound("sound/item.wav");
	sounds[5] = MLV_load_sound("sound/lvl.wav");
	sounds[6] = MLV_load_sound("sound/magic.wav");
	sounds[7] = MLV_load_sound("sound/pas.wav");
	sounds[8] = MLV_load_sound("sound/select.wav");
	sounds[9] = MLV_load_sound("sound/window_open.wav");
	sounds[10] = MLV_load_sound("sound/window_close.wav");
	sounds[11] = MLV_load_sound("sound/cristal.wav");
	sounds[12] = MLV_load_sound("sound/cursor.wav");
	sounds[13] = MLV_load_sound("sound/save.wav");
	sounds[14] = MLV_load_sound("sound/potion.wav");
	sounds[15] = MLV_load_sound("sound/equip.wav");
	sounds[16] = MLV_load_sound("sound/delete.wav");
}


void free_all(MLV_Image* images[], MLV_Music* musics[], MLV_Sound* sounds[]){
	int i;
	for(i=0; i<NBR_IMG; i++)
		MLV_free_image(images[i]);
	for(i=0; i<NBR_MUS; i++)
		MLV_free_music(musics[i]);
	for(i=0; i<NBR_SON; i++)
		MLV_free_sound(sounds[i]);
}
