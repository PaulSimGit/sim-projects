#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/intro.h"


/** Lance l'écran titre du jeu et demande une action à l'utilisateur.
 * \return 1 si on veut jouer, 2 si on veut continuer la partie et 0 si on veut quitter. */
int ecran_titre(MLV_Image* images[], MLV_Music* musics[], MLV_Sound* sounds[]){
	int index = 0, save_exist, mouseX=0, mouseY=0; MLV_Keyboard_button clavier; MLV_Event event;
	MLV_stop_music();
	MLV_play_music(musics[2], 10.0, -1);

	if(MLV_path_exists("saves/player_data.save") == 1) save_exist = 1;
	else save_exist = 0;

	while(1){
		MLV_draw_partial_image(images[0], 0, 0, 1824, 864, 0, 0); /*la belle image d'écan titre*/
		if(save_exist == 0) MLV_draw_partial_image(images[0], 2796, 5970, 292, 78, 1493, 397); /*griser reprendre si pas de save*/
		MLV_draw_partial_image(images[0], 2313, 6008, 40, 40, 1455, 200+210*index); /*flèche*/
		MLV_actualise_window();

		event = MLV_wait_keyboard_or_mouse(&clavier, NULL, NULL, &mouseX, &mouseY);
		if(event == MLV_KEY && clavier == MLV_KEYBOARD_DOWN){
			MLV_play_sound(sounds[12], 10.0); MLV_wait_milliseconds(50);
			index++;
			if(index>2) index=0;
			if(index==1 && save_exist == 0) index=2;
		}
		if(event == MLV_KEY && clavier == MLV_KEYBOARD_UP){
			MLV_play_sound(sounds[12], 10.0); MLV_wait_milliseconds(50);
			index--;
			if(index<0) index=2;
			if(index==1 && save_exist == 0) index=0;
		}
		if((event == MLV_KEY && clavier == MLV_KEYBOARD_RSHIFT) || event == MLV_MOUSE_BUTTON){
			if(event == MLV_MOUSE_BUTTON){
				if(mouse_clic(mouseX, mouseY, 1503, 166, 1747, 252)) index = 0;
				else if(mouse_clic(mouseX, mouseY, 1503, 415, 1774, 454)) index = 1;
				else if(mouse_clic(mouseX, mouseY, 1503, 617, 1721, 654)) index = 2;
				else continue;}
			switch(index){
				case 0: lancement_intro(images, musics, sounds); return 1; /*nouvelle partie*/
				case 1: if(save_exist == 1) return 2; /*reprendre une partie*/
						break;
				default: return 0; /*index==2, quitter le jeu*/
			}
		}
	}
}


/** Lance une animation de fondu avant le début d'une partie. */
void animation_lancement_jeu(MLV_Image* images[], MLV_Sound* sounds[]){
	int i;
	MLV_stop_all_sounds(); MLV_stop_music();
	MLV_play_sound(sounds[3], 10.0);

	for(i=0; i<40; i++){ /*fond noir progressif*/
		MLV_draw_partial_image(images[0], 0, 864*6, 1824, 864, 0, 0);
		MLV_actualise_window();
		MLV_wait_milliseconds(50);
	}
	MLV_stop_all_sounds();
}




/** Montre comment l'écran titre a été fait */
void easteregg_ecran_titre(MLV_Image* images[]){
	int i = 0, mouseX=0, mouseY=0;; MLV_Keyboard_button clavier; MLV_Event event;
	while(i < 10){
		/*images*/
		if(i!=9) MLV_draw_partial_image(images[0], 1824*2, 864*i, 1824, 864, 0, 0);
		else MLV_draw_partial_image(images[0], 0, 0, 1824, 864, 0, 0);
		/*flèches*/
		if(i>0) MLV_draw_partial_image(images[0], 2360, 5970, 78, 78, 16, 770);
		MLV_draw_partial_image(images[0], 270, 2931, 78, 78, 1730, 770);
		MLV_actualise_window();

		event = MLV_wait_keyboard_or_mouse(&clavier, NULL, NULL, &mouseX, &mouseY);
		if(i>0 && ((event == MLV_KEY && clavier == MLV_KEYBOARD_LEFT) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 16, 770, 16+78, 770+78)))){
			i--; MLV_wait_milliseconds(100); MLV_flush_event_queue();}
		if((event == MLV_KEY && clavier == MLV_KEYBOARD_RIGHT) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 1730, 770, 1730+78, 770+78))){
			i++; MLV_wait_milliseconds(100); MLV_flush_event_queue();}
	}
}


/** Affiche un tutoriel. On appuie sur R pour arrêter de lire.
 * \param c Les coordonnées du tutoriel dans le terrain. */
void affiche_tutoriel(MLV_Image* images[], Coordo c){
	int i, pX=33, pY=4, pIndice=0, mouseX=0, mouseY=0;;
	MLV_Keyboard_button clavier; MLV_Event event;
	/*On rentre dans le for si les coordonnés ne correspondent
	pas à la première stèle seule hors du temple*/
	if(c.x != 36 || c.y != 20){
		for(i=0; i<8; i++){
			if(c.x == pX && c.y == pY){
				pIndice = i+1; break;
			}
			pX += 3;
			if(i == 1 || i == 5) pX++;
			if(i == 3){pX = 33; pY = 8;}
		}
	}
	MLV_draw_partial_image(images[0], 1824*3, 864*pIndice, 1824, 864, 0, 0);
	MLV_actualise_window();
	while(1){
		event = MLV_wait_keyboard_or_mouse(&clavier, NULL, NULL, &mouseX, &mouseY);
		if((event == MLV_KEY && clavier == MLV_KEYBOARD_r) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 38, 738, 315, 820))){
			MLV_flush_event_queue(); return;}
		if(pIndice == 8 && ((event == MLV_KEY && clavier == MLV_KEYBOARD_RSHIFT) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 773, 521, 1026, 748)))){
			easteregg_ecran_titre(images) ; return;}
	}
}


/** Effectue l'animation des cristaux pour ouvrir la porte pour l'étage d'introduction.
 * Après cela, la porte s'ouvre et le joueur est content. */
void animation_ouverture_porte(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player player){
	int rayon = 0, angle, vitesse = 5, nb_rotation = 3, n = 0, i;
	player.direction = FOUILLE;
	MLV_play_sound(sounds[11], 10.0);

	for(angle=0; angle<360; angle+=vitesse){ /*fait tourner les cristaux*/
		affiche_jeu(images, *floor, player, 0, 0, 2, 0, 0, 0, NULL);
		affiche_cristal(images, angle, rayon);
		affiche_cristal(images, angle-90, rayon);
		affiche_cristal(images, angle-180, rayon);
		affiche_cristal(images, angle-270, rayon);
		MLV_actualise_window();
		MLV_wait_milliseconds(1);
		if(angle == 360-vitesse && n != nb_rotation-1){
			angle = 0; n++;}
		rayon += 2;
	}

	floor->terrain[TAILLE_Y/2-1][TAILLE_X/2+16][0].type = STAIR_UP; /*ouvre la porte*/
	affiche_jeu(images, *floor, player, 0, 0, 2, 0, 0, 0, NULL);
	MLV_actualise_window();

	for(i=0; i<2; i++){ /*le joueur est content*/
		affiche_jeu(images, *floor, player, 0, 0, 3+i, 0, 0, 0, NULL);
		MLV_actualise_window();
		MLV_wait_milliseconds(700);
	}
}


/** Affiche un cristal proche du personnage en fonction des paramètres donnés.
 * \param angle Direction en degré du cristal par rapport à la position du personnage.
 * \param rayon Distance entre le cristal et le personnage. */
void affiche_cristal(MLV_Image* images[], int angle, int rayon){
	double pi = 3.14159265358979323846;
	/*trainée du cristal*/
	MLV_draw_partial_image(images[10], 8*84, 1*84, 84, 84, 864+(rayon * sin(pi * 2 * (angle-30) /360)), 370+(rayon * cos(pi * 2 * (angle-30) /360)));
	MLV_draw_partial_image(images[10], 8*84, 1*84, 84, 84, 864+(rayon * sin(pi * 2 * (angle-20) /360)), 370+(rayon * cos(pi * 2 * (angle-20) /360)));
	MLV_draw_partial_image(images[10], 8*84, 1*84, 84, 84, 864+(rayon * sin(pi * 2 * (angle-10) /360)), 370+(rayon * cos(pi * 2 * (angle-10) /360)));
	/*cristal*/
	MLV_draw_partial_image(images[10], 7*84, 1*84, 84, 84, 864+(rayon * sin(pi * 2 * angle /360)), 370+(rayon * cos(pi * 2 * angle /360)));
}




/** Lance le jeu pour l'intro (étage 0), on quitte cette fonction dès qu'on emprunte l'escalier. */
void lancement_intro(MLV_Image* images[], MLV_Music* musics[], MLV_Sound* sounds[]){
	MLV_Keyboard_button clavier; MLV_Button_state etat;
	int action = 0, pressed = 0, res_event, i;
	Player player = creation_joueur(); player.etage = 0;
	Floor floor = creation_terrain_intro(0, TAILLE_X/2, TAILLE_Y/2);

	for(i=0; i<30; i++){ /*fond noir progressif*/
		MLV_draw_partial_image(images[0], 0, 864*6, 1824, 864, 0, 0);
		MLV_actualise_window();
		MLV_wait_milliseconds(10);}
	MLV_stop_music();
	MLV_play_music(musics[1], 10.0, -1);

	while(1){ /*boucle principale de l'étage d'introduction*/
		affiche_jeu(images, floor, player, 0, 0, 0, 0, 0, 0, NULL);

		res_event = evenements_intro(images, sounds, &floor, player); /*exécute événements de l'intro*/
		if(res_event == 1) pressed = 0;
		if(res_event == 2){free_terrain(&floor); return;} /*fin de l'intro*/
		if(res_event == 3){free_terrain(&floor); floor = creation_terrain_intro(1, TAILLE_X/2+7, TAILLE_Y/2-11); continue;} /*entrée temple*/
		if(res_event == 4){free_terrain(&floor); floor = creation_terrain_intro(0, TAILLE_X/2+7, TAILLE_Y/2-7); continue;} /*sortie temple*/

		MLV_actualise_window();
		action = get_action(&clavier, &etat, &pressed, &player.run);

		switch(action){ /*exécute action utilisateur*/
			case 1: player.direction = donne_direction(clavier);
					deplacement_perso(images, sounds, &floor, player);
					break;
			case 3: if(check_case(floor, HERE, floor.coordo_perso, 0) == TREASURE && res_event == 1)
						affiche_tutoriel(images, floor.coordo_perso); /*lecture tutoriel*/
					break;
			case 7: free_terrain(&floor); return; /*quitter manuellement (activez F1_STATE)*/
			case 8: switch_type_attaque(&player);
			default: break; /*0: aucune action*/
		}
	}
}


/** Gère les actions dans l'intro (étage 0).
 * \return 1 si un événement a été exécuté, 2 si on quitte l'intro,
 * 3 si on rentre dans le temple tutoriel, 4 si on quitte le temple tutoriel et 0 sinon. */
int evenements_intro(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player player){
	int x = floor->coordo_perso.x, y = floor->coordo_perso.y;
	int tx = TAILLE_X/2, ty = TAILLE_Y/2;
	if(check_case(*floor, HERE, floor->coordo_perso, 0) == TREASURE && player.direction == HAUT){ /*afficher message pour lire le tutoriel*/
		affiche_message(images, 1);
		return 1;}
	else if(y == ty && x == tx+13 && floor->terrain[ty-1][tx+16][0].type != STAIR_UP){ /*ouverture de la porte du labyrinthe*/
		animation_ouverture_porte(images, sounds, floor, player);
		affiche_jeu(images, *floor, player, 0, 0, 0, 0, 0, 0, NULL);
		MLV_actualise_window();
		return 1;}
	else if(y == ty-8 && x == tx+7) /*entrée dans le temple tutoriel*/
		return 3;
	else if(y == ty-10 && x == tx+7) /*sortie du temple tutoriel*/
		return 4;
	else if(check_case(*floor, HERE, floor->coordo_perso, 0) == STAIR_DOWN) /*entrée dans le labyrinthe*/
		return 2;
	return 0;
}


/** Génère le terrain de l'intro (étage 0). Ce terrain n'est pas aléatoire car il n'est utilisé que
 * pour mettre en place le lore autour du labyrinthe du jeu.
 * \param zone 0 pour générer l'extérieure de l'étage ou 1 pour générer le temple tutoriel.
 * \param coordo_persoX Coordonnées en X du personnage dans l'étage.
 * \param coordo_persoY Coordonnées en Y du personnage dans l'étage. */
Floor creation_terrain_intro(int zone, int coordo_persoX, int coordo_persoY){
	Floor floor;
	remplissage_initial(&floor);
	donne_forme_terrain_intro(&floor, zone);
	specialise_design_terrain_intro(&floor, zone);
	floor.coordo_perso.x = coordo_persoX;
	floor.coordo_perso.y = coordo_persoY;
	floor.theme = 5;
	init_vision(&floor);
	return floor;
}


/** Donne la forme du terrain pour l'intro (étage 0) en créant les sols requis. */
void donne_forme_terrain_intro(Floor* floor, int zone){
	int x, start = 0, end = 21, y;
	if(zone == 0){ /*extérieur étage 0*/
		for(y=-7; y<6; y++){
			switch(y){
				case -7: start = 7; end = 9; break;
				case -6: start = 7; end = 10; break;
				case -5: start = 7; end = 10; break;
				case -4: start = 11; end = 13; break;
				case -3: start = 3; end = 15; break;
				case -2: start = 3; end = 21; break;
				case -1: start = 2; end = 17; break;
				case 0: start = 0; end = 17; break;
				case 1: start = 1; end = 16; break;
				case 2: start = 2; end = 16; break;
				case 3: start = 2; end = 15; break;
				case 4: start = 4; end = 14; break;
				case 5: start = 5; end = 10; break;
				default: break;
			}
			for(x=start; x<end; x++) /*ground classique: herbe*/
				floor->terrain[TAILLE_Y/2+y][TAILLE_X/2+x][0].type = GROUND;
		}
		floor->terrain[TAILLE_Y/2-4][TAILLE_X/2+8][0].type = GROUND;
		floor->terrain[TAILLE_Y/2-2][TAILLE_X/2+15][0].type = WALL;
		floor->terrain[TAILLE_Y/2-1][TAILLE_X/2+15][0].type = WALL;
	}
	else{ /*temple tutoriel*/
		for(y=-19; y<-10; y++){
			for(x=0; x<15; x++) /*ground spécial 4: dalles de pierre*/
				floor->terrain[TAILLE_Y/2+y][TAILLE_X/2+x][0].type = GROUND_SP4;
		}
	}
}


/** Spécialise les murs du terrain de l'intro puis respécialise les murs/sols
 * en fonction du design requis pour créer le visuel du terrain demandé. */
void specialise_design_terrain_intro(Floor* floor, int zone){
	int i, x = TAILLE_X/2, y = TAILLE_Y/2, pX = 0, pY = 0;
	specialise_walls(floor);
	if(zone == 0){ /*extérieur étage 0*/
		floor->terrain[y-4][x+6][0].entity.wall.direc = E6; /*rivière (considérée comme un mur)*/
		floor->terrain[y-4][x+7][0].entity.wall.direc = E6;
		floor->terrain[y-4][x+9][0].entity.wall.direc = E6;
		floor->terrain[y-4][x+9][0].entity.wall.direc2 = NONE;
		floor->terrain[y-4][x+10][0].entity.wall.direc = WALL_SP1;
		floor->terrain[y-5][x+10][0].entity.wall.direc = WALL_SP2;
		floor->terrain[y-5][x+10][0].entity.wall.direc2 = NONE;
		floor->terrain[y-5][x+11][0].entity.wall.direc = E6;
		floor->terrain[y-4][x+5][0].entity.wall.direc = E1; /*murs autour de la rivière*/
		floor->terrain[y-5][x+5][0].entity.wall.direc = NO;
		floor->terrain[y-5][x+6][0].entity.wall.direc = E1;
		floor->terrain[y-6][x+10][0].entity.wall.direc = E2;
		floor->terrain[y-6][x+11][0].entity.wall.direc = N;
		floor->terrain[y-6][x+12][0].entity.wall.direc = NE;
		floor->terrain[y-5][x+12][0].entity.wall.direc = E2;
		floor->terrain[y][x+3][0].type = STAIR_UP; /*sol avec cailloux (stair_up considéré comme du sol)*/
		floor->terrain[y+1][x+4][0].type = STAIR_UP;
		floor->terrain[y+1][x+5][0].type = STAIR_UP;
		floor->terrain[y+1][x+6][0].type = STAIR_UP;
		floor->terrain[y-7][x+7][0].type = STAIR_UP;
		floor->terrain[y+5][x+7][0].type = STAIR_UP;
		floor->terrain[y][x+8][0].type = STAIR_UP;
		floor->terrain[y+4][x+8][0].type = STAIR_UP;
		floor->terrain[y-3][x+9][0].type = STAIR_UP;
		floor->terrain[y-1][x+9][0].type = STAIR_UP;
		floor->terrain[y+3][x+9][0].type = STAIR_UP;
		floor->terrain[y+4][x+9][0].type = STAIR_UP;
		floor->terrain[y-1][x+10][0].type = STAIR_UP;
		floor->terrain[y-1][x+13][0].type = STAIR_UP;
		floor->terrain[y+1][x+13][0].type = STAIR_UP;
		for(i=0; i<5; i++)
			floor->terrain[y][x+12+i][0].type = STAIR_UP;
		for(i=0; i<4; i++) /*dalles de pierres*/
			floor->terrain[y-2][x+16+i][0].type = GROUND_SP4;
		floor->terrain[y-3][x+8][0].type = GROUND_SP1; /*pont en bois (ground spécial considéré comme un pont en bois)*/
		floor->terrain[y-4][x+8][0].type = GROUND_SP1;
		floor->terrain[y-5][x+8][0].type = GROUND_SP1;
		floor->terrain[y-8][x+7][0].type = GROUND_SP2; /*entrée du temple (ground spécial considéré comme une entrée)*/
		floor->terrain[y-2][x+5][0].type = WALL; /*stèle de pierre (wall considérée comme une stèle)*/
		floor->terrain[y-2][x+5][0].entity.wall.direc = E5;
		floor->terrain[y-1][x+5][0].type = TREASURE; /*sol de stèle (treasure considéré comme un sol)*/
		floor->terrain[y][x+13][0].type = TREASURE_OPEN; /*sceau (coffre ouvert considéré comme un sceau)*/
		floor->terrain[y-1][x+16][0].type = WALL; /*porte du sceau (wall considéré comme une porte)*/
		floor->terrain[y-1][x+16][0].entity.wall.direc = E8;
		floor->terrain[y-2][x+20][0].type = STAIR_DOWN; /*entrée du labyrinthe*/
	}
	else{ /*temple tutoriel*/
		for(i=0; i<8; i++){ /*stèles de pierre et sol de stèle (wall et treasure considérés comme une stèle/un sol)*/
			floor->terrain[y-18+pY][x+2+pX][0].type = WALL;
			floor->terrain[y-18+pY][x+2+pX][0].entity.wall.direc = E5;
			floor->terrain[y-17+pY][x+2+pX][0].type = TREASURE;
			pX += 3;
			if(i == 1 || i == 5) pX++;
			if(i == 3){pY = 4; pX = 0;}
		}
		floor->terrain[y-10][x+7][0].type = GROUND_SP3; /*sortie du temple (ground spécial considéré comme une sortie)*/
	}
}
