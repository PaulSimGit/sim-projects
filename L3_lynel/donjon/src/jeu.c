#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/jeu.h"


/*Fonction principale du jeu qui renvoie 1 quand la partie est terminée.*/
int lancement_jeu(MLV_Image* images[], MLV_Music* musics[], MLV_Sound* sounds[], int jeu){
	MLV_Keyboard_button clavier; MLV_Button_state etat; Celltype check;
	int action = 0, pressed = 0, deplacement = 0, fin = 0;
	Entity* getEntity; Coordo c_mstr; Item potion = new_item(NO_ITEM, 1, 1);
	Floor floor; Player player;

	if(jeu == 1){ /*JOUER : nouvelle partie*/
		player = creation_joueur();
		floor = creation_terrain_complet(time(NULL), 1);}
	else if(jeu == 2){ /*REPRENDRE : continuer une partie*/
		chargement(&floor, &player, "saves/player_data.save");}

	MLV_stop_music();
	MLV_play_music(musics[0], 10.0, -1);
	MLV_flush_event_queue();

	while(1){
		if(deplacement != 0 && use_potion(&potion, &player.pv, &player.mp, player.def, player.intel, &player.crit, &player.bonus_exp) == 0)
			potion = new_item(NO_ITEM, 1, 1); /*utilisation d'une potion si possible*/

		affiche_jeu(images, floor, player, 0, 0, 0, 0, 0, 0, NULL);

		if(action != 4 && action != 5 && action != 6 && action != 7){
			if(evenements_jeu(images, sounds, &floor, &player, &potion)) /*actions du terrain*/
				pressed = 0;

			MLV_actualise_window();
			action = get_action(&clavier, &etat, &pressed, &player.run);}

		switch(action){ /*exécute action utilisateur*/
			case 1: player.direction = donne_direction(clavier);
					deplacement = deplacement_perso(images, sounds, &floor, player);
					if(deplacement == 1) /*déplacement*/
						fin = actions_monstres(images, sounds, &floor, &player);
					if(deplacement == 2){ /*combat*/
						getEntity = donne_entity(&floor, player.direction, floor.coordo_perso, 1);
						c_mstr = donne_coordo_voisin(floor.coordo_perso, player.direction);
						animation_combat(images, sounds, &floor, &player, &getEntity->monster, c_mstr, 1);
						fin = actions_monstres(images, sounds, &floor, &player);
						pressed = 0;}
					if(fin == 1){
						game_over(images, &floor, &player); return 1;}
					break;
			case 2: affiche_carte(images, sounds, floor, player); break; /*carte*/
			case 3: check = check_case(floor, HERE, floor.coordo_perso, 0);
					if(check == STAIR_DOWN || (check == STAIR_UP && player.etage != 1))
						passage_etage(images, &floor, &player, check); /*escalier*/
					break;
			case 4: action = affiche_stats(images, sounds, player); break; /*stats*/
			case 5: action = affiche_save(images, sounds); break; /*save*/
			case 6: action = affiche_inventaire(images, sounds, &player, &potion, new_item(NO_ITEM, 1, 1)); break; /*inventaire*/
			case 7: sauvegarde(floor, player, "saves/player_data.save"); free_terrain(&floor); return 1; /*sauvegarder, quitter*/
			case 8: switch_type_attaque(&player); /*change type d'attaque*/
			default: break; /*0-9: aucune action*/
		}
		if(action == 0) deplacement = 1;
		else if(action != 1) deplacement = 0;
	}
}


/** Récupère une action et renvoie son code en int.
 * \param clavier Pointeur sur un MLV_Keyboard_button.
 * \param etat Pointeur sur un MLV_Button_state.
 * \param pressed Pointeur qui est égal à 1 si un bouton de direction est continuellement pressé.
 * \param run Pointeur qui est égal au décalage graphique de l'affichage du jeu lors du déplacement
 * du joueur (vitesse de déplacement du joueur).
 * \return 1 (déplacement du joueur), 2 (affiche la carte), 3 (prise d'escalier),
 * 4 (affiche les stats), 5 (confirmation sauvegarde), 6 (affiche inventaire),
 * 7 (quitter le jeu), 8 (switch attaque physique/magique), 0-9 (aucune action). */
int get_action(MLV_Keyboard_button* clavier, MLV_Button_state* etat, int* pressed, int* run){
	MLV_Event event; MLV_Mouse_button mouse_button; int mouseX=0, mouseY=0;
	while(1){
		event = MLV_get_event(&*clavier, NULL, NULL, NULL, NULL, &mouseX, &mouseY, &mouse_button, &*etat);

		if(*etat == MLV_RELEASED) /*action de bouton relaché*/
			*pressed = 0; /*arrêt du déplacement continu*/
		if(*etat == MLV_PRESSED && event == MLV_MOUSE_BUTTON && mouse_button == MLV_BUTTON_LEFT){ /*action clic gauche*/
			*clavier = MLV_KEYBOARD_NONE; *etat = MLV_RELEASED; *pressed=0; /*mêmes retours que par bouton*/
			if(mouse_clic(mouseX, mouseY, 96, 684, 191, 778)){
				if(*run == 3) *run = 15;
				else *run = 3;
				return 9;}
			if(mouse_clic(mouseX, mouseY, 96, 280, 191, 375)) return 2;
			if(mouse_clic(mouseX, mouseY, 707, 684, 1149, 831)) return 3;
			if(mouse_clic(mouseX, mouseY, 1572, 580, 1667, 675)) return 4;
			if(mouse_clic(mouseX, mouseY, 1627, 709, 1723, 801)) return 5;
			if(mouse_clic(mouseX, mouseY, 1684, 580, 1780, 675)) return 6;
			if(mouse_clic(mouseX, mouseY, 36, 501, 243, 595)) return 8;
		}
		else if(*etat == MLV_PRESSED){ /*action de bouton pressé*/
			if(event == MLV_KEY || *pressed==1){
				if(*clavier == MLV_KEYBOARD_c){
					if(*run == 3) *run = 15; /*courir/marcher*/
					else *run = 3;
					return 9;}
				if(*clavier == MLV_KEYBOARD_RIGHT || *clavier == MLV_KEYBOARD_LEFT ||
					*clavier == MLV_KEYBOARD_DOWN || *clavier == MLV_KEYBOARD_UP){
						*pressed = 1; return 1;} /*déplacement du personnage*/
				if(*clavier == MLV_KEYBOARD_m) return 2; /*carte*/
				if(*clavier == MLV_KEYBOARD_RSHIFT) return 3; /*prise d'escalier*/
				if(*clavier == MLV_KEYBOARD_a) return 4; /*affiche les stats*/
				if(*clavier == MLV_KEYBOARD_s) return 5; /*confirmation sauvegarde*/
				if(*clavier == MLV_KEYBOARD_z) return 6; /*affiche inventaire*/
				if(*clavier == MLV_KEYBOARD_F1){
					if(F1_STATE) return 7; /*quitter le jeu*/
					else return 0;}
				if(*clavier == MLV_KEYBOARD_SPACE) return 8; /*switch entre attaque physique et magique*/
			}
		}
		MLV_flush_event_queue(); MLV_wait_milliseconds(10);
	}
	return 0; /*aucune action*/
}


/** Détecte et exécute les événements liés au jeu lui-même.
 * \param potion pointeur vers un objet NO_ITEM qui sera rempli d'une potion si on utilise une potion.
 * \return 1 si un événement a été exécuté et 0 sinon */
int evenements_jeu(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player* player, Item* potion){
	int index; Item item;
	switch(check_case(*floor, HERE, floor->coordo_perso, 0)){
		case TREASURE: /*ouverture d'un coffre*/
			player->direction = FOUILLE;
			item = floor->terrain[floor->coordo_perso.y][floor->coordo_perso.x][0].entity.treasure.item;
			index = ajoute_dans_inventaire(player->inventaire, item);
			animation_perso(images, sounds, floor, *player, false);
			if(index != -1){
				if(item.type == WEAPON || item.type == MAGIC_STAFF || item.type == ARMOR)
					equip_auto(player->inventaire, index, &player->def, &player->atk, &player->intel, &player->e_weapon, &player->e_staff, &player->e_armor);}
			else{ /*l'inventaire est complet*/
				affiche_inventaire(images, sounds, player, potion, item);
				affiche_jeu(images, *floor, *player, 0, 0, 0, 0, 0, 0, NULL);}
			return 1;
		case STAIR_DOWN: /*affiche message d'escalier*/
			affiche_message(images, 0); return 1;
		case STAIR_UP: /*affiche message d'escalier*/
			if(player->etage != 1){
				affiche_message(images, 0); return 1;}
			return 0;
		default: return 0;
	}
}


/** Permet aux monstres d'effectuer des actions seulement si ils se trouvent 
 * dans le champ de vision du joueur. On doit lancer cette fonction après un tour 
 * valide du joueur.
 * \return 1 si le joueur est mort et 0 sinon. */
int actions_monstres(MLV_Image* images[], MLV_Sound* sounds[], Floor* floor, Player* player){
	int i, direction; Coordo c; Entity* getEntity;
	Coordo* monstres = malloc(20 * sizeof(Coordo));
	int nb_mstr = dans_champ_vision(*floor, MONSTER, 1, monstres);
	
	for(i=0; i<nb_mstr; i++){ /*actions pour TOUS les monstres présents dans le champ de vision*/
		c.x = monstres[i].x; c.y = monstres[i].y;
		direction = direction_monstre(*floor, c.x, c.y);
		/*déplacement*/
		if(check_case(*floor, direction, c, 0) != TREASURE && check_case(*floor, direction, c, 1) != MONSTER
			&& !contact_joueur(floor->coordo_perso, direction, c))
				deplacement_monstre(images, floor, *player, direction, c);
		/*combat*/
		else if(contact_joueur(floor->coordo_perso, direction, c)){
			floor->terrain[c.y][c.x][1].entity.monster.direction = direction;
			getEntity = donne_entity(floor, HERE, c, 1);
			if(animation_combat(images, sounds, floor, player, &getEntity->monster, c, 0) == 1)
				return 1; /*joueur mort*/
		}
	}
	free(monstres); return 0;
}




/** Permet de prendre un escalier et monter/descendre un étage.
 * \param escalier STAIR_DOWN ou STAIR_UP si on veut descendre ou monter. */
void passage_etage(MLV_Image* images[], Floor* floor, Player* player, Celltype escalier){
	char* file_name; Player temp = *player;
	if(escalier == STAIR_DOWN && player->etage == player->last_etage){
		player->last_etage++;
		file_name = fichier_etage(player->etage);
		sauvegarde(*floor, *player, file_name); free(file_name);
		free_terrain(floor);
		player->etage++;
		*floor = creation_terrain_complet(time(NULL), player->last_etage);
	}
	else if(escalier == STAIR_DOWN && player->etage != player->last_etage){
		file_name = fichier_etage(player->etage);
		sauvegarde(*floor, *player, file_name); free(file_name);
		free_terrain(floor);
		player->etage++;
		file_name = fichier_etage(player->etage);
		chargement(floor, player, file_name); free(file_name);
		retablissement_stats(player, temp);
	}
	else if(escalier == STAIR_UP){
		file_name = fichier_etage(player->etage);
		sauvegarde(*floor, *player, file_name); free(file_name);
		free_terrain(floor);
		player->etage--;
		file_name = fichier_etage(player->etage);
		chargement(floor, player, file_name); free(file_name);
		retablissement_stats(player, temp);
	}
	animation_transition(images, *floor, *player);
}


/*sauvegarde les données du jeu dans un fichier binaire*/
void sauvegarde(Floor floor, Player player, char* file_name){
	Data data; Coordo escalier;
	FILE* save = fopen(file_name, "wb");
	if(save == NULL){
		printf("Erreur lors de la sauvegarde.\n");
		exit(EXIT_FAILURE);}
	else{
		data.coordo_mstr = recupere_coordo_all_obj(floor, MONSTER, 1, NULL);
		data.coordo_treasure = recupere_coordo_all_obj(floor, TREASURE, 0, &escalier);
		data.escalier = escalier;
		data.seed = floor.seed;
		data.coordo_perso = floor.coordo_perso;
		data.player = player;
		fwrite(&data, sizeof(Data), 1, save);
		fclose(save);
	}
}


/** Récupère les données du jeu dans le fichier binaire.
 * \param type 0 si on souhaite quitter le programme en cas d'erreur et 1 si on souhaite continuer
 * le programme en cas d'erreur. */
void chargement(Floor* floor, Player* player, char* file_name){
	Data data;
	FILE* load = fopen(file_name, "rb");
	if(load == NULL){
		printf("Erreur lors du chargement de la partie.\n");
		exit(EXIT_FAILURE);}
	else{
		fread(&data, sizeof(Data), 1, load);
		*player = data.player;
		creation_terrain(floor, data.seed);
		floor->coordo_perso = data.coordo_perso;
		init_vision(floor);
		floor->terrain[data.escalier.y][data.escalier.x][0].type = STAIR_DOWN;
		placement_all_obj(floor, MONSTER, data.coordo_mstr, 1);
		placement_all_obj(floor, TREASURE, data.coordo_treasure, 0);
		fclose(load);
	}
}


/*Renvoie le nom du fichier save correspondant à l'étage demandé.
La valeur de retour correspond à une chaîne du type "saves/floors/f3.save" et il faut
la free correctement.*/
char* fichier_etage(int etage){
	char* fichier = (char*)malloc(50); char numEtage[4];
	sprintf(fichier, "%s", "saves/floors/f");
	sprintf(numEtage, "%d", etage);
	strcat(fichier, numEtage);
	strcat(fichier, ".save");
	return fichier;
}


/*Rétabli les stats actuels du perso en les récupérant dans le Player temporaire.
On ne récupère pas etage, last_etage et direction car ils sont gérés directement dans la sauvegarde.*/
void retablissement_stats(Player* player, Player temp){
	int i;
	player->atk = temp.atk; player->def = temp.def;
	player->exp = temp.exp; player->intel = temp.intel;
	player->lvl = temp.lvl; player->mp = temp.mp;
	player->pv = temp.pv; player->run = temp.run;
	player->type_attaque = temp.type_attaque;
	player->e_armor = temp.e_armor;
	player->e_weapon = temp.e_weapon;
	player->e_staff = temp.e_staff;
	for(i=0; i<TAILLE_INVENTAIRE; i++){
		if(temp.inventaire[i].type == NO_ITEM)
			player->inventaire[i].type = NO_ITEM;
		else{
			player->inventaire[i].type = temp.inventaire[i].type;
			player->inventaire[i].entity = temp.inventaire[i].entity;}
	}
}