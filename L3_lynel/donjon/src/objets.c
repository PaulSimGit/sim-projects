#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/objets.h"


/** Crée et renvoie un nouvel objet (numéro i) de l'étage demandé.
 * \param type le type de l'objet.
 * \param etage l'étage dans lequel doit se générer l'objet.
 * \return l'objet créé. */
Item new_item(Itemtype type, int etage, int i){
	Item item;
	item.type = type;
	srand(time(NULL) + i);
	switch(type){
		case POTION_SOIN: item.entity.potion.duration = 0; break;
		case POTION_MAGIE: item.entity.potion.duration = 0; break;
		case POTION_REGEN: item.entity.potion.duration = 30; break;
		case POTION_PRECI: item.entity.potion.duration = 30; break;
		case POTION_APPRENTI: item.entity.potion.duration = 30; break;
		case WEAPON: item.entity.equipment.quality = 1 + rand()%etage; break;
		case ARMOR: item.entity.equipment.quality = 1 + rand()%etage; break;
		case MAGIC_STAFF: item.entity.equipment.quality = 1 + rand()%etage; break;
		default: break; /*NO_ITEM*/
	}
	return item;
}


/** Crée et renvoie un nouveau trésor (numéro i) dans l'étage demandé. Ce trésor contient un objet 
 * aléatoire (40% potion / 60% équipement).
 * \param etage l'étage dans lequel le trésor doit se générer.
 * \return le trésor créé. */
Treasure new_treasure(int etage, int i){
	Treasure treasure; int res;
	srand(time(NULL) + i);
	res = 1 + rand()%10; /*pour le moment, on ne génère qu'un seul objet par trésor*/
	if(res >= 1 && res <= 4){
		switch(1 + rand()%5){
			case 1: treasure.item = new_item(POTION_SOIN, etage, i); break;
			case 2: treasure.item = new_item(POTION_MAGIE, etage, i); break;
			case 3: treasure.item = new_item(POTION_REGEN, etage, i); break;
			case 4: treasure.item = new_item(POTION_PRECI, etage, i); break;
			case 5: treasure.item = new_item(POTION_APPRENTI, etage, i); break;
			default: break;}
	}
	if(res >= 5 && res <= 10){
		switch(1 + rand()%3){
			case 1: treasure.item = new_item(WEAPON, etage, i); break;
			case 2: treasure.item = new_item(ARMOR, etage, i); break;
			case 3: treasure.item = new_item(MAGIC_STAFF, etage, i); break;
			default: break;}
	}
	return treasure;
}


/** Utilise une potion. 
 * Les paramètres pv, mp, def, intel et crit correspondent aux statistiques homonymes du joueur.
 * \param item pointeur vers l'objet utilisé (ici ça doit être une potion).
 * \return 1 si une potion avec durée a été utilisée et 0 sinon. */
int use_potion(Item* item, int* pv, int* mp, int def, int intel, int* crit, int* bonus_exp){
	int res, pv_max = (def*10), mp_max = ((intel*10)-50);
	if(*pv > pv_max) *pv = pv_max;
	if(*mp > mp_max) *mp = mp_max;
	switch(item->type){
		case POTION_SOIN:
			res = (int)(pv_max*0.1);
			if(*pv + res < pv_max) *pv += res;
			else *pv = pv_max;
			return 0;
		case POTION_MAGIE:
			res = (int)(mp_max*0.1);
			if(*mp + res < mp_max) *mp += res;
			else *mp = mp_max;
			return 0;
		case POTION_REGEN:
			if(item->entity.potion.duration == 0)
				return 0;
			if(item->entity.potion.duration % 3 == 0){ /*tous les 3 tours*/
				if(*pv + 20 < pv_max) *pv += 20;
				else *pv = pv_max;
				if(*mp + 10 < mp_max) *mp += 10;
				else *mp = mp_max;
			}
			item->entity.potion.duration --;
			return 1;
		case POTION_PRECI:
			if(item->entity.potion.duration == 0){
				*crit = 5;
				return 0;}
			*crit = 15;
			item->entity.potion.duration --;
			return 1;
		case POTION_APPRENTI:
			if(item->entity.potion.duration == 0){
				*bonus_exp = 0;
				return 0;}
			*bonus_exp = 30;
			item->entity.potion.duration --;
			return 1;
		default: return 0;
	}
}


/** Permet d'équiper un équipement avec update_stat puis met à jour l'inventaire du joueur en 
 * retirant l'équipement anciennement porté. 
 * Les paramètres def, atk et intel correspondent aux statistiques homonymes du joueur. 
 * Les paramètres e_weapon, e_staff et e_armor correspondent aux indices d'objets équipés par le joueur, ou -1 si l'objet n'est pas équipé.
 * \param inventaire l'inventaire du joueur.
 * \param index indice de l'objet qu'on veut équiper. */
void equip_equipment(Item inventaire[TAILLE_INVENTAIRE], int index, int* def, int* atk, int* intel, int* e_weapon, int* e_staff, int* e_armor){
	Item item = inventaire[index];
	switch(item.type){
		case WEAPON: if(*e_weapon == -1){ /*équiper*/
						update_stat(item, atk, 1);
						*e_weapon = index;}
					else if(*e_weapon != index){ /*équiper et retirer ancien*/
						update_stat(inventaire[*e_weapon], atk, 0);
						update_stat(item, atk, 1);
						*e_weapon = index;}
					else{ /*e_weapon == index*/ /*retirer*/
						update_stat(item, atk, 0);
						*e_weapon = -1;}
					break;
		case MAGIC_STAFF: if(*e_staff == -1){
						update_stat(item, intel, 1);
						*e_staff = index;}
					else if(*e_staff != index){
						update_stat(inventaire[*e_staff], intel, 0);
						update_stat(item, intel, 1);
						*e_staff = index;}
					else{
						update_stat(item, intel, 0);
						*e_staff = -1;}
					break;
		case ARMOR: if(*e_armor == -1){
						update_stat(item, def, 1);
						*e_armor = index;}
					else if(*e_armor != index){
						update_stat(inventaire[*e_armor], def, 0);
						update_stat(item, def, 1);
						*e_armor = index;}
					else{
						update_stat(item, def, 0);
						*e_armor = -1;}
		default: break;
	}
}


/** Permet d'équiper automatiquement un équipement lorsqu'on le récupère dans un trésor si 
 * il est de qualité supérieure à l'équipement actuel. 
 * Cette fonction prend les mêmes paramètres que equip_equipment. */
void equip_auto(Item inventaire[TAILLE_INVENTAIRE], int index, int* def, int* atk, int* intel, int* e_weapon, int* e_staff, int* e_armor){
	Item item = inventaire[index];
	int quality = item.entity.equipment.quality;
	switch(item.type){
		case WEAPON: if(*e_weapon == -1 || (*e_weapon != -1 && quality > inventaire[*e_weapon].entity.equipment.quality))
						equip_equipment(inventaire, index, def, atk, intel, e_weapon, e_staff, e_armor);
					break;
		case MAGIC_STAFF: if(*e_staff == -1 || (*e_staff != -1 && quality > inventaire[*e_staff].entity.equipment.quality))
							equip_equipment(inventaire, index, def, atk, intel, e_weapon, e_staff, e_armor);
						break;
		case ARMOR: if(*e_armor == -1 || (*e_armor != -1 && quality > inventaire[*e_armor].entity.equipment.quality))
						equip_equipment(inventaire, index, def, atk, intel, e_weapon, e_staff, e_armor);
		default: break;
	}
}


/*Dans le cas où on souhaite jeter un équipement, on doit d'abord vérifier qu'il est 
équipé et si c'est le cas on le déséquipe (update des stats).*/
void jeter_equipment(Item inventaire[TAILLE_INVENTAIRE], int index, int* def, int* atk, int* intel, int* e_weapon, int* e_staff, int* e_armor){
	Item item = inventaire[index];
	switch(item.type){
		case WEAPON: if(*e_weapon == index){
						update_stat(item, atk, 0);
						*e_weapon = -1;}
					break;
		case MAGIC_STAFF: if(*e_staff == index){
						update_stat(item, intel, 0);
						*e_staff = -1;}
					break;
		case ARMOR: if(*e_armor == index){
						update_stat(item, def, 0);
						*e_armor = -1;}
		default: break;
	}
}


/** Permet d'update les stats du joueur suite à un équipement d'objet.
 * \param item L'objet équipé (ici ça doit être un équipement).
 * \param stat les stats associés à l'objet qu'il faut mettre à jour.
 * \param mode 1 si on veut équiper l'équipement et 0 si on veut retirer l'équipement. */
void update_stat(Item item, int* stat, int mode){
	if(mode == 1) *stat += item.entity.equipment.quality;
	else *stat -= item.entity.equipment.quality;
}


/** Permet d'ajouter un objet dans l'inventaire si l'inventaire n'est pas déjà rempli. 
 * \return Renvoie l'index de l'objet ajouté dans l'inventaire ou -1 si l'inventaire est complet. */
int ajoute_dans_inventaire(Item inventaire[TAILLE_INVENTAIRE], Item item){
	int i;
	for(i=0; i<TAILLE_INVENTAIRE; i++){
		if(inventaire[i].type == NO_ITEM){
			inventaire[i] = item;
			return i;
		}
	}
	return -1;
}


/*Règle le X de l'affichage d'un équipement en fonction de sa qualité.
En fonction de la valeur de PLAGE_OBJ, on pourra étendre l'affichage des équipements sur
les qualités.
Par exemple, si PLAGE_OBJ == 1, on utilisera chaque image pour sa qualité.
Si PLAGE_OBJ == 2 alors les équipements de qualité 1 et 2 auront l'image 1, ceux de qualité 3 et 4
auront l'image 2, etc...
Dès qu'on demande une qualité supérieure à 6xPLAGE_OBJ alors on utilisera les images de qualité 6.*/
int plage_item(int qualite){
	int i;
	for(i=1; i<6; i++){
		if(qualite >= (i-1)*PLAGE_OBJ+1 && qualite <= i*PLAGE_OBJ)
			return i;
	}
	return 6;
}