#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/personnage_monstre.h"


/*Crée et renvoie le personnage du jeu*/
Player creation_joueur(){
	int i;
	Player player;
	player.pv = 100; player.mp = 50;	
	player.lvl = 1; player.exp = 0;
	player.atk = 10; player.crit = 5;
	player.intel = 10; player.def = 10;
	player.etage = 1; player.last_etage = 1;
	player.direction = BAS;
	player.type_attaque = PHYSIQUE;
	player.run = 3; player.bonus_exp = 0;
	for(i=0; i<TAILLE_INVENTAIRE; i++)
		player.inventaire[i] = new_item(NO_ITEM, 1, i);
	player.e_weapon = -1; player.e_staff = -1;
	player.e_armor = -1;
	return player;
}


/*Crée et renvoie le monstre numéro i dans l'étage demandé.*/
Monster creation_monstre(int etage, int i){
	Monster monster;
	srand(time(NULL) + i);
	monster.pv = (13 + rand()%33) * (1*etage);
	monster.pv_max = monster.pv;
	monster.atk = (3 + rand()%7) * (0.9*etage);
	monster.crit = 5;
	monster.exp = (100 + rand()%115) + (30 * (1*etage));
	monster.direction = BAS;
	monster.type_attaque = PHYSIQUE;
	if(rand()%100 <= ((etage+1)*10)) /*((etage+1)*10)% de chance*/
		monster.type = LOBO;
	else
		monster.type = BLOB;
	return monster;
}


/*Crée et renvoie un boss dans l'étage demandé.*/
Monster creation_boss(int etage){
	Monster boss;
	srand(time(NULL));
	boss.pv = (13 + rand()%33) * (1.3*etage);
	boss.pv_max = boss.pv;
	boss.atk = (5 + rand()%9) * (1*etage);
	boss.crit = 5;
	boss.exp = (100 + rand()%115) + (80 * (1.2*etage));
	boss.direction = BAS;
	boss.type_attaque = PHYSIQUE;
	boss.type = BOSS;
	return boss;
}


/** Lance un combat entre le joueur et le monstre.
 * \param ordre Ordre du combat, 1 si le joueur attaque ou 0 si le monstre attaque.
 * \param degats Pointeur vers les dégâts qui résultent du combat.
 * \param chance Pointeur qui deviendra true si un coup critique se produit.
 * \return 1 si le joueur est mort, 2 si le monstre est mort ou 0 si ils sont encore vivants. */
int combat(Player* player, Monster* monster, int ordre, int* degats, bool* chance){
	if(ordre == 1){ /*le joueur attaque*/
		if(player->type_attaque == PHYSIQUE)
			*degats = ((int)(player->atk*0.8) + rand()%(int)(player->atk*1.2));
		else{ /*if MAGIQUE*/
			*degats = ((int)((2*player->intel)*0.8) + rand()%(int)((2*player->intel)*1.2));
			player->mp -= 20;}
		chance_critique(degats, player->crit, chance);
		monster->pv -= *degats;
		if(monster->pv <= 0)
			return 2;
	}
	if(ordre == 0){ /*le monstre attaque*/
		*degats = ((int)(monster->atk*0.8) + rand()%(int)(monster->atk*1.2));
		chance_critique(degats, monster->crit, chance);
		player->pv -= *degats;
		if(player->pv <= 0)
			return 1;
	}
	return 0;
}


/*Si le coup critique a lieu, les dégâts sont multipliés par 3 et modifiés dans le pointeur.*/
void chance_critique(int* degats, int crit, bool* chance){
	float c, r;
	c = ((float)crit/(float)100);
	r = (float)rand()/(float)(RAND_MAX);
	if(r <= c){
		*chance = true;
		*degats = *degats * 3;}
}


/*Interchange entre l'attaque physique et l'attaque magique pour le joueur.*/
void switch_type_attaque(Player* player){
	if(player->type_attaque == PHYSIQUE && player->mp > 0)
		player->type_attaque = MAGIQUE;
	else /*if MAGIQUE*/
		player->type_attaque = PHYSIQUE;
}


/** Vérifie si un monstre est en contact avec le joueur.
 * Cette fonction est similaire à check_case() mais n'est utile que si on cherche à vérifier si
 * un monstre est en contact avec le joueur.
 * Si vous voulez vérifier qu'un joueur est en contact avec un monstre, utilisez check_case().
 * \return true si on est en contact avec le joueur et false sinon */
bool contact_joueur(Coordo perso, Direction d, Coordo monstre){
	switch(d){
		case DROITE:
			return (monstre.x+1 == perso.x && monstre.y == perso.y);
		case GAUCHE:
			return (monstre.x-1 == perso.x && monstre.y == perso.y);
		case BAS:
			return (monstre.x == perso.x && monstre.y+1 == perso.y);
		default: /*HAUT*/
			return (monstre.x == perso.x && monstre.y-1 == perso.y);
	}
}




/*Renvoie une direction en fonction de la touche du keyboard choisie.*/
Direction donne_direction(MLV_Keyboard_button keyboard_direction){
	switch(keyboard_direction){
		case MLV_KEYBOARD_RIGHT: return DROITE;
		case MLV_KEYBOARD_LEFT: return GAUCHE;
		case MLV_KEYBOARD_DOWN: return BAS;
		default: return HAUT; /*UP*/
	}
}


/** Déplace l'index de sélection utilisateur dans le tableau de l'inventaire 
 * en fonction de la direction choisie.
 * \param index pointeur vers l'entier index du tableau de l'inventaire.
 * \param treizieme type de l'objet supplémentaire si l'inventaire est rempli ou NO_ITEM sinon. */
void deplacement_index_inventaire(MLV_Sound* sounds[], MLV_Event event, MLV_Keyboard_button clavier, int mouseX, int mouseY, int* index, Itemtype treizieme){
	int i;
	if(event == MLV_KEY){
		switch(clavier){
			case MLV_KEYBOARD_UP:
				if(*index != -1){
					MLV_play_sound(sounds[12], 10.0);
					*index -= 6;
					if(*index < 0) *index += 12;}
				break;
			case MLV_KEYBOARD_DOWN:
				if(*index != -1){
					MLV_play_sound(sounds[12], 10.0);
					*index += 6;
					if(*index > 11) *index -= 12;}
				break;
			case MLV_KEYBOARD_RIGHT:
				MLV_play_sound(sounds[12], 10.0);
				if(*index == 5 && treizieme != NO_ITEM){
					*index = -1; break;}
				if(*index == -1 && treizieme != NO_ITEM){
					*index = 0; break;}
				*index += 1;
				if(*index % 6 == 0) *index -= 6;
				break;
			case MLV_KEYBOARD_LEFT:
				MLV_play_sound(sounds[12], 10.0);
				if(*index == 0 && treizieme != NO_ITEM){
					*index = -1; break;}
				if(*index == -1 && treizieme != NO_ITEM){
					*index = 5; break;}
				*index -= 1;
				if(*index == 5) *index = 11;
				if(*index == -1) *index = 5;
			default: break;
		}
	}
	else if(event == MLV_MOUSE_BUTTON){
		if(treizieme != NO_ITEM){
			if(mouse_clic(mouseX, mouseY, 62, 457, 62+156, 457+156)){MLV_play_sound(sounds[12], 10.0); *index = -1;}}
		for(i=0; i<6; i++)
			if(mouse_clic(mouseX, mouseY, 372+184*i, 457, (372+184*i)+156, 457+156)){MLV_play_sound(sounds[12], 10.0); *index = i; break;}
		for(i=6; i<12; i++)
			if(mouse_clic(mouseX, mouseY, 372+184*(i-6), 640, (372+184*(i-6))+156, 640+156)){MLV_play_sound(sounds[12], 10.0); *index = i; break;}
	}
	MLV_wait_milliseconds(50);
}


/** Permet d'effectuer des actions dans l'inventaire du joueur.
 * \param clavier la touche utilisateur pressée.
 * \param potion pointeur qui sera rempli d'une potion si on utilise une potion.
 * \param item objet sur lequel le curseur se trouve actuellement.
 * \param treizieme objet supplémentaire si on récupère un objet alors que l'inventaire est rempli et NO_ITEM sinon.
 * \param index pointeur vers l'index de l'inventaire où on se trouve actuellement.
 * \param vide 0 si l'inventaire est vide et 1 sinon.
 * \return 0 pour quitter l'inventaire, 4 pour passer au menu stats, 5 pour le menu sauvegarde, -1 quand on utilise une potion ou
 * lorsqu'on jette un objet pour faire de la place dans un inventaire plein, et 1 dans tous les autres cas. */
int action_dans_inventaire(MLV_Sound* sounds[], MLV_Event event, MLV_Keyboard_button clavier, int mouseX, int mouseY, Player* player, Item* potion, Item item, Item treizieme, int* index, int vide){
	if(treizieme.type == NO_ITEM && ((event == MLV_KEY && (clavier == MLV_KEYBOARD_r || clavier == MLV_KEYBOARD_z)) || (event == MLV_MOUSE_BUTTON && (mouse_clic(mouseX, mouseY, 38, 738, 315, 820) || mouse_clic(mouseX, mouseY, 1684, 580, 1780, 675))))) return 0; /*quitter inventaire*/
	if(treizieme.type == NO_ITEM && ((event == MLV_KEY && clavier == MLV_KEYBOARD_a) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 1572, 580, 1667, 675)))) return 4; /*afficher stats*/
	if(treizieme.type == NO_ITEM && ((event == MLV_KEY && clavier == MLV_KEYBOARD_s) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 1627, 709, 1723, 801)))) return 5; /*sauvegarder*/
	if(!vide){ /*si l'inventaire n'est pas vide*/
		deplacement_index_inventaire(sounds, event, clavier, mouseX, mouseY, index, treizieme.type); /*déplacement curseur de l'inventaire*/
	
		if((event == MLV_KEY && clavier == MLV_KEYBOARD_u) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 480, 347, 896, 426))){ /*utiliser objet*/
			if(item.type == WEAPON || item.type == ARMOR || item.type == MAGIC_STAFF){ /*équiper équipement*/
				if(*index != -1){
					MLV_play_sound(sounds[15], 10.0); MLV_wait_milliseconds(100);
					equip_equipment(player->inventaire, *index, &player->def, &player->atk, &player->intel, &player->e_weapon, &player->e_staff, &player->e_armor);}
			}
			else if(item.type != NO_ITEM){ /*utiliser potion*/
				MLV_play_sound(sounds[14], 10.0); MLV_wait_milliseconds(100);
				*potion = item; /*la potion qu'on utilise est stockée dans le pointeur*/
				if(treizieme.type == NO_ITEM) /*vide la place de l'inventaire qui contenait la potion*/
					player->inventaire[*index].type = NO_ITEM;
				else{ /*stocke le 13e objet si on fait de la place dans l'inventaire*/
					if(*index != -1) player->inventaire[*index] = treizieme;}
				return -1;
			}
		}
		if((event == MLV_KEY && clavier == MLV_KEYBOARD_j) || (event == MLV_MOUSE_BUTTON && mouse_clic(mouseX, mouseY, 929, 347, 1345, 426))){ /*jeter objet*/
			if(item.type == WEAPON || item.type == ARMOR || item.type == MAGIC_STAFF){ /*jeter équipement*/
				if(*index != -1)
					jeter_equipment(player->inventaire, *index, &player->def, &player->atk, &player->intel, &player->e_weapon, &player->e_staff, &player->e_armor);
			}
			if(treizieme.type == NO_ITEM){ /*jeter objet*/
				if(player->inventaire[*index].type != NO_ITEM){
					MLV_play_sound(sounds[16], 10.0); MLV_wait_milliseconds(100);}
				player->inventaire[*index].type = NO_ITEM;}
			else{ /*stocke le 13e objet si on fait de la place dans l'inventaire*/
				MLV_play_sound(sounds[16], 10.0); MLV_wait_milliseconds(100);
				if(*index != -1) player->inventaire[*index] = treizieme;
				return -1;}
		}
	}
	return 1;
}


/*Renvoie les coordonnées voisines de c dans la direction demandée.*/
Coordo donne_coordo_voisin(Coordo c, Direction d){
	Coordo res = c;
	switch(d){
		case DROITE: res.x++; break;
		case GAUCHE: res.x--; break;
		case BAS: res.y++; break;
		default: res.y--; break; /*HAUT*/
	}
	return res;
}


/*Permet de convertir une direction donnée en une nouvelle direction (un int) pour récupérer
les bonnes frames d'une image lors d'un affichage.*/
Direction conversion_direc(Direction d){
	switch(d){
		case DROITE: return 0;
		case GAUCHE: return 1;
		case BAS: return 2;
		case HAUT: return 3;
		default: return d;
	}
}


/** Récupère les coordonnées du clic de la souris et teste si ils sont dans le bon rectangle donné
 * en paramètres (infX,infY) (supX, supY). */
int mouse_clic(int mouseX, int mouseY, int infX, int infY, int supX, int supY){
	if(infX <= mouseX && mouseX <= supX && infY <= mouseY && mouseY <= supY)
		return 1;
	return 0;
}
