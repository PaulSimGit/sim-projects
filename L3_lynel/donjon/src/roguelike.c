#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MLV/MLV_all.h>
#include "../include/intro.h"


int main(){
    int jeu = 1;
    MLV_Image* images[NBR_IMG];
    MLV_Music* musics[NBR_MUS];
    MLV_Sound* sounds[NBR_SON];

    MLV_create_window("Lynel Rogue", "Lynel Rogue", 1824, 864);
    MLV_draw_text(1700, 800, "Chargement...", MLV_COLOR_WHITE);
    MLV_actualise_window();

    MLV_init_audio();
    MLV_change_audio_buffer_size(1300);
    initialisation_images(images);
    initialisation_musiques(musics);
    initialisation_sounds(sounds);

    while(jeu != 0){
        jeu = ecran_titre(images, musics, sounds);
        if(jeu == 0) break;
	    animation_lancement_jeu(images, sounds);
        jeu = lancement_jeu(images, musics, sounds, jeu);
    }

    free_all(images, musics, sounds);
    MLV_free_audio();
    MLV_free_window();
    return 0;
}
