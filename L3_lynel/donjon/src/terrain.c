#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/terrain.h"


/** Récupère et renvoie le type de la case du terrain en fonction de la direction 
 * et des coordonnées demandées.
 * \param d Direction dans laquelle le test doit s'effectuer.
 * 			Si vous mettez d == HERE alors la fonction testera la case actuelle aux coordonnées c.
 * \param c Coordonnées du terrain où le test doit s'effectuer.
 * 			Influencé par la direction d sauf si d == HERE.
 * \param place 0 si on recherche des objets non mouvants ou 1 pour des objets qui se déplacent */
Celltype check_case(Floor floor, Direction d, Coordo c, int place){
	if(d != HERE){
		switch(d){
			case DROITE:
				return floor.terrain[c.y][c.x+1][place].type;
			case GAUCHE:
				return floor.terrain[c.y][c.x-1][place].type;
			case BAS:
				return floor.terrain[c.y+1][c.x][place].type;
			default: /*HAUT*/
				return floor.terrain[c.y-1][c.x][place].type;
		}
	}
	return floor.terrain[c.y][c.x][place].type; /*aucune direction*/
}


/*Similaire à check_case mais récupère un pointeur vers l'objet Entity (et non son type).
Attention, comme l'Entity de retour est un pointeur, ses modifications seront visibles en jeu.*/
Entity* donne_entity(Floor* floor, Direction d, Coordo c, int place){
	if(d != HERE){
		switch(d){
			case DROITE:
				return &floor->terrain[c.y][c.x+1][place].entity;
			case GAUCHE:
				return &floor->terrain[c.y][c.x-1][place].entity;
			case BAS:
				return &floor->terrain[c.y+1][c.x][place].entity;
			default: /*HAUT*/
				return &floor->terrain[c.y-1][c.x][place].entity;
		}
	}
	return &floor->terrain[c.y][c.x][place].entity; /*aucune direction*/
}


/** Récupère et renvoie les coordonnées/entités de tous les objets obj demandés du terrain.
 * \param obj le type des objets demandés
 * \param place 0 pour les objets immobiles et 1 pour ceux qui se déplacent.
 * \param escalier dans le cas où place == 0, on en profite pour sauvegarder la position de l'escalier. NULL sinon.
 * \return la liste des coordonnées/entités des objets demandés sur le terrain. */
Lst_Coordo_Entity recupere_coordo_all_obj(Floor floor, Celltype obj, int place, Coordo* escalier){
	int x, y; Lst_Coordo_Entity lst;
	lst.taille = 0;
	for(y=0; y<TAILLE_Y; y++){
		for(x=0; x<TAILLE_X; x++){
			if(floor.terrain[y][x][place].type == obj){
				lst.taille++;
				lst.c[lst.taille-1].entity = floor.terrain[y][x][place].entity;
				lst.c[lst.taille-1].coordo.x = x;
				lst.c[lst.taille-1].coordo.y = y;}
			else if(place == 0 && floor.terrain[y][x][place].type == STAIR_DOWN){
				escalier->x = x; escalier->y = y;}
		}
	}
	return lst;
}


/** Assigne des valeurs de coordonnées (vx,vy) dans le tableau tab de taille donnée.
 * Cette fonction utilise realloc() pour augmenter la taille du tableau avant ajout. */
void assigne_coordo_tab(Coordo** tab, int* taille, int vx, int vy){
	(*taille)++;
	*tab = realloc(*tab, (*taille)*sizeof(Coordo));
	(*tab)[(*taille)-1].x = vx;
	(*tab)[(*taille)-1].y = vy;
}




/* Initialise le champ de vision du joueur à la taille = 1 puis
rempli la liste avec creation_vision(). */
void init_vision(Floor* floor){
	floor->vision.champ = malloc(sizeof(Coordo));
	floor->vision.champ[0] = floor->coordo_perso;
	floor->vision.taille = 1;
	creation_vision(floor, floor->vision.champ[0], 5);
}


/*creer def façon récursive une liste des coordo correspondant à la vision du joueur*/
void creation_vision(Floor* floor, Coordo check, int restant){
	if(restant == 0)
		return;
	if(check_case(*floor, HAUT, check, 0) != WALL){
		assigne_coordo_tab(&floor->vision.champ, &floor->vision.taille, check.x, check.y-1);
		creation_vision(floor, floor->vision.champ[floor->vision.taille-1], restant-1);}
	if(check_case(*floor, BAS, check, 0) != WALL){
		assigne_coordo_tab(&floor->vision.champ, &floor->vision.taille, check.x, check.y+1);
		creation_vision(floor, floor->vision.champ[floor->vision.taille-1], restant-1);}
	if(check_case(*floor, GAUCHE, check, 0) != WALL){
		assigne_coordo_tab(&floor->vision.champ, &floor->vision.taille, check.x-1, check.y);
		creation_vision(floor, floor->vision.champ[floor->vision.taille-1], restant-1);}
	if(check_case(*floor, DROITE, check, 0) != WALL){
		assigne_coordo_tab(&floor->vision.champ, &floor->vision.taille, check.x+1, check.y);
		creation_vision(floor, floor->vision.champ[floor->vision.taille-1], restant-1);}
}


/** Recherche tous les objets obj dans le champ de vision actuel et stocke 
 * leur coordonnées dans la liste lst_c.
 * \param obj l'objet recherché (MONSTRE, COFFRE, etc...)
 * \param place 0 pour la recherche des objets immobiles et 1 pour les objets mouvants
 * \param lst_c la liste qui va contenir les coordonnées de tous les objets trouvés 
 * 				(ATTENTION, cette liste doit être allouée proprement avec une taille résonable !!)
 * \return Le nombre d'objets trouvé. */
int dans_champ_vision(Floor floor, Celltype obj, int place, Coordo* lst_c){
	int i, j, k, nbr = 1;
	for(i=floor.coordo_perso.y-5; i<floor.coordo_perso.y+6; i++){
		for(j=floor.coordo_perso.x-10; j<floor.coordo_perso.x+11; j++){
			for(k=0; k<floor.vision.taille; k++){
				if( floor.vision.champ[k].x == j && floor.vision.champ[k].y == i ){
					if(floor.terrain[i][j][place].type == obj){
						lst_c[nbr-1].x = j; lst_c[nbr-1].y = i;
						nbr++;
						break;}
				}
			}
		}
	}
	return nbr-1;
}




/*Donne une direction au monstre en fonction de son type.*/
Direction direction_monstre(Floor floor, int x, int y){
	int d, max = 0; Coordo c;
	c.x = x; c.y = y;
	switch(floor.terrain[y][x][1].entity.monster.type){
		case BLOB: 			
			while(max != 50){
				d = rand()%4;
				if(check_case(floor, d, c, 0) != WALL)
					break;
				max ++;
			}
			return d;
		default: return direction_intelligente(floor, x, y, 0, HERE, HERE); /*LOBO et BOSS*/
	}
}


/** On chercher récursivement la direction auquelle le monstre doit 
 * se déplacer pour atteindre le joueur.
 * \return la premiere direction permettant d'atteindre le joueur */
Direction direction_intelligente(Floor floor, int x, int y, int appel, Direction init, Direction last){
	Direction d = HERE;
	if(appel == 6)
		return d;
	if(x == floor.coordo_perso.x && y == floor.coordo_perso.y)
		return init;
	if(floor.terrain[y-1][x][0].type != WALL && last != BAS){
		if(init == HERE) 
			d = direction_intelligente(floor, x, y-1, appel+1, HAUT, HAUT);
		else
			d = direction_intelligente(floor, x, y-1, appel+1, init, HAUT);
		if(d != HERE)
			return d;
	}	
	if(floor.terrain[y+1][x][0].type != WALL && last != HAUT){
		if(init == HERE) 
			d = direction_intelligente(floor, x, y+1, appel+1, BAS, BAS);
		else
			d = direction_intelligente(floor, x, y+1, appel+1, init, BAS);
		if(d != HERE)
			return d;
	}
	if(floor.terrain[y][x-1][0].type != WALL && last != DROITE){
		if(init == HERE) 
			d = direction_intelligente(floor, x-1, y, appel+1, GAUCHE, GAUCHE);
		else
			d = direction_intelligente(floor, x-1, y, appel+1, init, GAUCHE);
		if(d != HERE)
			return d;
	}
	if(floor.terrain[y][x+1][0].type != WALL && last != GAUCHE){
		if(init == HERE) 
			d = direction_intelligente(floor, x+1, y, appel+1, DROITE, DROITE);
		else
			d = direction_intelligente(floor, x+1, y, appel+1, init, DROITE);
		if(d != HERE)
			return d;
	}
	return d;
}




/*le Mur à la position (i,j) devient de type donné*/
void donne_wall_direction(Floor* floor, Wallstype type, int i, int j){
	if(floor->terrain[i][j][0].entity.wall.direc == NONE) /*donne une direction*/
		floor->terrain[i][j][0].entity.wall.direc = type;
	else{ /*donne une 2e direction si on a déjà une direction*/
		if(floor->terrain[i][j][0].entity.wall.direc == NO || floor->terrain[i][j][0].entity.wall.direc == NE ||
			floor->terrain[i][j][0].entity.wall.direc == O || floor->terrain[i][j][0].entity.wall.direc == E ){
				floor->terrain[i][j][0].entity.wall.direc2 = floor->terrain[i][j][0].entity.wall.direc;
				floor->terrain[i][j][0].entity.wall.direc = type;}
		else
			floor->terrain[i][j][0].entity.wall.direc2 = type;
	}
}


/*spécialise les types de tous les murs du terrain*/
void specialise_walls(Floor* floor){
	int i, j;
	for(i=0; i<TAILLE_Y; i++){
		for(j=0; j<TAILLE_X; j++){
			if(floor->terrain[i][j][0].type == WALL){
				if(floor->terrain[i+1][j+1][0].type != WALL && floor->terrain[i][j+1][0].type == WALL &&
					floor->terrain[i+1][j][0].type == WALL && i+1 < TAILLE_Y && j+1 < TAILLE_X)
						donne_wall_direction(floor, NO, i, j);
				if(floor->terrain[i+1][j][0].type != WALL && floor->terrain[i][j+1][0].type == WALL &&
					floor->terrain[i][j-1][0].type == WALL && i+1 < TAILLE_Y)
						donne_wall_direction(floor, N, i, j);
				if(floor->terrain[i+1][j-1][0].type != WALL && floor->terrain[i][j-1][0].type == WALL &&
					floor->terrain[i+1][j][0].type == WALL && i+1 < TAILLE_Y)
						donne_wall_direction(floor, NE, i, j);
				if(floor->terrain[i][j+1][0].type != WALL && floor->terrain[i+1][j][0].type == WALL &&
					floor->terrain[i-1][j][0].type == WALL && j+1 < TAILLE_X)
						donne_wall_direction(floor, O, i, j);
				if(floor->terrain[i][j-1][0].type != WALL && floor->terrain[i+1][j][0].type == WALL &&
					floor->terrain[i-1][j][0].type == WALL)
						donne_wall_direction(floor, E, i, j);
				if(floor->terrain[i-1][j+1][0].type != WALL && floor->terrain[i-1][j][0].type == WALL && 
					((floor->terrain[i+1][j][0].type == WALL && i+1<TAILLE_Y) || i==TAILLE_Y-1) && floor->terrain[i][j+1][0].type == WALL && i>1 &&
					j+1 < TAILLE_X && ((floor->terrain[i+1][j+1][0].type == WALL && i+1<TAILLE_Y) || i==TAILLE_Y-1) )
						donne_wall_direction(floor, SO, i, j);
				if(floor->terrain[i-1][j][0].type != WALL && floor->terrain[i][j+1][0].type == WALL &&
					floor->terrain[i][j-1][0].type == WALL && ((floor->terrain[i+1][j][0].type == WALL && i+1<TAILLE_Y) || i==TAILLE_Y-1) && i>1)
						donne_wall_direction(floor, S, i, j);
				if(floor->terrain[i-1][j-1][0].type != WALL && floor->terrain[i-1][j][0].type == WALL &&
					((floor->terrain[i+1][j][0].type == WALL && i+1<TAILLE_Y) || i==TAILLE_Y-1) && floor->terrain[i][j-1][0].type == WALL && i>1 &&
					((floor->terrain[i+1][j-1][0].type == WALL && i+1<TAILLE_Y) || i==TAILLE_Y-1) )
						donne_wall_direction(floor, SE, i, j);
				if(floor->terrain[i+1][j][0].type != WALL && floor->terrain[i][j+1][0].type != WALL)
					donne_wall_direction(floor, E1, i, j);
				if(floor->terrain[i+1][j][0].type != WALL && floor->terrain[i][j-1][0].type != WALL)
					donne_wall_direction(floor, E2, i, j);
				if(floor->terrain[i-1][j][0].type != WALL && floor->terrain[i][j+1][0].type != WALL && i>1){
					if(floor->terrain[i+1][j][0].type == WALL && i>1)
						donne_wall_direction(floor, E3_DEG, i, j);
					else donne_wall_direction(floor, E3, i, j);}
				if(floor->terrain[i-1][j][0].type != WALL && floor->terrain[i][j-1][0].type != WALL && i>1){
					if(floor->terrain[i+1][j][0].type == WALL)
						donne_wall_direction(floor, E4_DEG, i, j);
					else donne_wall_direction(floor, E4, i, j);}
				if(floor->terrain[i][j-1][0].type != WALL && floor->terrain[i-1][j][0].type != WALL && floor->terrain[i+1][j][0].type != WALL)
					donne_wall_direction(floor, E5, i, j);
				if(floor->terrain[i][j+1][0].type != WALL && floor->terrain[i-1][j][0].type != WALL && floor->terrain[i+1][j][0].type != WALL)
					donne_wall_direction(floor, E6, i, j);
				if(floor->terrain[i+1][j][0].type != WALL && floor->terrain[i][j+1][0].type != WALL && floor->terrain[i][j-1][0].type != WALL)
					donne_wall_direction(floor, E7, i, j);
				if(floor->terrain[i-1][j][0].type != WALL && floor->terrain[i][j+1][0].type != WALL && floor->terrain[i][j-1][0].type != WALL)
					donne_wall_direction(floor, E8, i, j);
				if(floor->terrain[i-1][j][0].type != WALL && floor->terrain[i+1][j-1][0].type != WALL && floor->terrain[i+1][j+1][0].type != WALL && 
				   floor->terrain[i][j-1][0].type == WALL && floor->terrain[i][j+1][0].type == WALL && floor->terrain[i+1][j][0].type == WALL)
					donne_wall_direction(floor, E9, i, j);
			}
		}  
	}
}


/*affiche le terrain dans le terminal*/
void affiche_terrain_terminal(Floor floor){
    int x, y;
	for(y=0; y<TAILLE_Y; y++){
		for(x=0; x<TAILLE_X; x++){
			if(floor.terrain[y][x][0].type == GROUND)
				printf("o ");
            else if(floor.terrain[y][x][0].type == STAIR_UP)
                printf("X ");
			else if(floor.terrain[y][x][0].type == WALL)
				printf(". ");
			else if(floor.terrain[y][x][0].type == STAIR_DOWN)
				printf("H ");
			else if(floor.terrain[y][x][0].type == PLAYER)
				printf("P ");
			else if(floor.terrain[y][x][0].type == TREASURE)
				printf("? ");
			else if(floor.terrain[y][x][1].type == MONSTER)
				printf("M ");
		}
		printf("\n");
	}
}
