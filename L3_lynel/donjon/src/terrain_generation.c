#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/terrain_generation.h"


/*génère un terrain complet en fonction de sa seed, c'est à dire qu'on crée
le terrain, le champ de vision et on génère les objets*/
Floor creation_terrain_complet(unsigned int seed, int etage){
	Floor floor; int i;
	creation_terrain(&floor, seed);
	init_vision(&floor);

	generation_objet(&floor, seed, etage, STAIR_DOWN, 0, 25, 1, 0); /*génère l'escalier de fin*/
	bout(&floor, TREASURE, etage); /*génère 7~20 coffres*/
	bout(&floor, MONSTER, etage); /*génère 7~20 ennemis adjacents aux coffres*/
	for(i=0; i<3; i++) /*génère quelques monstres en plus*/
		generation_objet(&floor, seed, etage, MONSTER, 1, 10, 0, 20+i);
	for(i=0; i<5; i++) /*génère quelques monstres en plus*/
		generation_objet(&floor, seed, etage, MONSTER, 1, 15, 1, 23+i);
	generation_objet(&floor, seed, etage, TREASURE, 0, 2, 0, 0); /*génère le coffre bonus*/

	return floor;
}


/*modifie le terrain passé en paramètre pour générer un nouveau terrain à partir d'une seed*/
void creation_terrain(Floor* floor, unsigned int seed){
	int taille_expand = 4;
	/*liste des cases adjacentes à générer selon la distance de Manhattan*/
	Coordo *toexpand = malloc(4 * sizeof(Coordo));
	
	remplissage_initial(floor);
	floor->terrain[TAILLE_Y/2][TAILLE_X/2][0].type = GROUND;
	
	toexpand[0].x = TAILLE_X/2+1;
	toexpand[0].y = TAILLE_Y/2;
	toexpand[1].x = TAILLE_X/2-1;
	toexpand[1].y = TAILLE_Y/2;
	toexpand[2].x = TAILLE_X/2;
	toexpand[2].y = TAILLE_Y/2+1;
	toexpand[3].x = TAILLE_X/2;
	toexpand[3].y = TAILLE_Y/2-1;
	
	generation(floor, toexpand, taille_expand, seed);
	bout(floor, WALL, 1);
	floor->terrain[TAILLE_Y/2][TAILLE_X/2][0].type = STAIR_UP;

	specialise_walls(floor);

	floor->coordo_perso.x = TAILLE_X/2;
	floor->coordo_perso.y = TAILLE_Y/2;
	floor->seed = seed;
	srand(seed);
	floor->theme = rand()%5;
	/*on ne free pas toexpand car il a déjà été réalloué avec une taille de 0*/
}


/** Génère le terrain de manière aléatoire en fonction de sa seed
 * \param taille la taille actuelle de la liste toexpand */
void generation(Floor* floor, Coordo* toexpand, int taille, unsigned int seed){
	int r; Coordo c;
	if(taille == 0)
		return;

	srand(seed);
	r = rand()%taille;

	c.x = toexpand[r].x;
	c.y = toexpand[r].y;
	taille--;
	toexpand[r].x = toexpand[taille].x;
	toexpand[r].y = toexpand[taille].y;
	toexpand = realloc(toexpand, taille*sizeof(Coordo));
	
	if(admi(floor, c.y, c.x)){
		floor->terrain[c.y][c.x][0].type = GROUND;
		if((c.y-1) > 0){
			if(check_case(*floor, HAUT, c, 0) == WALL)
				assigne_coordo_tab(&toexpand, &taille, c.x, c.y-1);
		}
		if((c.y+1) < TAILLE_Y-1){
			if(check_case(*floor, BAS, c, 0) == WALL)
				assigne_coordo_tab(&toexpand, &taille, c.x, c.y+1);
		}
		if((c.x-1) > 0){
			if(check_case(*floor, GAUCHE, c, 0) == WALL)
				assigne_coordo_tab(&toexpand, &taille, c.x-1, c.y);
		}
		if((c.x+1) < TAILLE_X-1){
			if(check_case(*floor, DROITE, c, 0) == WALL)
				assigne_coordo_tab(&toexpand, &taille, c.x+1, c.y);
		}
	}
	generation(floor, toexpand, taille, seed);
	return;
}


/** Vérifie si une case (y,x) est admissible en la conparant à ses cases alentours
 * et à la case centrale.
 * \return 1 si la case est admissible et 0 sinon. */
int admi(Floor* floor, int y, int x){
	int i, j, max2 = 0, only1 = 0;
	if(floor->terrain[y][x][0].type != WALL)
		return 0;
    /*admissible par rapport à la case centrale*/
	for(i=-2; i<3; i++){
		for(j=-2; j<3; j++){
			if( (abs(x-(x-i)) + abs(y-(y-j)) == 1) && (floor->terrain[y+j][x+i][0].type == GROUND) )
				only1++;
			if( (abs(x-(x-i)) + abs(y-(y-j)) == 2) && (floor->terrain[y+j][x+i][0].type == GROUND) ){
				max2++;
				if(max2 > 2)
					return 0;
			}
		}
	}
	if(only1 == 1)
		return 1;
	return 0;
}


/** Transforme les dernières cases de sol en obj en suivant les conditions suivantes:
 * obj == WALL : adjacent à 3 autres murs.
 * obj == TREASURE : adjacent à 3 murs et valide toutes les 7 calculs corrects (~20 coffres).
 * obj == MONSTER : adjacent à 1 coffre (générez les monstres après avoir généré les coffres pour cette fonction).
 * Méthode: On met dans un tableau les cases sol à transformer en obj mais on ne
 * peut pas les convertir maintenant, sinon on va créer d'autres cases sol
 * à transformer en obj pendant ce processus (et donc générer des étages impossibles). */
void bout(Floor* floor, Celltype obj, int etage){
	int i, j, x, y, cmpt_w, cmpt_g, cmpt_tr, taille = 1, limiteur = 0;
	Coordo *tab = malloc(sizeof(Coordo));
	/*recherche des cases concernées*/
	for(y=0; y<TAILLE_Y; y++){
		for(x=0; x<TAILLE_X; x++){
			if(floor->terrain[y][x][0].type == GROUND){
				cmpt_w = 0; cmpt_g = 0; cmpt_tr = 0;
				for(i=-1; i<2; i++){
					for(j=-1; j<2; j++){
						if((abs(x-(x-i)) + abs(y-(y-j)) == 1) && (floor->terrain[y+j][x+i][0].type == WALL)){
							cmpt_w++; limiteur++;}
						else if((abs(x-(x-i)) + abs(y-(y-j)) == 1) && (floor->terrain[y+j][x+i][0].type == GROUND))
							cmpt_g++;
						else if((abs(x-(x-i)) + abs(y-(y-j)) == 1) && (floor->terrain[y+j][x+i][0].type == TREASURE))
							cmpt_tr++;
					}
				}
				if((obj == WALL && cmpt_w == 3 && cmpt_g == 1) ||
				   (obj == TREASURE && cmpt_w == 3 && cmpt_g == 1 && limiteur %7 == 0) ||
				   (obj == MONSTER && cmpt_w >= 1 && cmpt_g >= 1 && cmpt_tr == 1)){
					   tab[taille-1].x = x;
					   tab[taille-1].y = y;
					   taille++;
					   tab = realloc(tab, taille*sizeof(Coordo));
				}
			}
		}
	}
	for(i=0; i<taille-1; i++){ /*conversion des cases concernées*/
		if(obj == WALL)
			floor->terrain[tab[i].y][tab[i].x][0].type = WALL;
		else if(obj == TREASURE)
			placement_objet(floor, etage, TREASURE, tab[i], 0, i);
		else if(obj == MONSTER)
			placement_objet(floor, etage, MONSTER, tab[i], 1, i);
	}
	free(tab);
}


/** Permet de générer un objet du type voulu dans le terrain selon un certain calcul de distance
 * par rapport au centre du terrain. Le mode de calcul est au choix à mettre en paramètres.
 * \param obj type de l'objet à placer
 * \param place 0 pour les objets immobiles ou 1 pour se qui se déplacent
 * \param distance distance voulue par rapport au centre du terrain
 * \param mode 0 pour un résultat inférieur à la distance (<=) ou 1 pour supérieur (>=)
 * \param num le numéro de l'objet (pour une seed différente).*/
void generation_objet(Floor* floor, unsigned int seed, int etage, Celltype obj, int place, int distance, int mode, int num){
	int x, y, taille = 1; Coordo choisi; bool calcul;
	Coordo *tab = malloc(sizeof(Coordo));
	/*calcul des positions*/
	for(y=0; y<TAILLE_Y; y++){
		for(x=0; x<TAILLE_X; x++){
			if(mode == 0) /* <= */
				calcul = (abs(x-(TAILLE_X/2)) + abs(y-(TAILLE_Y/2)) <= distance) && 
					(floor->terrain[y][x][0].type == GROUND) && (floor->terrain[y][x][1].type == NOTHING);
			if(mode == 1) /* >= */
				calcul = (abs(x-(TAILLE_X/2)) + abs(y-(TAILLE_Y/2)) >= distance) && 
					(floor->terrain[y][x][0].type == GROUND) && (floor->terrain[y][x][1].type == NOTHING);
			if(calcul == true){
				tab[taille-1].x = x;
				tab[taille-1].y = y;
				taille++;
				tab = realloc(tab, taille*sizeof(Coordo));
			}
		}
	}
	/*placement objet*/
	if(taille != 1){
		srand(seed);
		choisi = tab[rand()%(taille-1)];
		placement_objet(floor, etage, obj, choisi, place, num);
		free(tab);
	}
}


/*Place un objet dans le terrain aux coordonnées données conformément à la génération. L'objet aura alors un
état aléatoire.*/
void placement_objet(Floor* floor, int etage, Celltype obj, Coordo choisi, int place, int num){
	floor->terrain[choisi.y][choisi.x][place].type = obj;
	if(obj == MONSTER)
		floor->terrain[choisi.y][choisi.x][place].entity.monster = creation_monstre(etage, num);
	if(obj == TREASURE)
		floor->terrain[choisi.y][choisi.x][place].entity.treasure = new_treasure(etage, num);
	if(etage % 5 == 0 && obj == STAIR_DOWN){ /*génération du boss tous les 5 étages en même temps que le STAIR_DOWN*/
		floor->terrain[choisi.y][choisi.x][1].type = MONSTER;
		floor->terrain[choisi.y][choisi.x][1].entity.monster = creation_boss(etage);
	}
}


/** Place tous les objets demandés à leur position respective. Les objets auront des états prédéfinis
 * dans la liste.
 * \param lst liste des coordonnées/d'entitées des objets demandés. */
void placement_all_obj(Floor* floor, Celltype obj, Lst_Coordo_Entity lst, int place){
	int i;
	for(i=0; i<lst.taille; i++){
		floor->terrain[lst.c[i].coordo.y][lst.c[i].coordo.x][place].type = obj;
		if(obj == MONSTER)
			floor->terrain[lst.c[i].coordo.y][lst.c[i].coordo.x][place].entity.monster = lst.c[i].entity.monster;
		if(obj == TREASURE)
			floor->terrain[lst.c[i].coordo.y][lst.c[i].coordo.x][place].entity.treasure = lst.c[i].entity.treasure;
	}
}


/** Remplissage initial du terrain avec des WALLS. Les objets mouvants sont également
 * initialisés à NOTHING. */
void remplissage_initial(Floor* floor){
	int x, y;
	for(y=0; y<TAILLE_Y; y++){
		for(x=0; x<TAILLE_X; x++){
			floor->terrain[y][x][0].type = WALL;
            floor->terrain[y][x][0].entity.wall.direc = NONE;
			floor->terrain[y][x][0].entity.wall.direc2 = NONE;
			floor->terrain[y][x][1].type = NOTHING;
		}
	}
}


/*libère la mémoire du terrain, c'est à dire seulement le champ de vision*/
void free_terrain(Floor* floor){
	free(floor->vision.champ);
}