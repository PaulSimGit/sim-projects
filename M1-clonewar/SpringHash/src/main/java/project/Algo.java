package project;

import java.util.Objects;

import static java.lang.Math.pow;

public class Algo {

    // Pour calculer le hash d'une chaine
    static long calcHash(String txt) {

        Objects.requireNonNull(txt);

        // Juste pour incrémenter
        int i;

        // Un nombre premier, pour pouvoir calculer le hash
        int prime = 7;

        // On calcule la valeur de la fenetre du texte, et du hash du pattern
        long txtHash = 0;
        for (i = 0; i < txt.length(); i++) {
            txtHash += txt.charAt(i) * pow(prime, txt.length() - 1 - i);
        }

        return txtHash;

    }

}
