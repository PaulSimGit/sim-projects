package project;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.objectweb.asm.*;
import org.objectweb.asm.util.*;
import org.objectweb.asm.tree.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Asm {

    public static ArrayList<Hash> generateAsm() throws Exception{

        ArrayList<Hash> allAsm = new ArrayList<>();

        // On lit la classe Algo.class, et on le traduit en ASM
        String path = System.getProperty("user.dir") + "\\target\\classes\\project\\Algo.class";
        InputStream in = new FileInputStream(path.replace("\\","\\\\"));
        ClassReader reader = new ClassReader(in);
        ClassNode classNode = new ClassNode();
        reader.accept(classNode,0);

        int line = 0; int result;
        final List<MethodNode> methods = classNode.methods;
        int flag = 0;
        StringBuilder res = new StringBuilder();
        for(MethodNode m: methods){
            InsnList inList = m.instructions;
            //System.out.println(m.name);
            for(int i = 0; i< inList.size(); i++){

                // output est la chaine traduite en ASM
                String output = insnToString(inList.get(i));
                System.out.println(output);
                /*
                L'ASM peut fournir des instructions non pertinantes, comme des L0, L23, etc...
                On va essayer de les enlever ci-possible, pas besoin de les enregistrer
                De plus pour les instructions qui affichent le numéro de ligne, on extrait
                la ligne et on ne l'enregistre pas dans la BDD.
                */
                if((result = checkAsmLine(output)) > -2){
                    if (result > -1){
                        if (flag == 0){ //Le flag sert pour la première fois
                            flag = 1;
                        }
                        else{
                            res = new StringBuilder(res.substring(0, res.length() - 1));
                            allAsm.add(new Hash(line, res.toString(), Algo.calcHash(res.toString())));    //Après la premiere fois dès qu'on passe
                                                                                    //a une nouvelle ligne on add le Hash de ce bloc
                            res = new StringBuilder();
                        }
                        line = result;
                    }
                    else{
                        output = checkAsmScrap(output).trim(); //On utilise trim pour le char de fin de ligne
                        res.append(output).append('-');
                        //System.out.println("line " + line + ": " + output);
                    }
                }

            }
        }

        return allAsm;
    }

    public static String insnToString(AbstractInsnNode insn){
        insn.accept(mp);
        StringWriter sw = new StringWriter();
        printer.print(new PrintWriter(sw));
        printer.getText().clear();
        return sw.toString();
    }

    public static int checkAsmLine(String string) {

        // On se débarasse des L0, L14 etc...
        if (Pattern.compile(" ?L[0-9]+").matcher(string).matches()){
            return -2;
        }

        String[] separated = string.split(" +");

        for(int i = 0; i < separated.length; i++){

            // Si on se trouve dans le cas de l'instruction qui donne le numéro de ligne
            if (separated[i].equals("LINENUMBER") && i < separated.length - 1){
                try {
                    return Integer.parseInt(separated[i+1]);
                } catch (NumberFormatException ignored) {}
            }
        }
        return -1;

    }

    public static String checkAsmScrap(String string) {

        // On va enlever les parties de l'Asm qui ne vont pas nous servir pour detecter des clones
        // Pour l'instant, on va faire comme l'exmple donné dans l'exemple
        if (Pattern.compile(" *INVOKESPECIAL").matcher(string).lookingAt()){
            Matcher partie = Pattern.compile("<.*?>").matcher(string);
            if(partie.find())
                return "INVOKESPECIAL " + partie.group();
            else
                return "INVOKESPECIAL ";
        }

        if (Pattern.compile(" *INVOKEVIRTUAL").matcher(string).lookingAt()){
            return "INVOKEVIRTUAL";
        }

        if (Pattern.compile(" *INVOKESTATIC").matcher(string).lookingAt()){
            Matcher partie = Pattern.compile("[A-Z][a-z]*\\.[A-Za-z]*").matcher(string);
            if(partie.find())
                return "INVOKESTATIC " + partie.group();
            else
                return "INVOKESTATIC ";
        }

        if (Pattern.compile(" *NEW").matcher(string).lookingAt()){
            return "NEW";
        }

        return string;
    }

    private static final Printer printer = new Textifier();
    private static final TraceMethodVisitor mp = new TraceMethodVisitor(printer);

}