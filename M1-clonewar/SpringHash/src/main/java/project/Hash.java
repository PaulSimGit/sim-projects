package project;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Hash {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    private int lineFound;
    private String string;
    private Long hash;

    public Hash() {}

    public Hash(int lineFound, String string, Long hash) {
        this.lineFound = lineFound;
        this.string = string;
        this.hash = hash;
    }

    @Override
    public String toString() {
        return String.format(
                "Line %d, string=%s, hash=%d",
                lineFound, string, hash);
    }
}