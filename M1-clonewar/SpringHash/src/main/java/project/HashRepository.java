package project;

import org.springframework.data.repository.CrudRepository;

public interface HashRepository extends CrudRepository<Hash, Long> {

}