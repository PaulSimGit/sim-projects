package project;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;


@RestController
@SpringBootApplication
public class Main {

    /*

    Si le pom.xml ne trouve pas des dépendances (lignes en rouge):
    Clique droit sur le pom.xml > Maven > Reload

    Lancer le programme, et aller sur ce lien
    http://localhost:9090/DEBATSIM
    Si le serveur est déjà utilisé, changer dans src/main/resources/application.properties

    */

    // Le logger va récupérer les valeurs de la base
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    private static final StringBuilder str = new StringBuilder();

    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }

    @Bean
    public CommandLineRunner demo(HashRepository repository) {
        return (args) -> {

            // On ajoute des lignes au serveur
            ArrayList<Hash> allAsm = Asm.generateAsm();
            repository.saveAll(allAsm);

            str.append("<h1>Clone War</h1>");
            str.append("<h2>Paul SIM et Julien DEBATS</h2><br>");

            // On affiche et construit le stringbuilder
            log.info("");
            for (Hash hash : repository.findAll()) {
                log.info(hash.toString());
                str.append(hash).append("<br>");
            }
            log.info("");

        };
    }

    @GetMapping("/DEBATSIM")
    public String demo2(@RequestParam(value = " ", defaultValue = " ") String string) {
        return String.format(str + string);
    }

}