# Paul SIM - Projets informatiques

Sauf précision du contraire, tous les projets ont des interfaces graphiques.
Il y a des rapports pour les projets.

</br></br></br>


## L1

</br>

### Bataille navale (Python)
Le jeu de la bataille navale, jouable à deux joueurs ou contre un ordi avec un pseudo IA

### Logimage (Python)
Appellé aussi Picross, c'est un jeu pour résoudre des puzzles, pour reconstituer une image seulement à partir des indices, qui disent combien de carrés noirs sur la ligne/colonne.
On peut créer ces propres puzzles, et il y a un mode solveur
(Aperçu dans mon CV)

### Snake (Python)

Jeu snake, avec plusieurs modes (normal, à deux, extrême et dans le noir)
Eu le premier prix (meilleur projet de la promotion, la vidéo est dans le git!)

</br></br></br>


## L2

</br>

### Skymaze (C)
Jeu en temps réel, utilisant les files de priorités. Labyrinthe où on doit éviter des flèches et utiliser divers objets pour atteindre l'arrivée.
Il y aussi un mode éditeur pour créer ces propres puzzle.
(Aperçu dans mon CV)

### Three to Go (C)
Le jeu se passe dans un bar, où on doit assembler des verres de la même forme ou couleur pour avoir le plus de combos.

</br></br></br>

## L3

</br>

### Lynel Rogue (C)
Jeu de type donjon rogue, où chaques étages sont générées aléatoirement, et il y a toute une panoplie d'armes, d'équipements et ennemies.
(Aperçu dans mon CV)

### Analyse Syntaxique (C)
Permet d'analyser si un texte est un code en C bien écrit, s'il n'y comporte pas d'erreurs de syntaxes (mal écrit, definit, manques de balises, etc...)
(Pas d'interface)

### Spledor (Java)
Le jeu de cartes Splendor, on peut jouer à plusieurs ou contre des pseudos IA.


</br></br></br>

## M1

</br>

### Clone War (Java)
Permet de detecter deux codes code s'ils ont des similitudes, pour détecter le plagiat.
Il reste des points à terminer. (Pas d'interface)
Utilise le hash code, bloc ASM, bootstap, et vue.
